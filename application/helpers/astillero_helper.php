<?php
function get_guid()
{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        return $charid;
}


function rol_direccion($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('roles_direcciones')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre )){
      return $nombre->rol;
    }else{
    return "sin medida";
   }
}

function nombre_lote($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('productos')
           ->where('productoId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre )){
      return $nombre->productoLote;
    }else{
    return "sin medida";
   }
}

function nombre_medida($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('medidas')
           ->where('medidaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre )){
      return $nombre->medidaNombre;
    }else{
    return "sin medida";
   }
}

function nombre_almacen($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('almacenes')
           ->where('alamacenId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre )){
      return $nombre->almacenNombre;
    }else{
    return "sin medida";
   }
}



function cordon_plaga($id_cordon,$id_plaga)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('cordones_plagas')
           ->where('cpIdCordon', $id_cordon)
           ->where('cpIdPlaga', $id_plaga);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre )){
      return 1;
    }else{
    return 0;
   }
}

function cordon_plaga_row($id_cordon,$id_plaga)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('cordones_plagas')
           ->where('cpIdCordon', $id_cordon)
           ->where('cpIdPlaga', $id_plaga);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre )){
      return $nombre;
    }else{
    return 0;
   }
}

function medida_unidad($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('medidas')
           ->where('medidaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->medidaUnidades;
}


function nombre_marca($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('marca')
           ->where('marcaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->marcaNombre;
}

function nombre_producto($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('productos')
           ->where('productoId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
      return $nombre->productoNombre;
    }else{
      return "";
    }
    //return $nombre->productoNombre;
}

function producto_cofepris($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('productos')
           ->where('productoId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
      return $nombre->productoRegCofepris;
    }else{
      return "";
    }
    //return $nombre->productoNombre;
}

function nombre_puestos($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('puestos')
           ->where('puestoId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->puestoNombre;
}

function nombre_boquilla($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('boquillas')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->boquilla;
}

function boquilla_aolicacion($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('boquillas')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->milimetrosxsegundo;
}

function nombre_rol($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('roles')
           ->where('rolId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->rolNombre;
}

function get_rol_id($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('usuarios')
           ->where('usuarioId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->idRol;
}

function get_rol_permiso($categoria,$permisoIdRol)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('permisos_roles')
           ->where('permisoCategoria', $categoria)
           ->where('permisoIdRol', $permisoIdRol);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre;
}

function codigo_postal()
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('datos')
           ->where('id', 1);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->cp;
}

function nombre_actividad($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('actividades')
           ->where('actividadId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
      return $nombre->actividadNombre;
    }else{
      return "sin actividad";
    }
    
}

function nombre_lugar_aplicacion($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('lugares_aplicacion')
           ->where('lugaresId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
      return $nombre->lugaresNombre;
    }else{
      return "sin lugar";
    }
    
}

function nombre_lugar_aplicacion_cliente($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('cliente_lugares')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
      return $nombre->lugar;
    }else{
      return "sin lugar";
    }
    
}

function nombre_plaga($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('plagas')
           ->where('plagaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    if(is_object($nombre)){
      return $nombre->plagaNombre;
    }else{
      return "sin plaga";
    }
    
}

function nombre_metodo($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('metodos')
           ->where('metodoId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    
    if(is_object($nombre)){
      return $nombre->metodoNombre;
    }else{
      return "sin metodo";
    }
}

function nombre_proveedor($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('proveedores')
           ->where('id', $id);
    $data = $CI->db->get();
    if($data->num_rows() > 0){
      $nombre = $data->row();
      return $nombre->nombre;
    }else{
      return "sin proveedor";
    }
}

function nombre_cliente($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('clientes')
           ->where('id', $id);
    $data = $CI->db->get();
    if($data->num_rows() > 0){
      $nombre = $data->row();
      return $nombre->nombre;
    }else{
      return "sin cliente";
    }
}

function nombre_usuario_completo($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('usuarios')
           ->where('usuarioId', $id);
    $data = $CI->db->get();
    if($data->num_rows() > 0){
      $nombre = $data->row();
      return $nombre->usuarioNombre." ".$nombre->usuarioApellidoPaterno." ".$nombre->usuarioApellidoMaterno;
    }else{
      return "sin nombre";
    }
    
}


?>
