<!doctype html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <!--html class="no-js" lang=""--> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $this->config->item('TITULO');?></title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <script src="<?php echo base_url();?>statics/tema/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <!--script type="text/javascript" src="<?php echo base_url().'statics/js/libraries/jquery.js'; ?>"></script-->
    <!--script src="<?php echo base_url();?>statics/tema/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>statics/tema/assets/js/plugins.js"></script-->
    <!--script src="<?php echo base_url();?>statics/tema/assets/js/main.js"></script-->

    <script>

    function login()
    {
        var band = 0;

        if($("#loginAstillero").val() == '')
        {
            $("#loginAstillero").css("border", "1px solid #FF0000");
            band++;
        }
        else{
            $("#loginastillero").css("border", "1px solid #ADA9A5");
        }

        if($("#passAstillero").val() == ''){
            $("#passAstillero").css("border", "1px solid #FF0000");
            band++;
        }
        else{
            $("#passAstillero").css("border", "1px solid #ADA9A5");
        }

        if($("#loginAstillero").val() != '' && $("#passAstillero").val() != ''){
            values = $.ajax({
                                url : $("#checkValues").attr("href"),
                                type : "POST",
                                data : {username:$("#loginAstillero").val(), password:$("#passAstillero").val()},
                                async : false
                            }).responseText;

            if(values == 0 || values == '0'){
                $("#errorLoginData").text("Por favor, verifique el usuario/password.").show();
                $("#loginAstillero").css("border", "1px solid #FF0000");
                $("#passAstillero").css("border", "1px solid #FF0000");
                band++;
            }
            else{
                $("#errorLoginData").hide();
                $("#loginAstillero").css("border", "1px solid #ADA9A5");
                $("#passAstillero").css("border", "1px solid #ADA9A5");
            }
        }

        if(band != 0)
        {
            $("#errorMessageLogin").text("Por favor, verifica los campos marcador.").show();
            return false;
        }
        else{
            $("#errorMessageLogin").hide();
            return true;
        }
    }
    

    </script>
</head>
<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                  <div id="errorMessageLogin" style="color: #FF0000; display: none"></div>
                  <div id="errorLoginData" style="color: #FF0000; display: none"></div>
                    <!--a href="index.html">
                        <img class="align-content" src="<?php echo base_url();?>statics/tema/images/logo.png" alt="">
                    </a-->
                    <h2 style="color:#fff"><?php echo $this->config->item('TITULO');?></h2>
                </div>
                <div class="login-form">
                  <?php echo anchor('login/checkDataLogin', '', array('id'=>'checkValues', 'style'=>'display: none')); ?>

                    <form action="<?php echo base_url()?>index.php/login/mainView" onsubmit="return login()" method="post">
                        <div class="form-group">
                            <label>Usuario</label>
                            <input type="text" class="form-control" placeholder="Usuario" id="loginAstillero" name="Login[adminUsername]" required/>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" placeholder="Contraseña" id="passAstillero" name="Login[adminPassword]" required/>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Entrar</button>

                    </form>
                </div>
            </div>
        </div>
    </div>





</body>
</html>
