<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
  <?php if(isset($header)): ?>
      <?php echo $header; ?>
  <?php endif; ?>

  <?php if(isset($included_css)): ?>
      <?php foreach($included_css as $files_css): ?>
        <link rel="stylesheet" href="<?php echo base_url().$files_css; ?>">
          
      <?php endforeach; ?>
  <?php endif; ?>

  <style type="text/css">

  .pagination > li > a, .pagination > li > span{color: #000!important;}

    
 
  </style>

  <?php if(isset($included_js)): ?>
      <?php foreach($included_js as $files_js): ?>
          <script type="text/javascript" src="<?php echo base_url().$files_js; ?>"></script>
      <?php endforeach; ?>
  <?php endif; ?>
 
</head>
<body>
        <!-- Left Panel -->

        <?php if(isset($menu)): ?>
            <?php echo $menu; ?>
        <?php endif; ?>

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <?php if(isset($header_dos)): ?>
            <?php echo $header_dos; ?>
        <?php endif; ?>
        <!-- Header-->

        <?php if(isset($titulo)): ?>
            <?php echo $titulo; ?>
        <?php endif; ?>

        <?php if(isset($contenido)): ?>
            <?php echo $contenido; ?>
        <?php endif; ?>


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <?php if(isset($footer)): ?>
        <?php echo $footer; ?>
    <?php endif; ?>


</body>
</html>
