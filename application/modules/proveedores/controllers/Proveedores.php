<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Proveedores extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));

      date_default_timezone_set('America/Mexico_City');

       if($this->session->userdata('id')){}else{redirect('login/');}

  }

  public function alta(){
    $titulo['titulo'] = "Datos" ;
    $titulo['titulo_dos'] = "Proveedor" ;
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('proveedores/alta', '', TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js')));
  }

  public function guarda_edicion(){

      if($this->input->post()){
        $textos = $this->input->post();
        $this->Mgeneral->save_editar_formulario($textos,'id','proveedores');
        echo "editado";
      }
    }

  public function guarda_datos(){
    $this->form_validation->set_rules('nombre', 'nombre', 'required');
    $this->form_validation->set_rules('rfc', 'rfc', 'required');
    $this->form_validation->set_rules('razon_social', 'razon_social', 'required');
    $this->form_validation->set_rules('calle', 'calle', 'required');
    $this->form_validation->set_rules('numero', 'numero', 'required');
    $this->form_validation->set_rules('colonia', 'colonia', 'required');
    $this->form_validation->set_rules('cp', 'cp', 'required');
    $this->form_validation->set_rules('municipio', 'municipio', 'required');
    $this->form_validation->set_rules('ciudad', 'ciudad', 'required');
    $this->form_validation->set_rules('estado', 'estado', 'required');
    $this->form_validation->set_rules('pais', 'pais', 'required');
    $this->form_validation->set_rules('telefono', 'telefono', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');
    //$this->form_validation->set_rules('datos_bancarios', 'datos_bancarios', 'required');
    $this->form_validation->set_rules('regimen', 'regimen', 'required');
    $response = validate($this);

    if($response['status']){

      $data['nombre'] = $this->input->post('nombre');
      $data['rfc'] = $this->input->post('rfc');
      $data['razon_social'] = $this->input->post('razon_social');
      $data['calle'] = $this->input->post('calle');
      $data['numero'] = $this->input->post('numero');
      $data['colonia'] = $this->input->post('colonia');
      $data['cp'] = $this->input->post('cp');
      $data['municipio'] = $this->input->post('municipio');
      $data['ciudad'] = $this->input->post('ciudad');
      $data['estado'] = $this->input->post('estado');
      $data['pais'] = $this->input->post('pais');
      $data['telefono'] = $this->input->post('telefono');
      $data['email'] = $this->input->post('email');
      $data['tados_bancarios'] = $this->input->post('datos_bancarios');
      $data['numero_cuenta'] = $this->input->post('numero_cuenta');
      $data['clabe'] = $this->input->post('clabe');
      $data['sucursal'] = $this->input->post('sucursal');
      $data['codigo_banco'] = $this->input->post('codigo_banco');

      $data['regimen'] = $this->input->post('regimen');
      $data['fecha_actualizacion'] = date('Y-m-d H:i:s');

      $this->Mgeneral->save_register('proveedores', $data);
    }
  //  echo $response;
  echo json_encode(array('output' => $response));
  }


  public function ver_proveedores(){
    $titulo['titulo'] = "Proveedores" ;
    $titulo['titulo_dos'] = "proveedores" ;
    $datos['rows'] = $this->Mgeneral->get_table('proveedores');
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('proveedores/ver_proveedores', $datos, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_css'=>array(''),
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                          'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js',
                                          'statics/tema/assets/js/popper.min.js',
                                         'statics/tema/assets/js/plugins.js',
                                         'statics/tema/assets/js/main.js',
                                         'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                         'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                         'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                         'statics/tema/assets/js/lib/data-table/datatables-init.js')));
  }


  public function ver($id_cliente){
    $titulo['titulo'] = "Proveedor" ;
    $titulo['titulo_dos'] = "Proveedor" ;
    $datos['row'] = $this->Mgeneral->get_row('id',$id_cliente,'proveedores');
    $datos['contactos'] = $this->Mgeneral->get_contacto_proveedor($id_cliente);
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('proveedores/ver', $datos, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js')));
  }

  public function editar_datos(){
    $this->form_validation->set_rules('nombre', 'nombre', 'required');
    $this->form_validation->set_rules('rfc', 'rfc', 'required');
    $this->form_validation->set_rules('razon_social', 'razon_social', 'required');
    $this->form_validation->set_rules('calle', 'calle', 'required');
    $this->form_validation->set_rules('numero', 'numero', 'required');
    $this->form_validation->set_rules('colonia', 'colonia', 'required');
    $this->form_validation->set_rules('cp', 'cp', 'required');
    $this->form_validation->set_rules('municipio', 'municipio', 'required');
    $this->form_validation->set_rules('ciudad', 'ciudad', 'required');
    $this->form_validation->set_rules('estado', 'estado', 'required');
    $this->form_validation->set_rules('pais', 'pais', 'required');
    $this->form_validation->set_rules('telefono', 'telefono', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');
    //$this->form_validation->set_rules('datos_bancarios', 'datos_bancarios', 'required');
    $this->form_validation->set_rules('regimen', 'regimen', 'required');
    $response = validate($this);

    if($response['status']){

      $data['nombre'] = $this->input->post('nombre');
      $data['rfc'] = $this->input->post('rfc');
      $data['razon_social'] = $this->input->post('razon_social');
      $data['calle'] = $this->input->post('calle');
      $data['numero'] = $this->input->post('numero');
      $data['colonia'] = $this->input->post('colonia');
      $data['cp'] = $this->input->post('cp');
      $data['municipio'] = $this->input->post('municipio');
      $data['ciudad'] = $this->input->post('ciudad');
      $data['estado'] = $this->input->post('estado');
      $data['pais'] = $this->input->post('pais');
      $data['telefono'] = $this->input->post('telefono');
      $data['email'] = $this->input->post('email');
      $data['tados_bancarios'] = $this->input->post('datos_bancarios');
      $data['numero_cuenta'] = $this->input->post('numero_cuenta');
      $data['clabe'] = $this->input->post('clabe');
      $data['sucursal'] = $this->input->post('sucursal');
      $data['codigo_banco'] = $this->input->post('codigo_banco');
      $data['regimen'] = $this->input->post('regimen');
      $data['fecha_actualizacion'] = date('Y-m-d H:i:s');

      $this->Mgeneral->update_table_row('proveedores',$data,'id',$this->input->post('id_cliente'));
    }
  //  echo $response;
  echo json_encode(array('output' => $response));
  }

  public function guardar_contacto($id_usuario){
      
      $response['status'] = true;
      //$response = validate($this);
    if($response['status']){
        $data['email'] = $this->input->post('contacto_email');
        $data['nombre'] = $this->input->post('contacto_nombre');
        $data['telefono'] = $this->input->post('contacto_telefono');
        $data['tipo'] = 2;
        $data['id_tabla'] = $id_usuario;
        
        $id_usuario = $this->Mgeneral->save_register('usuarios_contactos', $data);
      }
    echo json_encode(array('output' => $response));
    }
    public function eliminar_contacto($id,$id_usuario){
      $this->Mgeneral->delete_row('usuarios_contactos','id',$id);
      redirect('proveedores/ver/'.$id_usuario);
    }

}
