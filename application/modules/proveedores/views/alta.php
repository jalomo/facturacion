<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
label{
  font-family: 'Roboto', sans-serif;
}
input{
  font-family: 'Roboto', sans-serif;
}
</style>
<script>
     menu_activo = "proveedores";
$("#menu_provvedores_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/proveedores/guarda_datos";
  ajaxJson(url,{"nombre":$("#nombre").val(),
                "rfc":$("#rfc").val(),
                "razon_social":$("#razon_social").val(),
                "calle":$("#calle").val(),
                "numero":$("#numero").val(),
                "colonia":$("#colonia").val(),
                "cp":$("#cp").val(),
                "municipio":$("#municipio").val(),
                "ciudad":$("#ciudad").val(),
                "estado":$("#estado").val(),
                "pais":$("#pais").val(),
                "telefono":$("#telefono").val(),
                "email":$("#email").val(),
                "datos_bancarios":$("#datos_bancarios").val(),
                "numero_cuenta":$("#numero_cuenta").val(),
                "clabe":$("#clabe").val(),
                "sucursal":$("#sucursal").val(),
                "codigo_banco":$("#codigo_banco").val(),
                "regimen":$("#regimen").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/proveedores/ver_proveedores");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Alta Proveedor</strong>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Nuevo
            </button>

            <button type="button" class="btn btn-info btn_editar">Editar</button>

            <button type="button" class="btn btn-danger btn_eliminar">Eliminar</button>

        </div>
        <div class="card-body" style="background:#fff">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          

                      </div>

                      <div class="row">
                        <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                  <input id="nombre" name="nombre" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Nombre" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">RFC</label>
                                  <input id="rfc" name="rfc" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="RFC" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Razón Social</label>
                                <input id="razon_social" name="razon_social" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Razón Social" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Régimen</label>
                                  <input id="regimen" name="regimen" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          

                      </div>


                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Calle</label>
                                  <input id="calle" name="calle" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Calle" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="numero" name="numero" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="#" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Colonia</label>
                                <input id="colonia" name="colonia" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Colonia" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">CP</label>
                                <input id="cp" name="cp" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="CP" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Municipio</label>
                                  <input id="municipio" name="municipio" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Municipio" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Ciudad</label>
                                  <input id="ciudad" name="ciudad" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Ciudad" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>


                      <div class="row">
                          
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Estado</label>
                                <input id="estado" name="estado" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Estado" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">País</label>
                                <input id="pais" name="pais" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="País" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                  <input id="telefono" name="telefono" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Teléfono" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Email</label>
                                  <input id="email" name="email" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Email" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>


                      
                      <hr/>

                      <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Nombre Banco</label>
                                <input id="datos_bancarios" name="datos_bancarios" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Nombre Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número de cuenta</label>
                                  <input id="numero_cuenta" name="numero_cuenta" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Número de cuenta" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Clabe</label>
                                  <input id="clabe" name="clabe" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Clabe" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                <input id="sucursal" name="sucursal" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Sucursal" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Código Banco</label>
                                <input id="codigo_banco" name="codigo_banco" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Código Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>

                      <hr/>

                      <div align="right">
                        <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Guardar</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                        <div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                        </div>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
