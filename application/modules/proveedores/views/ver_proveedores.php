<script type="text/javascript">
       menu_activo = "proveedores";
$("#menu_provvedores_lista").last().addClass("menu_estilo");
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});
    } );
</script>

<script>
     menu_activo = "proveedores";
$("#menu_provvedores_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/proveedores/guarda_datos";
  ajaxJson(url,{"nombre":$("#nombre").val(),
                "rfc":$("#rfc").val(),
                "razon_social":$("#razon_social").val(),
                "calle":$("#calle").val(),
                "numero":$("#numero").val(),
                "colonia":$("#colonia").val(),
                "cp":$("#cp").val(),
                "municipio":$("#municipio").val(),
                "ciudad":$("#ciudad").val(),
                "estado":$("#estado").val(),
                "pais":$("#pais").val(),
                "telefono":$("#telefono").val(),
                "email":$("#email").val(),
                "datos_bancarios":$("#datos_bancarios").val(),
                "numero_cuenta":$("#numero_cuenta").val(),
                "clabe":$("#clabe").val(),
                "sucursal":$("#sucursal").val(),
                "codigo_banco":$("#codigo_banco").val(),
                "regimen":$("#regimen").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/proveedores/ver_proveedores");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});


$(".btn_eliminar").click(function(event){
                     event.preventDefault();

                     bootbox.dialog({
                         message: "Desea eliminar los registros seleccionados?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {

                                   $('.myCheckBox').each(function(){ //this.checked = true; 
                                      if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());
                                           id = $(this).val();
                                           url = "<?php echo base_url()?>index.php/proveedores/eliminar/"+id;//$("#delete"+id).attr('href');
                                           $("#borrar_"+id).slideUp();
                                          /// $.get(url);
                                                };
                                           });

                                   
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });


                    
                 });


  

  $("#todos_boton").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });

  $(".btn_editar").click(function(event){
                     event.preventDefault();

                      //$( "#todos_boton" ).prop( "checked", true );

                     //$('.myCheckBox').each(function(){ this.checked = true; });
                      aux = 1;
                      $('.myCheckBox').each(function(){ //this.checked = true; 
                            if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());           
                                id = $(this).val();
                                console.log(id);
                                var txt = $(".textNombre_"+id+"").html();
                                $(".textNombre_"+id+"").replaceWith("<input value='" + txt + "' name='"+aux+"[rfc]'/>");

                                var txt_des = $(".textDescripion_"+id+"").html();
                                $(".textDescripion_"+id+"").replaceWith("<input value='" + txt_des + "' name='"+aux+"[nombre]'/>");

                                var txt_email = $(".textEmail_"+id+"").html();
                                $(".textEmail_"+id+"").replaceWith("<input value='" + txt_email + "' name='"+aux+"[email]'/>");


                                var txt_telefono = $(".textTelefono_"+id+"").html();
                                $(".textTelefono_"+id+"").replaceWith("<input value='" + txt_telefono + "' name='"+aux+"[telefono]'/>");


                                var txt_id = $(".id_des_"+id+"").html();
                                $(".id_des_"+id+"").replaceWith("<input type='hidden' value='" + txt_id + "' name='"+aux+"[id]'/>");

                                aux = aux +1;
                            };
                      });

                      $("body").keypress(function(e) {
                        if(e.which == 13) {
                          //obtenerDatos();
                          
                          var form = $("#formulario_editar").serializeArray();
                         // console.log(form);
                            url = "<?php echo base_url()?>index.php/proveedores/guarda_edicion"
                            ajaxJson(url,form,
                                        "POST","",function(result){
                                            correoValido = false;
                                            console.log(result);
                                            exito_redirect("EDITADO CON EXITO","success","<?php echo base_url()?>index.php/proveedores/ver_proveedores");
                                            
                                          });

                        }
                      });



                    
                 });

});
</script>
<style type="text/css">
  
  .fullscreen-modal .modal-dialog {
  margin: 0;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
}
@media (min-width: 768px) {
  .fullscreen-modal .modal-dialog {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .fullscreen-modal .modal-dialog {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .fullscreen-modal .modal-dialog {
     width: 1170px;
  }
}
</style>

<div class="card-header">
  <!--a href="<?php echo base_url()?>index.php/proveedores/alta"><button type="button" class="btn btn-info">Alta Proveedor</button></a-->

  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Nuevo
            </button>

            <button type="button" class="btn btn-info btn_editar">Editar</button>

            <button type="button" class="btn btn-danger btn_eliminar">Eliminar</button>



</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                  <form id="formulario_editar">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>
                         
                           <input type="checkbox" name=""  id="todos_boton" class="todos_boton" >
                          
                        </th>
                        <th>RFC</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Teléfono</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($rows)):?>
                      <?php foreach($rows as $row):?>
                       <tr id="borrar_<?php echo $row->id?>">
                           <td><input type="checkbox" name="" class="myCheckBox" id="<?php echo $row->id?>" value="<?php echo $row->id?>"></td>
                          <td><span class="textNombre_<?php echo $row->id?> texto_nombre"><?php echo $row->rfc;?></span></td>
                          <td><span class="textDescripion_<?php echo $row->id?> texto_descripcion"><?php echo $row->nombre;?></span></td>

                          <td><span class="textEmail_<?php echo $row->id?> texto_email"><?php echo $row->email;?></span></td>
                          <td><span class="textTelefono_<?php echo $row->id?> texto_telefono"><?php echo $row->telefono;?></span></td>

                          <span hidden="hidden" class="id_des_<?php echo $row->id?> texto_id" type="hidden"><?php echo $row->id?></span>

                          <td>
                            <a href="<?php echo base_url()?>index.php/proveedores/ver/<?php echo $row->id;?>">
                            <button type="button" class="btn btn-info">Ver/Editar</button>
                            </a>
                          </td>
                        </tr>
                     <?php endforeach;?>
                     <?php endif;?>
                    </tbody>
                  </table>
                </form>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->



<!-- Modal -->
<div class="modal fullscreen-modal  fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva plaga </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          

                      </div>

                      <div class="row">
                        <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                  <input id="nombre" name="nombre" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Nombre" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">RFC</label>
                                  <input id="rfc" name="rfc" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="RFC" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Razón Social</label>
                                <input id="razon_social" name="razon_social" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Razón Social" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Régimen</label>
                                  <input id="regimen" name="regimen" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          

                      </div>


                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Calle</label>
                                  <input id="calle" name="calle" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Calle" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="numero" name="numero" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="#" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Colonia</label>
                                <input id="colonia" name="colonia" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Colonia" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">CP</label>
                                <input id="cp" name="cp" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="CP" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Municipio</label>
                                  <input id="municipio" name="municipio" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Municipio" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Ciudad</label>
                                  <input id="ciudad" name="ciudad" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Ciudad" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>


                      <div class="row">
                          
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Estado</label>
                                <input id="estado" name="estado" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Estado" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">País</label>
                                <input id="pais" name="pais" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="País" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                  <input id="telefono" name="telefono" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Teléfono" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Email</label>
                                  <input id="email" name="email" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Email" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>


                      
                      <hr/>

                      <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Nombre Banco</label>
                                <input id="datos_bancarios" name="datos_bancarios" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Nombre Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número de cuenta</label>
                                  <input id="numero_cuenta" name="numero_cuenta" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Número de cuenta" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Clabe</label>
                                  <input id="clabe" name="clabe" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Clabe" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                <input id="sucursal" name="sucursal" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Sucursal" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Código Banco</label>
                                <input id="codigo_banco" name="codigo_banco" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Código Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>

                      <hr/>

                      <div align="right">
                        <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Guardar</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                        <div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                        </div>
                      </div>
                  </form>
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>

