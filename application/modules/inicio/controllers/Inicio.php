<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Inicio extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function test(){
      $this->load->view('inicio/test', '', FALSE);
    }

    public function guarda_test(){

      $this->form_validation->set_rules('uno', 'uno', 'required');
      $this->form_validation->set_rules('dos', 'dos', 'required');
      $this->form_validation->set_rules('tres', 'tres', 'required');
      $this->form_validation->set_rules('cuatro', 'cuatro', 'required');
      echo validate($this);
    }

    public function index2(){

      $head = $this->load->view('inicio/head', '', TRUE);
      $header_logo = $this->load->view('inicio/header_logo', '', TRUE);
      $menu = $this->load->view('inicio/menu', '', TRUE);
      $contenido = $this->load->view('inicio/contenido', '', TRUE);
      $this->load->view('home', array('head'=>$head,
                                      'header_logo'=>$header_logo,
                                      'menu'=>$menu,
                                      'contenido'=>$contenido,
                                          'included_js'=>array('')));

    }


    public function index(){
      $titulo['titulo'] = "";//"Usuarios" ;
      $titulo['titulo_dos'] = "";//"Alta Usuarios" ;
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('inicio/inicio', '', TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }


  }
