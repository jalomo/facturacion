<div class="tr-topbar topbar-width-container topbar-bg-color-1 clearfix">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <a class="navbar-brand" href="index.html"><img class="img-responsive" src="images/logo2.png" alt="Logo"></a>
      </div>
      <div class="col-sm-9">
        <div class="topbar-left">
                    <div class="breaking-news">
                        <span># Newsfeed</span>
                        <div id="ticker">
                            <ul>
                                <li><a href="#">Remarkable Women - character design project</a></li>
                <li><a href="#">Remarkable Women - character design project</a></li>
                <li><a href="#">Remarkable Women - character design project</a></li>
                <li><a href="#">Remarkable Women - character design project</a></li>
                            </ul>
                      </div>
                  </div><!-- breaking-news -->
        </div><!-- /.topbar-left -->
        <div class="topbar-right">
          <div class="user">
            <div class="user-image">
              <img class="img-responsive img-circle" src="images/others/user.png" alt="Image">
            </div>
            <div class="dropdown user-dropdown">
              Hello,
              <a href="#" aria-expanded="true">Jhon Player<i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="sub-menu text-left">
                <li><a href="#">My Profile</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="#">Log Out</a></li>
              </ul>
            </div>
          </div>
          <div class="searchNlogin">
            <ul>
              <li class="search-icon"><i class="fa fa-search"></i></li>
              <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-bell-o" aria-hidden="true"></i></a></li>
              <li class="dropdown language-dropdown">
                <a href="#" aria-expanded="true"><span class="change-text">En</span> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <ul class="sub-menu text-center">
                  <li><a href="#">EN</a></li>
                  <li><a href="#">FR</a></li>
                  <li><a href="#">GR</a></li>
                  <li><a href="#">ES</a></li>
                </ul>
              </li>
            </ul>
            <div class="search">
              <form role="form">
                <input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
              </form>
            </div> <!--/.search-->
          </div>
        </div><!-- /.topbar-right -->
      </div>
    </div><!-- /.row -->
  </div><!-- /.container -->
</div><!-- /.tr-topbar -->
