<div class="col-sm-6">
  <div class="tr-content">
    <div class="tr-section bg-transparent">

      <div class="section-title">
        <div class="icon">
          <i class="fa fa-ioxhost" aria-hidden="true"></i>
        </div>
        <h1><a href="#">Technology</a></h1>
      </div>


      <div class="tr-post">
        <div class="entry-header">
          <div class="entry-thumbnail">
            <a href="details.html"><img class="img-responsive" src="images/post/13.jpg" alt="Image"></a>
          </div>
        </div>
        <div class="post-content">
          <div class="author">
            <a href="#"><img class="img-responsive img-circle" src="images/others/author1.png" alt="Image"></a>
          </div>
          <div class="entry-meta">
            <ul>
              <li>By <a href="#">Adam Hianks</a></li>
              <li>643 Share /<a href="#"> 9 Hour ago</a></li>
              <li>
                <ul>
                  <li>Share</li>
                  <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.entry-meta -->
          <h2 class="entry-title">
            <a href="details.html">Slow TV finds life online with Facebook Live and Periscope Actfile</a>
          </h2>
        </div>
      </div><!-- /.tr-post -->


      <div class="tr-post">
        <div class="entry-header">
          <div class="entry-thumbnail">
            <a href="details.html"><img class="img-responsive" src="images/post/14.jpg" alt="Image"></a>
          </div>
        </div>
        <div class="post-content">
          <div class="author">
            <a href="#"><img class="img-responsive img-circle" src="images/others/author3.png" alt="Image"></a>
          </div>
          <div class="entry-meta">
            <ul>
              <li>By <a href="#">Kolony Wispe</a></li>
              <li>62 Share /<a href="#"> 3 Hour ago</a></li>
              <li>
                <ul>
                  <li>Share</li>
                  <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.entry-meta -->
          <h2 class="entry-title">
            <a href="details.html">We Are Seeing The Effects Of The Minimum Wage Rise In San Francisco</a>
          </h2>
        </div>
      </div><!-- /.tr-post -->


      
    </div><!-- /.technology-section -->


  </div><!-- /.tr-content -->
</div>
