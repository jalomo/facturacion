<!DOCTYPE html>
<html lang="en">
	<head>
    <?php if(isset($head)): ?>
     <?php echo $head; ?>
    <?php endif; ?>
	</head>
	<body class="homepage-1">
		<div class="main-wrapper">
      <?php if(isset($header_logo)): ?>
       <?php echo $header_logo; ?>
      <?php endif; ?>

			<div class="container">
				<div class="row">
          <?php if(isset($menu)): ?>
           <?php echo $menu; ?>
          <?php endif; ?>


          <?php if(isset($contenido)): ?>
           <?php echo $contenido; ?>
          <?php endif; ?>

					<div class="col-sm-3 tr-sidebar tr-sticky">
						<div class="theiaStickySidebar">
							<div class="tr-section tr-widget tr-ad ad-before">
								<a href="#"><img class="img-responsive" src="images/ai/2.jpg" alt="Image"></a>
							</div><!-- /.tr-post -->

							<div class="tr-section tr-widget">
								<div class="widget-title">
									<span>This Is rising</span>
								</div>
								<ul class="medium-post-list">
									<li class="tr-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="details.html"><img class="img-responsive" src="images/post/9.jpg" alt="Image"></a>
											</div>
										</div>
										<div class="post-content">
											<div class="catagory">
												<a href="#">Entertainment</a>
											</div>
											<h2 class="entry-title">
												<a href="details.html">Howard Stern: 'I told Trump' I'm voting for Clinton</a>
											</h2>
										</div><!-- /.post-content -->
									</li>
									<li class="tr-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="details.html"><img class="img-responsive" src="images/post/10.jpg" alt="Image"></a>
											</div>
										</div>
										<div class="post-content">
											<div class="catagory">
												<a href="#">Business</a>
											</div>
											<h2 class="entry-title">
												<a href="details.html">Our closest relatives aren't fans of daylight saving time</a>
											</h2>
										</div><!-- /.post-content -->
									</li>
									<li class="tr-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="details.html"><img class="img-responsive" src="images/post/11.jpg" alt="Image"></a>
											</div>
										</div>
										<div class="post-content">
											<div class="catagory">
												<a href="#">Technology</a>
											</div>
											<h2 class="entry-title">
												<a href="details.html">And the most streamed Beatles song on Spotify is...</a>
											</h2>
										</div><!-- /.post-content -->
									</li>
									<li class="tr-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="details.html"><img class="img-responsive" src="images/post/12.jpg" alt="Image"></a>
											</div>
										</div>
										<div class="post-content">
											<div class="catagory">
												<a href="#">Sports</a>
											</div>
											<h2 class="entry-title">
												<a href="details.html">Our closest relatives aren't fans of daylight saving time</a>
											</h2>
										</div><!-- /.post-content -->
									</li>
								</ul>
							</div><!-- /.tr-section -->

							<div class="tr-weather">
								<div class="weather-top">
									<div class="row">
										<div class="col-xs-6">
											<div class="weather-image">
												<img class="img-responsive" src="images/others/weather1.png" alt="Image">
											</div>
										</div>
										<div class="col-xs-6">
											<span class="weather-temp">21 <sup>°C</sup></span>
											<span class="weather-type">Mostly Cloudy</span>
											<span>London, UK</span>
										</div>
									</div>
									<ul>
										<li><span><img class="img-responsive" src="images/others/weather2.png" alt="Image"></span>13%</li>
										<li><span><img class="img-responsive" src="images/others/weather3.png" alt="Image"></span>6.44 MPH</li>
									</ul>
								</div><!-- /.weather-top -->

								<div class="weather-bottom">
									<ul>
										<li>
											<div class="weather-icon">
												<img class="img-responsive" src="images/others/weather4.png" alt="Image">
											</div>
											<div class="weather-info">
												<span>23°</span>
												<span class="date">Sun, 3 July</span>
											</div>
										</li>
										<li>
											<div class="weather-icon">
												<img class="img-responsive" src="images/others/weather5.png" alt="Image">
											</div>
											<div class="weather-info">
												<span>26°</span>
												<span class="date">Sun, 3 July</span>
											</div>
										</li>
									</ul>
								</div><!-- /.weather-bottom -->
							</div><!-- /.weather-widget -->

							<div class="tr-section tr-widget tr-ad ad-before">
								<a href="#"><img class="img-responsive" src="images/ai/2.jpg" alt="Image"></a>
							</div><!-- /.tr-post -->

							<div class="tr-widget meta-widget meta-color">
								<div class="widget-title">
									<span>Weekly Toplist</span>
								</div>
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#author" data-toggle="tab"><i class="fa fa-user"></i>Authors</a></li>
									<li role="presentation"><a href="#comment" data-toggle="tab"><i class="fa fa-comment-o"></i>Comments</a></li>
								</ul>
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active fade in" id="author">
										<ul class="authors-post">
											<li>
												<div class="entry-meta">
													<div class="author-image">
														<a href="#"><img class="img-responsive" src="images/others/author4.png" alt="Image"></a>
													</div>
													<div class="author-info">
														<h2><a href="#">Janet Jackson</a></h2>
													</div>
												</div>
											</li>
											<li>
												<div class="entry-meta">
													<div class="author-image">
														<a href="#"><img class="img-responsive" src="images/others/author5.png" alt="Image"></a>
													</div>
													<div class="author-info">
														<h2><a href="#">Matt Cloey</a></h2>
													</div>
												</div>
											</li>
											<li>
												<div class="entry-meta">
													<div class="author-image">
														<a href="#"><img class="img-responsive" src="images/others/author6.png" alt="Image"></a>
													</div>
													<div class="author-info">
														<h2><a href="#">Kolony Wispe</a></h2>
													</div>
												</div>
											</li>
											<li>
												<div class="entry-meta">
													<div class="author-image">
														<a href="#"><img class="img-responsive" src="images/others/author7.png" alt="Image"></a>
													</div>
													<div class="author-info">
														<h2><a href="#">Matt Cloey</a></h2>
													</div>
												</div>
											</li>
											<li>
												<div class="entry-meta">
													<div class="author-image">
														<a href="#"><img class="img-responsive" src="images/others/author8.png" alt="Image"></a>
													</div>
													<div class="author-info">
														<h2><a href="#">Jhon dun</a></h2>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div role="tabpanel" class="tab-pane fade in" id="comment">
										<ul class="comment-list">
											<li>
												<div class="post-content">
													<div class="entry-meta">
														<span><a href="#">Jhon dun</a></span>
													</div>
													<h2 class="entry-title">
														<a href="#">2 students arrested after body-slamming principal</a>
													</h2>
												</div>
											</li>
											<li>
												<div class="post-content">
													<div class="entry-meta">
														<span><a href="#">Matt Cloey</a></span>
													</div>
													<h2 class="entry-title">
														<a href="#">5 students arrested after body-slamming principal</a>
													</h2>
												</div>
											</li>
											<li>
												<div class="post-content">
													<div class="entry-meta">
														<span><a href="#">Kolony Wispe</a></span>
													</div>
													<h2 class="entry-title">
														<a href="#">3 students arrested after body-slamming principal</a>
													</h2>
												</div>
											</li>
											<li>
												<div class="post-content">
													<div class="entry-meta">
														<span><a href="#">Janet Jackson</a></span>
													</div>
													<h2 class="entry-title">
														<a href="#">2 students arrested after body-slamming principal</a>
													</h2>
												</div>
											</li>
											<li>
												<div class="post-content">
													<div class="entry-meta">
														<span><a href="#">Matt Cloey</a></span>
													</div>
													<h2 class="entry-title">
														<a href="#">4 students arrested after body-slamming principal</a>
													</h2>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div><!-- meta-tab -->
						</div><!-- /.theiaStickySidebar -->
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- main-wrapper -->

		<footer id="footer">
			<div class="footer-menu">
				<div class="container">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Products</a></li>
						<li><a href="#">Career</a></li>
						<li><a href="#">Advertisement</a></li>
						<li><a href="#">Team</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="footer-widgets">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="widget widget-menu-2">
								<h2>Category</h2>
								<ul>
									<li><a href="#">Business</a></li>
									<li><a href="#">Politics</a></li>
									<li><a href="#">Sports</a></li>
									<li><a href="#">Environment</a></li>
									<li><a href="#">World</a></li>
									<li><a href="#">Technology</a></li>
									<li><a href="#">Health</a></li>
									<li><a href="#">Entertainment</a></li>
									<li><a href="#">Lifestyle</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="widget">
								<h2>Editions</h2>
								<ul>
									<li><a href="#">United States</a></li>
									<li><a href="#">China</a></li>
									<li><a href="#">India</a></li>
									<li><a href="#">Maxico</a></li>
									<li><a href="#">Middle East</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="widget widget-menu-3">
								<h2>Tag</h2>
								<ul>
									<li><a href="#">Gallery</a></li>
									<li><a href="#">Sports</a></li>
									<li><a href="#">Featured</a></li>
									<li><a href="#">Fashion</a></li>
									<li><a href="#">Entertainment</a></li>
									<li><a href="#">Business</a></li>
									<li><a href="#">Tech</a></li>
									<li><a href="#">Movies</a></li>
									<li><a href="#">Music</a></li>
									<li><a href="#">Packages</a></li>
									<li><a href="#">Amazon</a></li>
									<li><a href="#">Cars</a></li>
									<li><a href="#">Phones</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="widget">
								<h2>Products</h2>
								<ul>
									<li><a href="#">Ebooks</a></li>
									<li><a href="#">Newsfeeds</a></li>
									<li><a href="#">Reprints & Permissions</a></li>
									<li><a href="#">Magazine</a></li>
									<li><a href="#">College Guide</a></li>
								</ul>
							</div>
						</div>
					</div><!-- /.row -->
				</div>
			</div>
			<div class="footer-bottom text-center">
				<div class="container">
					<div class="footer-bottom-content">
						<div class="footer-logo">
							<a href="index.html"><img class="img-responsive" src="images/footer-logo2.png" alt="Logo"></a>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<address>
							<p>&copy; 2017 <a href="#">Newshub</a>. Email: <a href="#">info@newshub.com</a> | News: <a href="#">news.newshub.com</a> | Advertising: <a href="#">ad.newshub.com</a> <br>Phone: + 1234 8812345, 880112345 + 1359, 6356 + 112-11-9874</p>
						</address>
					</div><!-- /.footer-bottom-content -->
				</div><!-- /.container -->
			</div><!-- /.footer-bottom -->
		</footer><!-- /#footer -->


		<!-- JS -->
		<script src="<?php echo base_url();?>tema/js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>tema/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>tema/js/marquee.js"></script>
		<script src="<?php echo base_url();?>tema/js/moment.min.js"></script>
		<script src="<?php echo base_url();?>tema/js/theia-sticky-sidebar.js"></script>
		<script src="<?php echo base_url();?>tema/js/jquery.jplayer.min.js"></script>
		<script src="<?php echo base_url();?>tema/js/jplayer.playlist.min.js"></script>
		<script src="<?php echo base_url();?>tema/js/slick.min.js"></script>
		<script src="<?php echo base_url();?>tema/js/carouFredSel.js"></script>
		<script src="<?php echo base_url();?>tema/js/jquery.magnific-popup.js"></script>
		<script src="<?php echo base_url();?>tema/js/main.js"></script>
		<script>


		</script>
	</body>
</html>
