<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Theme Region">
<meta name="description" content="">

<title>Revista digital</title>

<!-- CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/bootstrap.min.css" >
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/animate.css">
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/jplayer.css">
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>tema/css/responsive.css">

<!-- font -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Signika+Negative" rel="stylesheet">

<!-- icons -->
<link rel="icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
<!-- icons -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Template Developed By ThemeRegion -->
