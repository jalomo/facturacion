<div class="col-sm-3 tr-sidebar tr-sticky">
  <div class="theiaStickySidebar">
    <div class="tr-menu sidebar-menu sidebar-menu-two menu-responsive">
      <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle toggle-white collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
        </div><!-- /navbar-header -->

        <div class="collapse navbar-collapse" id="navbar-collapse">
          <span class="discover">Categorias</span>
          <ul class="nav navbar-nav">
            <li class="active dropdown"><a  href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i>Inicio</a></li>
            <li><a href="index2.html"><i class="fa-google-wallet" aria-hidden="true"></i>Business</a></li>
            <li><a href="index3.html"><i class="fa fa fa-female" aria-hidden="true"></i>LifeStyle</a></li>
            <li><a href="common-list.html"><i class="fa fa-compass" aria-hidden="true"></i>Politics</a></li>
            <li><a href="index4.html"><i class="fa fa-futbol-o" aria-hidden="true"></i>Sports</a></li>
            <li><a href="index1.html"><i class="fa fa-bullhorn" aria-hidden="true"></i>Social</a></li>
            <li class="dropdown"><a href="#"><i class="fa fa-address-book-o" aria-hidden="true"></i>About</a>
              <ul class="sub-menu">
                <li><a href="about.html">About Page</a></li>
                <li><a href="about1.html">About Page-1</a></li>
              </ul>
            </li>
            <li class="dropdown"><a href="#"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a>
              <ul class="sub-menu">
                <li><a href="details.html">Details Page</a></li>
                <li><a href="details1.html">Details Page-1</a></li>
                <li><a href="details2.html">Details Page-2</a></li>
              </ul>
            </li>
            <li class="dropdown"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>Contact</a>
              <ul class="sub-menu">
                <li><a href="contact.html">Contact Page</a></li>
                <li><a href="contact1.html">Contact Page-1</a></li>
              </ul>
            </li>
            <li><a href="login.html"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a></li>
            <li><a href="register.html"><i class="fa fa-registered" aria-hidden="true"></i>Register</a></li>
            <li><a href="404.html"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>404 Page</a></li>
          </ul>
        </div>
      </nav><!-- /navbar -->
    </div><!-- /left-memu -->
  </div><!-- /.theiaStickySidebar -->
</div>
