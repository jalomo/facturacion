<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function(){

      $('#guardar_datos').click(function(event){
        event.preventDefault();

        var url ="<?php echo base_url()?>index.php/factura/guarda_datos_fac/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"receptor_nombre":$("#receptor_nombre").val(),
                      "receptor_id_cliente":$("#receptor_id_cliente").val(),
                      "factura_moneda":$("#factura_moneda").val(),
                      "fatura_lugarExpedicion":$("#fatura_lugarExpedicion").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "receptor_email":$("#receptor_email").val(),
                      "factura_folio":$("#factura_folio").val(),
                      "factura_serie":$("#factura_serie").val(),
                      "receptor_direccion":$("#receptor_direccion").val(),
                      "factura_formaPago":$("#factura_formaPago").val(),
                      "factura_medotoPago":$("#factura_medotoPago").val(),
                      "factura_tipoComprobante":$("#factura_tipoComprobante").val(),
                      "receptor_RFC":$("#receptor_RFC").val(),
                      "receptor_uso_CFDI":$("#receptor_uso_CFDI").val(),
                      "pagada":$("#pagada").val(),
                      //"almacen":$("#almacen").val(),
                      "comentario":$("#comentario").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");

          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","");

          }


        });
      });


      $("#cargando").hide();
      $('#guarda_factura').submit(function(event){
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();
        var url ="<?php echo base_url()?>index.php/factura/guarda_datos/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"receptor_nombre":$("#receptor_nombre").val(),
                      "receptor_id_cliente":$("#receptor_id_cliente").val(),
                      "factura_moneda":$("#factura_moneda").val(),
                      "fatura_lugarExpedicion":$("#fatura_lugarExpedicion").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "receptor_email":$("#receptor_email").val(),
                      "factura_folio":$("#factura_folio").val(),
                      "factura_serie":$("#factura_serie").val(),
                      "receptor_direccion":$("#receptor_direccion").val(),
                      "factura_formaPago":$("#factura_formaPago").val(),
                      "factura_medotoPago":$("#factura_medotoPago").val(),
                      "factura_tipoComprobante":$("#factura_tipoComprobante").val(),
                      "receptor_RFC":$("#receptor_RFC").val(),
                      "receptor_uso_CFDI":$("#receptor_uso_CFDI").val(),
                      "pagada":$("#pagada").val(),
                      "almacen":$("#almacen").val(),
                      "comentario":$("#comentario").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/factura/conceptos/<?php echo $factura->facturaID?>");
            $("#enviar").show();
            $("#cargando").hide();
          }


        });
      });


      $("#receptor_nombre").autocomplete({
            source: "<?php echo base_url()?>index.php/factura/buscar_cliente",
             select: function(event, ui) {
				$('#receptor_RFC').val(ui.item.rfc);
				$('#receptor_email').val(ui.item.email);
				$('#receptor_direccion').val(ui.item.ciudad);
        $('#receptor_id_cliente').val(ui.item.id);


           }
    });

      $.ajax({
          type: 'POST',
          url: "<?php echo base_url()?>index.php/factura/ver_relaciones_facturas",
          enctype: 'multipart/form-data',
          datatype: "JSON",
        //async: asincrono,
          cache: false,
          data: {id_factura:"<?php echo $factura->facturaID?>"},
          statusCode: {
              200: function (result) {
                //console.log(result);
                 $("#relacionados").html("");
                 $("#relacionados").html(result);
                 $(".eliminar_relacion").click(function(event){
                      event.preventDefault();
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                      $.get(url);
                  });

              },
              401: code400,
              404: code404,
              500: code500,
              409: code409
          }
      });

        $("#agregar_relacion").click(function(){

          $.ajax({
              type: 'POST',
              url: "<?php echo base_url()?>index.php/factura/relaciones_facturas",
              enctype: 'multipart/form-data',
              datatype: "JSON",
            //async: asincrono,
              cache: false,
              data: {tipo_relacion:$("#id_relacion").val(),uuid:$("#uuid").val(),id_factura:"<?php echo $factura->facturaID?>"},
              statusCode: {
                  200: function (result) {
                    //console.log(result);
                     $("#relacionados").html("");
                     $("#relacionados").html(result);
                     $(".eliminar_relacion").click(function(event){
                          event.preventDefault();
                          id = $(event.currentTarget).attr('flag');
                          url = $("#delete"+id).attr('href');
                          $("#borrar_"+id).slideUp();
                          $.get(url);
                      });

                  },
                  401: code400,
                  404: code404,
                  500: code500,
                  409: code409
              }
          });


        });

    });

function seleccionar_cliente(id_cliente){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/get_datos_cliente/"+id_cliente;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              var obj = JSON.parse(data);

              $('#receptor_nombre').val(obj.nombre);
              

              $('#receptor_RFC').val(obj.rfc);
              $('#receptor_email').val(obj.email);
              $('#receptor_direccion').val(obj.ciudad);
              $('#receptor_id_cliente').val(obj.id);
              $('#factura_formaPago').val(obj.factura_formaPago);
              $('#receptor_uso_CFDI').val(obj.uso_CFDI);



              console.log("SUCCESS : ", data);





            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}
</script>
<style>

label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>

<div class="content mt-3">
    <div class="animated fadeIn">

<div class="row">
  <div class="col-lg-12">
    <div class="card" style="background:#ced6e0">

        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="guarda_factura">
                    <!--div align="right">
                    <button id="guardar_datos" type="button" class="btn btn-outline-success">
                        <i class="fa fa-edit fa-lg"></i>&nbsp;
                        <span id="payment-button-amount">Guardar datos</span>

                    </button>
                  </div>
                  <hr/-->

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="factura_moneda">Cliente:</label>
                                <input id="receptor_nombre" name="receptor_nombre" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_nombre;?>" placeholder="Cliente" autocomplete="cc-exp" data-toggle="modal"  data-target="#exampleModal">
                                <input id="receptor_id_cliente" name="receptor_id_cliente" type="hidden" class="alto form-control cc-exp" value="<?php echo $factura->receptor_id_cliente;?>" placeholder="receptor_id_cliente" autocomplete="cc-exp" >
                            </div>
                        </div>

                        <div class="col-1">
                          <div class="form-group">
                            <label for="email">Folio:</label>
                            <input type="text" class="form-control-sm alto form-control" id="factura_folio" name="factura_folio" value="<?php echo $factura->factura_id?>">
                          </div>
                       </div>
                       <div class="col-1">
                         <div class="form-group">
                           <label for="email">Serie:</label>
                           <input type="text" class="form-control-sm alto form-control" id="factura_serie" name="factura_serie" value="<?php echo $factura->factura_id?>">
                         </div>
                      </div>
                       <div class="col-1">
                         <div class="form-group">
                           <label for="email">CP:</label>
                           <input type="text" class="form-control-sm alto form-control" id="fatura_lugarExpedicion" name="fatura_lugarExpedicion" value="<?php echo $emisor->cp;?>">
                         </div>
                      </div>
                      <div class="col-2">
                        <div class="form-group">
                          <label for="email">Fecha:</label>
                          <input type="text" class="form-control-sm alto form-control" id="factura_fecha" name="factura_fecha" value="<?php echo date('Y-m-d H:i:s');?>">
                        </div>
                     </div>
                     <div class="col-1">
                       <div class="form-group">
                         <label for="pagada">Pagada:</label>
                         <select class="form-control-sm alto form-control" name="pagada" id="pagada">
                           <option value="SI">SI</option>
                           <option value="NO">NO</option>
                         </select>


                     </div>
                    </div>
                    </div>

                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="receptor_email">Email:</label>
                                  <input id="receptor_email" name="receptor_email" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_email;?>" placeholder="Email" autocomplete="cc-exp">

                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="receptor_RFC">RFC:</label>
                                  <input id="receptor_RFC" name="receptor_RFC" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_RFC;?>" placeholder="RFC" autocomplete="cc-exp">

                              </div>
                          </div>


                          <div class="col-2">
                            <div class="form-group">
                              <label for="factura_formaPago">Forma de pago:</label>
                              <select  class="form-control-sm alto form-control" name="factura_formaPago" id="factura_formaPago">
                                <?php if($factura->factura_formaPago == "01"){?>
                                <option value="01" selected>1-Efectivo</option>
                              <?php }else{?>
                                <option value="01">1-Efectivo</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "02"){?>
                                <option value="02" selected>2-Cheque nominativo</option>
                              <?php }else{?>
                                <option value="02">2-Cheque nominativo</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "03"){?>
                                <option value="03" selected>3-Transferencia electrónica de fondos</option>
                              <?php }else{?>
                                <option value="03">3-Transferencia electrónica de fondos</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "04"){?>
                                <option value="04" selected>4-Tarjeta de crédito</option>
                              <?php }else{?>
                                <option value="04">4-Tarjeta de crédito</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "05"){?>
                                <option value="05" selected>5-Monedero electrónico</option>
                              <?php }else{?>
                                <option value="05">5-Monedero electrónico</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "06"){?>
                                <option value="06" selected>6-Dinero electrónico</option>
                              <?php }else{?>
                                <option value="06">6-Dinero electrónico</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "08"){?>
                                <option value="08" selected>8-Vales de despensa</option>
                              <?php }else{?>
                                <option value="08">8-Vales de despensa</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "12"){?>
                                <option value="12" selected>12-Dación en pago</option>
                              <?php }else{?>
                                <option value="12">12-Dación en pago</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "13"){?>
                                <option value="13" selected>13-Pago por subrogación</option>
                              <?php }else{?>
                                <option value="13">13-Pago por subrogación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "14"){?>
                                <option value="14" selected>14-Pago por consignación</option>
                              <?php }else{?>
                                <option value="14">14-Pago por consignación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "15"){?>
                                <option value="15" selected>15-Condonación</option>
                              <?php }else{?>
                                <option value="15">15-Condonación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "17"){?>
                                <option value="17" selected>17-Compensación</option>
                              <?php }else{?>
                                <option value="17">17-Compensación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "23"){?>
                                <option value="23" selected>23-Novación</option>
                              <?php }else{?>
                                <option value="23">23-Novación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "24"){?>
                                <option value="24" selected>24-Confusión</option>
                              <?php }else{?>
                                <option value="24">24-Confusión</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "25"){?>
                                <option value="25" selected>25-Remisión de deuda</option>
                              <?php }else{?>
                                <option value="25">25-Remisión de deuda</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "26"){?>
                                <option value="26" selected>26-Prescripción o caducidad</option>
                              <?php }else{?>
                                <option value="26">26-Prescripción o caducidad</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "27"){?>
                                <option value="27" selected>27-A satisfacción del acreedor</option>
                              <?php }else{?>
                                <option value="27">27-A satisfacción del acreedor</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "28"){?>
                                <option value="28" selected>28-Tarjeta de débito</option>
                              <?php }else{?>
                                <option value="28">28-Tarjeta de débito</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "29"){?>
                                <option value="29" selected>29-Tarjeta de servicios</option>
                              <?php }else{?>
                                <option value="29">29-Tarjeta de servicios</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "30"){?>
                                <option value="30" selected>30-Aplicación de anticipos</option>
                              <?php }else{?>
                                <option value="30">30-Aplicación de anticipos</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "99"){?>
                                <option value="99" selected>99-Por definir</option>
                              <?php }else{?>
                                <option value="99">99-Por definir</option>
                                <?php }?>

                              </select>

                            </div>
                         </div>
                         <div class="col-2">
                           <div class="form-group">
                             <label for="factura_medotoPago">Metodo pago:</label>
                              <select class="form-control-sm alto form-control" id="factura_medotoPago" name="factura_medotoPago">

                                <?php if($factura->factura_medotoPago == "PUE"){?>
                                <option value="PUE" selected>PUE-pago en una sola exhibión</option>
                                <?php }else{?>
                                  <option value="PUE">PUE-pago en una sola exhibión</option>
                                <?php }?>
                                <?php if($factura->factura_medotoPago == "PPD"){?>
                                <option value="PPD" selected>PPD-pago en parcialidades o diferido</option>
                                <?php }else{?>
                                  <option value="PPD">PPD-pago en parcialidades o diferido</option>
                                <?php }?>
                              </select>
                           </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="receptor_RFC">Tipo comprovante:</label>
                            <select class="form-control-sm alto form-control" id="factura_tipoComprobante" name="factura_tipoComprobante" >

                              <?php if($factura->factura_tipoComprobante =="I"){?>
                              <option value="I" selected>I-Ingreso</option>
                              <?php }else{?>
                                <option value="I">I-Ingreso</option>
                              <?php }?>
                              <?php if($factura->factura_tipoComprobante =="E"){?>
                              <option value="E" selected>E-Egreso</option>
                              <?php }else{?>
                                <option value="E">E-Egreso</option>
                              <?php }?>
                              <?php if($factura->factura_tipoComprobante =="T"){?>
                              <option value="T" selected>T-Traslado</option>
                              <?php }else{?>
                                <option value="T">T-Traslado</option>
                              <?php }?>
                              <?php if($factura->factura_tipoComprobante =="N"){?>
                              <option value="N" selected>N-Nomina</option>
                              <?php }else{?>
                                <option value="N">N-Nomina</option>
                              <?php }?>
                              <?php if($factura->factura_tipoComprobante =="P"){?>
                              <option value="P" selected>P-Pago</option>
                              <?php }else{?>
                                <option value="P">P-Pago</option>
                              <?php }?>
                            </select>
                          </div>
                       </div>
                      </div>


                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="email">Dirección:</label>
                                  <input id="receptor_direccion" name="receptor_direccion" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_direccion;?>" placeholder="Razon social" autocomplete="cc-exp">

                              </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                <label for="receptor_uso_CFDI">Uso del CFDI:</label>
                                <select class="form-control-sm alto form-control" id="receptor_uso_CFDI" name="receptor_uso_CFDI" >
                                  <?php if($factura->receptor_uso_CFDI =="G01"){?>
                                  <option value="G01" selected>G01	Adquisición de mercancias</option>
                                  <?php }else{?>
                                    <option value="G01" selected>G01	Adquisición de mercancias</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="G02"){?>
                                  <option value="G02" selected>G02	Devoluciones, descuentos o bonificaciones</option>
                                  <?php }else{?>
                                    <option value="G02">G02	Devoluciones, descuentos o bonificaciones</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="G03"){?>
                                  <option value="G03" selected>G03	Gastos en general</option>
                                  <?php }else{?>
                                    <option value="G03">G03	Gastos en general</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I01"){?>
                                  <option value="I01" selected>I01	Construcciones</option>
                                  <?php }else{?>
                                    <option value="I01">I01	Construcciones</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I02"){?>
                                  <option value="I02" selected>I02	Mobilario y equipo de oficina por inversiones</option>
                                  <?php }else{?>
                                    <option value="I02">I02	Mobilario y equipo de oficina por inversiones</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I03"){?>
                                  <option value="I03" selected>I03	Equipo de transporte</option>
                                  <?php }else{?>
                                    <option value="I03">I03	Equipo de transporte</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I04"){?>
                                  <option value="I04" selected>I04	Equipo de computo y accesorios</option>
                                  <?php }else{?>
                                    <option value="I04">I04	Equipo de computo y accesorios</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I05"){?>
                                  <option value="I05" selected>I05	Dados, troqueles, moldes, matrices y herramental</option>
                                  <?php }else{?>
                                    <option value="I05">I05	Dados, troqueles, moldes, matrices y herramental</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I06"){?>
                                  <option value="I06" selected>I06	Comunicaciones telefónicas</option>
                                  <?php }else{?>
                                    option value="I06">I06	Comunicaciones telefónicas</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I07"){?>
                                  <option value="I07" selected>I07	Comunicaciones satelitales</option>
                                  <?php }else{?>
                                    <option value="I07">I07	Comunicaciones satelitales</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I08"){?>
                                  <option value="I08" selected>I08	Otra maquinaria y equipo</option>
                                  <?php }else{?>
                                    <option value="I08">I08	Otra maquinaria y equipo</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D01"){?>
                                  <option value="D01" selected>D01	Honorarios médicos, dentales y gastos hospitalarios.</option>
                                  <?php }else{?>
                                    <option value="D01">D01	Honorarios médicos, dentales y gastos hospitalarios.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D02"){?>
                                  <option value="D02" selected>D02	Gastos médicos por incapacidad o discapacidad</option>
                                  <?php }else{?>
                                    <option value="D02">D02	Gastos médicos por incapacidad o discapacidad</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D03"){?>
                                  <option value="D03" selected>D03	Gastos funerales.</option>
                                  <?php }else{?>
                                    <option value="D03">D03	Gastos funerales.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D04"){?>
                                  <option value="D04" selected>D04	Donativos.</option>
                                  <?php }else{?>
                                    <option value="D04">D04	Donativos.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D05"){?>
                                  <option value="D05" selected>D05	Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                  <?php }else{?>
                                    <option value="D05">D05	Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D06"){?>
                                  <option value="D06" selected>D06	Aportaciones voluntarias al SAR.</option>
                                  <?php }else{?>
                                    <option value="D06">D06	Aportaciones voluntarias al SAR.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D07"){?>
                                  <option value="D07" selected>D07	Primas por seguros de gastos médicos.</option>
                                  <?php }else{?>
                                    <option value="D07" >D07	Primas por seguros de gastos médicos.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D08"){?>
                                  <option value="D08" selected>D08	Gastos de transportación escolar obligatoria.</option>
                                  <?php }else{?>
                                    <option value="D08">D08	Gastos de transportación escolar obligatoria.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D09"){?>
                                  <option value="D09" selected>D09	Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                  <?php }else{?>
                                    <option value="D09">D09	Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D10"){?>
                                  <option value="D10" selected>D10	Pagos por servicios educativos (colegiaturas)</option>
                                  <?php }else{?>
                                    <option value="D10">D10	Pagos por servicios educativos (colegiaturas)</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="P01"){?>
                                  <option value="P01" selected>P01	Por definir</option>
                                  <?php }else{?>
                                    <option value="P01">P01	Por definir</option>
                                  <?php }?>



                                </select>



                              </div>
                          </div>


                          <div class="col-1">
                            <div class="form-group">
                              <label for="factura_moneda">Moneda:</label>
                              <input type="text" class="form-control-sm alto form-control" id="factura_moneda" name="factura_moneda" value="<?php echo $factura->factura_moneda;?>" >
                            </div>
                         </div>

                         <!--div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Almacen</label>
                                <select class="form-control cc-exp form-control-sm" id="almacen" >
                                  <?php foreach($vehiculos as $row_vehiculo):?>
                                    <?php if($row_vehiculo->alamacenId == $factura->almacen):?>
                                      <option value="<?php echo $row_vehiculo->alamacenId?>" selected><?php echo $row_vehiculo->almacenNombre?></option>
                                    <?php else:?>
                                      <option value="<?php echo $row_vehiculo->alamacenId?>"><?php echo $row_vehiculo->almacenNombre?></option>
                                    <?php endif;?>
                                  <?php endforeach;?>
                                </select>
                            </div>
                          </div-->

                         <!--div class="col-3">
                           <div class="form-group">
                             <label for="comentario">Comentario:</label>
                             <input id="comentario" name="comentario" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->comentario;?>" placeholder="Comentario" autocomplete="cc-exp">
                         </div>
                         </div-->


                      </div>



<hr/>
<div><label>Agregar relaciones de CFDI</label></div>
                      <div class="row p-3 mb-2 bg-light text-dark" >

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">Tipo relación:</label>
                          <select class="form-control-sm form-control" id="id_relacion">
                           <option value="01">01.-nota de credito de los documentos</opcion>
                           <option value="02">02.-nota de debito de los documtntos relacionados</opcion>
                           <option value="03">03.-devoclución de mercancía sobre facturas o traslados</opcion>
                           <option value="04">04.-sustituto de los CFDI previos</opcion>
                           <option value="05">05.-traslados de mercancías facturados previamente</opcion>
                           <option value="06">06.-factura generada por los traslados previos</opcion>
                           <option value="07">07.-CFDI por aplicación de anticipo</opcion>
                           <option value="08">08.-factura generada por pagos en parcialidades</opcion>
                           <option value="09">09.-factura generada por pagos diferidos</opcion>
                          </select>
                      </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">UUID:</label>
                          <input id="uuid" name="uuid" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="UUID" autocomplete="cc-exp">
                      </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">Opción:</label><br/>
                          <button type="button" id="agregar_relacion" class="btn btn-outline-success">Agregar relación</button>
                      </div>
                      </div>

                      </div>

                      <div class="row" id="relacionados">
                        <div class="col-12">
                        </div>
                      </div>






<hr/>
                      <div class="" align="right">
                        <!--a href="<?php echo base_url()?>index.php/Fatura/conceptos/<?php echo $factura->facturaID?>"-->
                          <button id="payment-button" type="submit" class="btn  btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar y Siguiente</span>

                          </button>
                        <!--/a-->
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>RFC</th>
        <th>Razon social</th>
        <!--th>Email</th-->
        <th>Nombre</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      <?php if(is_array($clientes)):?>
      <?php foreach($clientes as $row):?>
        <tr id="<?php echo $row->id;?>">
          <td><?php echo $row->rfc;?></td>
          <td><?php echo $row->razon_social;?></td>
          <!--td><?php echo $row->email;?></td-->
          <td><?php echo $row->nombre;?></td>

          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_cliente(<?php echo $row->id;?>)" data-dismiss="modal" id="<?php echo $row->id;?>" data-id="<?php echo $row->id;?>">seleccionar</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody>
  </table>
        </div>
      </div>

    </div>
  </div>
</div>