

<script src="<?php echo base_url()?>statics/js/isloading.js"></script>
<script type="text/javascript">
      menu_activo = "facturacion";
$("#menu_facturacion_facturar").last().addClass("menu_estilo");
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});



      $(".eliminar_factura").click(function(event){
           event.preventDefault();

            bootbox.dialog({
               message: "Desea cancelar la factura?",
               closeButton: true,
               buttons:
               {
                   "danger":
                   {
                       "label": "Aceptar ",
                       "className": "btn-danger",
                       "callback": function () {

                         id = $(event.currentTarget).attr('flag');
                         url = $("#delete"+id).attr('href');
                         //$.get(url);
                         var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Cancelando Factura...</p>',
                            closeButton: false
                        });
                         
                         ajaxJson(url,{},
                                   "POST",true,function(result){

                           console.log(result);

                           json_response = JSON.parse(result);

                           obj_status = json_response.error;
                           if(obj_status == true){
                            dialog_load.modal('hide');

                             exito("<h3>ERROR intente de nuevo<h3/> ","danger");
                           }
                           if(obj_status == false){
                            dialog_load.modal('hide');

                             exito_redirect("FACTURA CANCELADA CON EXITO","success","<?php echo base_url()?>index.php/factura/lista");
                           }
                         });
                       }
                   },
                   "cancel":
                   {
                       "label": "<i class='icon-remove'></i> Cancelar",
                       "className": "btn-sm btn-info",
                       "callback": function () {

                       }
                   }

               }
           });
       });

    } );
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}

button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 11px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<div class="card-header">
  <div class="">
      <a href="<?php echo base_url();?>index.php/factura/crear_pre"><button type="button" class="btn btn-primary">Crear factura</button></a>
  </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                   <!--label>Filtro:</label>
                   <?php 
                   $color_filtro_todo = "btn-light";
                   $color_filtro_prefactura = "btn-light";
                   if($filtro == 1){
                      $color_filtro_todo = "btn-secondary";
                      $color_filtro_prefactura = "btn-light";
                   }?>
                   <?php if($filtro == 0){
                      $color_filtro_todo = "btn-light";
                      $color_filtro_prefactura = "btn-secondary";
                   }?>
                   <a class=""   href="<?php echo base_url()?>index.php/factura/lista/1">
                      <button type="button" class="btn <?php echo $color_filtro_todo;?>">Todo</button>
                   </a>
                   <a class=""   href="<?php echo base_url()?>index.php/factura/lista/0">
                      <button type="button" class="btn <?php echo $color_filtro_prefactura;?>">Pre-Facturas</button>
                   </a>
                   <br/-->
                   <br/>
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>RFC</th>
                        <th>M.P.</th>
                        <th>Nombre</th>
                        <th>Total</th>
                        <!--th>Abono</th>
                        <th>Saldo</th-->
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($facturas)):?>
                      <?php foreach($facturas as $row):?>
                        <tr style="<?php if($row->factura_medotoPago == "PPD"){echo "background:#ecf8c7;";}else{if( ($row->sat_error == "0")){ echo "background:#c7f8d9;"; }if($row->cancelar_EstatusUUID == "201"){echo "background:#fab1a0;";} }?>">
                          <td><?php echo $row->factura_fecha;?></td>
                          <td><?php echo $row->receptor_RFC;?></td>
                          <td><?php echo $row->factura_medotoPago;?></td>
                          <td><?php echo $row->receptor_nombre;?></td>
                          <td>
                            $<?php echo $this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID);?>
                          </td>
                          <!--td>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <?php $saldo1 = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                echo "$".$saldo1;
                              ?>
                            <?php endif;?>
                          </td-->
                          <!--td>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <?php 

                                $saldo = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                echo "$".(($this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID))-$saldo);
                              ?>
                            <?php endif;?>
                          </td-->

                          <td>
                            <?php if(($row->prefactura == 1) && ($row->sat_error!="0")):?>
                            <a target="_blank" href="<?php echo base_url()?>index.php/factura/genera_pdf_prefactura/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-success">PDF</button>
                            </a>
                            <?php endif;?>
                            <?php if( ($row->sat_error == "0")):?>
                            <a href="<?php echo base_url()?>statics/facturas/facturas_xml/<?php echo $row->facturaID;?>.xml" download>
                            <button type="button" class="btn btn-success">XML</button>
                            </a>
                            <a target="_blank" href="<?php echo base_url()?>index.php/factura/genera_pdf/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-success">PDF</button>
                            </a>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                               <a class="" flag="<?php echo $row->facturaID;?>" id="delete<?php echo $row->facturaID;?>" href="#">
                                <button type="button" class="btn btn-info" disabled>C. Pago</button>
                            </a>
                            <?php endif;?>
                             <!--a class="" flag="<?php echo $row->facturaID;?>" id="clonar<?php echo $row->facturaID;?>" href="#">
                                <button type="button" class="btn btn-info" disabled>Clonar</button>
                            </a-->

                            <?php if($row->cancelar_EstatusUUID != "201"):?>
                            <a class="eliminar_factura" flag="<?php echo $row->facturaID;?>" id="delete<?php echo $row->facturaID;?>" href="<?php echo base_url()?>index.php/factura/cancelar_factura/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-danger">Cancelar</button>
                            </a>
                           <?php endif;?>

                           <?php if($row->cancelar_EstatusUUID == "201"):?>
                            <a href="<?php echo base_url()?>index.php/factura/acuse_cancelacion_xml/<?php echo $row->facturaID;?>" download>
                            <button type="button" class="btn btn-warning">XML Cancelación</button>
                            </a>
                           <?php endif;?>



                          <?php endif;?>
                            <?php if($row->sat_error != "0"):?>
                            <a href="<?php echo base_url()?>index.php/factura/nueva/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-info">Ver/Editar</button>
                            </a>
                          <?php endif;?>
                          </td>
                        </tr>
                     <?php endforeach;?>
                     <?php endif;?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
