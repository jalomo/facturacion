<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Factura extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','astillero'));

        date_default_timezone_set('America/Mexico_City');

        // if($this->session->userdata('id')){}else{redirect('login/');}

    }

    
    public function crear(){
      $titulo['titulo'] = "Crear Factura" ;
      $titulo['titulo_dos'] = "Crear Factura" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/crear', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array(/*'statics/js/bootbox.min.js',*/
                                           'statics/js/general.js',
                                           'statics/js/factura.js',
                                         /*'statics/bootstrap4/js/bootstrap.min.js'*/)));

    }

    public function buscarproductos(){
      $filtro    = $this->input->get("term");
		$productos = $this->Mgeneral->buscar_producto($filtro);
		echo json_encode($productos);
    }



    public function lista($filtro = 1){
      $titulo['titulo'] = "Lista de Facturas" ;
      $titulo['titulo_dos'] = "Lista de facturas" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      if($filtro == 1){
          $datos['facturas'] = $this->Mgeneral->get_table('factura');
      }else{
        $datos['facturas'] = $this->Mgeneral->get_result('prefactura',1,'factura');
      }
      $datos['filtro'] = $filtro;
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/lista', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           
                                           'statics/bootstrap4/js/bootstrap.min.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));

    }

    /*
    status = 1.-creada, 2.- en proceso,3.-facturada
    */
    public function crear_pre(){
      $emisor = $this->Mgeneral->get_row('id',1,'datos');
      $id_factura = get_guid();
      $data['facturaID'] = $id_factura;
      $data['status'] = 1;
      $data['emisor_RFC'] = $emisor->rfc;
      $data['emisor_regimenFiscal'] = $emisor->regimen;
      $data['emisor_nombre'] = $emisor->razon_social;
      $data['receptor_uso_CFDI'] = "G03";
      $data['pagada'] = "SI";
      $this->Mgeneral->save_register('factura', $data);
      redirect('factura/nueva/'.$id_factura);

    }

    public function buscar_cliente(){
      $filtro    = $this->input->get("term");
  		$clientes = $this->Mgeneral->buscar_cliente($filtro);
  		echo json_encode($clientes);
    }

    public function nueva($id_factura){
      $datos['emisor'] = $this->Mgeneral->get_row('id',1,'datos');
      $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $datos['clientes'] = $this->Mgeneral->get_result('status',0,'clientes');//$this->Mgeneral->get_table('clientes');
      $titulo['titulo'] = "Crear Factura" ;
      $titulo['titulo_dos'] = "Crear Factura" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/nueva', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           /*'statics/js/factura.js',*/
                                           'statics/bootstrap4/js/bootstrap.min.js')));

    }

    public function relaciones_facturas(){
      $id_factura = $this->input->post('id_factura');
      $uuid = $this->input->post('uuid');
      $tipo_relacion = $this->input->post('tipo_relacion');
      $data['relacionados_tipoRelacion'] = $tipo_relacion;
      $data['relacion_UUID'] = $uuid;
      $data['relacion_idFactura'] = $id_factura;
      $this->Mgeneral->save_register('factura_cfdi_relacionados', $data);

      $rows = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $html_uuid = "";
      foreach($rows as $row):
        $html_uuid .= '<tr id="borrar_'.$row->relacionados_id.'">
                      <th scope="row">'.$row->relacionados_tipoRelacion.'</th>
                      <td>'.$row->relacion_UUID.'</td>
                      <td><a href="'.base_url().'index.php/factura/eliminar_relacion_id/'.$row->relacionados_id.'" flag="'.$row->relacionados_id.'" id="delete'.$row->relacionados_id.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
      endforeach;

      echo '<table class="table">
          <thead>
            <tr>

              <th scope="col">Relación</th>
              <th scope="col">UUID</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';
    }

    public function ver_relaciones_facturas(){
      $id_factura = $this->input->post('id_factura');
      $rows = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $html_uuid = "";
      foreach($rows as $row):
        $html_uuid .= '<tr id="borrar_'.$row->relacionados_id.'">
                      <th scope="row">'.$row->relacionados_tipoRelacion.'</th>
                      <td>'.$row->relacion_UUID.'</td>
                      <td><a href="'.base_url().'index.php/factura/eliminar_relacion_id/'.$row->relacionados_id.'" flag="'.$row->relacionados_id.'" id="delete'.$row->relacionados_id.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
      endforeach;

      echo '<table class="table" style="background:#fff">
          <thead>
            <tr>

              <th scope="col">Relación</th>
              <th scope="col">UUID</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';
    }

    public function eliminar_relacion_id($id){
      $this->Mgeneral->delete_row('factura_cfdi_relacionados','relacionados_id',$id);
    }

    public function guarda_datos($id_factura){
      $this->form_validation->set_rules('receptor_nombre', 'receptor_nombre', 'required');
      $this->form_validation->set_rules('receptor_id_cliente', 'receptor_id_cliente', 'required');
      $this->form_validation->set_rules('factura_moneda', 'factura_moneda', 'required');
      $this->form_validation->set_rules('fatura_lugarExpedicion', 'fatura_lugarExpedicion', 'required');
      $this->form_validation->set_rules('factura_fecha', 'factura_fecha', 'required');
      $this->form_validation->set_rules('receptor_email', 'receptor_email', 'required');
      $this->form_validation->set_rules('factura_folio', 'factura_folio', 'required');
      $this->form_validation->set_rules('factura_serie', 'factura_serie', 'required');
      $this->form_validation->set_rules('receptor_direccion', 'receptor_direccion', 'required');
      $this->form_validation->set_rules('factura_formaPago', 'factura_formaPago', 'required');
      $this->form_validation->set_rules('factura_medotoPago', 'factura_medotoPago', 'required');
      $this->form_validation->set_rules('factura_tipoComprobante', 'factura_tipoComprobante', 'required');
      $this->form_validation->set_rules('receptor_RFC', 'receptor_RFC', 'required');
      $this->form_validation->set_rules('receptor_uso_CFDI', 'receptor_uso_CFDI', 'required');
      $this->form_validation->set_rules('pagada', 'pagada', 'required');
      //$this->form_validation->set_rules('comentario', 'comentario', 'required');
      $response = validate($this);

      if($response['status']){

        $data['receptor_nombre'] = $this->input->post('receptor_nombre');
        $data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
        $data['factura_moneda'] = $this->input->post('factura_moneda');
        $data['fatura_lugarExpedicion'] = $this->input->post('fatura_lugarExpedicion');
        $data['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
        $data['receptor_email'] = $this->input->post('receptor_email');
        $data['factura_folio'] = $this->input->post('factura_folio');
        $data['factura_serie'] = $this->input->post('factura_serie');
        $data['receptor_direccion'] = $this->input->post('receptor_direccion');
        $data['factura_formaPago'] = $this->input->post('factura_formaPago');
        $data['factura_medotoPago'] = $this->input->post('factura_medotoPago');
        $data['factura_tipoComprobante'] = $this->input->post('factura_tipoComprobante');
        $data['receptor_RFC'] = $this->input->post('receptor_RFC');
        $data['receptor_uso_CFDI'] = $this->input->post('receptor_uso_CFDI');
        $data['pagada'] = $this->input->post('pagada');
        $data['comentario'] = $this->input->post('comentario');

        $this->Mgeneral->update_table_row('factura',$data,'facturaID',$id_factura);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));

    }


    public function guarda_datos_fac($id_factura){
      $this->form_validation->set_rules('receptor_nombre', 'receptor_nombre');
      $this->form_validation->set_rules('receptor_id_cliente', 'receptor_id_cliente');
      $this->form_validation->set_rules('factura_moneda', 'factura_moneda');
      $this->form_validation->set_rules('fatura_lugarExpedicion', 'fatura_lugarExpedicion');
      $this->form_validation->set_rules('factura_fecha', 'factura_fecha');
      $this->form_validation->set_rules('receptor_email', 'receptor_email');
      $this->form_validation->set_rules('factura_folio', 'factura_folio');
      $this->form_validation->set_rules('factura_serie', 'factura_serie');
      $this->form_validation->set_rules('receptor_direccion', 'receptor_direccion');
      $this->form_validation->set_rules('factura_formaPago', 'factura_formaPago');
      $this->form_validation->set_rules('factura_medotoPago', 'factura_medotoPago');
      $this->form_validation->set_rules('factura_tipoComprobante', 'factura_tipoComprobante');
      $this->form_validation->set_rules('receptor_RFC', 'receptor_RFC');
      $this->form_validation->set_rules('receptor_uso_CFDI', 'receptor_uso_CFDI');
      $this->form_validation->set_rules('pagada', 'pagada');
      $this->form_validation->set_rules('comentario', 'comentario');

      $response = validate($this);
      $response['status'] = true;

      if($response['status']){

        $data['receptor_nombre'] = $this->input->post('receptor_nombre');
        $data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
        $data['factura_moneda'] = $this->input->post('factura_moneda');
        $data['fatura_lugarExpedicion'] = $this->input->post('fatura_lugarExpedicion');
        $data['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
        $data['receptor_email'] = $this->input->post('receptor_email');
        $data['factura_folio'] = $this->input->post('factura_folio');
        $data['factura_serie'] = $this->input->post('factura_serie');
        $data['receptor_direccion'] = $this->input->post('receptor_direccion');
        $data['factura_formaPago'] = $this->input->post('factura_formaPago');
        $data['factura_medotoPago'] = $this->input->post('factura_medotoPago');
        $data['factura_tipoComprobante'] = $this->input->post('factura_tipoComprobante');
        $data['receptor_RFC'] = $this->input->post('receptor_RFC');
        $data['receptor_uso_CFDI'] = $this->input->post('receptor_uso_CFDI');
        $data['pagada'] = $this->input->post('pagada');
        $data['comentario'] = $this->input->post('comentario');

        $this->Mgeneral->update_table_row('factura',$data,'facturaID',$id_factura);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));

    }


    public function conceptos($id_factura){
      $datos['emisor'] = $this->Mgeneral->get_row('id',1,'datos');
      $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datos['medidas'] = $this->Mgeneral->get_result('status',0,'medidas');//$this->Mgeneral->get_table('medidas');
      $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $datos['productos'] = $this->Mgeneral->get_result('status',0,'productos');//$this->Mgeneral->get_table('productos');
      $datos['facturaID'] = $id_factura;
      $titulo['titulo'] = "Factura" ;
      $titulo['titulo_dos'] = "prefactura" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/conceptos', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           /*'statics/js/factura.js',*/
                                           'statics/bootstrap4/js/bootstrap.min.js')));

    }

    public function buscar_producto(){
      $filtro    = $this->input->get("term");
  		$clientes = $this->Mgeneral->buscar_producto($filtro);
  		echo json_encode($clientes);
    }


    public function get_dados_producto($id_producto){
    //  $id_cliente = $this->input->post('id_cliente');
      $res = $this->Mgeneral->get_row('productoId',$id_producto,'productos');
      echo json_encode($res);
    }


    public function guarda_datos_concepto($id_factura){
      //$this->form_validation->set_rules('receptor_nombre', 'receptor_nombre');
      //$response = validate($this);
      $response['status'] = true;

      if($response['status']){

        $data['concepto_facturaId'] = $id_factura;
        $data['concepto_NoIdentificacion'] = $this->input->post('concepto_NoIdentificacion');
        $data['concepto_unidad'] = $this->input->post('concepto_unidad');
        //$data['concepto_descuento'] = $this->input->post('concepto_descuento');
        $data['clave_sat'] = $this->input->post('clave_sat');
        $data['unidad_sat'] = $this->input->post('unidad_sat');
        $data['concepto_nombre'] = $this->input->post('concepto_nombre');
        $data['concepto_precio'] = $this->input->post('concepto_precio');
        $data['concepto_importe'] = $this->input->post('concepto_importe');
        $data['impuesto_iva'] = $this->input->post('impuesto_iva');
        $data['impuesto_iva_tipoFactor'] = $this->input->post('impuesto_iva_tipoFactor');
        $data['impuesto_iva_tasaCuota'] = $this->input->post('impuesto_iva_tasaCuota');
        $data['impuesto_ISR'] = $this->input->post('impuesto_ISR');
        $data['impuesto_ISR_tasaFactor'] = $this->input->post('impuesto_ISR_tasaFactor');
        $data['impuestoISR_tasaCuota'] = $this->input->post('impuestoISR_tasaCuota');
        $data['tipo'] = $this->input->post('tipo');
        $data['nombre_interno'] = $this->input->post('nombre_interno');
        $data['id_producto_servicio_interno'] = $this->input->post('id_producto_servicio_interno');
        $data['concepto_cantidad'] = $this->input->post('concepto_cantidad');
        $data['importe_iva'] = $this->input->post('importe_iva');
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        $this->Mgeneral->save_register('factura_conceptos',$data);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));

    }

    public function eliminar_concepto($id_concepto,$id_factura){
      $this->Mgeneral->delete_row('factura_conceptos','concepto_id',$id_concepto);
      redirect("factura/conceptos/".$id_factura);

    }

    public function ver_factura($id_factura){

      $datos['emisor'] = $this->Mgeneral->get_row('id',1,'datos');
      $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
      $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $datos['facturaID'] = $id_factura;
      $titulo['titulo'] = "Factura" ;
      $titulo['titulo_dos'] = "prefactura" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      //$menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('factura/ver', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>'',
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           /*'statics/js/factura.js',*/
                                           'statics/bootstrap4/js/bootstrap.min.js')));

    }



    public function generar_factura($id_factura){
      $factura_nueva = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      //error_reporting(1);
      //ini_set('display_errors', 1);
      require $this->config->item('url_real').'cfdi/xml/lib/autoload.php';//'C:\xampp\htdocs\elastillero\cfdi\xml\lib\autoload.php';
      $cfdi = new Comprobante();
      // Preparar valores
      $moneda = 'MXN';
      $subtotal  = $this->Mgeneral->factura_subtotal($id_factura);//190.00;
      $iva       = $this->Mgeneral->factura_iva_total($id_factura);//30.40;
      //$descuento =   1.40;
      $total     = $this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura);
      $fecha     = time();
      // Establecer valores generales
      $cfdi->LugarExpedicion   = $factura_nueva->fatura_lugarExpedicion;//'12345';
      $cfdi->FormaPago         = $factura_nueva->factura_formaPago;//'27';
      $cfdi->MetodoPago        = $factura_nueva->factura_medotoPago;//'PUE';
      $cfdi->Folio             = $factura_nueva->factura_folio;//'24';
      $cfdi->Serie             = $factura_nueva->factura_serie;//'A';
      $cfdi->TipoDeComprobante = $factura_nueva->factura_tipoComprobante;//'I';
      $cfdi->TipoCambio        = 1;
      $cfdi->Moneda            = $moneda;
      $cfdi->setSubTotal($subtotal);
      $cfdi->setTotal($total);
      //$cfdi->setDescuento($descuento);
      $cfdi->setFecha($fecha);

      // Agregar emisor
      $cfdi->Emisor = Emisor::init(
          $factura_nueva->emisor_RFC,//'LAN7008173R5',                    // RFC
          $factura_nueva->emisor_regimenFiscal,//'622',                             // Régimen Fiscal
          $factura_nueva->emisor_nombre//'Emisor Ejemplo'                   // Nombre (opcional)
      );

      // Agregar receptor
      $cfdi->Receptor = Receptor::init(
          $factura_nueva->receptor_RFC,//'TCM970625MB1',                   // RFC
          $factura_nueva->receptor_uso_CFDI,//'I08',                             // Uso del CFDI
          $factura_nueva->receptor_nombre//'Receptor Ejemplo'                 // Nombre (opcional)
      );

      $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      foreach($conceptos_factura as $concepto_fac):
        // Preparar datos del concepto 1
        $concepto = Concepto::init(
            $concepto_fac->clave_sat,//'52141807',                        // clave producto SAT
            $concepto_fac->concepto_cantidad,//'2',                               // cantidad
            $concepto_fac->unidad_sat,//'P83',                             // clave unidad SAT
            $concepto_fac->concepto_nombre,//'Nombre del producto de ejemplo',
            $concepto_fac->concepto_precio,//95.00,                             // precio
            $concepto_fac->concepto_importe//190.00                             // importe
        );
        $concepto->NoIdentificacion = $concepto_fac->id_producto_servicio_interno;//'PR01'; // clave de producto interna
        $concepto->Unidad = $concepto_fac->unidad_sat;//'Servicio';       // unidad de medida interna
        //$concepto->Descuento = 0.0;

        // Agregar impuesto (traslado) al concepto 1
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Tasa';
        $traslado->TasaOCuota = 0.16;
        $traslado->Base = $concepto_fac->concepto_importe;//$subtotal;
        $traslado->Importe = $concepto_fac->importe_iva;//$iva;
        $concepto->agregarImpuesto($traslado);

        // Agregar concepto 1 a CFDI
        $cfdi->agregarConcepto($concepto);

        // Agregar más conceptos al CFDI
        // $concepto = Concepto::init(...);
        // ...
        // $cfdi->agregarConcepto($concepto);
     endforeach;


      // Mostrar XML del CFDI generado hasta el momento
      // header('Content-type: application/xml; charset=UTF-8');
      // echo $cfdi->obtenerXml();
      // die;

      // Cargar certificado que se utilizará para generar el sello del CFDI
      $cert = new UtilCertificado();

      // Si no se especifica la ruta manualmente, se intentará obtener automatícamente
      // UtilCertificado::establecerRutaOpenSSL();

      $ok = $cert->loadFiles(
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
          ''.$this->config->item('url_real').'cfdi/EKU9003173C9.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
          ''.$this->config->item('url_real').'cfdi/EKU9003173C9.key',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
          '12345678a'
      );
      if(!$ok) {
          die('Ha ocurrido un error al cargar el certificado.');
      }

      $ok = $cfdi->sellar($cert);
      if(!$ok) {
          die('Ha ocurrido un error al sellar el CFDI.');
      }

      // Mostrar XML del CFDI con el sello
      //header('Content-type: application/xml; charset=UTF-8');
      //header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');

      //echo $cfdi->obtenerXml();
      if($cfdi->obtenerXml()){
            //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
             file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
            //file_put_contents("https://planificadorempresarial.com/facturacion/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
            $data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
            $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
            $this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);
            $this->enviar_factura($id_factura);
           // echo $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";
      }else{
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = "Ocurrio un error";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }
      //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
      //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
      //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

      //$this->enviar_factura($id_factura);

      die;

      // Mostrar objeto que contiene los datos del CFDI
      print_r($cfdi);


      die;



      die('OK');
    }


    public function enviar_factura($id_factura){
      //echo "".base_url()."index.php/factura/generar_factura/".$id_factura;
      //echo  $datos_fac->url_prefactura;
      $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');

      # Username and Password, assigned by FINKOK
      $username = 'liberiusg@gmail.com';
      $password = '*Libros7893811';

      # Read the xml file and encode it on base64
      $invoice_path = $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";//"C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml";//$datos_fac->url_prefactura;//"".base_url()."index.php/factura/generar_factura";
      $xml_file = fopen($invoice_path, "rb");
      $xml_content = fread($xml_file, filesize($invoice_path));
      fclose($xml_file);

      # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
      #$xml_content = base64_encode($xml_content);

      # Consuming the stamp service
      $url = "https://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
      $client = new SoapClient($url);

      $params = array(
        "xml" => $xml_content,
        "username" => $username,
        "password" => $password
      );
      $response = $client->__soapCall("stamp", array($params));
      //print_r($response);
      ####mostrar el XML timbrado solamente, este se mostrara solo si el XML ha sido timbrado o recibido satisfactoriamente.
      //print $response->stampResult->xml;

      ####mostrar el código de error en caso de presentar alguna incidencia
      #print $response->stampResult->Incidencias->Incidencia->CodigoError;
      ####mostrar el mensaje de incidencia en caso de presentar alguna
      if(isset($response->stampResult->Incidencias->Incidencia->MensajeIncidencia)){
        $data_sat['sat_error'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $data_sat['sat_codigo_error'] = $response->stampResult->Incidencias->Incidencia->CodigoError;
        //echo $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }else{
        $data_sat['sat_uuid'] = $response->stampResult->UUID;
        $data_sat['sat_fecha'] = $response->stampResult->Fecha;
        $data_sat['sat_codestatus'] = $response->stampResult->CodEstatus;
        $data_sat['sat_satseal'] = $response->stampResult->SatSeal;
        $data_sat['sat_nocertificadosat'] = $response->stampResult->NoCertificadoSAT;
        $data_sat['sat_error'] = "0";
        $data_sat['sat_codigo_error'] = "0";
        $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
        file_put_contents($this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml", $response->stampResult->xml);
        $data_respone['error'] = 0;
        $data_respone['error_mensaje'] = "Factura timbrada";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }
      #print $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
    }

    public function genera_pdf($id_factura){
      $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');

      //$datos_cliente = $this->Mgeneral->get_row('id',$datos_fac->receptor_id_cliente,'clientes');

      $datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
      $logo = $this->Mgeneral->get_row('id',1,'informacion')->logo;
      require $this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
      $pdfTemplateDir = $this->config->item('url_real').'cfdi/pdf/lib/templates/';

      $pdf = new Cfdi2Pdf($pdfTemplateDir);

      // datos del archivo. No se mostrarán en el PDF (requeridos)
      $pdf->autor  = 'www.fordmylsaqueretaro.mx/';
      $pdf->titulo = 'Factura';
      $pdf->asunto = 'CFDI';

      // texto a mostrar en la parte superior (requerido)
      $pdf->encabezado = 'Mylsa Querétaro';//$datas_empresa->nombre;

      // nombre del archivo PDF (opcional)
      $pdf->nombreArchivo = 'pdf-cfdi.pdf';

      // mensaje a mostrar en el pie de pagina (opcional)
      $pdf->piePagina = 'Factura';

      // texto libre a mostrar al final del documento (opcional)
      $pdf->mensajeFactura = "";//$datos_cliente->comentario_extra;

      // Solo compatible con CFDI 3.3 (opcional)
      $pdf->direccionExpedicion = 'Av. Constituyentes No. 42 Ote, Villas del Sol Querétaro';//$datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;

      // ruta del logotipo (opcional)
      $pdf->logo = $this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';

      // mensaje a mostrar encima del documento (opcional)
      // $pdf->mensajeSello = 'CANCELADO';

      // Cargar el XML desde un string...
      // $ok = $pdf->cargarCadenaXml($cadenaXml);

      // Cargar el XML desde un archivo...
      // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
      // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
      // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
      $archivoXml = $this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';

      $ok = $pdf->cargarArchivoXml($archivoXml);

      if($ok) {
          // Generar PDF para mostrar en el explorador o descargar
          $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

          // Guardar PDF en la ruta especificada
          // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
          // $ok = $pdf->guardarPdf($ruta);

          // Obtener PDF como string
          // $pdfStr = $pdf->obtenerPdf();

          if($ok) {
              // PDF generado correctamente.
          } else {
              echo 'Error al generar PDF.';
          }
      }else{
          echo 'Error al cargar archivo XML.';
      }

    }

    public function cancelar_factura($id_factura){
      $factura_datos = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      # Generar el certificado y llave en formato .pem
        shell_exec("openssl x509 -inform DER -outform PEM -in ".$this->config->item('url_real')."cfdi/xml/ejemplos/LAN7008173R5.cer -pubkey -out ".$this->config->item('url_real')."cfdi/LAN7008173R5.cer.pem");
        shell_exec("openssl pkcs8 -inform DER -in ".$this->config->item('url_real')."cfdi/xml/ejemplos/LAN7008173R5.key -passin pass:12345678a -out ".$this->config->item('url_real')."cfdi/LAN7008173R5.key.pem");
        shell_exec("openssl rsa -in ".$this->config->item('url_real')."cfdi/LAN7008173R5.key.pem -des3 -out ".$this->config->item('url_real')."cfdi/LAN7008173R5.enc -passout pass:*Libros7893811");

        # Username and Password, assigned by FINKOK
        $username = 'liberiusg@gmail.com';
        $password = '*Libros7893811';
        # Consuming the cancel service
        # Read the x509 certificate file on PEM format and encode it on base64
        $cer_path = $this->config->item('url_real').'cfdi/LAN7008173R5.cer.pem';
        $cer_file = fopen($cer_path, "r");
        $cer_content = fread($cer_file, filesize($cer_path));
        fclose($cer_file);
        # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
        #$cer_content = base64_encode($cer_content);

        # Read the Encrypted Private Key (des3) file on PEM format and encode it on base64
        $key_path = $this->config->item('url_real')."cfdi/LAN7008173R5.enc";
        $key_file = fopen($key_path, "r");
        $key_content = fread($key_file,filesize($key_path));
        fclose($key_file);
        # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
        #$key_content = base64_encode($key_content);

        $taxpayer_id = 'LAN7008173R5'; # The RFC of the Emisor
        $invoices = array($factura_datos->sat_uuid); # A list of UUIDs

        $url = "https://demo-facturacion.finkok.com/servicios/soap/cancel.wsdl";
        $client = new SoapClient($url);
        $params = array(
          "UUIDS" => array('uuids' => $invoices),
          "username" => $username,
          "password" => $password,
          "taxpayer_id" => $taxpayer_id,
          "cer" => $cer_content,
          "key" => $key_content,
          "get_sat_status" => false
        );
        $response = $client->__soapCall("cancel", array($params));
        //print_r($response);
        //echo $response->cancelResult->CodEstatus ;
        //die;
        
      /*  echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Fecha;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Folios->Folio->UUID;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Folios->Folio->EstatusUUID;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->Folios->Folio->EstatusCancelacion;
        echo "<br/>";
        echo "<br/>";
        echo $response->cancelResult->RfcEmisor;
        */

        if(is_object($response)){
          if($response->cancelResult->Folios->Folio->EstatusUUID == "201"){
            $data_cancelar['cancelar_Fecha'] = $response->cancelResult->Fecha;
            $data_cancelar['cancelar_UUID'] = $response->cancelResult->Folios->Folio->UUID;
            $data_cancelar['cancelar_EstatusUUID'] = $response->cancelResult->Folios->Folio->EstatusUUID;
            $data_cancelar['cancelar_EstatusCancelacion'] = $response->cancelResult->Folios->Folio->EstatusCancelacion;
            $data_cancelar['cancelar_RfcEmisor'] = $response->cancelResult->RfcEmisor;
            $data_cancelar['acuse_cancelacion'] = $response->cancelResult->Acuse;
            $this->Mgeneral->update_table_row('factura',$data_cancelar,'facturaID',$id_factura);
            //redirect("factura/lista");
            $data_cancelar['error'] = false;
            echo json_encode($data_cancelar);
          }else{
            $data_cancelar['error'] = true;
            $data_cancelar['mensaje'] = "error1";
            echo json_encode($data_cancelar);
          }

        }else{
           $data_cancelar['error'] = true;
           $data_cancelar['mensaje'] = "error2";
            echo json_encode($data_cancelar);
        }


    }

    public function acuse_cancelacion_xml($id_factura){ 
      $factura_datos = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      header('Content-type: application/xml; charset=UTF-8');
      header('Content-Disposition: attachment; filename="xml/'.$id_factura.'.xml');
      if(is_object($factura_datos)){
        print_r($factura_datos->acuse_cancelacion);
      }else{
        echo "Error al generar xml";
      }
      
    }



    public function generar_prefactura($id_factura){
      $factura_nueva = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      //error_reporting(1);
      //ini_set('display_errors', 1);
      require $this->config->item('url_real').'cfdi41/xml/lib/autoload.php';//'C:\xampp\htdocs\elastillero\cfdi\xml\lib\autoload.php';
      $cfdi = new Comprobante();
      // Preparar valores
      $moneda = 'MXN';
      $subtotal  = $this->Mgeneral->factura_subtotal($id_factura);//190.00;
      $iva       = $this->Mgeneral->factura_iva_total($id_factura);//30.40;
      //$descuento =   1.40;
      $total     = $this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura);
      $fecha     = time();
      // Establecer valores generales
      $cfdi->LugarExpedicion   = $factura_nueva->fatura_lugarExpedicion;//'12345';
      $cfdi->FormaPago         = $factura_nueva->factura_formaPago;//'27';
      $cfdi->MetodoPago        = $factura_nueva->factura_medotoPago;//'PUE';
      $cfdi->Folio             = $factura_nueva->factura_folio;//'24';
      $cfdi->Serie             = $factura_nueva->factura_serie;//'A';
      $cfdi->TipoDeComprobante = $factura_nueva->factura_tipoComprobante;//'I';
      $cfdi->TipoCambio        = 1;
      $cfdi->Moneda            = $moneda;
      $cfdi->setSubTotal($subtotal);
      $cfdi->setTotal($total);
      //$cfdi->setDescuento($descuento);
      $cfdi->setFecha($fecha);

      // Agregar emisor
      $cfdi->Emisor = Emisor::init(
          $factura_nueva->emisor_RFC,//'LAN7008173R5',                    // RFC
          $factura_nueva->emisor_regimenFiscal,//'622',                             // Régimen Fiscal
          $factura_nueva->emisor_nombre//'Emisor Ejemplo'                   // Nombre (opcional)
      );

      // Agregar receptor
      $cfdi->Receptor = Receptor::init(
          $factura_nueva->receptor_RFC,//'TCM970625MB1',                   // RFC
          $factura_nueva->receptor_uso_CFDI,//'I08',                             // Uso del CFDI
          $factura_nueva->receptor_nombre,//'Receptor Ejemplo'                 // Nombre (opcional)
          '28983',//CP del receptor
          '601'//regimen fisal del receptor
      );

      $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      foreach($conceptos_factura as $concepto_fac):
        // Preparar datos del concepto 1
        $concepto = Concepto::init(
            $concepto_fac->clave_sat,//'52141807',                        // clave producto SAT
            $concepto_fac->concepto_cantidad,//'2',                               // cantidad
            $concepto_fac->unidad_sat,//'P83',                             // clave unidad SAT
            $concepto_fac->concepto_nombre,//'Nombre del producto de ejemplo',
            $concepto_fac->concepto_precio,//95.00,                             // precio
            $concepto_fac->concepto_importe//190.00                             // importe
        );
        $concepto->NoIdentificacion = $concepto_fac->id_producto_servicio_interno;//'PR01'; // clave de producto interna
        $concepto->Unidad = $concepto_fac->unidad_sat;//'Servicio';       // unidad de medida interna
        //$concepto->Descuento = 0.0;

        // Agregar impuesto (traslado) al concepto 1
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Tasa';
        $traslado->TasaOCuota = 0.16;
        $traslado->Base = $subtotal;
        $traslado->Importe = $iva;
        $concepto->agregarImpuesto($traslado);

        // Agregar concepto 1 a CFDI
        $cfdi->agregarConcepto($concepto);

        // Agregar más conceptos al CFDI
        // $concepto = Concepto::init(...);
        // ...
        // $cfdi->agregarConcepto($concepto);
     endforeach;


      // Mostrar XML del CFDI generado hasta el momento
      // header('Content-type: application/xml; charset=UTF-8');
       //echo $cfdi->obtenerXml();
       echo $this->config->item('url_real').'cfdi41/archs_pem/EKU9003173C9.key.pem';
       die;

      // Cargar certificado que se utilizará para generar el sello del CFDI
      $cert = new UtilCertificado();

      // Si no se especifica la ruta manualmente, se intentará obtener automatícamente
      // UtilCertificado::establecerRutaOpenSSL();

      $ok = $cert->loadFiles(
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
          //''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
          //''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.key',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
          ''.$this->config->item('url_real').'cfdi41/archs_pem/EKU9003173C9.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
          ''.$this->config->item('url_real').'cfdi41/archs_pem/EKU9003173C9.key.pem',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
          '12345678a'
      );
      if(!$ok) {
          die('Ha ocurrido un error al cargar el certificado.');
      }

      $ok = $cfdi->sellar($cert);
      if(!$ok) {
          die('Ha ocurrido un error al sellar el CFDI.');
      }

      // Mostrar XML del CFDI con el sello
      //header('Content-type: application/xml; charset=UTF-8');
      //header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');

      //echo $cfdi->obtenerXml();
      if($cfdi->obtenerXml()){
            //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
            file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
            $data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
            $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
            $this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

           $data_sat['prefactura'] = 1;
           $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
           $data_respone['error'] = 0;

           $data_respone['pdf'] = base_url()."index.php/factura/genera_pdf_prefactura/".$id_factura."";
           $data_respone['xml'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
           $data_respone['error_mensaje'] = "Factura timbrada";
          echo json_encode($data_respone);
          die;
            
      }else{
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = "Ocurrio un error";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }
      //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
      //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
      //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

      //$this->enviar_factura($id_factura);

      die;

      // Mostrar objeto que contiene los datos del CFDI
      print_r($cfdi);


      die;



      die('OK');
    }

    public function genera_pdf_prefactura($id_factura){
      $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      $datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
      $logo = $this->Mgeneral->get_row('id',1,'informacion')->logo;
      require $this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
      $pdfTemplateDir = $this->config->item('url_real').'cfdi/pdf/lib/templates/';

      $pdf = new Cfdi2Pdf($pdfTemplateDir);

      // datos del archivo. No se mostrarán en el PDF (requeridos)
      $pdf->autor  = 'www.fumigacioneselastillero.com';
      $pdf->titulo = 'Factura';
      $pdf->asunto = 'CFDI';

      // texto a mostrar en la parte superior (requerido)
      $pdf->encabezado = "Ford Mylsa Querétaro";//$datas_empresa->nombre;

      // nombre del archivo PDF (opcional)
      $pdf->nombreArchivo = 'pdf-cfdi.pdf';

      // mensaje a mostrar en el pie de pagina (opcional)
      $pdf->piePagina = 'Factura';

      // texto libre a mostrar al final del documento (opcional)
      $pdf->mensajeFactura = '';

      // Solo compatible con CFDI 3.3 (opcional)
      $pdf->direccionExpedicion = 'Av Constituyentes 42, Villas del Sol, 76046 Santiago de Querétaro, Qro.';//$datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;

      // ruta del logotipo (opcional)
      $pdf->logo = $this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';

      // mensaje a mostrar encima del documento (opcional)
      // $pdf->mensajeSello = 'CANCELADO';

      // Cargar el XML desde un string...
      // $ok = $pdf->cargarCadenaXml($cadenaXml);

      // Cargar el XML desde un archivo...
      // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
      // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
      // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
      $archivoXml = $this->config->item('url_real')."/statics/facturas/prefactura/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';

      $ok = $pdf->cargarArchivoXml($archivoXml);

      if($ok) {
          // Generar PDF para mostrar en el explorador o descargar
          $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

          // Guardar PDF en la ruta especificada
          // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
          // $ok = $pdf->guardarPdf($ruta);

          // Obtener PDF como string
          // $pdfStr = $pdf->obtenerPdf();

          if($ok) {
              // PDF generado correctamente.
          } else {
              echo 'Error al generar PDF.';
          }
      }else{
          echo 'Error al cargar archivo XML.';
      }

    }

    public function leer_xml($factura_xml){
        $xml = simplexml_load_file(base_url()."statics/facturas/facturas_xml/".$factura_xml.".xml"); 
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('t', $ns['tfd']);
         
         
        //EMPIEZO A LEER LA INFORMACION DEL CFDI E IMPRIMIRLA 
        foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante){ 
              echo $cfdiComprobante['Version']; 
              echo "<br />"; 
              echo $cfdiComprobante['Fecha']; 
              echo "<br />"; 
              echo $cfdiComprobante['Sello']; 
              echo "<br />"; 
              echo $cfdiComprobante['Total']; 
              echo "<br />"; 
              echo $cfdiComprobante['SubTotal']; 
              echo "<br />"; 
              echo $cfdiComprobante['Certificado']; 
              echo "<br />"; 
              echo $cfdiComprobante['FormaDePago']; 
              echo "<br />"; 
              echo $cfdiComprobante['NoCertificado']; 
              echo "<br />"; 
              echo $cfdiComprobante['TipoDeComprobante']; 
              echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){ 
           echo $Emisor['rfc']; 
           echo "<br />"; 
           echo $Emisor['Nombre']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal') as $DomicilioFiscal){ 
           echo $DomicilioFiscal['Pais']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Calle']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Estado']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Colonia']; 
           echo "<br />"; 
           echo $DomicilioFiscal['Municipio']; 
           echo "<br />"; 
           echo $DomicilioFiscal['NoExterior']; 
           echo "<br />"; 
           echo $DomicilioFiscal['CodigoPostal']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:ExpedidoEn') as $ExpedidoEn){ 
           echo $ExpedidoEn['Pais']; 
           echo "<br />"; 
           echo $ExpedidoEn['Calle']; 
           echo "<br />"; 
           echo $ExpedidoEn['Estado']; 
           echo "<br />"; 
           echo $ExpedidoEn['Colonia']; 
           echo "<br />"; 
           echo $ExpedidoEn['NoExterior']; 
           echo "<br />"; 
           echo $ExpedidoEn['CodigoPostal']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor){ 
           echo $Receptor['rfc']; 
           echo "<br />"; 
           echo $Receptor['nombre']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio') as $ReceptorDomicilio){ 
           echo $ReceptorDomicilio['Pais']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Calle']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Estado']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Colonia']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['Municipio']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['NoExterior']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['NoInterior']; 
           echo "<br />"; 
           echo $ReceptorDomicilio['CodigoPostal']; 
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto){ 
           echo "<br />"; 
           echo $Concepto['Unidad']; 
           echo "<br />"; 
           echo $Concepto['Importe']; 
           echo "<br />"; 
           echo $Concepto['Cantidad']; 
           echo "<br />"; 
           echo $Concepto['Descripcion']; 
           echo "<br />"; 
           echo $Concepto['ValorUnitario']; 
           echo "<br />";   
           echo "<br />"; 
        } 
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado') as $Traslado){ 
           echo $Traslado['Tasa']; 
           echo "<br />"; 
           echo $Traslado['Importe']; 
           echo "<br />"; 
           echo $Traslado['Impuesto']; 
           echo "<br />";   
           echo "<br />"; 
        } 
         
        //ESTA ULTIMA PARTE ES LA QUE GENERABA EL ERROR
        foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
           echo $tfd['SelloCFD']; 
           echo "<br />"; 
           echo $tfd['FechaTimbrado']; 
           echo "<br />"; 
           echo $tfd['UUID']; 
           echo "<br />"; 
           echo $tfd['NoCertificadoSAT']; 
           echo "<br />"; 
           echo $tfd['Version']; 
           echo "<br />"; 
           echo $tfd['SelloSAT']; 
        } 
    }


    public function genera_pdf_prueba(){
      //$datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');

      //$datos_cliente = $this->Mgeneral->get_row('id',$datos_fac->receptor_id_cliente,'clientes');

      //$datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
      $logo = $this->Mgeneral->get_row('id',1,'informacion')->logo;
      require $this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
      $pdfTemplateDir = $this->config->item('url_real').'cfdi/pdf/lib/templates/';

      $pdf = new Cfdi2Pdf($pdfTemplateDir);

      // datos del archivo. No se mostrarán en el PDF (requeridos)
      $pdf->autor  = 'https://sohex.mx';
      $pdf->titulo = 'Factura';
      $pdf->asunto = 'CFDI';

      // texto a mostrar en la parte superior (requerido)
      $pdf->encabezado = 'SISTEMAS OPERATIVOS HEXADECIMAL SA DE CV';//$datas_empresa->nombre;

      // nombre del archivo PDF (opcional)
      $pdf->nombreArchivo = 'pdf-cfdi.pdf';

      // mensaje a mostrar en el pie de pagina (opcional)
      $pdf->piePagina = 'Factura';

      // texto libre a mostrar al final del documento (opcional)
      $pdf->mensajeFactura = "";//$datos_cliente->comentario_extra;

      // Solo compatible con CFDI 3.3 (opcional)
      $pdf->direccionExpedicion = '';//$datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;

      // ruta del logotipo (opcional)
      $pdf->logo = $this->config->item('url_real').'logo.png';///dirname(__FILE__).'/logo.png';

      // mensaje a mostrar encima del documento (opcional)
      // $pdf->mensajeSello = 'CANCELADO';

      // Cargar el XML desde un string...
      // $ok = $pdf->cargarCadenaXml($cadenaXml);

      // Cargar el XML desde un archivo...
      // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
      // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
      // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
      $archivoXml = $this->config->item('url_real')."/statics/factura_p/F0000000305 ALSOPBIKE.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';

      $ok = $pdf->cargarArchivoXml($archivoXml);

      if($ok) {
          // Generar PDF para mostrar en el explorador o descargar
          $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

          // Guardar PDF en la ruta especificada
          // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
          // $ok = $pdf->guardarPdf($ruta);

          // Obtener PDF como string
          // $pdfStr = $pdf->obtenerPdf();

          if($ok) {
              // PDF generado correctamente.
          } else {
              echo 'Error al generar PDF.';
          }
      }else{
          echo 'Error al cargar archivo XML.';
      }

    }



    public function api_genera_factaura(){

      $json = file_get_contents('php://input');

      // Converts it into a PHP object
      $factura_campo = json_decode($json);



      $emisor = $this->Mgeneral->get_row('id',1,'datos');
      $id_factura = get_guid();
      $data['facturaID'] = $id_factura;
      $data['status'] = 1;
      $data['emisor_RFC'] = $emisor->rfc;
      $data['emisor_regimenFiscal'] = $emisor->regimen;
      $data['emisor_nombre'] = $emisor->razon_social;
      $data['receptor_uso_CFDI'] = "G03";
      $data['pagada'] = "SI";
      //$data['sucursal'] = $this->validar_capo_json($factura_campo->sucursal);
      //$data['tipo'] = $this->validar_capo_json($factura_campo->tipo);
      $this->Mgeneral->save_register('factura', $data);

     

      $data_datos['receptor_nombre'] = $this->validar_capo_json($factura_campo->receptor_nombre);
      //$data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');
      $data_datos['factura_moneda'] = $this->validar_capo_json($factura_campo->moneda);
      $data_datos['fatura_lugarExpedicion'] = $this->validar_capo_json($factura_campo->LugarExpedicion);
      $data_datos['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
      $data_datos['receptor_email'] =  $this->validar_capo_json($factura_campo->receptor_email);
      $data_datos['factura_folio'] = $this->validar_capo_json($factura_campo->Folio);
      $data_datos['factura_serie'] = $this->validar_capo_json($factura_campo->Serie);
      $data_datos['receptor_direccion'] = $this->validar_capo_json($factura_campo->receptor_direccion);
      $data_datos['factura_formaPago'] = $this->validar_capo_json($factura_campo->FormaPago);
      $data_datos['factura_medotoPago'] = $this->validar_capo_json($factura_campo->MetodoPago);
      $data_datos['factura_tipoComprobante'] = $this->validar_capo_json($factura_campo->TipoDeComprobante);
      $data_datos['receptor_RFC'] = $this->validar_capo_json($factura_campo->receptor_RFC);
      $data_datos['receptor_uso_CFDI'] = $this->validar_capo_json($factura_campo->receptor_uso_CFDI);
      //$data['pagada'] = $this->input->post('pagada');
      $data_datos['comentario'] = $this->validar_capo_json($factura_campo->comentario);
      //$data['almacen'] = $this->input->post('almacen');

     
      $this->Mgeneral->update_table_row('factura',$data_datos,'facturaID',$id_factura);



      

      foreach($factura_campo->conceptos as $concepto_fac):

       

        $data_conceptos['concepto_facturaId'] = $id_factura;
        $data_conceptos['concepto_NoIdentificacion'] = $this->validar_capo_json($concepto_fac->concepto_NoIdentificacion);
        $data_conceptos['concepto_unidad'] = $this->validar_capo_json($concepto_fac->unidad_interna);
        //$data_conceptos['concepto_descuento'] = $this->input->post('concepto_descuento');
        $data_conceptos['clave_sat'] = $this->validar_capo_json($concepto_fac->clave_sat);
        $data_conceptos['unidad_sat'] = $this->validar_capo_json($concepto_fac->unidad_sat);
        $data_conceptos['concepto_nombre'] = $this->validar_capo_json($concepto_fac->concepto_nombre);
        $data_conceptos['concepto_precio'] = $this->validar_capo_json($concepto_fac->concepto_precio);
        $data_conceptos['concepto_importe'] = $this->validar_capo_json($concepto_fac->concepto_importe);
        $data_conceptos['impuesto_iva'] = $this->validar_capo_json($concepto_fac->concepto_importe);
        $data_conceptos['impuesto_iva_tipoFactor'] = 0;//$this->input->post('impuesto_iva_tipoFactor');
        $data_conceptos['impuesto_iva_tasaCuota'] = 0;//$this->input->post('impuesto_iva_tasaCuota');
        $data_conceptos['impuesto_ISR'] = 0;//$this->input->post('impuesto_ISR');
        $data_conceptos['impuesto_ISR_tasaFactor'] = $this->validar_capo_json($concepto_fac->TipoFactor);
        $data_conceptos['impuestoISR_tasaCuota'] = $this->validar_capo_json($concepto_fac->TasaOCuota);
        $data_conceptos['tipo'] = '0';//$this->input->post('tipo');
        $data_conceptos['nombre_interno'] = $this->validar_capo_json($concepto_fac->concepto_nombre);
        $data_conceptos['id_producto_servicio_interno'] = $this->validar_capo_json($concepto_fac->id_producto_servicio_interno);
        $data_conceptos['concepto_cantidad'] = $this->validar_capo_json($concepto_fac->concepto_cantidad);
        $data_conceptos['importe_iva'] = $this->validar_capo_json($concepto_fac->Importe);
        $data_conceptos['fecha_creacion'] = date('Y-m-d H:i:s');
        $this->Mgeneral->save_register('factura_conceptos',$data_conceptos);
        

        

      endforeach;


      //$this->generar_factura($id_factura);
      $this->generar_prefactura($id_factura);
      
    }

    function validar_capo_json($campo){
      try {
        if(!isset($campo)) {
            throw new Exception("Falta campo".$campo);
          }
          return $campo;
      }
      //catch exception
      catch(Exception $e) {
        echo 'Message: ' .$e->getMessage();
        die();
      }

    }


}
