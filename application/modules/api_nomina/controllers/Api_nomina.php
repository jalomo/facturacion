<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Api_nomina extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','astillero'));

        date_default_timezone_set('America/Mexico_City');

        // if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function leer(){
        echo exec('whoami');
        die();
        $json = '{"lugar_expedicion":"28983","forma_pago":"27","metodo_pago":"PUE","folio":"21","serie":"A","tipo_comprobante":"I","subtotal":190,"iva":11.4,"descuento":1.4,"total":200,"rfc_emisor":"LAN7008173R5","regimen_fiscal_emisor":"622","nombre_emisor":"Emisor Ejemplo","rfc_receptor":"LAN7008173R5","uso_cfdi":"I08","nombre_receptor":"nombre","receptor_email":"mail@mail.com","receptor_direccion":"receptor_direccion","comentarios":"comentarios","conceptos":[{"clave_sat":"52141807","cantidad":2,"nombre_concepto":"nombre producto","precio":95,"importe":190,"clave_interna":"PR01","unidad_interna":"servicio","descuento":0,"impuesto":"002","tipo_factor":"Cuota","tasa_cuota":0.16,"base_subtotal":190,"base_iva":11.4},{"clave_sat":"52141807","cantidad":2,"nombre_concepto":"nombre producto","precio":95,"importe":190,"clave_interna":"PR01","unidad_interna":"servicio","descuento":0,"impuesto":"002","tipo_factor":"Cuota","tasa_cuota":0.16,"base_subtotal":190,"base_iva":11.4},{"clave_sat":"52141807","cantidad":2,"nombre_concepto":"nombre producto","precio":95,"importe":190,"clave_interna":"PR01","unidad_interna":"servicio","descuento":0,"impuesto":"002","tipo_factor":"Cuota","tasa_cuota":0.16,"base_subtotal":190,"base_iva":11.4}]}';

            $obj = json_decode($json);
            echo $obj->lugar_expedicion;
            echo $obj->receptor_email;
            //echo $obj->conceptos;
            foreach($obj->conceptos as $row):
                echo $row->clave_interna;
            endforeach;
            //var_dump($obj);

           

            // GUARDA DATOS CABECERA ------------------------------
            $id_factura = get_guid();
            $data['facturaID'] = $id_factura;
            $data['status'] = 1;
            $data['emisor_RFC'] = $obj->rfc_emisor;
            $data['emisor_regimenFiscal'] = $obj->regimen_fiscal_emisor;
            $data['emisor_nombre'] = $obj->nombre_emisor;
            $data['pagada'] = "SI";

            $data['receptor_nombre'] = $obj->rfc_receptor;
            $data['receptor_id_cliente'] = "";
            $data['factura_moneda'] = $obj->nombre_receptor;

            $data['fatura_lugarExpedicion'] = $obj->lugar_expedicion;
            $data['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
            $data['receptor_email'] = $obj->receptor_email;

            $data['factura_folio'] = $obj->folio;
            $data['factura_serie'] = $obj->serie;

            $data['receptor_direccion'] = $obj->receptor_direccion;
            $data['factura_formaPago'] = $obj->forma_pago;
            $data['factura_medotoPago'] = $obj->metodo_pago;
            $data['factura_tipoComprobante'] = $obj->tipo_comprobante;
            $data['receptor_RFC'] = $obj->rfc_receptor;
            $data['receptor_uso_CFDI'] = $obj->uso_cfdi;
            $data['comentario'] = $obj->comentarios;

            $this->Mgeneral->save_register('factura', $data);
            //------------------------------------------------------

            // CONCEPTOS ------------------------------------------
            foreach($obj->conceptos as $row):
                echo $row->clave_interna;
                $data_conceptos['concepto_facturaId'] = $id_factura;
                $data_conceptos['concepto_NoIdentificacion'] = $this->input->post('concepto_NoIdentificacion');
                $data_conceptos['concepto_unidad'] = $this->input->post('concepto_unidad');
                //$data['concepto_descuento'] = $this->input->post('concepto_descuento');
                $data_conceptos['clave_sat'] = $this->input->post('clave_sat');
                $data_conceptos['unidad_sat'] = $this->input->post('unidad_sat');
                $data_conceptos['concepto_nombre'] = $this->input->post('concepto_nombre');
                $data_conceptos['concepto_precio'] = $this->input->post('concepto_precio');
                $data_conceptos['concepto_importe'] = $this->input->post('concepto_importe');
                $data_conceptos['impuesto_iva'] = $this->input->post('impuesto_iva');
                $data_conceptos['impuesto_iva_tipoFactor'] = $this->input->post('impuesto_iva_tipoFactor');
                $data_conceptos['impuesto_iva_tasaCuota'] = $this->input->post('impuesto_iva_tasaCuota');
                $data_conceptos['impuesto_ISR'] = $this->input->post('impuesto_ISR');
                $data_conceptos['impuesto_ISR_tasaFactor'] = $this->input->post('impuesto_ISR_tasaFactor');
                $data_conceptos['impuestoISR_tasaCuota'] = $this->input->post('impuestoISR_tasaCuota');
                $data_conceptos['tipo'] = $this->input->post('tipo');
                $data_conceptos['nombre_interno'] = $this->input->post('nombre_interno');
                $data_conceptos['id_producto_servicio_interno'] = $this->input->post('id_producto_servicio_interno');
                $data_conceptos['concepto_cantidad'] = $this->input->post('concepto_cantidad');
                $data_conceptos['importe_iva'] = $this->input->post('importe_iva');
                $data_conceptos['fecha_creacion'] = date('Y-m-d H:i:s');
                $this->Mgeneral->save_register('factura_conceptos',$data_conceptos);
            endforeach;

            //----------------------------------------------------

    }

    public function prueba(){
        $data['lugar_expedicion'] = "28983";
        $data['forma_pago'] = "27";
        $data['metodo_pago'] = "PUE";
        $data['folio'] = "21";
        $data['serie'] = "A";
        $data['tipo_comprobante'] = "I";

        $data['subtotal'] = 190;
        $data['iva'] = 11.40;
        $data['descuento'] = 1.40;
        $data['total'] = 200.00;

        $data['rfc_emisor'] = "LAN7008173R5";
        $data['regimen_fiscal_emisor'] = "622";
        $data['nombre_emisor'] = "Emisor Ejemplo";

        $data['rfc_receptor'] = "LAN7008173R5";
        $data['uso_cfdi'] = "I08";
        $data['nombre_receptor'] = "nombre";
        $data['receptor_email'] = "mail@mail.com";
        $data['receptor_direccion'] = "receptor_direccion";

        $data['comentarios'] = "comentarios";

        

        
        $data['conceptos'] = array();
       for($i = 0; $i <3; $i ++):

        $conceptos = array();

        $conceptos['clave_sat'] = "52141807";
        $conceptos['cantidad'] = 2;
        $conceptos['nombre_concepto'] = "nombre producto";
        $conceptos['precio'] = 95.00;
        $conceptos['importe'] = 190.00;

        $conceptos['clave_interna'] = "PR01";
        $conceptos['unidad_interna'] = "servicio";
        $conceptos['descuento'] = 0.0;

        $conceptos['impuesto'] = "002";
        $conceptos['tipo_factor'] = "Cuota";
        $conceptos['tasa_cuota'] = 0.16;
        $conceptos['base_subtotal'] = 190;
        $conceptos['base_iva'] = 11.40;

        array_push($data['conceptos'],$conceptos);

       endfor;

       echo json_encode($data);
    }

    public function genera(){

        $lugar_expedicion = "12345";
        $forma_pago = "27";
        $metodo_pago = "PUE";
        $folio = "21";
        $serie = "A";
        $tipo_comprobante = "I";



        $moneda = 'MXN';
        $subtotal  = 190.00;
        $iva       =  11.40;
        $descuento =   1.40;
        $total     = 200.00;
        $fecha     = time();

        //emisor
        $rfc_emisor = "LAN7008173R5";
        $regimen_fiscal_emisor = "622";
        $nombre_emisor = "Emisor Ejemplo";

        //receptor
        $rfc_receptor = "LAN7008173R5";
        $uso_cfdi = "I08";
        $nombre_receptor = "Emisor Ejemplo";

        
        // Establecer valores generales
        $cfdi->LugarExpedicion   = $lugar_expedicion;
        $cfdi->FormaPago         = $forma_pago;
        $cfdi->MetodoPago        = $metodo_pago;
        $cfdi->Folio             = $folio;
        $cfdi->Serie             = $serie;
        $cfdi->TipoDeComprobante = $tipo_comprobante;
        $cfdi->TipoCambio        = 1;
        $cfdi->Moneda            = $moneda;
        $cfdi->setSubTotal($subtotal);
        $cfdi->setTotal($total);
        $cfdi->setDescuento($descuento);
        $cfdi->setFecha($fecha);
        
        // Agregar emisor
        $cfdi->Emisor = Emisor::init(
            $rfc_emisor,                    // RFC
            $regimen_fiscal_emisor,                             // Régimen Fiscal
            $nombre_emisor                   // Nombre (opcional)
        );
        
        // Agregar receptor
        $cfdi->Receptor = Receptor::init(
            $rfc_receptor,                   // RFC
            $uso_cfdi,                             // Uso del CFDI
            $nombre_receptor                 // Nombre (opcional)
        );
        
        // Preparar datos del concepto 1
        $concepto = Concepto::init(
            '52141807',                        // clave producto SAT
            '2',                               // cantidad
            'P83',                             // clave unidad SAT
            'Nombre del producto de ejemplo',
            95.00,                             // precio
            190.00                             // importe
        );
        $concepto->NoIdentificacion = 'PR01'; // clave de producto interna
        $concepto->Unidad = 'Servicio';       // unidad de medida interna
        $concepto->Descuento = 0.0;
        
        // Agregar impuesto (traslado) al concepto 1
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Cuota';
        $traslado->TasaOCuota = 0.16;
        $traslado->Base = $subtotal;
        $traslado->Importe = $iva;
        $concepto->agregarImpuesto($traslado);
        
        
    }
}