<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Marca extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $titulo['titulo'] = "Marcas" ;
      $titulo['titulo_dos'] = "Alta Marca" ;
      $rows['rows'] = $this->Mgeneral->get_result('status',0,'marca');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('marca/alta', $rows, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
    }

    public function guarda_edicion(){

      if($this->input->post()){
        $textos = $this->input->post();
        $this->Mgeneral->save_editar_formulario($textos,'marcaId','marca');
        echo "editado";
      }
    }

    public function guarda(){
      $this->form_validation->set_rules('marcaNombre', 'marcaNombre', 'required');
      $response = validate($this);
      if($response['status']){
        $data['marcaNombre'] = $this->input->post('marcaNombre');
        $this->Mgeneral->save_register('marca', $data);
      }
    echo json_encode(array('output' => $response));
    }

    public function editar($id){
       $rows['mar'] = $this->Mgeneral->get_row('marcaId',$id,'marca');
      $titulo['titulo'] = "Marcas" ;
      $titulo['titulo_dos'] = "Alta Marca" ;
      $rows['rows'] = $this->Mgeneral->get_table('marca');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('marca/editar', $rows, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
    }

    public function editar_marca($id){
       $this->form_validation->set_rules('marcaNombre', 'marcaNombre', 'required');
      $response = validate($this);
      if($response['status']){
        $data['marcaNombre'] = $this->input->post('marcaNombre');
        $this->Mgeneral->update_table_row('marca',$data,'marcaId',$id);
      }
    echo json_encode(array('output' => $response));
    }
    public function eliminar($id_plaga){
      $data['status'] = 1;
      $this->Mgeneral->update_table_row('marca',$data,'marcaId',$id_plaga);

    }

}
