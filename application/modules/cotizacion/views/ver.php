<script>
$(document).ready(function(){
  $("#cargando").hide();
$('#enviar_factura').submit(function(event){
  event.preventDefault();

  var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Enviando datos...</p>',
                            closeButton: false
                        });

  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/factura/generar_factura/<?php echo $factura->facturaID?>";
  ajaxJson(url,{},
            "POST",true,function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_error = json_response.error;
    if(obj_error == 0){
      dialog_load.modal('hide');
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/factura/lista/");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_error == 1){
      dialog_load.modal('hide');
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+json_response.error_mensaje,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    


  });
});


$('#enviar_prefactura').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/cotizacion/generar_prefactura/<?php echo $factura->facturaID?>";
  ajaxJson(url,{},
            "POST",true,function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_error = json_response.error;
    if(obj_error == 0){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/cotizacion/lista/");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_error == 1){
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+json_response.error_mensaje,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    


  });
});

});
</script>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Receptor</h5>
        <h6 class="card-text">
          <small class="text-muted">Razón Social:</small>
          <?php echo $factura->receptor_nombre?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">RFC:</small>
          <?php echo $factura->receptor_RFC?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">Email:</small>
          <?php echo $factura->receptor_email?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">Dirección:</small>
          <?php echo $factura->receptor_direccion?>
        </h6>
      </div>
    </div>


  </div>
  <div class="col-md-6">

    <div class="card">
      <div class="card-body">
        <h6 class="card-text">
          <h6 class="card-text">
            <small class="text-muted">Fecha:</small>
            <?php echo $factura->factura_fecha?>
          </h6>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">Folio:</small>
          <?php echo $factura->factura_folio?>
          <small class="text-muted">Serie:</small>
          <?php echo $factura->factura_folio?>
        </h6>
        <h5 class="card-title">Emisor</h5>
        <h6 class="card-text">
          <small class="text-muted">Razón Social:</small>
          <?php echo $emisor->razon_social?>
        </h6>
        <h6 class="card-text">
          <small class="text-muted">RFC:</small>
          <?php echo $emisor->rfc?>
        </h6>
      </div>
    </div>

  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <div class="card">

      <div class="card-body">
        <h5 class="card-title" align="center">Conceptos</h5>

        <div class="row">
          <div class="col-md-1">
            <small class="text-muted">Clave</small>

          </div>
          <div class="col-md-1">
            <small class="text-muted">Cantidad</small>
          </div>
          <div class="col-md-1">
            <small class="text-muted">Unidad</small>
          </div>
          <div class="col-md-6">
            <small class="text-muted">Descripción</small>
          </div>
          <div class="col-md-1">
            <small class="text-muted">Precio</small>
          </div>
          <div class="col-md-1">
            <small class="text-muted">Importe</small>
          </div>
          <div class="col-md-1">
            <small class="text-muted">IVA</small>
          </div>

          <?php foreach($conceptos as $conce):?>
            <div class="col-md-1">
              <small class="text"><?php echo $conce->clave_sat;?></small>
            </div>
            <div class="col-md-1">
              <small class="text"><?php echo $conce->concepto_cantidad;?></small>
            </div>
            <div class="col-md-1">
              <small class="text"><?php echo $conce->unidad_sat;?></small>
            </div>
            <div class="col-md-6">
              <small class="text"><?php echo $conce->concepto_nombre;?></small>
            </div>
            <div class="col-md-1">
              <small class="text">$<?php echo $conce->concepto_precio;?></small>
            </div>
            <div class="col-md-1">
              <small class="text">$<?php echo $conce->concepto_importe;?></small>
            </div>
            <div class="col-md-1">
              <small class="text">$<?php echo $conce->concepto_importe*.16;?></small>
            </div>
            <hr/>
          <?php endforeach;?>

        </div>

      </div>
    </div>

  </div>

</div>


<!--div class="row">
  <div class="col-md-12">

    <div class="card">

      <div class="card-body">
        <h5 class="card-title" align="center">CFDI Relacionados</h5>

        <div class="row">
          <div class="col-md-6">
            <small class="text-muted">Tipo de relación</small>

          </div>
          <div class="col-md-6">
            <small class="text-muted">UUID</small>
          </div>


          <?php foreach($factura_fcdi_relacionados as $relacion):?>
            <div class="col-md-6">
              <small class="text"><?php echo $relacion->relacionados_tipoRelacion;?></small>
            </div>
            <div class="col-md-6">
              <small class="text"><?php echo $relacion->relacion_UUID;?></small>
            </div>
            <hr/>
          <?php endforeach;?>

        </div>

      </div>
    </div>

  </div>

</div-->


<div class="row">
  <!--div class="col-md-8">

    <div class="card">

      <div class="card-body">
        <h5 class="card-title" align="center">Datos generales</h5>

        <div class="row">
          <div class="col-md-12">
            <small class="text">Tipo de comprobante: <?php echo $factura->factura_tipoComprobante;?></small>
            <br/>
            <small class="text">Forma de pago: <?php echo $factura->factura_formaPago;?></small>
            <br/>
            <small class="text">Método de pago: <?php echo $factura->factura_medotoPago;?></small>
            <br/>
            <small class="text">Total con letra: </small>

          </div>




        </div>

      </div>
    </div>

  </div-->

 <div class="col-md-4">
   <div class="card">

     <div class="card-body">


       <div class="row">
         <div class="col-md-6">
           <small class="text">Subtotal: $<?php echo $this->Mgeneral->factura_subtotal($facturaID);?></small>
           <br/>
           <!--small class="text">Descuento:<?php echo $this->Mgeneral->factura_descuento($facturaID);?></small>
           <br/-->
           <small class="text">IVA: $<?php echo $this->Mgeneral->factura_iva_total($facturaID);?></small>
           <br/>
           <small class="text">Total: $<?php echo $this->Mgeneral->factura_subtotal($facturaID) + $this->Mgeneral->factura_iva_total($facturaID);?></small>
         </div>




       </div>

     </div>
   </div>
 </div>


</div>




<div align="right">
  <!--label>Opciones:</label-->

<form method="post" id="enviar_prefactura">
  <button id="enviar2" type="submit" class="btn btn-lg btn-info ">
      <i class="fa fa-edit fa-lg"></i>&nbsp;
      <span id="payment-button-amount">Siguiente</span>

  </button>
</form>
<br/>

<!--form method="post" id="enviar_factura">
  <button id="enviar" type="submit" class="btn btn-lg btn-info ">
      <i class="fa fa-edit fa-lg"></i>&nbsp;
      <span id="payment-button-amount">Sellar y Firmar</span>

  </button>
</form-->
<button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>

  <!--a href="<?php echo base_url()?>index.php/factura/generar_factura/<?php echo $facturaID;?>">
  <button id="enviar_factura" type="button" class="btn btn-lg btn-info ">
      <i class="fa fa-edit fa-lg"></i>&nbsp;
      <span id="payment-button-amount">Sellar y Firmar</span>

  </button>
</a-->
  <!--div align="center">
   <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
 </div-->
</div>
