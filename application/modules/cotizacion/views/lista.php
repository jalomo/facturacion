

<script src="<?php echo base_url()?>statics/js/isloading.js"></script>
<script type="text/javascript">
      menu_activo = "cotizacion";
$("#menu_cotizacion_cotiza").last().addClass("menu_estilo");
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});



      $(".prefactura").click(function(event){
           event.preventDefault();

            bootbox.dialog({
               message: "Desea enviar a prefactura?",
               closeButton: true,
               buttons:
               {
                   "danger":
                   {
                       "label": "Aceptar ",
                       "className": "btn-danger",
                       "callback": function () {

                         id = $(event.currentTarget).attr('flag');
                         url = $("#delete"+id).attr('href');
                         //$.get(url);
                         var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Enviando ...</p>',
                            closeButton: false
                        });
                         
                         ajaxJson(url,{},
                                   "POST",true,function(result){

                           console.log(result);

                           json_response = JSON.parse(result);

                           obj_status = json_response.error;
                           if(obj_status == true){
                            dialog_load.modal('hide');

                             exito("<h3>ERROR intente de nuevo<h3/> ","danger");
                           }
                           if(obj_status == false){
                            dialog_load.modal('hide');

                             exito_redirect("ENVIADA CON EXITO","success","<?php echo base_url()?>index.php/factura/lista");
                           }
                         });
                       }
                   },
                   "cancel":
                   {
                       "label": "<i class='icon-remove'></i> Cancelar",
                       "className": "btn-sm btn-info",
                       "callback": function () {

                       }
                   }

               }
           });
       });

    } );
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}

button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 11px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<div class="card-header">
  <div class="">
      <a href="<?php echo base_url();?>index.php/cotizacion/crear_pre"><button type="button" class="btn btn-primary">Crear cotización</button></a>
  </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                   <!--label>Filtro:</label-->
                   <?php 
                   $color_filtro_todo = "btn-light";
                   $color_filtro_prefactura = "btn-light";
                   if($filtro == 1){
                      $color_filtro_todo = "btn-secondary";
                      $color_filtro_prefactura = "btn-light";
                   }?>
                   <?php if($filtro == 0){
                      $color_filtro_todo = "btn-light";
                      $color_filtro_prefactura = "btn-secondary";
                   }?>
                   <!--a class=""   href="<?php echo base_url()?>index.php/factura/lista/1">
                      <button type="button" class="btn <?php echo $color_filtro_todo;?>">Todo</button>
                   </a>
                   <a class=""   href="<?php echo base_url()?>index.php/factura/lista/0">
                      <button type="button" class="btn <?php echo $color_filtro_prefactura;?>">Pre-Facturas</button>
                   </a-->
                   
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>RFC</th>
                        <!--th>M.P.</th-->
                        <th>Nombre</th>
                        <th>Total</th>
                        <!--th>Abono</th>
                        <th>Saldo</th-->
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($facturas)):?>
                      <?php foreach($facturas as $row):?>
                        <tr style="<?php //if($row->factura_medotoPago == "PPD"){echo "background:#ecf8c7;";}else{if($row->cancelar_EstatusUUID == "202"){echo "background:#fab1a0;";} if( ($row->sat_error == "0")){ echo "background:#c7f8d9;"; }}?>">

                          <td><?php echo $row->factura_fecha;?></td>
                          <td><?php echo $row->receptor_RFC;?></td>
                          <!--td><?php echo $row->factura_medotoPago;?></td-->
                          <td><?php echo $row->receptor_nombre;?></td>
                          <td>
                            $<?php echo $this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID);?>
                          </td>
                          <!--td>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <?php $saldo1 = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                echo "$".$saldo1;
                              ?>
                            <?php endif;?>
                          </td-->
                          <!--td>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <?php 

                                $saldo = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                echo "$".(($this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID))-$saldo);
                              ?>
                            <?php endif;?>
                          </td-->

                          <td>
                            
                            <a target="_blank" href="<?php echo base_url()?>index.php/pdf/cotizacion/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-success">PDF </button>
                            </a>
                            
                            
                            
                            <a href="<?php echo base_url()?>index.php/cotizacion/nueva/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-info">Ver/Editar</button>
                            </a>
                         

                          <a class="prefactura" flag="<?php echo $row->facturaID;?>" id="delete<?php echo $row->facturaID;?>" href="<?php echo base_url()?>index.php/cotizacion/convertir_prefactura/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-danger">Prefactura</button>
                            </a>
                          </td>
                        </tr>
                     <?php endforeach;?>
                     <?php endif;?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
