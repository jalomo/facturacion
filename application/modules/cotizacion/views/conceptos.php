<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function(){


      $(".textnumeros").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

      $("#cargando").hide();
      $('#enviar_concepto').submit(function(event){
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();
        var url ="<?php echo base_url()?>index.php/cotizacion/guarda_datos_concepto/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"concepto_NoIdentificacion":$("#concepto_NoIdentificacion").val(),
                      "concepto_unidad":$("#concepto_unidad").val(),
                      //"concepto_descuento":$("#concepto_descuento").val(),
                      "clave_sat":$("#clave_sat").val(),
                      "unidad_sat":$("#unidad_sat").val(),
                      "concepto_nombre":$("#concepto_nombre").val(),
                      "concepto_precio":$("#concepto_precio").val(),
                      "concepto_importe":$("#concepto_importe").val(),
                      "impuesto_iva":$("#impuesto_iva").val(),
                      "impuesto_iva_tipoFactor":$("#impuesto_iva_tipoFactor").val(),
                      "impuesto_iva_tasaCuota":$("#impuesto_iva_tasaCuota").val(),
                      //"impuesto_ISR":$("#factura_medotoPago").val(),
                      //"impuesto_ISR_tasaFactor":$("#factura_tipoComprobante").val(),
                      //"impuestoISR_tasaCuota":$("#receptor_RFC").val(),
                      "tipo":$("#tipo").val(),
                      //"nombre_interno":$("#pagada").val(),
                      "id_producto_servicio_interno":$("#id_producto_servicio_interno").val(),
                      "concepto_cantidad":$("#concepto_cantidad").val(),
                      "importe_iva":$("#importe_iva").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/cotizacion/conceptos/<?php echo $factura->facturaID?>");
            $("#enviar").show();
            $("#cargando").hide();
          }


        });
      });

    /*  $("#concepto_nombre").autocomplete({
            source: "<?php echo base_url()?>index.php/Factura/buscar_producto",
             select: function(event, ui) {



           }
    });*/
    });


    function seleccionar_producto(id_producto){
      //$('#exampleModal').modal('hide');
      var url_sis ="<?php echo base_url()?>index.php/factura/get_dados_producto/"+id_producto;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                  var obj = JSON.parse(data);
                  $('#concepto_nombre').val(obj.productoNombre);
                  $('#concepto_NoIdentificacion').val(obj.productoReferencia);
                  $('#concepto_unidad').val(obj.productoMedida);
                  $('#clave_sat').val(obj.clave_sat);
                  $('#unidad_sat').val(obj.unidad_sat);
                  $('#id_producto_servicio_interno').val(obj.productoId);
                  $('#tipo').val(obj.productoClasificado);
                  $('#precios').html("Precio público:$"+obj.productoPrecioPublico+
                                     "&nbsp &nbsp Precio mayoreo:$"+obj.productoPrecioMayoreo+
                                     "&nbsp &nbsp Cantidad mayoreo:"+obj.productoCantidadMayoreo+
                                     "&nbsp &nbsp Precio credito:$"+obj.productoPrecioCredito);

                  precios = '<select class="form-control cc-exp alto form-control-sm " id="seleccion_precio">';
                  precios +='<option value="'+obj.productoPrecioPublico+'">Publico: $'+obj.productoPrecioPublico+'</option>';
                  precios +='<option value="'+obj.productoPrecioMayoreo+'">Mayore:$'+obj.productoPrecioMayoreo+'</option>';
                  precios +='<option value="'+obj.productoPrecioCredito+'">Credito:$'+obj.productoPrecioCredito+'</option>';
                  precios +='</select>';
                  precios +='<label>Cantidad mayoreo:'+obj.productoCantidadMayoreo+'</label>';

                  

                  $("#precios").html(precios);

                  $("#seleccion_precio").change(function(){
                      var selectedCountry = $(this).children("option:selected").val();
                      //alert("precio- " + selectedCountry);
                      $("#concepto_precio").val(selectedCountry);

                      $("#concepto_importe").val(selectedCountry*$("#concepto_cantidad").val());

                      $("#importe_iva").val($("#concepto_importe").val()*.16);

                      $("#impuesto_ieps_tasaCuota").val((obj.ieps/100));
                      $("#importe_ieps").val( $("#concepto_importe").val() * (obj.ieps/100));

                      
                      
                  });





                  console.log("SUCCESS : ", data);
                  seleccionar_producto_unidad(id_producto);

                  $("#concepto_cantidad").val(1);
                  $("#concepto_precio").val(obj.productoPrecioPublico);
                  $("#concepto_importe").val(obj.productoPrecioPublico*1);
                  $("#importe_iva").val($("#concepto_importe").val()*.16);

                    $("#impuesto_ieps_tasaCuota").val((obj.ieps/100));
                    $("#importe_ieps").val( $("#concepto_importe").val() * (obj.ieps/100));
                    console.log("SUCCESS IEPS: ", obj.ieps);
                    console.log("cantidad mayoreo", obj.productoCantidadMayoreo );

                  $("#concepto_cantidad").keyup(function(){
                    console.log("cantidad mayoreo", obj.productoCantidadMayoreo );
                    $("#concepto_cantidad").css("background-color", "yellow");
                    //alert($(this).val());
                    if($("#concepto_cantidad").val() < obj.productoCantidadMayoreo ){

                      $("#concepto_precio").val(obj.productoPrecioPublico);
                      $("#concepto_importe").val(obj.productoPrecioPublico*$("#concepto_cantidad").val());
                      $("#importe_iva").val($("#concepto_importe").val()*.16);

                      $("#impuesto_ieps_tasaCuota").val((obj.ieps/100));
                      $("#importe_ieps").val( $("#concepto_importe").val() * (obj.ieps/100));

                    }else if($("#concepto_cantidad").val() >= obj.productoCantidadMayoreo){

                      $("#concepto_precio").val(obj.productoPrecioMayoreo);
                      $("#concepto_importe").val(obj.productoPrecioMayoreo*$("#concepto_cantidad").val());
                      $("#importe_iva").val($("#concepto_importe").val()*.16);

                      $("#impuesto_ieps_tasaCuota").val((obj.ieps/100));
                    $("#importe_ieps").val( $("#concepto_importe").val() * (obj.ieps/100));
                    }


                  });



                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
    }


    function seleccionar_producto_unidad(id_producto){

      var url_sis ="<?php echo base_url()?>index.php/cotizacion/unidades_productos/"+id_producto;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                 $("#load_unidad").html("");
                 $("#load_unidad").html(data);



                  console.log("SUCCESS : ", data);





                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });

    }

</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Alta de conceptos </strong>

        </div>
        <div class="card-body" style="background:#bdc3c7">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="enviar_concepto">



                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre concepto</label>
                                  <input id="concepto_nombre" name="concepto_nombre" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="Nombre" autocomplete="cc-exp" data-toggle="modal"  data-target="#exampleModal">
                                  <input type="hidden" id="id_producto_servicio_interno" name="id_producto_servicio_interno"/>
                                  <input type="hidden" id="tipo" name="tipo"/>
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">No. identificación interna</label>
                                  <input id="concepto_NoIdentificacion" name="concepto_NoIdentificacion" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="Numero identificación interna" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Unidad interna</label>
                                <input id="concepto_unidad" name="concepto_unidad" type="text" class="form-control cc-exp alto form-control-sm" value="" placeholder="Unidad interna" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                                  <div id="load_unidad"></div>
                            </div>
                          </div>
                      </div>



                      <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1 ">Clave de producto SAT</label>
                                <select class="form-control alto form-control-sm" id="clave_sat" name="clave_sat">
                                    <option value="10191506">10191506-Mata roedores</option>
                                    <option value="10191509">10191509-Plaguisidas PRIRENAT</option>
                                    <option value="10191703">10191703-Insectos voladores trampas PRIRENAT</option>
                                    <option value="10191701">10191701-Trampas para control de animal</option>
                                    <option value="10191706">10191706-Cebos, T-REX</option>
                                    <option value="10191700">10191700-dispositivos para control de plagas</option>
                                    <option value="72102100">72102100-Control de plagas</option>
                                    <option value="14111518">14111518-Letreros para estación</option>
                                    <option value="10191703">10191703-Lampara de luz 15 wats</option>
                                    <option value="72101507">72101507-Servicio de reparacipon de tablaroca contra goteras y reparación de piso</option>
                                    <option value="72101507">72101507-Mantenimiento y arreglo de filtración por puerta y ventana de agua</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad SAT</label>
                              <select class="form-control alto form-control-sm" id="unidad_sat" name="unidad_sat">
                                <option value="H87">H87-Pieza(Múltiplos / Fracciones / Decimales)</option>
                                <option value="EA">EA-Elemento(Unidades de venta)</option>
                                <option value="E48">E48-Unidad de Servicio(Unidades específicas de la industria (varias))</option>
                                <option value="ACT">ACT-Actividad(Unidades de venta)</option>
                                <option value="KGM">KGM-Kilogramo(Mecánica)</option>
                                <option value="E51">E51-Trabajo(Unidades específicas de la industria (varias))</option>
                                <option value="A9">A9-Tarifa(Diversos)</option>
                                <option value="MTR">MTR-Metro(Tiempo y Espacio)</option>
                                <option value="AB">AB-Paquete a granel(Diversos)</option>
                                <option value="BB">BB-Caja base(Unidades específicas de la industria (varias))</option>
                                <option value="KT">KT-Kit(Unidades de venta)</option>
                                <option value="SET">SET-Conjunto(Unidades de venta)</option>
                                <option value="LTR">LTR-Litro(Tiempo y Espacio)</option>
                                <option value="XBX">XBX-Caja(Unidades de empaque)</option>
                                <option value="MON">MON-Mes(Tiempo y Espacio)</option>
                                <option value="DAY">DAY-Día(Tiempo y Espacio)</option>
                                <option value="HUR">HUR-Hora(Tiempo y Espacio)</option>
                                <option value="MTK">MTK-Metro cuadrado(Tiempo y Espacio)</option>
                                <option value="11">11-Equipos(Diversos)</option>
                                <option value="MGM">MGM-Miligramo(Mecánica)</option>
                                <option value="XPK">XPK-Paquete(Unidades de empaque)</option>
                                <option value="XKI">XKI-Kit(Conjunto de piezas)(Unidades de empaque)</option>
                                <option value="AS">AS-Variedad(Diversos)</option>
                                <option value="GRM">GRM-Gramo(Mecánica)</option>
                                <option value="PR">PR-Par(Números enteros / Números / Ratios)</option>
                                <option value="DPC">DPC-Docenas de piezas(Unidades de venta)</option>
                                <option value="xun">xun-Unidad(Unidades de empaque)</option>
                                <option value="XLT">XLT-Lote(Unidades de empaque)</option>
                                <option value="10">10-Grupos(Diversos)</option>
                                <option value="MLT">MLT-Mililitro(Tiempo y Espacio)</option>
                                <option value="E54">E54-Viaje(Unidades específicas de la industria (varias))</option>
                              </select>
                          </div>
                        </div>


                        <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Cantidad</label>
                                <input id="concepto_cantidad" name="concepto_cantidad" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="" placeholder="Cantidad" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">

                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Precio</label>
                                <input id="concepto_precio" name="concepto_precio" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="" placeholder="Precio" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                                
                                  <div id="precios">
                                    
                                  </div>

                            </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Importe</label>
                              <input id="concepto_importe" name="concepto_importe" type="text" class="form-control cc-exp alto form-control-sm textnumeros" value="" placeholder="Importe" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                      </div>






                      <div class="row">
                          <div class="col-12">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Impuestos</label>
                              </div>
                          </div>

                      </div>


                      <div class="row">
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">IVA</label>
                                  <input id="impuesto_iva" name="impuesto_iva" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="002" autocomplete="cc-exp" value="002">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Factor</label>
                                  <input id="impuesto_iva_tipoFactor" name="impuesto_iva_tipoFactor" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Cuota" autocomplete="cc-exp" value="cuota">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Tasa cuota</label>
                                <input id="impuesto_iva_tasaCuota" name="impuesto_iva_tasaCuota" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Tasa cuota" autocomplete="cc-exp" value=".16">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Importe Impuesto</label>
                                <input id="importe_iva" name="importe_iva" type="text" class="form-control cc-exp alto form-control-sm" placeholder="Tasa cuota" autocomplete="cc-exp" value="">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>


                      </div>

                      <!--div class="row">
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">IEPS</label>
                                  <input id="impuesto_ieps" name="impuesto_ieps" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="002" autocomplete="cc-exp" value="003">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Factor</label>
                                  <input id="impuesto_ieps_factor" name="impuesto_ieps_factor" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Cuota" autocomplete="cc-exp" value="cuota">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Tasa cuota</label>
                                <input id="impuesto_ieps_tasaCuota" name="impuesto_ieps_tasaCuota" type="text" class="form-control cc-exp alto form-control-sm"  placeholder="Tasa cuota" autocomplete="cc-exp" value=".0">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Importe Impuesto</label>
                                <input id="importe_ieps" name="importe_ieps" type="text" class="form-control cc-exp alto form-control-sm" placeholder="Tasa cuota" autocomplete="cc-exp" value="">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>


                      </div-->



                      <div align="right">
                        <button id="enviar1" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Agregar concepto</span>
                            <span id="cargando" style="display:none;">Sending…</span>
                        </button>
                        <!--div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                       </div-->
                      </div>
                  </form>
              </div>

          </div>

        </div>
    </div> <!-- .card -->

    <div class="row" style="background:#fff;">
      <div class="col-12" id="factura_concepos_load">

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Concepto</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Precio</th>
              <th scope="col">IVA</th>
              <th scope="col">Total</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($conceptos as $conc):?>
            <tr>
              <th ><?php echo $conc->concepto_nombre;?></th>
              <td><?php echo $conc->concepto_cantidad;?></td>
              <td><?php echo $conc->concepto_precio;?></td>
              <td><?php echo $conc->importe_iva;?></td>
              <td><?php echo $conc->concepto_importe+$conc->importe_iva;?></td>
              <td><a href="<?php echo base_url();?>index.php/factura/eliminar_concepto/<?php echo $conc->concepto_id;?>/<?php echo $facturaID;?>" class="text-danger">Eliminar</a></td>
            </tr>
          <?php endforeach;?>
          </tbody>
        </table>

      </div>
    </div>


    <div align="right">
      <a href="<?php echo base_url()?>index.php/cotizacion/ver_factura/<?php echo $facturaID;?>">
      <button id="enviar1" type="button" class="btn btn-lg btn-info ">
          <i class="fa fa-edit fa-lg"></i>&nbsp;
          <span id="payment-button-amount">Siguiente</span>

      </button>
     </a>
      <!--div align="center">
       <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
     </div-->
    </div>

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->





<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un productos o servicio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Producto</th>
        <th>Referencia</th>
        <th>Contenido</th>
        <th>Precio publico</th>
        <th>Precio mayoreo</th>
        <th>Cantidad mayoreo</th>
        <th>Precio credito</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      <?php if(is_array($productos)):?>
      <?php foreach($productos as $row):?>
        <tr id="<?php echo $row->productoId;?>">
          <td><?php echo $row->productoNombre;?></td>
          <td><?php echo $row->productoReferencia;?></td>
          <td><?php echo $row->procuctoContenido;?></td>
          <td><?php echo $row->productoPrecioPublico;?></td>
          <td><?php echo $row->productoPrecioMayoreo;?></td>
          <td><?php echo $row->productoCantidadMayoreo;?></td>
          <td><?php echo $row->productoPrecioCredito;?></td>
          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_producto(<?php echo $row->productoId;?>)" data-dismiss="modal" id="<?php echo $row->productoId;?>" data-id="<?php echo $row->productoId;?>">seleccionar</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody>
  </table>
        </div>
      </div>

    </div>
  </div>
</div>
