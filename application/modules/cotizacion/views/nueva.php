<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function(){

      $('#guardar_datos').click(function(event){
        event.preventDefault();

        var url ="<?php echo base_url()?>index.php/cotizacion/guarda_datos_fac/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"receptor_nombre":$("#receptor_nombre").val(),
                      "receptor_id_cliente":$("#receptor_id_cliente").val(),
                      "factura_moneda":$("#factura_moneda").val(),
                      "fatura_lugarExpedicion":$("#fatura_lugarExpedicion").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "receptor_email":$("#receptor_email").val(),
                      "factura_folio":$("#factura_folio").val(),
                      "factura_serie":$("#factura_serie").val(),
                      "receptor_direccion":$("#receptor_direccion").val(),
                      "factura_formaPago":$("#factura_formaPago").val(),
                      "factura_medotoPago":$("#factura_medotoPago").val(),
                      "factura_tipoComprobante":$("#factura_tipoComprobante").val(),
                      "receptor_RFC":$("#receptor_RFC").val(),
                      "receptor_uso_CFDI":$("#receptor_uso_CFDI").val(),
                      "pagada":$("#pagada").val(),
                      "fecha_conclucion":$("#fecha_conclucion").val(),
                      "comentario":$("#comentario").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");

          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","");

          }


        });
      });


      $("#cargando").hide();
      $('#guarda_factura').submit(function(event){
        event.preventDefault();
        $("#enviar").hide();
        $("#cargando").show();
        var url ="<?php echo base_url()?>index.php/cotizacion/guarda_datos/<?php echo $factura->facturaID?>";
        ajaxJson(url,{"receptor_nombre":$("#receptor_nombre").val(),
                      "receptor_id_cliente":$("#receptor_id_cliente").val(),
                      "factura_moneda":$("#factura_moneda").val(),
                      "fatura_lugarExpedicion":$("#fatura_lugarExpedicion").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "factura_fecha":$("#factura_fecha").val(),
                      "receptor_email":$("#receptor_email").val(),
                      "factura_folio":$("#factura_folio").val(),
                      "factura_serie":$("#factura_serie").val(),
                      "receptor_direccion":$("#receptor_direccion").val(),
                      "factura_formaPago":$("#factura_formaPago").val(),
                      "factura_medotoPago":$("#factura_medotoPago").val(),
                      "factura_tipoComprobante":$("#factura_tipoComprobante").val(),
                      "receptor_RFC":$("#receptor_RFC").val(),
                      "receptor_uso_CFDI":$("#receptor_uso_CFDI").val(),
                      "pagada":$("#pagada").val(),
                       "fecha_conclucion":$("#fecha_conclucion").val(),
                      "comentario":$("#comentario").val()},
                  "POST","",function(result){
          correoValido = false;
          console.log(result);
          json_response = JSON.parse(result);
          obj_output = json_response.output;
          obj_status = obj_output.status;
          if(obj_status == false){
            aux = "";
            $.each( obj_output.errors, function( key, value ) {
              aux +=value+"<br/>";
            });
            exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
            $("#enviar").show();
            $("#cargando").hide();
          }
          if(obj_status == true){
            exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/cotizacion/conceptos/<?php echo $factura->facturaID?>");
            $("#enviar").show();
            $("#cargando").hide();
          }


        });
      });


      $("#receptor_nombre").autocomplete({
            source: "<?php echo base_url()?>index.php/factura/buscar_cliente",
             select: function(event, ui) {
				$('#receptor_RFC').val(ui.item.rfc);
				$('#receptor_email').val(ui.item.email);
				$('#receptor_direccion').val(ui.item.ciudad);
        $('#receptor_id_cliente').val(ui.item.id);


           }
    });

      $.ajax({
          type: 'POST',
          url: "<?php echo base_url()?>index.php/cotizacion/ver_relaciones_facturas",
          enctype: 'multipart/form-data',
          datatype: "JSON",
        //async: asincrono,
          cache: false,
          data: {id_factura:"<?php echo $factura->facturaID?>"},
          statusCode: {
              200: function (result) {
                //console.log(result);
                 $("#relacionados").html("");
                 $("#relacionados").html(result);
                 $(".eliminar_relacion").click(function(event){
                      event.preventDefault();
                      id = $(event.currentTarget).attr('flag');
                      url = $("#delete"+id).attr('href');
                      $("#borrar_"+id).slideUp();
                      $.get(url);
                  });

              },
              401: code400,
              404: code404,
              500: code500,
              409: code409
          }
      });

        $("#agregar_relacion").click(function(){

          $.ajax({
              type: 'POST',
              url: "<?php echo base_url()?>index.php/cotizacion/relaciones_facturas",
              enctype: 'multipart/form-data',
              datatype: "JSON",
            //async: asincrono,
              cache: false,
              data: {tipo_relacion:$("#id_relacion").val(),uuid:$("#uuid").val(),id_factura:"<?php echo $factura->facturaID?>"},
              statusCode: {
                  200: function (result) {
                    //console.log(result);
                     $("#relacionados").html("");
                     $("#relacionados").html(result);
                     $(".eliminar_relacion").click(function(event){
                          event.preventDefault();
                          id = $(event.currentTarget).attr('flag');
                          url = $("#delete"+id).attr('href');
                          $("#borrar_"+id).slideUp();
                          $.get(url);
                      });

                  },
                  401: code400,
                  404: code404,
                  500: code500,
                  409: code409
              }
          });


        });

    });

function seleccionar_cliente(id_cliente){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/get_datos_cliente/"+id_cliente;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              var obj = JSON.parse(data);

              $('#receptor_nombre').val(obj.nombre);
              

              $('#receptor_RFC').val(obj.rfc);
              $('#receptor_email').val(obj.email);
              $('#receptor_direccion').val(obj.ciudad);
              $('#receptor_id_cliente').val(obj.id);
              $('#factura_formaPago').val(obj.factura_formaPago);
              $('#receptor_uso_CFDI').val(obj.uso_CFDI);



              console.log("SUCCESS : ", data);





            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}
</script>
<style>

label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>

<div class="content mt-3">
    <div class="animated fadeIn">

<div class="row">
  <div class="col-lg-12">
    <div class="card" style="background:#fff">

        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="guarda_factura">
                    <!--div align="right">
                    <button id="guardar_datos" type="button" class="btn btn-outline-success">
                        <i class="fa fa-edit fa-lg"></i>&nbsp;
                        <span id="payment-button-amount">Guardar datos</span>

                    </button>
                  </div>
                  <hr/-->

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="factura_moneda">Cliente:</label>
                                <input id="receptor_nombre" name="receptor_nombre" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_nombre;?>" placeholder="Cliente" autocomplete="cc-exp" data-toggle="modal"  data-target="#exampleModal">
                                <input id="receptor_id_cliente" name="receptor_id_cliente" type="hidden" class="alto form-control cc-exp" value="<?php echo $factura->receptor_id_cliente;?>" placeholder="receptor_id_cliente" autocomplete="cc-exp" >
                            </div>
                        </div>

                        <div class="col-1">
                          <div class="form-group">
                            <label for="email">Folio:</label>
                            <input type="text" class="form-control-sm alto form-control" id="factura_folio" name="factura_folio" value="<?php echo $factura->factura_id?>">
                          </div>
                       </div>
                       <div class="col-1">
                         <div class="form-group">
                           <label for="email">Serie:</label>
                           <input type="text" class="form-control-sm alto form-control" id="factura_serie" name="factura_serie" value="<?php echo $factura->factura_id?>">
                         </div>
                      </div>
                       <div class="col-2">
                        
                         
                      </div>
                      <div class="col-2">
                        
                     </div>
                     <div class="col-1">
                       <!--div class="form-group">
                         <label for="pagada">Pagada:</label>
                         <select class="form-control-sm alto form-control" name="pagada" id="pagada">
                           <option value="SI">SI</option>
                           <option value="NO">NO</option>
                         </select>


                     </div-->
                    </div>
                    </div>

                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="receptor_email">Email:</label>
                                  <input id="receptor_email" name="receptor_email" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_email;?>" placeholder="Email" autocomplete="cc-exp">

                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="receptor_RFC">RFC:</label>
                                  <input id="receptor_RFC" name="receptor_RFC" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_RFC;?>" placeholder="RFC" autocomplete="cc-exp">

                              </div>
                          </div>


                          <div class="col-1">

                             <div class="form-group">
                              <label for="factura_moneda">Moneda:</label>
                              <input type="text" class="form-control-sm alto form-control" id="factura_moneda" name="factura_moneda" value="<?php echo $factura->factura_moneda;?>" >
                            </div>
                            
                         </div>
                         <div class="col-1">
                          <div class="form-group">
                           <label for="email">CP:</label>
                           <input type="text" class="form-control-sm alto form-control" id="fatura_lugarExpedicion" name="fatura_lugarExpedicion" value="<?php echo $emisor->cp;?>">
                         </div>
                           
                        </div>
                        <div class="col-2">
                          
                       </div>
                      </div>


                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="email">Dirección:</label>
                                  <input id="receptor_direccion" name="receptor_direccion" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_direccion;?>" placeholder="Razon social" autocomplete="cc-exp">

                              </div>
                          </div>

                          <div class="col-2">
                            <div class="form-group">
                          <label for="email">Fecha:</label>
                          <input type="text" class="form-control-sm alto form-control" id="factura_fecha" name="factura_fecha" value="<?php echo date('Y-m-d H:i:s');?>">
                        </div>
                              
                          </div>


                         

                         <div class="col-2">
                            <div class="form-group">
                          <label for="email">Fecha conclucion:</label>
                          <input type="text" class="form-control-sm alto form-control" id="fecha_conclucion" name="fecha_conclucion" value="<?php echo $factura->fecha_conclucion;?>">
                        </div>
                         </div>


                      </div>

                      <div class="row">

                        
                        
                      </div>



<!--hr/>
<div><label>Agregar relaciones de CFDI</label></div>
                      <div class="row p-3 mb-2 bg-light text-dark" >

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">Tipo relación:</label>
                          <select class="form-control-sm form-control" id="id_relacion">
                           <option value="01">01.-nota de credito de los documentos</opcion>
                           <option value="02">02.-nota de debito de los documtntos relacionados</opcion>
                           <option value="03">03.-devoclución de mercancía sobre facturas o traslados</opcion>
                           <option value="04">04.-sustituto de los CFDI previos</opcion>
                           <option value="05">05.-traslados de mercancías facturados previamente</opcion>
                           <option value="06">06.-factura generada por los traslados previos</opcion>
                           <option value="07">07.-CFDI por aplicación de anticipo</opcion>
                           <option value="08">08.-factura generada por pagos en parcialidades</opcion>
                           <option value="09">09.-factura generada por pagos diferidos</opcion>
                          </select>
                      </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">UUID:</label>
                          <input id="uuid" name="uuid" type="text" class="form-control-sm alto form-control cc-exp" value="" placeholder="UUID" autocomplete="cc-exp">
                      </div>
                      </div>

                      <div class="col-4">
                        <div class="form-group">
                          <label for="comentario">Opción:</label><br/>
                          <button type="button" id="agregar_relacion" class="btn btn-outline-success">Agregar relación</button>
                      </div>
                      </div>

                      </div>

                      <div class="row" id="relacionados">
                        <div class="col-12">
                        </div>
                      </div>






<hr/-->

            <div class="col-8">
                      <div class="" align="right">
                        <!--a href="<?php echo base_url()?>index.php/Fatura/conceptos/<?php echo $factura->facturaID?>"-->
                          <button id="payment-button" type="submit" class="btn  btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar y Siguiente</span>

                          </button>
                        <!--/a-->
                      </div>


                      </div>


                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>RFC</th>
        <th>Razon social</th>
        <!--th>Email</th-->
        <th>Nombre</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      <?php if(is_array($clientes)):?>
      <?php foreach($clientes as $row):?>
        <tr id="<?php echo $row->id;?>">
          <td><?php echo $row->rfc;?></td>
          <td><?php echo $row->razon_social;?></td>
          <!--td><?php echo $row->email;?></td-->
          <td><?php echo $row->nombre;?></td>

          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_cliente(<?php echo $row->id;?>)" data-dismiss="modal" id="<?php echo $row->id;?>" data-id="<?php echo $row->id;?>">seleccionar</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody>
  </table>
        </div>
      </div>

    </div>
  </div>
</div>