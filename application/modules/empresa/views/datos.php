<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>
<script>
    menu_activo = "empresa";
$("#menu_empresa_datos").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/empresa/guarda_datos";
  ajaxJson(url,{"nombre":$("#nombre").val(),
                "rfc":$("#rfc").val(),
                "razon_social":$("#razon_social").val(),
                "calle":$("#calle").val(),
                "numero":$("#numero").val(),
                "colonia":$("#colonia").val(),
                "cp":$("#cp").val(),
                "municipio":$("#municipio").val(),
                "ciudad":$("#ciudad").val(),
                "estado":$("#estado").val(),
                "pais":$("#pais").val(),
                "telefono":$("#telefono").val(),
                "email":$("#email").val(),
                //"datos_bancarios":$("#datos_bancarios").val(),
                "regimen":$("#regimen").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/empresa/datos");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Datos Empresa</strong>
            Ultima actualización: <?php echo $row->fecha_actualizacion;?>
        </div>
        <div class="card-body" style="background:#fff">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                  <input id="nombre" name="nombre" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->nombre;?>" placeholder="Nombre" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">RFC</label>
                                  <input id="rfc" name="rfc" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->rfc;?>" placeholder="RFC" autocomplete="cc-exp" style="width:110px">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                           <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                  <input id="telefono" name="telefono" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->telefono;?>" placeholder="Teléfono" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Email</label>
                                  <input id="email" name="email" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->email;?>" placeholder="Email" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                      </div>

                      <div class="row">
                          
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Razón Social</label>
                                <input id="razon_social" name="razon_social" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->razon_social;?>" placeholder="Razón Social" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Régimen</label>
                                  <select class="form-control-sm form-control" name="regimen" id="regimen">
                                       <?php if($row->regimen == "601" ){?>
                                       <option value="601" selected>601.-General de ley personas morales</option>
                                       <?php }else{?>
                                      <option value="601">601.-General de ley personas morales</option>
                                       <?php }?>
                                       <?php if($row->regimen == "603" ){?>
                                       <option value="603" selected>603.-personas morales con fines no lucrativos</option>
                                       <?php }else{?>
                                      <option value="603">603.-personas morales con fines no lucrativos</option>
                                       <?php }?>
                                       <?php if($row->regimen == "603" ){?>
                                       <option value="605" selected>603.-sueldos y salarios e ingresos asimilados a salarios</option>
                                       <?php }else{?>
                                       <option value="605">605.-sueldos y salarios e ingresos asimilados a salarios</option>
                                       <?php }?>
                                       <?php if($row->regimen == "606" ){?>
                                       <option value="606" selected>606.-arrendamiento</option>
                                       <?php }else{?>
                                       <option value="606">606.-arrendamiento</option>
                                       <?php }?>
                                       <?php if($row->regimen == "608" ){?>
                                       <option value="608" selected>608.-demás ingresos</option>
                                       <?php }else{?>
                                       <option value="608">608.-demás ingresos</option>
                                       <?php }?>
                                       <?php if($row->regimen == "621" ){?>
                                       <option value="621" selected>621.-incorporación fiscal</option>
                                       <?php }else{?>
                                       <option value="621">621.-incorporación fiscal</option>
                                       <?php }?>
                                       <?php if($row->regimen == "622" ){?>
                                       <option value="622" selected>622.-Actividad Agrícolas, ganaderas, silvícolas y pesqueras</option>
                                       <?php }else{?>
                                       <option value="622">622.-Actividad Agrícolas, ganaderas, silvícolas y pesqueras</option>
                                       <?php }?>
                                  </select >

                              </div>
                          </div>
                      </div>




                      <div class="row">
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Calle</label>
                                  <input id="calle" name="calle" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->calle;?>" placeholder="Calle" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="numero" name="numero" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->numero;?>" placeholder="Número" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Colonia</label>
                                <input id="colonia" name="colonia" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->colonia;?>" placeholder="Colonia" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">CP</label>
                                <input id="cp" name="cp" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->cp;?>" placeholder="CP" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Municipio</label>
                                  <input id="municipio" name="municipio" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->municipio;?>" placeholder="Municipio" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Ciudad</label>
                                  <input id="ciudad" name="ciudad" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->ciudad;?>" placeholder="Ciudad" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Estado</label>
                                <input id="estado" name="estado" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->estado;?>" placeholder="Estado" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">País</label>
                                <input id="pais" name="pais" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->pais;?>" placeholder="País" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>


                      <div class="row">
                         
                          <div class="col-6">
                            <!--div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Datos Bancarios</label>
                                <input id="datos_bancarios" name="datos_bancarios" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->tados_bancarios;?>" placeholder="Datos Bancarios" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div-->
                          </div>
                      </div>

                      <br/>

                      <div align="right">
                          <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                          <div align="center">
                           <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
