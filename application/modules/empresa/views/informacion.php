<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>
<script>
      menu_activo = "empresa";
$("#menu_empresa_informacion").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/empresa/guarda_informacion";
  ajaxJson(url,{"mision":$("#mision").val(),
                "vision":$("#vision").val(),
                "eslogan":$("#eslogan").val(),
                "latitud":$("#latitud").val(),
                "longitud":$("#longitud").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/empresa/informacion");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Información Empresa</strong>
            Ultima actualización: <?php echo $row->fecha_actualizacion;?>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Eslogan</label>
                              <input id="eslogan" name="eslogan" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->eslogan;?>" placeholder="Descripción" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                      </div>

                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Misión</label>
                                  <textarea class="form-control-sm form-control" id="mision" name="mision" rows="4" cols="50" placeholder="Misión"><?php echo $row->mision;?></textarea>

                              </div>
                          </div>

                      </div>

                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Visión</label>
                                  <textarea class="form-control-sm form-control" id="vision" name="vision" rows="4" cols="50" placeholder="Visión"><?php echo $row->vision;?></textarea>

                              </div>
                          </div>

                      </div>


                      <div class="row">
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Latitud</label>
                              <input id="latitud" name="latitud" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->latitud;?>" placeholder="latitud" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Longitud</label>
                              <input id="longitud" name="longitud" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->longitud;?>" placeholder="longitud" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                      </div>


                    <div class="row">
                        <div class="col-6">

                      <div align="right">
                          <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Editar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                          <div align="center">
                           <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                          </div>
                      </div>

                       </div>
                        </div>


                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
