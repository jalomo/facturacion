<script>
       menu_activo = "empresa";
$("#menu_empresa_logo").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url_sis ="<?php echo base_url()?>index.php/empresa/edita_logo";

  // Get form
        var form = $('#alta_usuario')[0];

		// Create an FormData object
        var data = new FormData(form);

  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
              //  $("#result").text(data);
                console.log("SUCCESS : ", data);
              //  $("#btnSubmit").prop("disabled", false);
                exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/empresa/logo");
                $("#enviar").show();
                $("#cargando").hide();

            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
                //$("#btnSubmit").prop("disabled", false);
                exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                $("#enviar").show();
                $("#cargando").hide();

            }
        });
});

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Logo</strong>

        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario" enctype="multipart/form-data">





                      <div class="row">
                        <div class="col-6">
                          <div class="control-group">
                            <label class="control-label" for="basicinput">Logo</label>
                            <div class="controls">
                              <input type="file" id="image" name="image" placeholder="" class="span8" required="">
                              <!--span class="help-inline">500px X 500px</span-->
                            </div>
                          </div>
                        </div>

                        <div class="col-6">
                          <img src="<?php echo base_url().'/'.$row->logo;?>" width="100px"/>
                        </div>


                      </div>

                      <br/>
                      <br/>


                      <div align="right">
                          <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Editar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                          <div align="center">
                           <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
