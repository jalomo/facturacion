<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Empresa extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function datos(){
      $titulo['titulo'] = "Datos" ;
      $titulo['titulo_dos'] = "Empresa" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'datos');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('empresa/datos', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }

    public function guarda_datos(){
      $this->form_validation->set_rules('nombre', 'nombre', 'required');
      $this->form_validation->set_rules('rfc', 'rfc', 'required');
      $this->form_validation->set_rules('razon_social', 'razon_social', 'required');
      $this->form_validation->set_rules('calle', 'calle', 'required');
      $this->form_validation->set_rules('numero', 'numero', 'required');
      $this->form_validation->set_rules('colonia', 'colonia', 'required');
      $this->form_validation->set_rules('cp', 'cp', 'required');
      $this->form_validation->set_rules('municipio', 'municipio', 'required');
      $this->form_validation->set_rules('ciudad', 'ciudad', 'required');
      $this->form_validation->set_rules('estado', 'estado', 'required');
      $this->form_validation->set_rules('pais', 'pais', 'required');
      $this->form_validation->set_rules('telefono', 'telefono', 'required');
      $this->form_validation->set_rules('email', 'email', 'required');
      //$this->form_validation->set_rules('datos_bancarios', 'datos_bancarios', 'required');
      //$this->form_validation->set_rules('regimen', 'regimen', 'required');
      $response = validate($this);

      if($response['status']){

        $data['nombre'] = $this->input->post('nombre');
        $data['rfc'] = $this->input->post('rfc');
        $data['razon_social'] = $this->input->post('razon_social');
        $data['calle'] = $this->input->post('calle');
        $data['numero'] = $this->input->post('numero');
        $data['colonia'] = $this->input->post('colonia');
        $data['cp'] = $this->input->post('cp');
        $data['municipio'] = $this->input->post('municipio');
        $data['ciudad'] = $this->input->post('ciudad');
        $data['estado'] = $this->input->post('estado');
        $data['pais'] = $this->input->post('pais');
        $data['telefono'] = $this->input->post('telefono');
        $data['email'] = $this->input->post('email');
        $data['tados_bancarios'] = $this->input->post('datos_bancarios');
        $data['regimen'] = $this->input->post('regimen');
        $data['fecha_actualizacion'] = date('Y-m-d H:i:s');

        $this->Mgeneral->update_table_row('datos',$data,'id',1);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
    }



    public function informacion(){
      $titulo['titulo'] = "Empresa" ;
      $titulo['titulo_dos'] = "Información" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('empresa/informacion', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }

    public function guarda_informacion(){
      $this->form_validation->set_rules('eslogan', 'eslogan', 'required');
      $this->form_validation->set_rules('mision', 'mision', 'required');
      $this->form_validation->set_rules('vision', 'vision', 'required');
      $this->form_validation->set_rules('latitud', 'latitud', 'required');
      $this->form_validation->set_rules('longitud', 'longitud', 'required');

      $response = validate($this);

      if($response['status']){

        $data['eslogan'] = $this->input->post('eslogan');
        $data['mision'] = $this->input->post('mision');
        $data['vision'] = $this->input->post('vision');
        $data['latitud'] = $this->input->post('latitud');
        $data['longitud'] = $this->input->post('longitud');
        $data['fecha_actualizacion'] = date('Y-m-d H:i:s');

        $this->Mgeneral->update_table_row('informacion',$data,'id',1);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
    }


    public function logo(){
      $titulo['titulo'] = "Empresa" ;
      $titulo['titulo_dos'] = "Logo" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('empresa/logo', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }

    public function edita_logo(){

      var_dump($_FILES['image']['name']);
      $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['image']['name']);
      $path_to_save = 'statics/logo/';
      if(!file_exists($path_to_save)){
        mkdir($path_to_save, 0777, true);
      }
      move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save.$name);
      $data['logo'] = $path_to_save.$name;
     $this->Mgeneral->update_table_row('informacion',$data,'id',1);

    }
  }
