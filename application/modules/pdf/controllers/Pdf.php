<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Pdf extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url','astillero'));

      date_default_timezone_set('America/Mexico_City');

       //if($this->session->userdata('id')){}else{redirect('login/');}

  }

  public function cotizacion($id_factura){
    $datos['emisor'] = $this->Mgeneral->get_row('id',1,'datos');
    $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
    $datos['factura_fcdi_relacionados'] = $this->Mgeneral->get_result('relacion_idFactura',$id_factura,'factura_cfdi_relacionados');
    $datos['conceptos'] = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
    $datos['facturaID'] = $id_factura;
    $this->load->view('pdf/cotizacion', $datos, false);
  }

  public function orden_venta($id_orden = null){
    

    $comentarios = $this->load->view('pdf/comentarios', '', TRUE);
    $contenido = $this->load->view('pdf/orden_venta', array('comentarios'=>$comentarios), TRUE);
    $this->load->view('pdf/main', array('contenido'=>$contenido));
  }

  public function imprimir(){
    $this->load->view('pdf/imprimir', '', false);
  }

  public function servicio($id_servicio){
    $data['id_servicio'] = $id_servicio;
    $data['servicio'] = $this->Mgeneral->get_row('servicioId',$id_servicio,'servicios');
    $data['id_cliente'] = $data['servicio']->servicioIdCliente;
    $data['proveedor'] = $this->Mgeneral->get_row('id',$data['servicio']->servicioIdCliente,'clientes');
    $data['rows'] = $this->Mgeneral->get_table('servicios');
    $data['comentarios'] = $this->load->view('pdf/comentarios', '', TRUE);
    $contenido = $this->load->view('pdf/servicios', $data, TRUE);
    $this->load->view('pdf/main', array('contenido'=>$contenido));

  }


  public function certificado($id_servicio){
    $data['id_servicio'] = $id_servicio;
    $data['servicio'] = $this->Mgeneral->get_row('servicioId',$id_servicio,'servicios');
    $data['id_cliente'] = $data['servicio']->servicioIdCliente;
    $data['proveedor'] = $this->Mgeneral->get_row('id',$data['servicio']->servicioIdCliente,'clientes');
    $data['rows'] = $this->Mgeneral->get_table('servicios');
    $data['comentarios'] = $this->load->view('pdf/comentarios', '', TRUE);
    $contenido = $this->load->view('pdf/certificado', $data, TRUE);
    $this->load->view('pdf/main', array('contenido'=>$contenido));

  }
}