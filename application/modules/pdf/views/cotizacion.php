<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>
<?php
$piePagina = "Orden de Servicio";
$colorFondo = "#78e08f";
$colorTexto = "#000";
$charsPerLineBase = 111;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$tipoComprobante ="este es el tipo de comprovante";
$cce11 = "otro campo";
?>
<style type="text/css">


.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}
.text-justificado{
    text-align: justify;
  text-justify: inter-word;
}

.text-muted{color:#777;}

*{
    font-size: 7pt;
    line-height: 125%;
}
.font-large{
    font-size: 12pt;
}
.font-medium,
.font-medium *{
    font-size: 9pt;
}
.font-system{
    font-family:courier;
    line-height: 110%;
}

p{
    margin:0;
}
h1{
    margin:0;
}
h2{
    margin:0;
}
h5{
    margin:0;
}
table{
    border-spacing: 0;
    border-collapse: collapse;
}

.spacing{
    height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
    margin-top:0.5mm;
}
.spacing-top-1mm{
    margin-top:1mm;
}
.spacing-top-2mm{
    margin-top:2mm;
}
.spacing-top-3mm{
    margin-top:3mm;
}
.spacing-bottom{
    margin-top:1mm;
}
.spacing-bottom-2mm{
    margin-bottom:2mm;
}


.100p{
    width:100%;
}
.99p{
    width:99%;
}
.80p{
    width:80%;
}
.75p{
    width:75%;
}
.60p{
    width:60%;
}
.50p{
    width:50%;
}
.40p{
    width:40%;
}
.33p{
    width:33%;
}
.34p{
    width:34%;
}
.25p{
    width:25%;
}

th,
.bg-gray{
    background: <?php echo $colorFondo; ?>;
    color: <?php echo $colorTexto; ?>;
    font-weight: bold;
}
.cell-padding,
.cell-padding-narrow,
.cell-padding-big,
.cell-padding-h {
    padding-left: 1.6mm;
    padding-right: 1.6mm;
}
.cell-padding,
.cell-padding-v {
    padding-top: 1.3mm;
    padding-bottom: 1.3mm;
}
.cell-padding-narrow{
    padding-top: 1mm;
    padding-bottom: 1mm;
}
.cell-padding-big{
    padding-top: 2.6mm;
    padding-bottom: 2.6mm;
}

.border-gray{
    border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
    border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
    border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
    border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
    padding-top: 1.2mm;
    padding-bottom: 0mm;
}
table.productos tr.last-row td {
    padding-bottom: 1.1mm;
}
table.sat-info{

}
table.sat-info h5{
    line-height: 120%;
}

thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }


</style>

<table class="page-head">
        <tr>
            <td style="width:28%;text-align:center">
                <img src="<?php echo base_url()?>statics/tema/images/logo.png" style="height:36px">
        </td>
            <td style="width:43%;">
                <h2 style="margin-top:2mm; color:#353b48" class="font-large text-center">EL ASTILLERO HIGIENE AMBIENTAL S.A. DE C.V.</h2>

                <p style="margin-top:1mm; color:#353b48" class="text-center">www.fumigacioneselastillero.com</p>

            </td>
            <td style="width:1%"></td>
            <td style="width:28%">
        <table>
          <tr>
            <td style="width:100%" class="cell-padding ">
              <table>
                <tr>
                  <td style="width:100%"><p><span><?php echo 'Av. Tecoman, Rinconada San Pablo'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo '256, CP:28050'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Colima Col."; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Tel: (01 312)158 14 00"; ?></span></p></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
            </td>
        </tr>
    </table>

<table>
       <tr>
          <td style="width:100%"><p><b>Cotización:<?php echo $factura->factura_id?></b> <span></span></p></td>
          <td style="width:100%"><p><b>Fecha: <?php echo $factura->factura_fecha?></b> <span></span></p></td>
          <td style="width:100%"><p><b>Fecha conclución:<?php echo $factura->fecha_conclucion?></b> <span></span></p></td>
       </tr>
                  
 </table>

<table class="spacing-top-2mm">
        <tr>
            <td style="width:49.5%;vertical-align:top">
                <table>
                    <!--tr><th style="width:100%" class="text-center cell-padding-v">Atención a:</th></tr-->
                    <tr>
                        <td style="width:100%" class="cell-padding ">
                            <table>
                                <tr>
                                    <td style="width:100%"><p><b>Razón Social:</b> <span><?php echo $factura->receptor_nombre?></span></p></td>
                                </tr>
                                <tr>
                                    <td style="width:100%"><p><b>RFC:</b> <span><?php echo $factura->receptor_RFC?></span></p></td>
                                </tr>
                                <tr>
                                    <!--td style="width:100%"><p><b>Régimen Fiscal:</b> <span><?php echo "FISICA"; ?></span></p></td-->
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:1%">
            </td>
            <td style="width:49.5%;vertical-align:top">
                <table>
                    <tr>
              <td style="width:100%" class="cell-padding ">
                            <table>
                                <tr>
                                    <td style="width:100%"><p><b>Email:</b> <span>email@email.com</span></p></td>
                                </tr>
                                <tr>
                                    <td style="width:100%"><p><b>Tel:</b> <span>3122345687</span></p></td>
                                </tr>
                                <tr>
                                    <td style="width:100%"></td>
                                </tr>


                            </table>
                        </td>
            </tr>
                    <tr>
                        <td style="width:100%" class="cell-padding ">
                            <table>
                                
                                <!--tr>
                                    <td style="width:100%"><p><b>L.A.B.:</b> <span><?php echo "hola"; ?></span></p></td>
                                </tr-->

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


<table class="productos" class="spacing-top-2mm">
        <thead>
            <tr><th style="width:100%" colspan="9" class="text-center cell-padding-v">CONCEPTOS</th></tr>
            <tr>
                <th class="cell-padding " style="width: 10%">Clave</th>
                <th class="cell-padding" style="width:10%">Cantidad</th>
                <th class="cell-padding" style="width: 20%">Descripción</th>
                <th class="cell-padding" style="width:10%">Presentación</th>
                <th class="cell-padding " style="width:8%">%IVA</th>
                <th class="cell-padding " style="width:8%">%IEPS</th>

                <th class="cell-padding " style="width:10%">Precio</th>
                <th class="cell-padding " style="width:10%">Descuento</th>
                <th class="cell-padding " style="width:10%">Importe</th>
            </tr>
        </thead>
        <tbody>

           <?php foreach($conceptos as $conce):?>
            <?php $producto_aux = $this->Mgeneral->get_row('productoId',$conce->id_producto_servicio_interno,'productos');?>
             <tr>
                <td style="width:10%" class="cell-padding-narrow "><?php echo $producto_aux->productoReferencia;?></td>
                <td style="width:10%" class="cell-padding-narrow "><?php echo $conce->concepto_cantidad;?></td>
                <td style="width:20%"class="cell-padding-narrow "><?php echo $conce->concepto_nombre;?></td>
                <td style="width:10%" class="cell-padding-narrow "><?php echo nombre_medida($conce->concepto_unidad);?></td>
                <td style="width:8%" class="cell-padding-narrow text-right">$<?php echo $conce->concepto_importe*.16;?></td>

                <td style="width:8%" class="cell-padding-narrow  text-right"><?php echo $conce->importe_ieps; ?></td>

                <td style="width:10%" class="cell-padding-narrow text-right">$<?php echo $conce->concepto_precio;?></td>
                <td style="width:10%" class="cell-padding-narrow text-right"></td>
                <td style="width:10%" class="cell-padding-narrow text-right">$<?php echo $conce->concepto_importe;?></td>
             </tr>

             <tr colspan="9">
                <tr><th style="width:90%" colspan="9" class=""></th></tr>
               
             </tr>



           
          <?php endforeach;?>

           


            

    


        </tbody>
    </table>



<table class="spacing-top-2mm">
    <tr>
      <td style="width:74%">
        <table>

           <tr>
            <td style="width:18%" class="">Atención: Precios sujetos a cambios sin previo aviso.</td>

            
          </tr>

           <tr>
            <td style="width:18%" class="">Esperando ser favorecidos con su pedido, estamos a sus ordenes
para cualquier información al respecto.</td>
          </tr>
          <tr>
           <td style="width:74%" class="text-center">Atentamente: <br/> <br/> <br/> <br/> <br/></td>
          </tr>

          <tr>
           <td style="width:74%" class="text-center">_______________________________________</td>
          </tr>

          <tr>
           <td style="width:74%" class="text-center">ADRIANA ANABELL LOZA MAGAÑA</td>
          </tr>
         
          <!--tr>
            <th style="width:18%" class="cell-padding-narrow">Forma de Pago</th>
            <td style="width:82%" class="cell-padding-narrow border-gray">dfdfd</td>
          </tr>
          <tr>
            <th style="width:18%" class="cell-padding-narrow">Método de Pago</th>
            <td style="width:82%" class="cell-padding-narrow border-gray">metodo pago</td>
          </tr-->
          
        </table>
      </td>
      <td style="width:1%">
      </td>
      <td style="width:25%">
        <table>
          <tr>
            <th style="width:45%" class="cell-padding-narrow">Subtotal</th>
            <td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $this->Mgeneral->factura_subtotal($facturaID);?></td>
          </tr>
          <!--tr>
            <th style="width:45%" class="cell-padding-narrow">Descuento</th>
            <td style="width:55%" class="text-right cell-padding-narrow border-gray">$</td>
          </tr-->
          <tr>
            <th style="width:45%" class="cell-padding-narrow">IVA</th>
            <td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $this->Mgeneral->factura_iva_total($facturaID);?></td>
          </tr>
          <!--tr>
            <th style="width:45%" class="cell-padding-narrow">IEPS</th>
            <td style="width:55%" class="text-right cell-padding-narrow border-gray">$</td>
          </tr-->
          <tr>
            <th style="width:45%" class="cell-padding-narrow">TOTAL</th>
            <td style="width:55%" class="text-right text-bold cell-padding-narrow border-gray">$<?php echo $this->Mgeneral->factura_subtotal($facturaID) + $this->Mgeneral->factura_iva_total($facturaID);?></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>




<?php 
require_once $this->config->item('url_real').'mpdf/vendor/autoload.php';
    //try {
        $html = ob_get_clean();
        //ob_clean();

        //$mpdf = new \mPDF('utf-8', 'A4-P');
        //$mpdf = new \Mpdf\Mpdf(['mode' => 'c']);
        $mpdf = new \Mpdf\Mpdf(['tempDir' => $this->config->item('url_real') . 'temp1/']);

        $mpdf = new \Mpdf\Mpdf([ 'format' => 'A4-P','debug' => TRUE]);
        
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);

        $direccion = 'statics/cotizaciones/cotizacion_'.date("Y-m-d H:i:s").'.pdf';
        
        //$mpdf->Output();
        $mpdf->Output($direccion, 'F'); //guarda a ruta
        $mpdf->Output('cotizacion_1.pdf','I'); //muestra a usuario

        //Guardamos el documento
        $pdf = array(
            'pdf_evaluacion_SOES_1' => $direccion,
            'fecha_actualizacion' => date("Y-m-d H:i:s"),
        );

       

 ?>