
<?php
/**
 * Template para generar PDF de CFDI 3.3
 * Soporta complementos de Pagos 1.0 y Comercio Exterior 1.1
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.1.0 (10/05/2018)
 */
$piePagina = "Orden de Servicio";
$colorFondo = "#78e08f";
$colorTexto = "#000";
$charsPerLineBase = 111;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$tipoComprobante ="este es el tipo de comprovante";
$cce11 = "otro campo";
?>
<style type="text/css">


.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

.text-muted{color:#777;}

*{
    font-size: 7pt;
    line-height: 125%;
}
.font-large{
    font-size: 12pt;
}
.font-medium,
.font-medium *{
    font-size: 9pt;
}
.font-system{
    font-family:courier;
    line-height: 110%;
}

p{
    margin:0;
}
h1{
    margin:0;
}
h2{
    margin:0;
}
h5{
    margin:0;
}
table{
    border-spacing: 0;
    border-collapse: collapse;
}

.spacing{
    height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
    margin-top:0.5mm;
}
.spacing-top-1mm{
    margin-top:1mm;
}
.spacing-top-2mm{
    margin-top:2mm;
}
.spacing-top-3mm{
    margin-top:3mm;
}
.spacing-bottom{
    margin-top:1mm;
}
.spacing-bottom-2mm{
    margin-bottom:2mm;
}


.100p{
    width:100%;
}
.99p{
    width:99%;
}
.80p{
    width:80%;
}
.75p{
    width:75%;
}
.60p{
    width:60%;
}
.50p{
    width:50%;
}
.40p{
    width:40%;
}
.33p{
    width:33%;
}
.34p{
    width:34%;
}
.25p{
    width:25%;
}

th,
.bg-gray{
    background: <?php echo $colorFondo; ?>;
    color: <?php echo $colorTexto; ?>;
    font-weight: bold;
}
.cell-padding,
.cell-padding-narrow,
.cell-padding-big,
.cell-padding-h {
    padding-left: 1.6mm;
    padding-right: 1.6mm;
}
.cell-padding,
.cell-padding-v {
    padding-top: 1.3mm;
    padding-bottom: 1.3mm;
}
.cell-padding-narrow{
    padding-top: 1mm;
    padding-bottom: 1mm;
}
.cell-padding-big{
    padding-top: 2.6mm;
    padding-bottom: 2.6mm;
}

.border-gray{
    border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
    border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
    border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
    border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
    padding-top: 1.2mm;
    padding-bottom: 0mm;
}
table.productos tr.last-row td {
    padding-bottom: 1.1mm;
}
table.sat-info{

}
table.sat-info h5{
    line-height: 120%;
}

thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }


</style>

<page backtop="<?php echo $pageMargin ?>mm" backbottom="<?php echo $bottomPageMargin ?>mm" backleft="<?php echo $pageMargin ?>mm" backright="<?php echo $pageMargin ?>mm">
    <page_footer>
        <table style="padding-bottom:<?php echo $footerMargin ?>mm">
            <tr>
                <td style="padding-left:<?php echo $pageMargin-($footerDefaultMargin/2) ?>mm" class="75p">
                    <?php if(!empty($piePagina)) echo $piePagina ?>
                </td>
                <td style="padding-right:<?php echo $pageMargin-$footerDefaultMargin ?>mm" class="25p text-right">Página [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>

    <table class="page-head">
        <tr>
            <td style="width:28%;text-align:center">
                <img src="<?php echo base_url()?>statics/tema/images/logo.png" style="height:36px">
        </td>
            <td style="width:43%;">
                <h2 style="margin-top:2mm; color:#353b48" class="font-large text-center">EL ASTILLERO HIGIENE AMBIENTAL S.A. DE C.V.</h2>

                <p style="margin-top:1mm; color:#353b48" class="text-center">www.fumigacioneselastillero.com</p>

            </td>
            <td style="width:1%"></td>
            <td style="width:28%">
        <table>
          <tr>
            <td style="width:100%" class="cell-padding border-gray">
              <table>
                <tr>
                  <td style="width:100%"><p><span><?php echo 'Av. Tecoman, Rinconada San Pablo'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo '256, CP:28050'; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Colima Col."; ?></span></p></td>
                </tr>
                <tr>
                  <td style="width:100%"><p><span><?php echo "Tel: (01 312)158 14 00"; ?></span></p></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
            </td>
        </tr>
    </table>





    <table class="spacing-top-2mm">
        <tr>
            <td style="width:49.5%;vertical-align:top">
                <table>
                    <tr><th style="width:100%" class="text-center cell-padding-v">CLIENTE:</th></tr>
                    <tr>
                        <td style="width:100%" class="cell-padding border-gray">
                            <table>
                                <tr>
                                    <td style="width:100%"><p><b>Razón Social:</b> <span><?php echo "Gregorio Jalomo Larios"; ?></span></p></td>
                                </tr>
                                <tr>
                                    <td style="width:100%"><p><b>RFC:</b> <span><?php echo "JALG860311HCMLRR07"; ?></span></p></td>
                                </tr>
                                <tr>
                                    <td style="width:100%"><p><b>Régimen Fiscal:</b> <span><?php echo "FISICA"; ?></span></p></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:1%">
            </td>
            <td style="width:49.5%;vertical-align:top">
                <table>
                    <tr>
              <td style="width:100%" class="cell-padding border-gray">
                            <table>
                                <tr>
                                    <td style="width:100%"><p><b>REFERENCIA:</b> <span><?php echo "GREGORIO JALOMO"; ?></span></p></td>
                                </tr>


                            </table>
                        </td>
            </tr>
                    <tr>
                        <td style="width:100%" class="cell-padding border-gray">
                            <table>
                                <tr>
                                    <td style="width:100%"><p><b>CERTIFICADO:</b> <span><?php echo "ABCDE"; ?></span></p></td>
                                </tr>
                                <tr>
                                    <td style="width:100%"><p><b>Fecha :</b> <span><?php echo "26-10-2019"; ?></span></p></td>
                                </tr>
                                <!--tr>
                                    <td style="width:100%"><p><b>L.A.B.:</b> <span><?php echo "hola"; ?></span></p></td>
                                </tr-->

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>







  <table class="productos" class="spacing-top-2mm">
        <thead>
            <tr><th style="width:100%" colspan="8" class="text-center cell-padding-v">CONCEPTOS</th></tr>
            <tr>
                <th class="cell-padding" style="width: 30%">Producto</th>
                <th class="cell-padding" style="width:10%">Actividad</th>
                <th class="cell-padding" style="width: 10%">Lugar</th>
                <th class="cell-padding" style="width:10%">Plaga</th>
                <th class="cell-padding text-right" style="width:10%">Dosis</th>
                <th class="cell-padding text-right" style="width:10%">Cantidad</th>

        <th class="cell-padding text-right" style="width:10%">Medida</th>
        <th class="cell-padding text-right" style="width:10%">Metodo</th>
            </tr>
        </thead>
        <tbody>

            <tr>
                <td style="width: 30%" class="cell-padding-narrow border-left"><?php echo "producto"; ?></td>
                <td style="width:10%" class="cell-padding-narrow"><?php echo "actividad"; ?></td>
                <td style="width: 10%" class="cell-padding-narrow"><?php echo "lugar"; ?></td>
                <td style="width:10%" class="cell-padding-narrow"><?php echo "plaga"; ?></td>
                <td style="width:10%" class="cell-padding-narrow text-right"><?php echo "dossis"; ?></td>
                <td style="width:10%" class="cell-padding-narrow text-right "><?php echo "cantidad"; ?></td>
                <td style="width:10%" class="cell-padding-narrow text-right"><?php echo "medida"; ?></td>
                <td style="width:10%" class="cell-padding-narrow text-right"><?php echo "metodo"; ?></td>
      </tr>


    


        </tbody>
    </table>








<?php
if(isset($comentarios)): 
    echo $comentarios; 
 endif; 
?>


    <div class="spacing-top-2mm">
        <p class="text-center text-bold">Este documento es servicio...<?php echo "2134";?></p>
    </div>


</page>



