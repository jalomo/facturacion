<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Servicios extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url','astillero'));

      date_default_timezone_set('America/Mexico_City');

       if($this->session->userdata('id')){}else{redirect('login/');}

  }

  public function configuracion(){
    $data['rows'] = $this->Mgeneral->get_table('roles');
    $titulo['titulo'] = "Servicios" ;
    $titulo['titulo_dos'] = "Servicios" ;
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('servicios/configuracion', $data, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
  }
  public function actualiza_rol(){
    $id_rol = $this->input->post('idRol');
    $data['rolTecnico'] = 1;
    $this->Mgeneral->update_table_row('roles',$data,'rolId',$id_rol);

    $response['status'] = true;

    if($response['status']){

      
    }
  //  echo $response;
  echo json_encode(array('output' => $response));
  }


   public function unidades_productos($id_producto){
    $unidades = $this->Mgeneral->get_pro_uni($id_producto);//$this->Mgeneral->get_unidades_productos2($id_producto,$tipo);
    $html = '<select class="form-control cc-exp form-control-sm" id="saMedida" name="saMedida">';
    if(is_array($unidades)):
        foreach($unidades as $uni):
          
          $html .='<option class="" value="'.$uni.'">'.nombre_medida($uni).'</option> ';
         
        endforeach;
        else:
        $producto_venta = $this->Mgeneral->get_row('productoId',$id_producto,'productos');
        $html .='<option class="" value="'.$producto_venta->unidad_compra.'">'.nombre_medida($producto_venta->unidad_compra).'</option> ';
      endif;
        $html .='</select>';

        echo $html;

  }

  public function lista($id_cliente = null){
    if($id_cliente !=null){
      $data['rows'] = $this->Mgeneral->get_result('servicioIdCliente',$id_cliente,'servicios');
    }else{
      $data['rows'] = $this->Mgeneral->get_table('servicios');
    }
    $titulo['titulo'] = "Servicios" ;
    $titulo['titulo_dos'] = "Servicios" ;
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('servicios/lista', $data, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
  }

  public function crear_servicio(){
    $data['servicioFechaCreacion'] = date('Y-m-d H:i:s');
    //$data['orden_status'] = 1;
    $data['servicio_id'] = get_guid();
    $data['servicioCertificado'] = $data['servicio_id'];
    $data['servicioIDCreador'] = $this->session->userdata('id');
    //$data['orden_id_vendedor'] = $this->session->userdata('id');
    $id_orden = $this->Mgeneral->save_register('servicios', $data);
    $data_actualizar['servicioReferencia'] = date('Y-m-d')."-".$id_orden;
    $this->Mgeneral->update_table_row('servicios',$data_actualizar,'servicioId',$id_orden);

    redirect('servicios/nueva/'.$id_orden);
  }

  public function nueva($id_servicio){

    $data['servicio'] = $this->Mgeneral->get_row('servicioId',$id_servicio,'servicios');
    $data['id_cliente'] = $data['servicio']->servicioIdCliente;
    $data['proveedor'] = $this->Mgeneral->get_row('id',$data['servicio']->servicioIdCliente,'clientes');
    $data['rows'] = $this->Mgeneral->get_table('servicios');

    $data['vehiculos'] = $this->Mgeneral->get_table('almacenes');//$this->Mgeneral->get_result('almacenTipo',1,'almacenes');

    $data['usuarios'] = $this->Mgeneral->get_table('usuarios');
    $data['clientes'] = $this->Mgeneral->get_table('clientes');
    $data['actividades'] = $this->Mgeneral->get_table('actividades');
    $data['lugares_aplicacion'] = $this->Mgeneral->get_table('cliente_lugares');;//$this->Mgeneral->get_table('lugares_aplicacion');
    $data['metodos'] = $this->Mgeneral->get_table('metodos');
    $data['plagas'] = $this->Mgeneral->get_table('plagas');
    $data['productos'] = $this->Mgeneral->get_table('productos');
    $data['medidas'] = $this->Mgeneral->get_table('medidas');
    $data['id_servicio'] = $id_servicio;
    $titulo['titulo'] = "Servicio" ;
    $titulo['titulo_dos'] = "Servicio" ;
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('servicios/nueva', $data, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js')));
  }


  public function selecciona_lugares_clientes($id_cliente){
    $html = '<select class="form-control cc-exp form-control-sm" id="saIdLugar" >';

    $lugares_aplicacion = $this->Mgeneral->get_result('id_cliente',$id_cliente,'cliente_lugares');
    //if(is_object($lugares_aplicacion)){
     foreach($lugares_aplicacion as $row_lugar_aplicacion):
        $html .='<option value="'.$row_lugar_aplicacion->id.'">'.$row_lugar_aplicacion->lugar.' Metros lineales: '.$row_lugar_aplicacion->metros.'</option>';
     endforeach;                  
    //}
    $html.='</select>';
    echo $html;


  }

  public function get_medida_metodo($id_metodo,$id_lugar){
    $metodo = $this->Mgeneral->get_row('metodoId',$id_metodo,'metodos');
    if(is_object($metodo)){
    if($metodo->metodoBoquilla == 1){
      $row = $this->Mgeneral->get_row('id',$id_lugar,'cliente_lugares');
       echo  number_format((($row->metros/2.2) * boquilla_aolicacion($row->id_boquilla)));
    }
    }
  }

  public function guarda_servicio($id_servicio){
    /*$this->form_validation->set_rules('ordenIdProveedor', 'ordenIdProveedor');
    $this->form_validation->set_rules('ordenReferencia', 'ordenReferencia');
    $this->form_validation->set_rules('ordenFechaLimite', 'ordenFechaLimite');
    $this->form_validation->set_rules('ordenComentario', 'ordenComentario');
    $this->form_validation->set_rules('ordenRepresentante', 'ordenRepresentante');
    $this->form_validation->set_rules('ordenTelefono', 'ordenTelefono');

    $response = validate($this);
    */
    $response['status'] = true;

    if($response['status']){

      $data['servicioIdCliente'] = $this->input->post('servicioIdCliente');
      $data['servicioDomicilioCalle'] = $this->input->post('servicioDomicilioCalle');
      $data['servicioDomicilioColonia'] = $this->input->post('servicioDomicilioColonia');
      $data['servicioDomicilioMmunicipio'] = $this->input->post('servicioDomicilioMmunicipio');
      $data['servicioDomicilioEstado'] = $this->input->post('servicioDomicilioEstado');
      $data['servicioDomicilioNumero'] = $this->input->post('servicioDomicilioNumero');
      $data['servicioDomicilioCP'] = $this->input->post('servicioDomicilioCP');
      $data['servicioReferenciaLugar'] = $this->input->post('servicioReferenciaLugar');
      $data['servicioCertificado'] = $this->input->post('servicioCertificado');
      $data['servicioIDCreador'] = $this->input->post('servicioIDCreador');
      $data['servicioFolio'] = $this->input->post('servicioFolio');
      $data['servicioReferencia'] = $this->input->post('servicioReferencia');
      $data['servicioFechaServicio'] = $this->input->post('servicioFechaServicio');
      $data['servicioFechaProximaVisita'] = $this->input->post('servicioFechaProximaVisita');
      $data['servicioComentariosConfidenciales'] = $this->input->post('servicioComentariosConfidenciales');
      $data['servicioComentariosCliente'] = $this->input->post('servicioComentariosCliente');
      $data['servicioIdUsuarioEncargado'] = $this->input->post('servicioIdUsuarioEncargado');
      $data['servicioIdVehiculo'] = $this->input->post('servicioIdVehiculo');
      $data['servicioFechaCreacion'] = $this->input->post('servicioFechaCreacion');

      $this->Mgeneral->update_table_row('servicios',$data,'servicioId',$id_servicio);
    }
  //  echo $response;
  echo json_encode(array('output' => $response));

  }

  public function get_datos_cliente($id_cliente){
    $cliente = $this->Mgeneral->get_row('id',$id_cliente,'clientes');
    echo json_encode($cliente);
  }

  public function get_datos_producto($id_producto){
    $producto = $this->Mgeneral->get_row('productoId',$id_producto,'productos');
    echo json_encode($producto);
  }

  public function guarda_actividad($id_servicio){
    $dosisTexto = $this->input->post("dosisTexto");
    $dosisGet = explode(":",$dosisTexto);
    $servicio_orden = $this->Mgeneral->get_row('servicioId',$id_servicio,'servicios');
    $data['saTipoDosis'] = $dosisGet[0];
    $data['saNombreDosis'] = $dosisGet[1];
    $data['saIdServicio'] = $id_servicio;
    $data['saIdActividad'] = $this->input->post('saIdActividad');
    $data['saIdLugar'] = $this->input->post('saIdLugar');
    $data['saIdPlaga'] = $this->input->post('saIdPlaga');
    $data['saIdProducto'] = $this->input->post('saIdProducto');
    $data['saDosis'] = $this->input->post('saDosis');
    $data['saCantidad'] = $this->input->post('saCantidad');
    $data['saMedida'] = $this->input->post('saMedida');
    $data['saIdMetodo'] = $this->input->post('saIdMetodo');
    $this->Mgeneral->save_register('servicios_actividades', $data);
    $rows = $this->Mgeneral->get_result('saIdServicio',$id_servicio,'servicios_actividades');
    $html_prod = "";
    $html_opciones = '';
    
    foreach($rows as $row):
      if($servicio_orden->status_orden==0){
        $html_opciones = '<td>
                     <!--a href="'.base_url().'index.php/servicios/editar_actividad/'.$row->saId.'" data-saId="'.$row->saId.'" data-nombre="'.nombre_producto($row->saIdProducto).'" data-saIdProducto="'.$row->saIdProducto.'" data-saIdActividad="'.$row->saIdActividad.'" data-saIdLugar="'.$row->saIdLugar.'" data-saIdPlaga="'.$row->saIdPlaga.'" data-saDosis="'.$row->saDosis.'" data-saCantidad="'.$row->saCantidad.'" data-saMedida="'.$row->saMedida.'" data-saIdMetodo="'.$row->saIdMetodo.'" data-toggle="modal" data-target="#exampleModalActividad"   flag="'.$row->saId.'"  class="editar_actividad"><button type="button" class="btn btn-warning">Editar</button></a-->

                      <a href="'.base_url().'index.php/servicios/eliminar_actividad/'.$row->saId.'" flag="'.$row->saId.'" id="delete'.$row->saId.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a>

                    </td>';
    }
      $html_prod .= '<tr id="borrar_'.$row->saId.'">
                    <th scope="row">'.nombre_producto($row->saIdProducto).'</th>
                    <td>'.nombre_actividad($row->saIdActividad).'</td>
                    <td>'.nombre_lugar_aplicacion_cliente($row->saIdLugar).'</td>
                    <td>'.nombre_plaga($row->saIdPlaga).'</td>
                    <th>'.$row->saNombreDosis.'</th>
                    <!--td>'.$row->saCantidad/medida_unidad($row->saMedida).'</td-->
                    <td>'.$row->saCantidad.'</td>
                    <td></td>
                    <td>'.nombre_medida($row->saMedida).'</td>
                    <td>'.nombre_metodo($row->saIdMetodo).'</td>
                    '.$html_opciones.'
                  </tr>';
    endforeach;

    echo '<table class="table">
        <thead>
          <tr>
            <th scope="col">Producto</th>
            <th scope="col">Actividad</th>
            <th scope="col">Lugar</th>
            <th scope="col">Plaga</th>
            <th scope="col">Dosis</th>
            <th scope="col">Cantidad teórica</th>
            <th scope="col">Medida</th>
            <th scope="col">Metodo</th>
            <th scope="col">Opciones</th>
          </tr>
        </thead>
        <tbody>
          '.$html_prod.'
        </tbody>
      </table>';
  }

  public function carga_actividades($id_servicio){
    $servicio_orden = $this->Mgeneral->get_row('servicioId',$id_servicio,'servicios');
    $rows = $this->Mgeneral->get_result('saIdServicio',$id_servicio,'servicios_actividades');
    $html_prod = "";
    $html_opciones = '';
    
    foreach($rows as $row):
      if($servicio_orden->status_orden==0){
        $html_opciones = '<td>
                      <!--a href="'.base_url().'index.php/servicios/editar_actividad/'.$row->saId.'" data-saId="'.$row->saId.'" data-nombre="'.nombre_producto($row->saIdProducto).'" data-saIdProducto="'.$row->saIdProducto.'" data-saIdActividad="'.$row->saIdActividad.'" data-saIdLugar="'.$row->saIdLugar.'" data-saIdPlaga="'.$row->saIdPlaga.'" data-saDosis="'.$row->saDosis.'" data-saCantidad="'.$row->saCantidad.'" data-saMedida="'.$row->saMedida.'" data-saIdMetodo="'.$row->saIdMetodo.'" data-toggle="modal" data-target="#exampleModalActividad"   flag="'.$row->saId.'"  class="editar_actividad"><button type="button" class="btn btn-warning">Editar</button></a-->

                      <a href="'.base_url().'index.php/servicios/eliminar_actividad/'.$row->saId.'" flag="'.$row->saId.'" id="delete'.$row->saId.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a>

                    </td>';
    }
      $html_prod .= '<tr id="borrar_'.$row->saId.'">
                    <th scope="row">'.nombre_producto($row->saIdProducto).'</th>
                    <td>'.nombre_actividad($row->saIdActividad).'</td>
                    <td>'.nombre_lugar_aplicacion_cliente($row->saIdLugar).'</td>
                    <td>'.nombre_plaga($row->saIdPlaga).'</td>
                    <th>'.$row->saNombreDosis.'</th>
                    <!--td>'.$row->saCantidad/medida_unidad($row->saMedida).'</td-->
                    <td>'.$row->saCantidad.'</td>
                    <td></td>
                    <td>'.nombre_medida($row->saMedida).'</td>
                    <td>'.nombre_metodo($row->saIdMetodo).'</td>
                    '.$html_opciones.'
                  </tr>';
    endforeach;

    echo '<table class="table">
        <thead>
          <tr>
            <th scope="col">Producto</th>
            <th scope="col">Actividad</th>
            <th scope="col">Lugar</th>
            <th scope="col">Plaga</th>
            <th scope="col">Dosis</th>
            <th scope="col">Cantidad teórica</th>
            <th scope="col">Cantidad real</th>
            <th scope="col">Medida</th>
            <th scope="col">Metodo</th>
            <th scope="col">Opciones</th>
          </tr>
        </thead>
        <tbody>
          '.$html_prod.'
        </tbody>
      </table>';

  }

  public function editar_actividad($id_actividad){

    //$servicio_orden = $this->Mgeneral->get_row('servicioId',$id_servicio,'servicios');
   // $data['saIdServicio'] = $id_servicio;
    $data['saIdActividad'] = $this->input->post('saIdActividad');
    $data['saIdLugar'] = $this->input->post('saIdLugar');
    $data['saIdPlaga'] = $this->input->post('saIdPlaga');
    $data['saIdProducto'] = $this->input->post('saIdProducto');
    $data['saDosis'] = $this->input->post('saDosis');
    $data['saCantidad'] = $this->input->post('saCantidad');
    $data['saMedida'] = $this->input->post('saMedida');
    $data['saIdMetodo'] = $this->input->post('saIdMetodo');
   
    $this->Mgeneral->update_table_row('servicios_actividades',$data,'saId',$this->input->post('saId'));

  }

  public function eliminar_actividad($id_actividad){
    $this->Mgeneral->delete_row('servicios_actividades','saId',$id_actividad);
  }

  public function clonar_servicio($id_servicio){

    $servicio = $this->Mgeneral->get_row('servicioId',$id_servicio,'servicios');
    $data['servicioFechaCreacion'] = date('Y-m-d H:i:s');
    $data['servicioFechaCreacion'] = date('Y-m-d')."-".$id_servicio;
    $data['servicio_id'] = get_guid();

    $data['servicioIdCliente'] = $servicio->servicioIdCliente;
    $data['servicioDomicilioCalle'] = $servicio->servicioDomicilioCalle;
    $data['servicioDomicilioColonia'] = $servicio->servicioDomicilioColonia;
    $data['servicioDomicilioMmunicipio'] = $servicio->servicioDomicilioMmunicipio;
    $data['servicioDomicilioEstado'] = $servicio->servicioDomicilioEstado;
    $data['servicioDomicilioNumero'] = $servicio->servicioDomicilioNumero;
    $data['servicioDomicilioCP'] = $servicio->servicioDomicilioCP;
    $data['servicioReferenciaLugar'] = $servicio->servicioReferenciaLugar;
    $data['servicioCertificado'] = $servicio->servicioCertificado;
    $data['servicioIDCreador'] = $servicio->servicioIDCreador;
    $data['servicioFolio'] = $servicio->servicioFolio;
    $data['servicioReferencia'] = $servicio->servicioReferencia;
    $data['servicioFechaServicio'] = $servicio->servicioFechaServicio;
    $data['servicioFechaProximaVisita'] = $servicio->servicioFechaProximaVisita;
    $data['servicioComentariosConfidenciales'] = $servicio->servicioComentariosConfidenciales;
    $data['servicioComentariosCliente'] = $servicio->servicioComentariosCliente;
    $data['servicioIdUsuarioEncargado'] = $servicio->servicioIdUsuarioEncargado;
    $data['servicioIdVehiculo'] = $servicio->servicioIdVehiculo;
    $data['servicioFechaCreacion'] = $servicio->servicioFechaCreacion;
    $data['estatus'] = 1;
    $id_ser_nuevo = $this->Mgeneral->save_register('servicios', $data);

    $actividades = $this->Mgeneral->get_result('saIdServicio',$id_servicio,'servicios_actividades');
    foreach($actividades as $row_actividades){
      $data_actividades['saIdServicio'] = $id_ser_nuevo;
      $data_actividades['saIdActividad'] = $row_actividades->saIdActividad;
      $data_actividades['saIdLugar'] = $row_actividades->saIdLugar;
      $data_actividades['saIdPlaga'] = $row_actividades->saIdPlaga;
      $data_actividades['saIdProducto'] = $row_actividades->saIdProducto;
      $data_actividades['saDosis'] = $row_actividades->saDosis;
      $data_actividades['saCantidad'] = $row_actividades->saCantidad;
      $data_actividades['saMedida'] = $row_actividades->saMedida;
      $data_actividades['saIdMetodo'] = $row_actividades->saIdMetodo;
      $this->Mgeneral->save_register('servicios_actividades', $data_actividades);
    }

    redirect('servicios/lista');


  }

  public function finalizar_servicio($id_Servicio){
      $data['status_orden'] = 1;
      $this->Mgeneral->update_table_row('servicios',$data,'servicioId',$id_Servicio);
      echo 1;
  }

  public function prueba_pdf(){


    //Load the library
      $this->load->library('Html2pdf');
      
      //Set folder to save PDF to
      $this->html2pdf->folder($this->config->item('url_real')."statics/facturas/prefactura/");
      
      //Set the filename to save/download as
      $this->html2pdf->filename('test2.pdf');
      
      //Set the paper defaults
      $this->html2pdf->paper('a4', 'portrait');
      
      $data = array(
        'title' => 'PDF Created',
        'message' => 'Hello World!'
      );
      
      //Load html view
      $this->html2pdf->html($this->load->view('alta', $data, true));
      
      if($this->html2pdf->create('save')) {
        //PDF was successfully saved or downloaded
        echo 'PDF saved';
      }
  }
}
