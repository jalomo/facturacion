<script type="text/javascript">
     menu_activo = "servicio";
$("#menu_servicio_orden").last().addClass("menu_estilo");
    $(document).ready(function() {
     $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});
    } );
</script>

<div class="card-header">
  <a href="<?php echo base_url()?>index.php/servicios/crear_servicio">
    <button type="button" class="btn btn-info"><i class="fas fa-plus-circle"></i>&nbsp;Nuevo servicio</button>
  </a>



</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
          <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Cliente</th>
                <th>Fecha creación</th>
                <th>Fecha Proxima visita</th>
                <th>Estatus</th>


                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php if(is_array($rows)):?>
              <?php foreach($rows as $row):?>
                <tr>
                  <td>
                    <?php if($row->estatus == 1){echo "*";}?>
                    <?php echo nombre_cliente($row->servicioIdCliente);?></td>
                  <td><?php echo $row->servicioFechaCreacion	;?></td>
                  <td><?php echo $row->servicioFechaProximaVisita;?></td>

                  <td> 
                    <?php if($row->status_orden == 1):?>
                      <p class="text-success">Terminada</p>
                    <?php endif;?>
                    <?php if($row->status_orden == 0):?>
                      Creada
                    <?php endif;?>
                    

                  </td>


                  <td>
                    <!--a href="<?php echo base_url()?>pdfhtml/servicios/pdf.php?id=<?php echo $row->servicio_id;?>" target="_blank">
                    <button type="button" class="btn btn-info"><i class="fas fa-file-pdf"></i>&nbsp;PDF</button>
                    </a-->
                    <a href="<?php echo base_url()?>index.php/pdf/servicio/<?php echo $row->servicioId;?>" target="_blank">
                    <button type="button" class="btn btn-info"><i class="fas fa-file-pdf"></i>&nbsp;PDF</button>
                    </a>

                    <a href="<?php echo base_url()?>index.php/servicios/nueva/<?php echo $row->servicioId;?>">
                    <button type="button" class="btn btn-info"><i class="fas fa-eye"></i>&nbsp;Ver</button>
                    </a>

                    <!--a href="<?php echo base_url()?>pdfhtml/servicios/pdf.php?id=<?php echo $row->servicio_id;?>" target="_blank">
                    <button type="button" class="btn btn-info"><i class="fas fa-award"></i>&nbsp;Certificado</button>
                    </a-->


                    <!--a href="<?php echo base_url()?>index.php/cordones/alta/<?php echo $row->servicioId;?>" >
                    <button type="button" class="btn btn-info"><i class="fas fa-circle-notch"></i>&nbsp;Cordones</button>
                    </a-->

                    <?php if($row->status_orden == 1):?>
                    <a href="<?php echo base_url()?>index.php/pdf/certificado/<?php echo $row->servicioId;?>" target="_blank">
                    <button type="button" class="btn btn-info"><i class="fas fa-file-pdf"></i>&nbsp;Certificado</button>
                    </a>
                    <?php endif;?>

                    <!--a href="<?php echo base_url()?>index.php/servicios/clonar_servicio/<?php echo $row->servicioId;?>">
                    <button type="button" class="btn btn-info">Clonar</button-->
                    </a>
                  </td>
                </tr>
             <?php endforeach;?>
             <?php endif;?>
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
