<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>
<script>
       menu_activo = "servicio";
$("#menu_servicio_orden").last().addClass("menu_estilo");
$(document).ready(function(){

  seleccionar_lugares(<?php echo $id_cliente;?>);

  $('#saIdMetodo').on('change',function(event){
    
        var metodoId = $(this).val();
        var lugar = $("#saIdLugar").val();
        
        var url ="<?php echo base_url()?>index.php/servicios/get_medida_metodo/"+metodoId+"/"+lugar;
        ajaxJson(url,{},
              "POST","",function(result){
        
          $("#saCantidad").val(result);
        
        
      });
    });

  $('#saIdLugar').on('change',function(event){
    
        var  lugar = $(this).val();
        var metodoId = $("#saIdMetodo").val();
        
        var url ="<?php echo base_url()?>index.php/servicios/get_medida_metodo/"+metodoId+"/"+lugar;
        ajaxJson(url,{},
              "POST","",function(result){
        
          $("#saCantidad").val(result);
        
        
      });
    });

    $('#finalizar').click(function(event){
    event.preventDefault();
    
    bootbox.confirm({
      title: "",
      message: "Al finalizar el servicio no podrás editar",
      buttons: {
          cancel: {
              label: '<i class="fa fa-times"></i> Cancelar'
          },
          confirm: {
              label: '<i class="fa fa-check"></i> Finalizar servicio'
          }
      },
      callback: function (result) {
        if(result){// si es true
          var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Enviando Datos...</p>',
                            closeButton: false
                        });
            var url ="<?php echo base_url()?>index.php/servicios/finalizar_servicio/<?php echo $id_servicio?>";
            ajaxJson(url,{},
                      "POST","",function(result){
              
              
              if(result == 1){
                dialog_load.modal('hide');
                exito_redirect("SERVICIO FINALIZADO CON EXITO","success","");
              }
            });
        }
          
      }
    });
  });

  $("#servicioFechaCreacion").datepicker({
    //altFormat: "yy-mm-dd",
    appendText: "(MM-mm-YYY)"
  });

  $("#servicioFechaServicio").datepicker({
    //altFormat: "yy-mm-dd",
    appendText: "(MM-mm-YYY)"
  });

  $("#servicioFechaProximaVisita").datepicker({
    //altFormat: "yy-mm-dd",
    appendText: "(MM-mm-YYY)"
  });


  $('#enviar').click(function(event){
    event.preventDefault();
    var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Enviando Datos...</p>',
                            closeButton: false
                        });
    var url ="<?php echo base_url()?>index.php/servicios/guarda_servicio/<?php echo $id_servicio?>";
    ajaxJson(url,{"servicioIdCliente":$("#servicioIdCliente").val(),
                  "servicioDomicilioCalle":$("#servicioDomicilioCalle").val(),
                  "servicioDomicilioColonia":$("#servicioDomicilioColonia").val(),
                  "servicioDomicilioMmunicipio":$("#servicioDomicilioMmunicipio").val(),
                  "servicioDomicilioEstado":$("#servicioDomicilioEstado").val(),
                  "servicioDomicilioNumero":$("#servicioDomicilioNumero").val(),
                  "servicioDomicilioCP":$("#servicioDomicilioCP").val(),
                  "servicioReferenciaLugar":$("#servicioReferenciaLugar").val(),
                  "servicioCertificado":$("#servicioCertificado").val(),
                  "servicioIDCreador":$("#servicioIDCreador").val(),
                  "servicioFolio":$("#servicioFolio").val(),
                  "servicioReferencia":$("#servicioReferencia").val(),
                  "servicioFechaServicio":$("#servicioFechaServicio").val(),
                  "servicioFechaProximaVisita":$("#servicioFechaProximaVisita").val(),
                  "servicioComentariosConfidenciales":$("#servicioComentariosConfidenciales").val(),
                  "servicioComentariosCliente":$("#servicioComentariosCliente").val(),
                  "servicioIdUsuarioEncargado":$("#servicioIdUsuarioEncargado").val(),
                  "servicioIdVehiculo":$("#servicioIdVehiculo").val(),
                  "servicioFechaCreacion":$("#servicioFechaCreacion").val()},
              "POST","",function(result){
      correoValido = false;
      console.log(result);
      json_response = JSON.parse(result);
      obj_output = json_response.output;
      obj_status = obj_output.status;
      if(obj_status == false){
        dialog_load.modal('hide');
        aux = "";
        $.each( obj_output.errors, function( key, value ) {
          aux +=value+"<br/>";
        });
        exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      }
      if(obj_status == true){
        dialog_load.modal('hide');
        exito_redirect("DATOS GUARDADOS CON EXITO","success","");
      }
    });
  });



refrescar_lista_productos();

$("#agregar_actividad").click(function(){

  if($("#productoId").val() != ""){
  $.ajax({
      type: 'POST',
      url: "<?php echo base_url()?>index.php/servicios/guarda_actividad/<?php echo $id_servicio?>",
      enctype: 'multipart/form-data',
      datatype: "JSON",
    //async: asincrono,
      cache: false,
      data: {saIdActividad:$("#saIdActividad").val(),
      saIdLugar:$("#saIdLugar").val(),
      saIdPlaga:$("#saIdPlaga").val(),
      saIdProducto:$("#saIdProducto").val(),
      saDosis:$("#saDosis").val(),
      saCantidad:$("#saCantidad").val(),
      saMedida:$("#saMedida").val(),
      saIdMetodo:$("#saIdMetodo").val(),
      dosisTexto:$("#saDosis option:selected").text()},
      statusCode: {
          200: function (result) {
            //console.log(result);
            $("#load_actividades").html("");
            $("#load_actividades").html(result);
            $(".eliminar_relacion").click(function(event){
                 event.preventDefault();
                 bootbox.dialog({
                     message: "Desea eliminar la actividad?",
                     closeButton: true,
                     buttons:
                     {
                         "danger":
                         {
                             "label": "<i class='icon-remove'></i>Eliminar ",
                             "className": "btn-danger",
                             "callback": function () {
                               id = $(event.currentTarget).attr('flag');
                               url = $("#delete"+id).attr('href');
                               $("#borrar_"+id).slideUp();
                               $.get(url);
                             }
                         },
                         "cancel":
                         {
                             "label": "<i class='icon-remove'></i> Cancelar",
                             "className": "btn-sm btn-info",
                             "callback": function () {

                             }
                         }

                     }
                 });
             });
              $( ".editar_actividad" ).click(function(event) {
                var avtividad_row = $(event.target);

                $("#saIdActividad_e").val($(event.currentTarget).attr('data-saidactividad'));
                $("#saIdLugar_e").val($(event.currentTarget).attr('data-saIdLugar'));
                $("#saIdPlaga_e").val($(event.currentTarget).attr('data-saIdPlaga'));
                $("#saIdProducto_e").val($(event.currentTarget).attr('data-saIdProducto'));
                $("#saDosis_e").val($(event.currentTarget).attr('data-saDosis'));
                $("#saCantidad_e").val($(event.currentTarget).attr('data-saCantidad'));
                $("#saMedida_e").val($(event.currentTarget).attr('data-saMedida'));
                $("#saIdMetodo_e").val($(event.currentTarget).attr('data-saIdMetodo'));
                $("#producto_e").val($(event.currentTarget).attr('data-nombre'));
                $("#saId_e").val($(event.currentTarget).attr('data-saId'));
                
                

              });
             /*$( ".editar_pro" ).click(function(event) {
               var button = $(event.target);
               var id = button.data('id');
               var cantidad = button.data('cantidad');
               var precio = button.data('precio');
               var total = button.data('total');
               var nombre_producto = button.data('nombre');
               $("#precio_modal").val(precio);
               $("#cantidad_modal").val(cantidad);
               $("#total_modal").val(total);
               $("#id_prod_orden").val(id);
               $("#exampleModalLabel").text(nombre_producto);

             });
             $(".guardar_prod_modal").click(function(event){
               editar_productos($("#id_prod_orden").val(),$("#cantidad_modal").val(),$("#precio_modal").val());
             });*/
          },
          401: code400,
          404: code404,
          500: code500,
          409: code409
      }
  });
}else{

  bootbox.dialog({
      message: "Producto no encontrado",
      closeButton: true,
      buttons:
      {

          "cancel":
          {
              "label": "<i class='icon-remove'></i> Aceptar",
              "className": "btn-sm btn-danger",
              "callback": function () {

              }
          }

      }
  });

}
});







function refrescar_lista_productos(){
  $.ajax({
      type: 'POST',
      url: "<?php echo base_url()?>index.php/servicios/carga_actividades/<?php echo $id_servicio?>",
      enctype: 'multipart/form-data',
      datatype: "JSON",
    //async: asincrono,
      cache: false,
      data: {id_servicio:"<?php echo $id_servicio?>"},
      statusCode: {
          200: function (result) {
            //console.log(result);
             $("#load_actividades").html("");
             $("#load_actividades").html(result);
             $(".eliminar_relacion").click(function(event){
                  event.preventDefault();
                  bootbox.dialog({
                      message: "Desea eliminar la actividad?",
                      closeButton: true,
                      buttons:
                      {
                          "danger":
                          {
                              "label": "<i class='icon-remove'></i>Eliminar ",
                              "className": "btn-danger",
                              "callback": function () {
                                id = $(event.currentTarget).attr('flag');
                                url = $("#delete"+id).attr('href');
                                $("#borrar_"+id).slideUp();
                                $.get(url);
                              }
                          },
                          "cancel":
                          {
                              "label": "<i class='icon-remove'></i> Cancelar",
                              "className": "btn-sm btn-info",
                              "callback": function () {

                              }
                          }

                      }
                  });
              });
              $( ".editar_actividad" ).click(function(event) {
                var avtividad_row = $(event.target);

                $("#saIdActividad_e").val($(event.currentTarget).attr('data-saidactividad'));
                $("#saIdLugar_e").val($(event.currentTarget).attr('data-saIdLugar'));
                $("#saIdPlaga_e").val($(event.currentTarget).attr('data-saIdPlaga'));
                $("#saIdProducto_e").val($(event.currentTarget).attr('data-saIdProducto'));
                $("#saDosis_e").val($(event.currentTarget).attr('data-saDosis'));
                $("#saCantidad_e").val($(event.currentTarget).attr('data-saCantidad'));
                $("#saMedida_e").val($(event.currentTarget).attr('data-saMedida'));
                $("#saIdMetodo_e").val($(event.currentTarget).attr('data-saIdMetodo'));
                $("#producto_e").val($(event.currentTarget).attr('data-nombre'));
                $("#saId_e").val($(event.currentTarget).attr('data-saId'));
                
                

              });
              $("#editar_actividad_server").click(function(){
                

                
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url()?>index.php/servicios/editar_actividad/<?php echo $id_servicio?>",
                    enctype: 'multipart/form-data',
                    datatype: "JSON",
                  //async: asincrono,
                    cache: false,
                    data: {saIdActividad:$("#saIdActividad_e").val(),
                    saIdLugar:$("#saIdLugar_e").val(),
                    saIdPlaga:$("#saIdPlaga_e").val(),
                    saIdProducto:$("#saIdProducto_e").val(),
                    saDosis:$("#saDosis_e").val(),
                    saCantidad:$("#saCantidad_e").val(),
                    saMedida:$("#saMedida_e").val(),
                    saIdMetodo:$("#saIdMetodo_e").val(),
                    saId:$("#saId_e").val()},
                    statusCode: {
                        200: function (result) {

                          //console.log(result);
                          
                          $('#exampleModalActividad').modal('hide');
                          refrescar_lista_productos();
                          

                          
                           
                        },
                        401: code400,
                        404: code404,
                        500: code500,
                        409: code409
                    }
                });

              });
              $(".guardar_prod_modal").click(function(event){
                editar_productos($("#id_prod_orden").val(),$("#cantidad_modal").val(),$("#precio_modal").val());
              });
          },
          401: code400,
          404: code404,
          500: code500,
          409: code409
      }
  });
}





});



function seleccionar_cliente(id_cliente){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/get_datos_cliente/"+id_cliente;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              var obj = JSON.parse(data);
              $('#nombre').val(obj.nombre);
              $('#rfc').val(obj.rfc);
              $('#razon_social').val(obj.razon_social);
              $('#servicioIdCliente').val(obj.id);

              seleccionar_lugares(id_cliente);



              console.log("SUCCESS : ", data);





            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}

function seleccionar_lugares(id_cliente){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/selecciona_lugares_clientes/"+id_cliente;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              console.log("SUCCESS : ", data);
              console.log("ID_CLIENTE : ", id_cliente);
              
              $("#lugares_html").html("");
              $("#lugares_html").html(data);




            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}

function seleccionar_producto(id_producto){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/get_datos_producto/"+id_producto;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              var obj = JSON.parse(data);
              $('#producto').val(obj.productoNombre);
              $('#saIdProducto').val(obj.productoId);
              $('#saMedida').val(obj.productoMedida);

              html = '<select class="form-control cc-exp form-control-sm" id="saDosis" name="saDosis">'
                         +' <option value="'+obj.productoDosisAlta+'" >ALTA:'+obj.productoDosisAlta+obj.productoMedidaAlta+'</option>'
                         + '<option value="'+obj.productoDosisBaja+'" >BAJA:'+obj.productoDosisBaja+obj.productoMedidaBaja+'</option>'
                        +'</select>';
             
              $("#dosis_html").html("");
              $("#dosis_html").html(html);

              seleccionar_producto_unidad(id_producto);

              /*$('#saDosis').on('change',function(event){

                $('#saDosis').find(':selected').data('tag');

                var avtividad_row = $(event.target);

                alert($(event.currentTarget).attr('tag'));
                
                   
                });
*/





              console.log("SUCCESS : ", data);





            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}




 function seleccionar_producto_unidad(id_producto){

      var url_sis ="<?php echo base_url()?>index.php/servicios/unidades_productos/"+id_producto;
      $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                processData: false,
                contentType: false,
                datatype: "JSON",
                cache: false,
                timeout: 600000,
                success: function (data) {
                 $("#load_unidad").html("");
                 $("#load_unidad").html(data);



                  console.log("SUCCESS : ", data);





                },
                error: function (e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                }
            });

    }
</script>
<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <label class="card-title">
               <?php if($servicio->status_orden ==0):?>
                        
              <?php else:?>
                    <h2>Servicio finalizado</h2>
              <?php endif;?>

        </div>
        <div class="card-body" style="background:#ced6e0">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">
                <form action="" method="post" novalidate="novalidate" id="">

                <div class="row">
                  <div class="col-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1 tipo_letra">Cliente</label>
                                <input id="nombre" name="nombre" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($proveedor)){echo $proveedor->nombre;}?>" placeholder="Cliente" autocomplete="off" data-toggle="modal"  data-target="#exampleModal">
                                <input id="servicioIdCliente" name="servicioIdCliente" type="hidden" class="form-control cc-exp form-control-sm" value="<?php if(is_object($proveedor)){echo $proveedor->id;}?>" placeholder="ordenIdProveedor" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                          <div class="form-group">
                              <input id="rfc" name="rfc" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($proveedor)){echo $proveedor->rfc;}?>" placeholder="RFC" autocomplete="off">
                          </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                            <input id="razon_social" name="razon_social" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($proveedor)){echo $proveedor->razon_social;}?>" placeholder="Razón Social" autocomplete="off">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-12" align="center">
                        <label>Domicilio del Servicio</label>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <input id="servicioDomicilioCalle" name="servicioDomicilioCalle" type="text" class="form-control-sm form-control cc-exp" value="<?php if(is_object($servicio)){echo $servicio->servicioDomicilioCalle;}?>" placeholder="Calle" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <input id="servicioDomicilioColonia" name="servicioDomicilioColonia" type="text" class="form-control-sm form-control cc-exp" value="<?php if(is_object($servicio)){echo $servicio->servicioDomicilioColonia;}?>" placeholder="Colonia" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <input id="servicioDomicilioMmunicipio" name="servicioDomicilioMmunicipio" type="text" class="form-control-sm form-control cc-exp" value="<?php if(is_object($servicio)){echo $servicio->servicioDomicilioMmunicipio;}?>" placeholder="Municipio" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <input id="servicioDomicilioEstado" name="servicioDomicilioEstado" type="text" class="form-control-sm form-control cc-exp" value="<?php if(is_object($servicio)){echo $servicio->servicioDomicilioEstado;}?>" placeholder="Estado" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <input id="servicioDomicilioNumero" name="servicioDomicilioNumero" type="text" class="form-control-sm form-control cc-exp" value="<?php if(is_object($servicio)){echo $servicio->servicioDomicilioNumero;}?>" placeholder="Número" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <input id="servicioDomicilioCP" name="servicioDomicilioCP" type="text" class="form-control-sm form-control cc-exp" value="<?php if(is_object($servicio)){echo $servicio->servicioDomicilioCP;}?>" placeholder="CP" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input id="servicioReferenciaLugar" name="servicioReferenciaLugar" type="text" class="form-control-sm form-control cc-exp" value="<?php if(is_object($servicio)){echo $servicio->servicioReferenciaLugar;}?>" placeholder="Referencia lugar" autocomplete="off">
                            </div>
                        </div>

                    </div>





                  </div>
                  <!-- SEGUNDA COLUMNA  -->
                  <div class="col-6">

                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Certificado</label>
                            <input id="servicioCertificado" name="servicioCertificado" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioCertificado;}?>" placeholder="Certificado" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-3">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">ID creador</label>
                            <input id="servicioIDCreador" name="servicioIDCreador" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioIDCreador;}?>" placeholder="ID creador" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-5">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Fecha de creación</label>
                            <input id="servicioFechaCreacion" name="servicioFechaCreacion" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioFechaCreacion;}?>" placeholder="Fecha creación" autocomplete="off">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Referencia</label>
                            <input id="servicioReferencia" name="servicioReferencia" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioReferencia;}?>" placeholder="Referencia" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1" style="font-size: 13px;">Fecha hora del servicio</label>
                            <input id="servicioFechaServicio" name="servicioFechaServicio" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioFechaServicio;}?>" placeholder="Fecha" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="form-group">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1" style="font-size: 13px;">Fecha proxima visita</label>
                              <input id="servicioFechaProximaVisita" name="servicioFechaProximaVisita" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioFechaProximaVisita;}?>" placeholder="Fecha" autocomplete="off">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">COMENTARIOS CONFIDENCIALES SOBRE OPORTUNIDADES DE MEJORA PARA EL CLIENTE</label>
                            <input id="servicioComentariosConfidenciales" name="servicioComentariosConfidenciales" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioComentariosConfidenciales;}?>" placeholder="Comentario" autocomplete="off">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">COMENTARIOS DEL CLIENTE SOBRE EL SERVICIO</label>
                            <input id="servicioComentariosCliente" name="servicioComentariosCliente" type="text" class="form-control cc-exp form-control-sm" value="<?php if(is_object($servicio)){echo $servicio->servicioComentariosCliente;}?>" placeholder="Comentario" autocomplete="off">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-6">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Usuario</label>
                            <select class="form-control cc-exp form-control-sm" >
                              <?php foreach($usuarios as $row_usuario):?>
                                <option value="<?php echo $row_usuario->usuarioId?>"><?php echo $row_usuario->usuarioNombre?></option>
                              <?php endforeach;?>
                            </select>
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Almacen</label>
                            <select class="form-control cc-exp form-control-sm" id="servicioIdVehiculo" >
                              <?php foreach($vehiculos as $row_vehiculo):?>
                                <option value="<?php echo $row_vehiculo->alamacenId?>"><?php echo $row_vehiculo->almacenNombre?></option>
                              <?php endforeach;?>
                            </select>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>


                <hr/>

              <label>Captura de Actividad</label>

              <div class="row" style="background:#fff;">
                  <div class="col-3">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Actividad</label>
                        <select class="form-control cc-exp form-control-sm" id="saIdActividad" >
                          <?php foreach($actividades as $row_actividad):?>
                            <option value="<?php echo $row_actividad->actividadId?>"><?php echo $row_actividad->actividadNombre?></option>
                          <?php endforeach;?>
                        </select>
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="form-group">
                      <div class="form-group">
                          <label for="cc-exp" class="control-label mb-1">Método</label>
                          <select class="form-control cc-exp form-control-sm" id="saIdMetodo" >
                            <option value="0"></option>
                            <?php foreach($metodos as $row_metodos):?>
                            
                              <option value="<?php echo $row_metodos->metodoId?>" flag="<?php echo $row_metodos->metodoBoquilla?>"><?php echo $row_metodos->metodoNombre?></option>
                            <?php endforeach;?>
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Lugar</label>
                        <!--select class="form-control cc-exp form-control-sm" id="saIdLugar" >
                          <?php foreach($lugares_aplicacion as $row_lugar_aplicacion):?>
                            <option value="<?php echo $row_lugar_aplicacion->id?>"><?php echo $row_lugar_aplicacion->lugar."  Metros lineales:".$row_lugar_aplicacion->metros?></option>
                          <?php endforeach;?>
                        </select-->
                        <div id="lugares_html"></div>
                    </div>
                  </div>
                  <div class="col-2">
                    <label for="cc-exp" class="control-label mb-1">Plaga</label>
                    <select class="form-control cc-exp form-control-sm" id="saIdPlaga">
                      <?php foreach($plagas as $row_plaga):?>
                        <option value="<?php echo $row_plaga->plagaId?>"><?php echo $row_plaga->plagaNombre?></option>
                      <?php endforeach;?>
                    </select>

                  </div>
                  <div class="col-3">
                    <div class="form-group">

                    </div>
                  </div>
              </div>



                <div class="row" style="background:#fff;">
                  <div class="col-3">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Producto</label>
                        <input id="producto" name="producto" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="Producto" autocomplete="off" data-toggle="modal"  data-target="#exampleModalProducto">
                        <input id="saIdProducto" name="saIdProducto" type="hidden" class="form-control cc-exp form-control-sm" value="" placeholder="id" autocomplete="off">
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Dosis</label>
                        <!--select class="form-control cc-exp form-control-sm" id="saDosis" name="saDosis">
                          <option>Alta</option>
                          <option>baja</option>
                        </select-->
                        <div id="dosis_html"></div>
                        <!--input id="saDosis" name="saDosis" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="Dosis" autocomplete="cc-exp"-->
                    </div>
                  </div>
                  <div class="col-2">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Cantidad</label>
                        <input id="saCantidad" name="saCantidad" type="text"class="textnumeros form-control cc-exp form-control-sm" value="" placeholder="Cantidad" autocomplete="off">
                    </div>
                  </div>
                  <div class="col-1">
                    <div class="form-group">
                          <label for="cc-exp" class="control-label mb-1">Medida</label>
                          <!--select class="form-control cc-exp form-control-sm" id="saMedida" name="saMedida">
                            <?php foreach($medidas as $row_medidas):?>
                              <option value="<?php echo $row_medidas->medidaId?>"><?php echo $row_medidas->medidaNombre?></option>
                            <?php endforeach;?>
                          </select-->
                          <div id="load_unidad"></div>
                      </div>
                    <!--div class="form-group">
                        <label for="cc-exp" class="control-label mb-1 text-dark">Medida</label>
                        <input id="saMedida" name="saMedida" type="text" class="textnumeros form-control cc-exp form-control-sm" value="" placeholder="Medida" autocomplete="cc-exp">
                    </div-->
                  </div>
                  
                  <div class="col-2">
                    <div class="form-group">
                      <br/>
                      <button id="agregar_actividad" type="button" class="btn btn-info ">

                          <span id="payment-button-amount">Agregar</span>

                      </button>
                    </div>
                  </div>
                </div>

              <div class="row" style="background:#fff;">
                <div class="col-12" id="load_actividades">
                </div>
              </div>


                      <div align="right">
                        <?php if($servicio->status_orden ==0):?>
                        <button id="enviar" type="button" class="btn btn-primary" >
                            <i class="fa fa-save fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Guardar datos</span>
                        </button>

                        <button id="" type="button" class="btn btn-secondary" >
                            &nbsp;
                            <span id="">Envia a revisión</span>
                        </button>

                        <button id="finalizar" type="button" class="btn btn-warning" >
                            &nbsp;
                            <span id="payment-button-amount">Finalizar orden</span>
                        </button>
                      <?php else:?>
                        <h2>Servicio finalizado</h2>
                      <?php endif;?>

                      </div>


                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->




<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>RFC</th>
        <th>Razon social</th>
        <!--th>Email</th-->
        <th>Nombre</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      <?php if(is_array($clientes)):?>
      <?php foreach($clientes as $row):?>
        <tr id="<?php echo $row->id;?>">
          <td><?php echo $row->rfc;?></td>
          <td><?php echo $row->razon_social;?></td>
          <!--td><?php echo $row->email;?></td-->
          <td><?php echo $row->nombre;?></td>

          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_cliente(<?php echo $row->id;?>)" data-dismiss="modal" id="<?php echo $row->id;?>" data-id="<?php echo $row->id;?>">seleccionar</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody>
  </table>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModalProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Producto</th>
        <th>Producto referencia</th>

        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      <?php if(is_array($productos)):?>
      <?php foreach($productos as $row_producto):?>
        <tr id="<?php echo $row_producto->productoId;?>">
          <td><?php echo $row_producto->productoNombre;?></td>
          <td><?php echo $row_producto->productoReferencia;?></td>
          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_producto(<?php echo $row_producto->productoId;?>)" data-dismiss="modal" id="<?php echo $row_producto->productoId;?>" data-id="<?php echo $row_producto->productoId;?>">seleccionar</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody>
  </table>
        </div>
      </div>

    </div>
  </div>
</div>




<div class="modal fade" id="exampleModalActividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-12">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Actividad</label>
                        <select class="form-control cc-exp form-control-sm" id="saIdActividad_e" >
                          <?php foreach($actividades as $row_actividad):?>
                            <option value="<?php echo $row_actividad->actividadId?>"><?php echo $row_actividad->actividadNombre?></option>
                          <?php endforeach;?>
                        </select>
                    </div>
              </div>
          </div>
          <div class="row">
            <div class="col-12">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Lugar</label>
                        <select class="form-control cc-exp form-control-sm" id="saIdLugar_e" >
                          <?php foreach($lugares_aplicacion as $row_lugar_aplicacion):?>
                            <option value="<?php echo $row_lugar_aplicacion->id?>"><?php echo $row_lugar_aplicacion->lugar;?></option>
                          <?php endforeach;?>
                        </select>
                    </div>
                  </div>
          </div>
          <div class="row">
            <div class="col-12">
                    <label for="cc-exp" class="control-label mb-1">Plaga</label>
                    <select class="form-control cc-exp form-control-sm" id="saIdPlaga_e">
                      <?php foreach($plagas as $row_plaga):?>
                        <option value="<?php echo $row_plaga->plagaId?>"><?php echo $row_plaga->plagaNombre?></option>
                      <?php endforeach;?>
                    </select>

                  </div>
          </div>


                <div class="col-12">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Producto</label>
                        <input id="producto_e" name="producto_e" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="Producto" autocomplete="off" data-toggle="modal"  data-target="#exampleModalProducto" >
                        <input id="saIdProducto_e" name="saIdProducto_e" type="hidden" class="form-control cc-exp form-control-sm" value="" placeholder="id" autocomplete="off">
                         <input id="saId_e" name="saId_e" type="hidden" class="form-control cc-exp form-control-sm" value="" placeholder="id" autocomplete="cc-exp">
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Dosis</label>
                        <input id="saDosis_e" name="saDosis_e" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="Dosis" autocomplete="off">
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Cantidad</label>
                        <input id="saCantidad_e" name="saCantidad_e" type="text"class="textnumeros form-control cc-exp form-control-sm" value="" placeholder="Cantidad" autocomplete="off">
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                          <label for="cc-exp" class="control-label mb-1">Medida</label>
                          <select class="form-control cc-exp form-control-sm" id="saMedida_e" >
                            <?php foreach($medidas as $row_medidas):?>
                              <option value="<?php echo $row_medidas->medidaId?>"><?php echo $row_medidas->medidaNombre?></option>
                            <?php endforeach;?>
                          </select>
                      </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                      <div class="form-group">
                          <label for="cc-exp" class="control-label mb-1">Método</label>
                          <select class="form-control cc-exp form-control-sm" id="saIdMetodo_e" >
                            <?php foreach($metodos as $row_metodos):?>
                              <option value="<?php echo $row_metodos->metodoId?>"><?php echo $row_metodos->metodoNombre?></option>
                            <?php endforeach;?>
                          </select>
                      </div>
                    </div>
                  </div>



         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary " id="editar_actividad_server" >Guardar cambios</button>
      </div>
    </div>
  </div>
</div>

