<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>
<script>
    menu_activo = "usuarios";
$("#menu_roles_user").last().addClass("menu_estilo");
$(document).ready(function(){
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/servicios/actualiza_rol";
  ajaxJson(url,{"idRol":$("#idRol").val()
                },
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      idRol = obj_output.idRol;
      exito_redirect("EDITADO CON EXITO","success","<?php echo base_url()?>index.php/servicios/configuracion/");
    }


  });
});

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title"></strong>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Rol de Técnico de Campo</label>
                                  <select class="form-control form-control-sm cc-exp" name="idRol" id="idRol">
                                    <?php foreach($rows as $row):?>
                                        <?php if($row->rolTecnico == 1):?>
                                            <option value="<?php echo $row->rolId;?>" selected> <?php echo $row->rolNombre;?></option>
                                        <?php else:?>
                                            <option value="<?php echo $row->rolId;?>"> <?php echo $row->rolNombre;?></option>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                  </select>
                              </div>
                          </div>
                          
                          
                      </div>




                      <div class="row">
                          <div class="col-3">
                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                      </div>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->


