


<script type="text/javascript">
       menu_activo = "facturacion";
$("#menu_facturacion_complemento").last().addClass("menu_estilo");
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});
    } );
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>

<div class="card-header">
  <div class="card-header">
      <a href="<?php echo base_url();?>index.php/complemento_pago/crear_pre_complemento"><button type="button" class="btn btn-primary">Crear complemento de pago</button></a>
  </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
          <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>

                <th>RFC</th>
                <th>Nombre</th>
                <th>Email</th>

                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php if(is_array($complementos)):?>
              <?php foreach($complementos as $row):?>
                <tr>

                  <td><?php echo $row->receptorRFC;?></td>
                  <td><?php echo $row->receptorNombre;?></td>
                  <td><?php echo $row->receptorRFC;?></td>


                  <td>
                    <?php if($row->sat_error == "0"):?>
                    <a href="<?php echo base_url()?>statics/facturas/complementoxml/<?php echo $row->complementoID;?>.xml" download>
                    <button type="button" class="btn btn-success">XML</button>
                    </a>
                    <a target="_blank" href="<?php echo base_url()?>index.php/complemento_pago/genera_pdf/<?php echo $row->complementoID;?>">
                    <button type="button" class="btn btn-success">PDF</button>
                    </a>

                  <?php endif;?>
                    <a href="<?php echo base_url()?>index.php/complemento_pago/nueva/<?php echo $row->complementoID;?>">
                    <button type="button" class="btn btn-info">Ver</button>
                    </a>
                  </td>
                </tr>
             <?php endforeach;?>
             <?php endif;?>
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
