<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Complemento_pago extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','astillero'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }


    public function lista_complementos(){
      $titulo['titulo'] = "Lista Complementos" ;
      $titulo['titulo_dos'] = "Lista Complementos" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $datos['complementos'] = $this->Mgeneral->get_table('complemento_pago');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('complemento_pago/lista_complementos', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));

    }

    public function nueva($id_complemento){
      $datos['clientes'] = $this->Mgeneral->get_result('status',0,'clientes');//$this->Mgeneral->get_table('clientes');
      $datos['complemento'] = $this->Mgeneral->get_row('complementoID',$id_complemento,'complemento_pago');
      $datos['complemento_datos'] = $this->Mgeneral->get_row('id_complemento',$id_complemento,'complemento_datos');
      $datos['complementoID'] = $id_complemento;
      $titulo['titulo'] = "Complemento de pago" ;
      $titulo['titulo_dos'] = "Complemento de Pago" ;
      $datos['row'] = $this->Mgeneral->get_row('id',1,'informacion');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('complemento_pago/complemento_pago', $datos, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           /*'statics/js/factura.js',*/
                                           'statics/bootstrap4/js/bootstrap.min.js')));
    }



    public function buscar_factura($rfc){

      $rows = $this->Mgeneral->get_facturas_ppd($rfc);//get_result('receptor_RFC',$rfc,'factura');
      $html_facturas = "";
      foreach($rows as $row):
        $total = $this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID);
        $html_facturas .= '<tr id="'.$row->receptor_RFC.'">
                      <th scope="row">'.$row->receptor_RFC.'</th>
                      <td>'.$row->factura_medotoPago.'</td>
                      <td>'.$total.'</td>
                      <td>'.(($this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID))-$this->Mgeneral->complementoPagoPagado($row->sat_uuid)).'</td>
                      
                      <td><button type="button" class="btn elegir_fac" id="'.$row->sat_uuid.'" >Seleccionar</button></td>
                    </tr>';
      endforeach;


      echo '<table class="table" style="background:#fff">
          <thead>
            <tr>

              <th scope="col">RFC</th>
              <th scope="col">Metodo pago</th>
              <th scope="col">Total</th>
              <th scope="col">Saldo</th>
             
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_facturas.'
          </tbody>
        </table>';
    }

    public function crear_pre_complemento(){
      $emisor = $this->Mgeneral->get_row('id',1,'datos');
      $id_complemento = get_guid();
      $data['complementoID'] = $id_complemento;
      $data['status'] = 1;
      $data['emisorRFC'] = $emisor->rfc;
      $data['emisorRegimenFiscal'] = $emisor->regimen;
      $data['emisorNombre'] = $emisor->razon_social;
      $data['receptorUsoCFDI'] = "P01";
      $data['monedaG'] = "XXX";
      $data['tipoDeComprobante'] = "P";
      $data['lugarExpedicion'] = codigo_postal();
      $data['fecha_creacion'] = date('Y-m-d H:i:s');
      //$data['receptor_uso_CFDI'] = "G03";

      $id_tabla = $this->Mgeneral->save_register('complemento_pago', $data);

      $data_c['folio'] = $id_tabla;
      $data_c['serie'] = $id_tabla.'-'.date('Y-m-d');
      $this->Mgeneral->update_table_row('complemento_pago',$data_c,'complementoID',$id_complemento);

      $data_complemento['id_complemento'] = $id_complemento;
      $this->Mgeneral->save_register('complemento_datos', $data_complemento);
      redirect('complemento_pago/nueva/'.$id_complemento);

    }

    public function get_datos_cliente($id_cliente){
    //  $id_cliente = $this->input->post('id_cliente');
      $res = $this->Mgeneral->get_row('id',$id_cliente,'clientes');
      echo json_encode($res);
    }

    public function guarda_datos($id_complemento){
      /*$this->form_validation->set_rules('receptor_nombre', 'receptor_nombre', 'required');
      $this->form_validation->set_rules('receptor_id_cliente', 'receptor_id_cliente', 'required');
      $this->form_validation->set_rules('factura_moneda', 'factura_moneda', 'required');
      $this->form_validation->set_rules('fatura_lugarExpedicion', 'fatura_lugarExpedicion', 'required');
      $this->form_validation->set_rules('factura_fecha', 'factura_fecha', 'required');
      $this->form_validation->set_rules('receptor_email', 'receptor_email', 'required');
      $this->form_validation->set_rules('factura_folio', 'factura_folio', 'required');
      $this->form_validation->set_rules('factura_serie', 'factura_serie', 'required');
      $this->form_validation->set_rules('receptor_direccion', 'receptor_direccion', 'required');
      $this->form_validation->set_rules('factura_formaPago', 'factura_formaPago', 'required');
      $this->form_validation->set_rules('factura_medotoPago', 'factura_medotoPago', 'required');
      $this->form_validation->set_rules('factura_tipoComprobante', 'factura_tipoComprobante', 'required');
      $this->form_validation->set_rules('receptor_RFC', 'receptor_RFC', 'required');
      $this->form_validation->set_rules('receptor_uso_CFDI', 'receptor_uso_CFDI', 'required');
      $this->form_validation->set_rules('pagada', 'pagada', 'required');
      $this->form_validation->set_rules('comentario', 'comentario', 'required');*/
      $response = validate($this);
      $response['status'] = true;
      if($response['status']){


        $data['receptorNombre'] = $this->input->post("receptor_nombre");
        $data['receptorId'] = $this->input->post("receptor_id_cliente");
        $data['lugarExpedicion'] = $this->input->post("lugarExpedicion");
        $data['fechaPago'] = $this->input->post("fecha_pago");
        $data['receptor_email'] = $this->input->post("receptor_email");
        $data['folio'] = $this->input->post("factura_folio");
        $data['serie'] = $this->input->post("factura_serie");
        $data['comentario'] = $this->input->post("comentario");
        $data['receptorRFC'] = $this->input->post("receptorRFC");
        $this->Mgeneral->update_table_row('complemento_pago',$data,'complementoID',$id_complemento);

        $data_complemento['fecha'] = $this->input->post("complemento_fecha");
        $data_complemento['forma_pago'] = $this->input->post("complemento_forma_pago");
        $data_complemento['complemento_moneda'] = 'MXN';//$this->input->post("complemento_moneda");
        $data_complemento['tipo_cambio'] = $this->input->post("complemento_tipoCambio");
        $data_complemento['total_pago'] = $this->input->post("complemento_totalPago");
        $data_complemento['no_operacion'] = $this->input->post("complemento_no_operacion");
        $data_complemento['banco_ordenante'] = $this->input->post("complemento_banco_ordenante");
        $data_complemento['cta_ordenante'] = $this->input->post("complemento_cta_ordenante");
        $data_complemento['rfc_beneficiario'] = $this->input->post("complemento_rfc_beneficiario");
        $data_complemento['cta_beneficiario'] = $this->input->post("complemento_cta_beneficiario");
        $data_complemento['rfc_ordenante'] = $this->input->post("complemento_rfc_ordenante");
         $this->Mgeneral->update_table_row('complemento_datos',$data_complemento,'id_complemento',$id_complemento);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));

    }

    public function relaciones_complementos(){
      $id_factura = $this->input->post('id_complemento');
      $uuid = $this->input->post('uuid');
      $seriep = $this->input->post('seriep');
      $foliop = $this->input->post('foliop');
      $moneda_pe = $this->input->post('moneda_pe');
      $tipo_cambio_p = $this->input->post('tipo_cambio_p');
      $metodo_pago_p = $this->input->post('metodo_pago_p');
      $numero_parcial = $this->input->post('numero_parcial');
      $deuda_pagar = $this->input->post('deuda_pagar');
      $importe_pagado = $this->input->post('importe_pagado');
      $nuevo_saldo = $this->input->post('nuevo_saldo');

      $data['uuid'] = $uuid;
      $data['id_complemento'] = $id_factura;
      $data['serie'] = $seriep;
      $data['folio'] = $foliop;
      $data['monedaDR'] = $moneda_pe;
      $data['tipoCambioDR'] = $tipo_cambio_p;
      $data['metodoDePagoDR'] = $metodo_pago_p;
      $data['numParcialidad'] = $numero_parcial;
      $data['impSaldoAnt'] = $deuda_pagar;
      $data['impPagado'] = $importe_pagado;
      $data['impSaldoInsoluto'] = $nuevo_saldo;
      $data['fecha_creacion'] = time();



      $this->Mgeneral->save_register('complemento_cfdi_relacionado_p', $data);

      $rows = $this->Mgeneral->get_result('id_complemento',$id_factura,'complemento_cfdi_relacionado_p');
      $html_uuid = "";
      foreach($rows as $row):
        $html_uuid .= '<tr id="borrar2_'.$row->ccrpId.'">
                      <th scope="row">'.$row->uuid.'</th>
                      <td>'.$row->numParcialidad.'</td>
                      <td>'.$row->impSaldoAnt.'</td>
                      <td>'.$row->impPagado.'</td>
                      <td>'.$row->impSaldoInsoluto.'</td>
                      <td><a href="'.base_url().'index.php/complemento_pago/eliminar_relacion_complemento/'.$row->ccrpId.'" flag="'.$row->ccrpId.'" id="delete2'.$row->ccrpId.'" class="eliminar_relacion_p"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
      endforeach;

      echo '<table class="table" style="background:#fff">
          <thead>
            <tr>

              <th scope="col">UUID</th>
              <th scope="col">No.</th>
              <th scope="col">Por pagar</th>
              <th scope="col">Imp. pagado</th>
              <th scope="col">saldo</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';

    }

    public function datos_pago($uuid){

      $fatcura = $this->Mgeneral->get_row('sat_uuid',$uuid,'factura');

      $saldo = $this->Mgeneral->complementoPagoPagado($uuid);
      $total_saldo =  (($this->Mgeneral->factura_subtotal($fatcura->facturaID) + $this->Mgeneral->factura_iva_total($fatcura->facturaID))-$saldo);

      $saldo1 = $this->Mgeneral->complementoPagoPagado($uuid);

      $numero_pago = $this->Mgeneral->complemento_numero_pagos($uuid) + 1;
      $data['numero_pago'] = $numero_pago;
      $data['saldo'] = $total_saldo;
      $data['abonos'] =  $saldo1;
      echo json_encode($data);
    }

    public function ver_relaciones_complementos(){
      $id_factura = $this->input->post('id_factura');
      $rows = $this->Mgeneral->get_result('id_complemento',$id_factura,'complemento_cfdi_relacionado_p');
      $rows_complemento = $this->Mgeneral->get_row('complementoID',$id_factura,'complemento_pago');
      $html_uuid = "";
      foreach($rows as $row):
        if($rows_complemento->sat_error != "0"){
        $html_uuid .= '<tr id="borrar2_'.$row->ccrpId.'">
                      <th scope="row">'.$row->uuid.'</th>
                      <td>'.$row->numParcialidad.'</td>
                      <td>'.$row->impSaldoAnt.'</td>
                      <td>'.$row->impPagado.'</td>
                      <td>'.$row->impSaldoInsoluto.'</td>
                      <td><a href="'.base_url().'index.php/complemento_pago/eliminar_relacion_complemento/'.$row->ccrpId.'" flag="'.$row->ccrpId.'" id="delete2'.$row->ccrpId.'" class="eliminar_relacion_p"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
        }else{
          $html_uuid .= '<tr id="borrar2_'.$row->ccrpId.'">
                        <th scope="row">'.$row->uuid.'</th>
                        <td>'.$row->numParcialidad.'</td>
                        <td>'.$row->impSaldoAnt.'</td>
                        <td>'.$row->impPagado.'</td>
                        <td>'.$row->impSaldoInsoluto.'</td>
                        <td></td>
                      </tr>';
        }
      endforeach;

      echo '<table class="table" style="background:#fff">
          <thead>
            <tr>

              <th scope="col">UUID</th>
              <th scope="col">No.</th>
              <th scope="col">Por pagar</th>
              <th scope="col">Imp. pagado</th>
              <th scope="col">saldo</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';
    }

    public function eliminar_relacion_complemento($id){
      $this->Mgeneral->delete_row('complemento_cfdi_relacionado_p','ccrpId',$id);
    }

    public function relaciones_facturas(){
      $id_factura = $this->input->post('id_complemento');
      $uuid = $this->input->post('uuid');
      $tipo_relacion = $this->input->post('tipo_relacion');
      $data['tipo'] = $tipo_relacion;
      $data['uuid'] = $uuid;
      $data['relacion_complemento'] = $id_factura;
      $this->Mgeneral->save_register('complemento_cdfi_relacionado', $data);

      $rows = $this->Mgeneral->get_result('relacion_complemento',$id_factura,'complemento_cdfi_relacionado');
      $html_uuid = "";
      foreach($rows as $row):
        $html_uuid .= '<tr id="borrar_'.$row->ccrId.'">
                      <th scope="row">'.$row->tipo.'</th>
                      <td>'.$row->uuid.'</td>
                      <td><a href="'.base_url().'index.php/complemento_pago/eliminar_relacion_id/'.$row->ccrId.'" flag="'.$row->ccrId.'" id="delete'.$row->ccrId.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
      endforeach;

      echo '<table class="table" style="background:#fff">
          <thead>
            <tr>

              <th scope="col">Relación</th>
              <th scope="col">UUID</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';
    }

    public function ver_relaciones_facturas(){
      $id_factura = $this->input->post('id_factura');
      $rows = $this->Mgeneral->get_result('relacion_complemento',$id_factura,'complemento_cdfi_relacionado');
      $html_uuid = "";
      foreach($rows as $row):
        $html_uuid .= '<tr id="borrar_'.$row->ccrId.'">
                      <th scope="row">'.$row->tipo.'</th>
                      <td>'.$row->uuid.'</td>
                      <td><a href="'.base_url().'index.php/complemento_pago/eliminar_relacion_id/'.$row->ccrId.'" flag="'.$row->ccrId.'" id="delete'.$row->ccrId.'" class="eliminar_relacion"><button type="button" class="btn btn-danger">borrar</button></a></td>
                    </tr>';
      endforeach;

      echo '<table class="table" style="background:#fff">
          <thead>
            <tr>

              <th scope="col">Relación</th>
              <th scope="col">UUID</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            '.$html_uuid.'
          </tbody>
        </table>';
    }

    public function eliminar_relacion_id($id){
      $this->Mgeneral->delete_row('complemento_cdfi_relacionado','ccrId',$id);
    }

    public function genera_complemento_pago($id_complemento){
      $complemento_pago = $this->Mgeneral->get_row('complementoID',$id_complemento,'complemento_pago');
      $complemento_datos = $this->Mgeneral->get_row('id_complemento',$id_complemento,'complemento_datos');

      $complemento_cfdi_relacionado_p = $this->Mgeneral->get_result('id_complemento',$id_complemento,'complemento_cfdi_relacionado_p');
      $complemento_cfdi_relacionado = $this->Mgeneral->get_result('relacion_complemento',$id_complemento,'complemento_cdfi_relacionado');
      //error_reporting(1);
      //ini_set('display_errors', 1);
      require $this->config->item('url_real').'cfdi/xml/lib/autoload.php';

      // Especificar la ruta hacia OpenSSL si es necesario
      // UtilCertificado::establecerRutaOpenSSL('E:\OpenSSL-Win64\bin\openssl.exe');

      $cfdi = new Comprobante();

      // Requerido para pago10
      $cfdi->customAttrs['xmlns:pago10'] = 'http://www.sat.gob.mx/Pagos';
      $cfdi->customAttrs['xsi:schemaLocation'] .= ' http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd';

      // Preparar valores
      $moneda = 'XXX';
      $totalPago = '';
      $fecha  = time();
      $tipoCambio = '';
      $folio = $complemento_pago->serie;
      $serie = $complemento_pago->folio;
      $lugarExpedicion = $complemento_pago->lugarExpedicion;
      $fechaPago = time();
      $formaPago = '';

      // Establecer valores generales
      $cfdi->TipoDeComprobante = 'P';
      $cfdi->LugarExpedicion   = $lugarExpedicion;
      $cfdi->Folio             = $folio;
      $cfdi->Serie             = $serie;
      $cfdi->TipoCambio        = $tipoCambio;
      $cfdi->Moneda            = 'XXX';
      $cfdi->setSubTotal(0);
      $cfdi->setTotal(0);
      $cfdi->setFecha($fecha);

      // Agregar emisor
      $cfdi->Emisor = Emisor::init(
          $complemento_pago->emisorRFC,       // RFC
          $complemento_pago->emisorRegimenFiscal, // Régimen Fiscal
          $complemento_pago->emisorNombre        // Nombre (opcional)
      );

      // Agregar receptor
      $cfdi->Receptor = Receptor::init(
          $complemento_pago->receptorRFC,                   // RFC
          $complemento_pago->receptorUsoCFDI,             // Uso del CFDI
          $complemento_pago->receptorNombre // Nombre (opcional)
      );

      // Agregar concepto genérico
      $cfdi->agregarConcepto(Concepto::init('84111506', 1, 'ACT', 'Pago', 0, 0));

      // Agregar CFDIs relacionados (si aplica)
      if($complemento_cfdi_relacionado){
      $tipoRelacion = '02';
      $cfdi->CfdiRelacionados = CfdiRelacionados::init($tipoRelacion);
      foreach($complemento_cfdi_relacionado as $com_rela){
        $cfdi->CfdiRelacionados->agregarUUID($com_rela->uuid);
      }
    }

      // Preparar datos del Complemento de Pago
      $complemento = new ComplementoPago();
      $pagoEl = $complemento->addPago(
          time(),//$complemento_datos->fecha,
          $complemento_datos->forma_pago,
          $complemento_datos->complemento_moneda,
          $complemento_datos->tipo_cambio,
          $complemento_datos->total_pago,
          $complemento_datos->no_operacion, // pago num operacion (opcional)
          $complemento_datos->rfc_ordenante, // pago rfc ordenante (opcional)
          $complemento_datos->banco_ordenante, // pago banco ordenante (opcional)
          $complemento_datos->cta_ordenante, // pago cta ordenante (opcional)
          $complemento_datos->rfc_beneficiario, // pago rfc beneficiario (opcional)
          $complemento_datos->cta_beneficiario // pago cta beneficiari (opcional)
      );

      foreach($complemento_cfdi_relacionado_p as $complem_p){
      // Agregar datos del CFDI relacionado (factura a pagar)
      $complemento->addDoctoRelacionado($pagoEl,
          $complem_p->uuid, // IdDocumento (UUID)
          $complem_p->serie, // Serie
          $complem_p->folio, // Folio
          $complem_p->monedaDR, // MonedaDR (moneda)
          $complem_p->tipoCambioDR, // TipoCambioDR (tipo de cambio)
          $complem_p->metodoDePagoDR, // MetodoDePagoDR (método de pago)
          $complem_p->numParcialidad, // NumParcialidad (número de abono)
          $complem_p->impSaldoAnt, // ImpSaldoAnt (deuda por pagar)
          $complem_p->impPagado, // ImpPagado (importe de este abono)
          $complem_p->impSaldoInsoluto // ImpSaldoInsoluto (nuevo saldo a pagar)
      );

    }

      // Agregar más CFDIs relacionados
      // $complemento->addDoctoRelacionado($pagoEl,
      //     // ...
      // );

      // Agregar complemento de pago al CFDI
      $cfdi->Complemento[] = $complemento;

      // Mostrar XML del CFDI generado hasta el momento (antes de sellar)
      // header('Content-type: application/xml; charset=UTF-8');
      // echo $cfdi->obtenerXml();
      // die;

      // Cargar certificado que se utilizará para generar el sello del CFDI
      $cert = new UtilCertificado();
      $ok = $cert->loadFiles(
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.key',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
          '12345678a'
      );
      if(!$ok) {
          die('Ha ocurrido un error al cargar el certificado.');
      }

      $ok = $cfdi->sellar($cert);
      if(!$ok) {
          die('Ha ocurrido un error al sellar el CFDI.');
      }

      // Mostrar XML del CFDI con el sello
      //header('Content-type: application/xml; charset=UTF-8');
      //echo $cfdi->obtenerXml();
      if($cfdi->obtenerXml()){
            //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
            file_put_contents($this->config->item('url_real')."statics/facturas/precomplemento/$id_complemento.xml", $cfdi->obtenerXml());
            $data_url_prefactura['url_precomplemento'] = base_url()."statics/facturas/precomplemento/$id_complemento.xml";
            $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
            $this->Mgeneral->update_table_row('complemento_pago',$data_url_prefactura,'complementoID',$id_complemento);
            $this->enviar_complemento($id_complemento);
      }else{
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = "Ocurrio un error";
        $data_respone['factura'] = $id_complemento;
        echo json_encode($data_respone);
        die;
      }
      die;

      // Mostrar objeto que contiene los datos del CFDI
      // print_r($cfdi);
    }


    public function enviar_complemento($id_factura){
      $datos_fac = $this->Mgeneral->get_row('complementoID',$id_factura,'complemento_pago');

      # Username and Password, assigned by FINKOK
      $username = 'liberiusg@gmail.com';
      $password = '*Libros7893811';

      # Read the xml file and encode it on base64
      $invoice_path = $this->config->item('url_real')."statics/facturas/precomplemento/$id_factura.xml";//"C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml";//$datos_fac->url_prefactura;//"".base_url()."index.php/factura/generar_factura";
      $xml_file = fopen($invoice_path, "rb");
      $xml_content = fread($xml_file, filesize($invoice_path));
      fclose($xml_file);

      # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
      #$xml_content = base64_encode($xml_content);

      # Consuming the stamp service
      $url = "https://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
      $client = new SoapClient($url);

      $params = array(
        "xml" => $xml_content,
        "username" => $username,
        "password" => $password
      );
      $response = $client->__soapCall("stamp", array($params));
      //print_r($response);
      ####mostrar el XML timbrado solamente, este se mostrara solo si el XML ha sido timbrado o recibido satisfactoriamente.
      //print $response->stampResult->xml;

      ####mostrar el código de error en caso de presentar alguna incidencia
      #print $response->stampResult->Incidencias->Incidencia->CodigoError;
      ####mostrar el mensaje de incidencia en caso de presentar alguna
      if(isset($response->stampResult->Incidencias->Incidencia->MensajeIncidencia)){
        $data_sat['sat_error'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $data_sat['sat_codigo_error'] = $response->stampResult->Incidencias->Incidencia->CodigoError;
        //echo $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $this->Mgeneral->update_table_row('complemento_pago',$data_sat,'complementoID',$id_factura);

        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        
        //redirect('complemento_pago/lista_complementos');
        die;
      }else{
        $data_sat['sat_uuid'] = $response->stampResult->UUID;
        $data_sat['sat_fecha'] = $response->stampResult->Fecha;
        $data_sat['sat_codestatus'] = $response->stampResult->CodEstatus;
        $data_sat['sat_satseal'] = $response->stampResult->SatSeal;
        $data_sat['sat_nocertificadosat'] = $response->stampResult->NoCertificadoSAT;
        $data_sat['sat_error'] = "0";
        $data_sat['sat_codigo_error'] = "0";

        $this->Mgeneral->update_table_row('complemento_pago',$data_sat,'complementoID',$id_factura);
        file_put_contents($this->config->item('url_real')."/statics/facturas/complementoxml/$id_factura.xml", $response->stampResult->xml);
        $data_respone['error'] = 0;
        $data_respone['error_mensaje'] = "Factura timbrada";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);

        //redirect('complemento_pago/lista_complementos');
        die;
      }
      #print $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
    }


    public function genera_pdf($id_factura){
      $datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
      $datos_fac = $this->Mgeneral->get_row('complementoID',$id_factura,'complemento_pago');
      $logo = $this->Mgeneral->get_row('id',1,'informacion')->logo;
      require $this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
      $pdfTemplateDir = $this->config->item('url_real').'cfdi/pdf/lib/templates/';

      $pdf = new Cfdi2Pdf($pdfTemplateDir);

      // datos del archivo. No se mostrarán en el PDF (requeridos)
      $pdf->autor  = 'www.fumigacioneselastillero.com';
      $pdf->titulo = 'Complemento pago';
      $pdf->asunto = 'Comprobante CFDI';

      // texto a mostrar en la parte superior (requerido)
      $pdf->encabezado = $datas_empresa->nombre;

      // nombre del archivo PDF (opcional)
      $pdf->nombreArchivo = 'pdf-cfdi.pdf';

      // mensaje a mostrar en el pie de pagina (opcional)
      $pdf->piePagina = 'Complemento de pago';

      // texto libre a mostrar al final del documento (opcional)
      $pdf->mensajeFactura = '';

      // Solo compatible con CFDI 3.3 (opcional)
      $pdf->direccionExpedicion = $datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;

      // ruta del logotipo (opcional)
      $pdf->logo = $this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';

      // mensaje a mostrar encima del documento (opcional)
      // $pdf->mensajeSello = 'CANCELADO';

      // Cargar el XML desde un string...
      // $ok = $pdf->cargarCadenaXml($cadenaXml);

      // Cargar el XML desde un archivo...
      // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
      // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
      // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
      $archivoXml = $this->config->item('url_real')."/statics/facturas/complementoxml/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';

      $ok = $pdf->cargarArchivoXml($archivoXml);

      if($ok) {
          // Generar PDF para mostrar en el explorador o descargar
          $ok = $pdf->generarPdf(false); // true: descargar. false: mostrar en explorador

          // Guardar PDF en la ruta especificada
          // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
          // $ok = $pdf->guardarPdf($ruta);

          // Obtener PDF como string
          // $pdfStr = $pdf->obtenerPdf();

          if($ok) {
              // PDF generado correctamente.
          } else {
              echo 'Error al generar PDF.';
          }
      }else{
          echo 'Error al cargar archivo XML.';
      }

    }




}
