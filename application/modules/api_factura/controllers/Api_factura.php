<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Api_factura extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url','astillero','correo'));

        date_default_timezone_set('America/Mexico_City');

        // if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function enviar_correo($id_factura){

        
        //$this->load->config('email');
        $this->load->library('email');

        $row = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');

        

        $correo = "".$row->receptor_email."";
        $titulo = "facturacion xehos";
        $cuerpo = "hola mundo";

        $archivos = array();
        $xml = base_url().'statics/facturas/facturas_xml/'.$row->facturaID.'.xml';
        $archivos = array($xml);

        $data['nombre'] = "";
        $data['pdf'] = base_url()."index.php/api_factura/genera_pdf/".$row->facturaID;
        $cuerpo = $this->blade->render('api_factura/correo', $data);


        enviar_correo($correo,$titulo,$cuerpo,$archivos);
    }

    public function crear_factura(){
      $emisor = $this->Mgeneral->get_row('id',1,'datos');
      $id_factura = get_guid();
      $data['facturaID'] = $id_factura;
      $data['status'] = 1;
      $data['emisor_RFC'] = $emisor->rfc;
      $data['emisor_regimenFiscal'] = $emisor->regimen;
      $data['emisor_nombre'] = $emisor->razon_social;
      $data['receptor_uso_CFDI'] = "G03";
      $data['pagada'] = "SI";
      $this->Mgeneral->save_register('factura', $data);


      $data['receptor_nombre'] = $this->input->post('receptor_nombre');
      $data['receptor_id_cliente'] = $this->input->post('receptor_id_cliente');

      $data['factura_moneda'] = 'MXN';//$this->input->post('factura_moneda');
      $data['fatura_lugarExpedicion'] = $this->input->post('fatura_lugarExpedicion');
      $data['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
      $data['receptor_email'] = $this->input->post('receptor_email');
      $data['factura_folio'] = $this->input->post('factura_folio');
      $data['factura_serie'] = $this->input->post('factura_serie');
      $data['receptor_direccion'] = $this->input->post('receptor_direccion');
      $data['factura_formaPago'] = $this->input->post('factura_formaPago');
      $data['factura_medotoPago'] = $this->input->post('factura_medotoPago');
      $data['factura_tipoComprobante'] = 'I';//$this->input->post('factura_tipoComprobante');
      $data['receptor_RFC'] = $this->input->post('receptor_RFC');
      $data['receptor_uso_CFDI'] = $this->input->post('receptor_uso_CFDI');
      $data['pagada'] = 'SI';//$this->input->post('pagada');
      $data['comentario'] = '';//$this->input->post('comentario');
      $data['lugar_tipo'] = 1;
      $this->Mgeneral->update_table_row('factura',$data,'facturaID',$id_factura);


      $data1['concepto_facturaId'] = $id_factura;
      $data1['concepto_NoIdentificacion'] = $this->input->post('concepto_NoIdentificacion');//Numero identificación interna
      $data1['concepto_unidad'] = 8;//es servicio en la tabla de medidas//$this->input->post('concepto_unidad');//Medida interna

      //$data['concepto_descuento'] = $this->input->post('concepto_descuento');
      $data1['clave_sat'] = '84131609';//$this->input->post('clave_sat');// clave sat 84131609
      $data1['unidad_sat'] =  'E48';//$this->input->post('unidad_sat');// unidad sat  E48

      $data1['concepto_nombre'] = $this->input->post('concepto_nombre');//nombre del concepto
      $data1['concepto_precio'] = $this->input->post('concepto_precio');// precio del concepto
      $data1['concepto_importe'] = $this->input->post('concepto_importe');//esto es el precio con iva
      $data1['impuesto_iva'] = '002';//$this->input->post('impuesto_iva');// esto es solo el iva

      $data1['impuesto_iva_tipoFactor'] = 'cuota';//$this->input->post('impuesto_iva_tipoFactor');//cuota
      $data1['impuesto_iva_tasaCuota'] = '.16';//$this->input->post('impuesto_iva_tasaCuota');// .16
      //$data['impuesto_ISR'] = $this->input->post('impuesto_ISR');
      //$data['impuesto_ISR_tasaFactor'] = $this->input->post('impuesto_ISR_tasaFactor');
      //$data['impuestoISR_tasaCuota'] = $this->input->post('impuestoISR_tasaCuota');
      $data1['tipo'] = 0;//$this->input->post('tipo');
      $data1['nombre_interno'] = '';//$this->input->post('nombre_interno');
      $data1['id_producto_servicio_interno'] = $this->input->post('id_producto_servicio_interno');// id del producto
      $data1['concepto_cantidad'] = $this->input->post('concepto_cantidad');
      $data1['importe_iva'] = $this->input->post('importe_iva');
      $data1['fecha_creacion'] = date('Y-m-d H:i:s');

      $this->Mgeneral->save_register('factura_conceptos',$data1);

      $this->generar_factura($id_factura);

      //$this->enviar_correo($id_factura);
      

      //$this->enviar_factura($id_factura);


    }


    public function generar_factura($id_factura){
        //echo "generar factura";
        $factura_nueva = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
        //error_reporting(1);
        //ini_set('display_errors', 1);
        require $this->config->item('url_real').'cfdi/xml/lib/autoload.php';//'C:\xampp\htdocs\elastillero\cfdi\xml\lib\autoload.php';
        $cfdi = new Comprobante();
        // Preparar valores
        $moneda = 'MXN';
        $subtotal  = $this->Mgeneral->factura_subtotal($id_factura);//190.00;
        $iva       = $this->Mgeneral->factura_iva_total($id_factura);//30.40;
        //$descuento =   1.40;
        $total     = $this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura);
        $fecha     = time();
        // Establecer valores generales
        $cfdi->LugarExpedicion   = $factura_nueva->fatura_lugarExpedicion;//'12345';
        $cfdi->FormaPago         = $factura_nueva->factura_formaPago;//'27';
        $cfdi->MetodoPago        = $factura_nueva->factura_medotoPago;//'PUE';
        $cfdi->Folio             = $factura_nueva->factura_folio;//'24';
        $cfdi->Serie             = $factura_nueva->factura_serie;//'A';
        $cfdi->TipoDeComprobante = $factura_nueva->factura_tipoComprobante;//'I';
        $cfdi->TipoCambio        = 1;
        $cfdi->Moneda            = $moneda;
        $cfdi->setSubTotal($subtotal);
        $cfdi->setTotal($total);
        //$cfdi->setDescuento($descuento);
        $cfdi->setFecha($fecha);
  
        // Agregar emisor
        $cfdi->Emisor = Emisor::init(
            $factura_nueva->emisor_RFC,//'LAN7008173R5',                    // RFC
            $factura_nueva->emisor_regimenFiscal,//'622',                             // Régimen Fiscal
            $factura_nueva->emisor_nombre//'Emisor Ejemplo'                   // Nombre (opcional)
        );
  
        // Agregar receptor
        $cfdi->Receptor = Receptor::init(
            $factura_nueva->receptor_RFC,//'TCM970625MB1',                   // RFC
            $factura_nueva->receptor_uso_CFDI,//'I08',                             // Uso del CFDI
            $factura_nueva->receptor_nombre//'Receptor Ejemplo'                 // Nombre (opcional)
        );
  
        $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
        foreach($conceptos_factura as $concepto_fac):
          // Preparar datos del concepto 1
          $concepto = Concepto::init(
              $concepto_fac->clave_sat,//'52141807',                        // clave producto SAT
              $concepto_fac->concepto_cantidad,//'2',                               // cantidad
              $concepto_fac->unidad_sat,//'P83',                             // clave unidad SAT
              $concepto_fac->concepto_nombre,//'Nombre del producto de ejemplo',
              $concepto_fac->concepto_precio,//95.00,                             // precio
              $concepto_fac->concepto_importe//190.00                             // importe
          );
          $concepto->NoIdentificacion = $concepto_fac->id_producto_servicio_interno;//'PR01'; // clave de producto interna
          $concepto->Unidad = $concepto_fac->unidad_sat;//'Servicio';       // unidad de medida interna
          //$concepto->Descuento = 0.0;
  
          // Agregar impuesto (traslado) al concepto 1
          $traslado = new ConceptoTraslado;
          $traslado->Impuesto = '002';          // IVA
          $traslado->TipoFactor = 'Tasa';
          $traslado->TasaOCuota = 0.16;
          $traslado->Base = $concepto_fac->concepto_importe;//$subtotal;
          $traslado->Importe = $concepto_fac->importe_iva;//$iva;
          $concepto->agregarImpuesto($traslado);
  
          // Agregar concepto 1 a CFDI
          $cfdi->agregarConcepto($concepto);
  
          // Agregar más conceptos al CFDI
          // $concepto = Concepto::init(...);
          // ...
          // $cfdi->agregarConcepto($concepto);
       endforeach;
  
  
        // Mostrar XML del CFDI generado hasta el momento
        // header('Content-type: application/xml; charset=UTF-8');
        // echo $cfdi->obtenerXml();
        // die;
  
        // Cargar certificado que se utilizará para generar el sello del CFDI
        $cert = new UtilCertificado();
  
        // Si no se especifica la ruta manualmente, se intentará obtener automatícamente
        // UtilCertificado::establecerRutaOpenSSL();
  
        $ok = $cert->loadFiles(
            //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
            //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
            ''.$this->config->item('url_real').'cfdi/EKU9003173C9.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
            ''.$this->config->item('url_real').'cfdi/EKU9003173C9.key',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
            '12345678a'
        );
        if(!$ok) {
            die('Ha ocurrido un error al cargar el certificado.');
        }
  
        $ok = $cfdi->sellar($cert);
        if(!$ok) {
            die('Ha ocurrido un error al sellar el CFDI.');
        }
  
        // Mostrar XML del CFDI con el sello
        //header('Content-type: application/xml; charset=UTF-8');
        //header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');
  
        //echo $cfdi->obtenerXml();
        if($cfdi->obtenerXml()){
              //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
               file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
              //file_put_contents("https://planificadorempresarial.com/facturacion/statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
              $data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
              $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
              $this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);
              //echo "enviar";
              $this->enviar_factura($id_factura);
             // echo $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";
        }else{
          $data_respone['error'] = 1;
          $data_respone['error_mensaje'] = "Ocurrio un error";
          $data_respone['factura'] = $id_factura;
          echo json_encode($data_respone);
          die;
        }
        //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
        //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
        //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);
  
        //$this->enviar_factura($id_factura);
  
        die;
  
        // Mostrar objeto que contiene los datos del CFDI
        print_r($cfdi);
  
  
        die;
  
  
  
        die('OK');
      }


      public function enviar_factura($id_factura){
        //echo "".base_url()."index.php/factura/generar_factura/".$id_factura;
        //echo  $datos_fac->url_prefactura;
        $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
  
        # Username and Password, assigned by FINKOK
        $username = 'liberiusg@gmail.com';
        $password = '*Libros7893811';
  
        # Read the xml file and encode it on base64
        $invoice_path = $this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml";//"C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml";//$datos_fac->url_prefactura;//"".base_url()."index.php/factura/generar_factura";
        $xml_file = fopen($invoice_path, "rb");
        $xml_content = fread($xml_file, filesize($invoice_path));
        fclose($xml_file);
  
        # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
        #$xml_content = base64_encode($xml_content);
  
        # Consuming the stamp service
        $url = "https://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
        $client = new SoapClient($url);
  
        $params = array(
          "xml" => $xml_content,
          "username" => $username,
          "password" => $password
        );
        $response = $client->__soapCall("stamp", array($params));
        //print_r($response);
        ####mostrar el XML timbrado solamente, este se mostrara solo si el XML ha sido timbrado o recibido satisfactoriamente.
        //print $response->stampResult->xml;
  
        ####mostrar el código de error en caso de presentar alguna incidencia
        #print $response->stampResult->Incidencias->Incidencia->CodigoError;
        ####mostrar el mensaje de incidencia en caso de presentar alguna
        if(isset($response->stampResult->Incidencias->Incidencia->MensajeIncidencia)){
          $data_sat['sat_error'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
          $data_sat['sat_codigo_error'] = $response->stampResult->Incidencias->Incidencia->CodigoError;
          //echo $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
          $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
          $data_respone['error'] = 1;
          $data_respone['error_mensaje'] = $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
          $data_respone['factura'] = $id_factura;
          echo json_encode($data_respone);
          die;
        }else{
          $data_sat['sat_uuid'] = $response->stampResult->UUID;
          $data_sat['sat_fecha'] = $response->stampResult->Fecha;
          $data_sat['sat_codestatus'] = $response->stampResult->CodEstatus;
          $data_sat['sat_satseal'] = $response->stampResult->SatSeal;
          $data_sat['sat_nocertificadosat'] = $response->stampResult->NoCertificadoSAT;
          $data_sat['sat_error'] = "0";
          $data_sat['sat_codigo_error'] = "0";
          $this->Mgeneral->update_table_row('factura',$data_sat,'facturaID',$id_factura);
          file_put_contents($this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml", $response->stampResult->xml);
          $data_respone['error'] = 0;
          $data_respone['error_mensaje'] = "Factura timbrada";
          $data_respone['factura'] = $id_factura;
          echo json_encode($data_respone);
          $this->enviar_correo($id_factura);
          die;
        }
        #print $response->stampResult->Incidencias->Incidencia->MensajeIncidencia;
      }



      public function genera_pdf($id_factura){
        $datos_fac = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
  
        //$datos_cliente = $this->Mgeneral->get_row('id',$datos_fac->receptor_id_cliente,'clientes');
  
        $datas_empresa = $this->Mgeneral->get_row('id',1,'datos');
        $logo = $this->Mgeneral->get_row('id',1,'informacion')->logo;
        require $this->config->item('url_real').'cfdi/pdf/lib/Cfdi2Pdf.php';
        $pdfTemplateDir = $this->config->item('url_real').'cfdi/pdf/lib/templates/';
  
        $pdf = new Cfdi2Pdf($pdfTemplateDir);
  
        // datos del archivo. No se mostrarán en el PDF (requeridos)
        $pdf->autor  = 'www.xehos.com/';
        $pdf->titulo = 'Factura';
        $pdf->asunto = 'CFDI';
  
        // texto a mostrar en la parte superior (requerido)
        $pdf->encabezado = 'XEHOS';//$datas_empresa->nombre;
  
        // nombre del archivo PDF (opcional)
        $pdf->nombreArchivo = 'pdf-cfdi.pdf';
  
        // mensaje a mostrar en el pie de pagina (opcional)
        $pdf->piePagina = 'Factura';
  
        // texto libre a mostrar al final del documento (opcional)
        $pdf->mensajeFactura = "";//$datos_cliente->comentario_extra;
  
        // Solo compatible con CFDI 3.3 (opcional)
        $pdf->direccionExpedicion = 'Av. Constituyentes No. 42 Ote, Villas del Sol Querétaro';//$datas_empresa->municipio." ".$datas_empresa->ciudad." ".$datas_empresa->estado;
  
        // ruta del logotipo (opcional)
        $pdf->logo = $this->config->item('url_real').$logo;///dirname(__FILE__).'/logo.png';
  
        // mensaje a mostrar encima del documento (opcional)
        // $pdf->mensajeSello = 'CANCELADO';
  
        // Cargar el XML desde un string...
        // $ok = $pdf->cargarCadenaXml($cadenaXml);
  
        // Cargar el XML desde un archivo...
        // $archivoXml = 'ejemplo_cfdi33_nomina12.xml';
        // $archivoXml = 'ejemplo_cfdi33_cce11.xml';
        // $archivoXml = 'ejemplo_cfdi33_pagos10.xml';
        $archivoXml = $this->config->item('url_real')."/statics/facturas/facturas_xml/$id_factura.xml";//'ejemplos/1C963675246992B7A8293A0BAFE053DD.xml';
  
        $ok = $pdf->cargarArchivoXml($archivoXml);
  
        if($ok) {
            // Generar PDF para mostrar en el explorador o descargar
            $ok = $pdf->generarPdf(true); // true: descargar. false: mostrar en explorador
  
            // Guardar PDF en la ruta especificada
            // $ruta = dirname(__FILE__).DIRECTORY_SEPARATOR;
            // $ok = $pdf->guardarPdf($ruta);
  
            // Obtener PDF como string
            // $pdfStr = $pdf->obtenerPdf();
  
            if($ok) {
                // PDF generado correctamente.
            } else {
                echo 'Error al generar PDF.';
            }
        }else{
            echo 'Error al cargar archivo XML.';
        }
  
      }


    // DE AQUÍ PARA ABAJO SON PRUEBAS , NO SE UTILIZAN
    public function leer(){
        echo exec('whoami');
        die();
        $json = '{"lugar_expedicion":"28983","forma_pago":"27","metodo_pago":"PUE","folio":"21","serie":"A","tipo_comprobante":"I","subtotal":190,"iva":11.4,"descuento":1.4,"total":200,"rfc_emisor":"LAN7008173R5","regimen_fiscal_emisor":"622","nombre_emisor":"Emisor Ejemplo","rfc_receptor":"LAN7008173R5","uso_cfdi":"I08","nombre_receptor":"nombre","receptor_email":"mail@mail.com","receptor_direccion":"receptor_direccion","comentarios":"comentarios","conceptos":[{"clave_sat":"52141807","cantidad":2,"nombre_concepto":"nombre producto","precio":95,"importe":190,"clave_interna":"PR01","unidad_interna":"servicio","descuento":0,"impuesto":"002","tipo_factor":"Cuota","tasa_cuota":0.16,"base_subtotal":190,"base_iva":11.4},{"clave_sat":"52141807","cantidad":2,"nombre_concepto":"nombre producto","precio":95,"importe":190,"clave_interna":"PR01","unidad_interna":"servicio","descuento":0,"impuesto":"002","tipo_factor":"Cuota","tasa_cuota":0.16,"base_subtotal":190,"base_iva":11.4},{"clave_sat":"52141807","cantidad":2,"nombre_concepto":"nombre producto","precio":95,"importe":190,"clave_interna":"PR01","unidad_interna":"servicio","descuento":0,"impuesto":"002","tipo_factor":"Cuota","tasa_cuota":0.16,"base_subtotal":190,"base_iva":11.4}]}';

            $obj = json_decode($json);
            echo $obj->lugar_expedicion;
            echo $obj->receptor_email;
            //echo $obj->conceptos;
            foreach($obj->conceptos as $row):
                echo $row->clave_interna;
            endforeach;
            //var_dump($obj);

           

            // GUARDA DATOS CABECERA ------------------------------
            $id_factura = get_guid();
            $data['facturaID'] = $id_factura;
            $data['status'] = 1;
            $data['emisor_RFC'] = $obj->rfc_emisor;
            $data['emisor_regimenFiscal'] = $obj->regimen_fiscal_emisor;
            $data['emisor_nombre'] = $obj->nombre_emisor;
            $data['pagada'] = "SI";

            $data['receptor_nombre'] = $obj->rfc_receptor;
            $data['receptor_id_cliente'] = "";
            $data['factura_moneda'] = $obj->nombre_receptor;

            $data['fatura_lugarExpedicion'] = $obj->lugar_expedicion;
            $data['factura_fecha'] = date('Y-m-d H:i:s');//$this->input->post('numero');
            $data['receptor_email'] = $obj->receptor_email;

            $data['factura_folio'] = $obj->folio;
            $data['factura_serie'] = $obj->serie;

            $data['receptor_direccion'] = $obj->receptor_direccion;
            $data['factura_formaPago'] = $obj->forma_pago;
            $data['factura_medotoPago'] = $obj->metodo_pago;
            $data['factura_tipoComprobante'] = $obj->tipo_comprobante;
            $data['receptor_RFC'] = $obj->rfc_receptor;
            $data['receptor_uso_CFDI'] = $obj->uso_cfdi;
            $data['comentario'] = $obj->comentarios;

            $this->Mgeneral->save_register('factura', $data);
            //------------------------------------------------------

            // CONCEPTOS ------------------------------------------
            foreach($obj->conceptos as $row):
                echo $row->clave_interna;
                $data_conceptos['concepto_facturaId'] = $id_factura;
                $data_conceptos['concepto_NoIdentificacion'] = $this->input->post('concepto_NoIdentificacion');
                $data_conceptos['concepto_unidad'] = $this->input->post('concepto_unidad');
                //$data['concepto_descuento'] = $this->input->post('concepto_descuento');
                $data_conceptos['clave_sat'] = $this->input->post('clave_sat');
                $data_conceptos['unidad_sat'] = $this->input->post('unidad_sat');
                $data_conceptos['concepto_nombre'] = $this->input->post('concepto_nombre');
                $data_conceptos['concepto_precio'] = $this->input->post('concepto_precio');
                $data_conceptos['concepto_importe'] = $this->input->post('concepto_importe');
                $data_conceptos['impuesto_iva'] = $this->input->post('impuesto_iva');
                $data_conceptos['impuesto_iva_tipoFactor'] = $this->input->post('impuesto_iva_tipoFactor');
                $data_conceptos['impuesto_iva_tasaCuota'] = $this->input->post('impuesto_iva_tasaCuota');
                $data_conceptos['impuesto_ISR'] = $this->input->post('impuesto_ISR');
                $data_conceptos['impuesto_ISR_tasaFactor'] = $this->input->post('impuesto_ISR_tasaFactor');
                $data_conceptos['impuestoISR_tasaCuota'] = $this->input->post('impuestoISR_tasaCuota');
                $data_conceptos['tipo'] = $this->input->post('tipo');
                $data_conceptos['nombre_interno'] = $this->input->post('nombre_interno');
                $data_conceptos['id_producto_servicio_interno'] = $this->input->post('id_producto_servicio_interno');
                $data_conceptos['concepto_cantidad'] = $this->input->post('concepto_cantidad');
                $data_conceptos['importe_iva'] = $this->input->post('importe_iva');
                $data_conceptos['fecha_creacion'] = date('Y-m-d H:i:s');
                $this->Mgeneral->save_register('factura_conceptos',$data_conceptos);
            endforeach;

            //----------------------------------------------------

    }

    public function prueba(){
        $data['lugar_expedicion'] = "28983";
        $data['forma_pago'] = "27";
        $data['metodo_pago'] = "PUE";
        $data['folio'] = "21";
        $data['serie'] = "A";
        $data['tipo_comprobante'] = "I";

        $data['subtotal'] = 190;
        $data['iva'] = 11.40;
        $data['descuento'] = 1.40;
        $data['total'] = 200.00;

        $data['rfc_emisor'] = "LAN7008173R5";
        $data['regimen_fiscal_emisor'] = "622";
        $data['nombre_emisor'] = "Emisor Ejemplo";

        $data['rfc_receptor'] = "LAN7008173R5";
        $data['uso_cfdi'] = "I08";
        $data['nombre_receptor'] = "nombre";
        $data['receptor_email'] = "mail@mail.com";
        $data['receptor_direccion'] = "receptor_direccion";

        $data['comentarios'] = "comentarios";

        

        
        $data['conceptos'] = array();
       for($i = 0; $i <3; $i ++):

        $conceptos = array();

        $conceptos['clave_sat'] = "52141807";
        $conceptos['cantidad'] = 2;
        $conceptos['nombre_concepto'] = "nombre producto";
        $conceptos['precio'] = 95.00;
        $conceptos['importe'] = 190.00;

        $conceptos['clave_interna'] = "PR01";
        $conceptos['unidad_interna'] = "servicio";
        $conceptos['descuento'] = 0.0;

        $conceptos['impuesto'] = "002";
        $conceptos['tipo_factor'] = "Cuota";
        $conceptos['tasa_cuota'] = 0.16;
        $conceptos['base_subtotal'] = 190;
        $conceptos['base_iva'] = 11.40;

        array_push($data['conceptos'],$conceptos);

       endfor;

       echo json_encode($data);
    }

    public function genera(){

        $lugar_expedicion = "12345";
        $forma_pago = "27";
        $metodo_pago = "PUE";
        $folio = "21";
        $serie = "A";
        $tipo_comprobante = "I";



        $moneda = 'MXN';
        $subtotal  = 190.00;
        $iva       =  11.40;
        $descuento =   1.40;
        $total     = 200.00;
        $fecha     = time();

        //emisor
        $rfc_emisor = "LAN7008173R5";
        $regimen_fiscal_emisor = "622";
        $nombre_emisor = "Emisor Ejemplo";

        //receptor
        $rfc_receptor = "LAN7008173R5";
        $uso_cfdi = "I08";
        $nombre_receptor = "Emisor Ejemplo";

        
        // Establecer valores generales
        $cfdi->LugarExpedicion   = $lugar_expedicion;
        $cfdi->FormaPago         = $forma_pago;
        $cfdi->MetodoPago        = $metodo_pago;
        $cfdi->Folio             = $folio;
        $cfdi->Serie             = $serie;
        $cfdi->TipoDeComprobante = $tipo_comprobante;
        $cfdi->TipoCambio        = 1;
        $cfdi->Moneda            = $moneda;
        $cfdi->setSubTotal($subtotal);
        $cfdi->setTotal($total);
        $cfdi->setDescuento($descuento);
        $cfdi->setFecha($fecha);
        
        // Agregar emisor
        $cfdi->Emisor = Emisor::init(
            $rfc_emisor,                    // RFC
            $regimen_fiscal_emisor,                             // Régimen Fiscal
            $nombre_emisor                   // Nombre (opcional)
        );
        
        // Agregar receptor
        $cfdi->Receptor = Receptor::init(
            $rfc_receptor,                   // RFC
            $uso_cfdi,                             // Uso del CFDI
            $nombre_receptor                 // Nombre (opcional)
        );
        
        // Preparar datos del concepto 1
        $concepto = Concepto::init(
            '52141807',                        // clave producto SAT
            '2',                               // cantidad
            'P83',                             // clave unidad SAT
            'Nombre del producto de ejemplo',
            95.00,                             // precio
            190.00                             // importe
        );
        $concepto->NoIdentificacion = 'PR01'; // clave de producto interna
        $concepto->Unidad = 'Servicio';       // unidad de medida interna
        $concepto->Descuento = 0.0;
        
        // Agregar impuesto (traslado) al concepto 1
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Cuota';
        $traslado->TasaOCuota = 0.16;
        $traslado->Base = $subtotal;
        $traslado->Importe = $iva;
        $concepto->agregarImpuesto($traslado);
        
        
    }


    
}