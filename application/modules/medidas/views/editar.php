<script>
         menu_activo = "catalogo";
$("#menu_catalogos_medidas").last().addClass("menu_estilo");
$(document).ready(function(){
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/medidas/editar_medida/<?php echo $medi->medidaId?>";
  ajaxJson(url,{"medidaNombre":$("#medidaNombre").val(),"medidaUnidades":$("#medidaUnidades").val(), "medidaUnidadNombre":$("#medidaUnidadNombre").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect("GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/medidas/alta");
    }


  });
});



});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Alta de medidas</strong>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre Medida</label>
                                  <input id="medidaNombre" name="medidaNombre" type="text" class="form-control cc-exp form-control-sm" value="<?php echo $medi->medidaNombre;?>" placeholder="Nombre" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Unidades por medida</label>
                                  <input id="medidaUnidades" name="medidaUnidades" type="text" class="form-control cc-exp form-control-sm" value="<?php echo $medi->medidaUnidades;?>" placeholder="#" autocomplete="off" style="width:45px;">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Unidad Nombre</label>
                                  <input id="medidaUnidadNombre" name="medidaUnidadNombre" type="text" class="form-control cc-exp form-control-sm" value="<?php echo $medi->medidaUnidadNombre;?>" placeholder="unidad nombre" autocomplete="off" >
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          
                      </div>


                    <div class="row">
                      <div class="col-6">
                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Editar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                     </div>
                     </div>


                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
