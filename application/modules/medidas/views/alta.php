<script>
         menu_activo = "catalogo";
$("#menu_catalogos_medidas").last().addClass("menu_estilo");
$(document).ready(function(){
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/medidas/guarda";
  ajaxJson(url,{"medidaNombre":$("#medidaNombre").val(),"medidaUnidades":$("#medidaUnidades").val(), "medidaUnidadNombre":$("#medidaUnidadNombre").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect("GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/medidas/alta");
    }


  });
});

$(".eliminar_relacion").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar el registro?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });





$(".btn_eliminar").click(function(event){
                     event.preventDefault();

                     bootbox.dialog({
                         message: "Desea eliminar los registros seleccionados?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {

                                   $('.myCheckBox').each(function(){ //this.checked = true; 
                                      if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());
                                           id = $(this).val();
                                           url = "<?php echo base_url()?>index.php/medidas/eliminar/"+id;//$("#delete"+id).attr('href');
                                           $("#borrar_"+id).slideUp();
                                           $.get(url);
                                                };
                                           });

                                   
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });


                    
                 });


  

  $("#todos_boton").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });

  $(".btn_editar").click(function(event){
                     event.preventDefault();

                      aux = 1;
                      $('.myCheckBox').each(function(){ //this.checked = true; 
                            if ($(this).is(':checked')) {
                                      /*
     <td><input type="checkbox" name="" class="myCheckBox" id="<?php echo $row->medidaId?>" value="<?php echo $row->medidaId?>"></td>
                  <td><span class="textNombre_<?php echo $row->medidaId?> texto_nombre"><?php echo $row->medidaNombre;?></span></td>
                  <td><span class="textUnidad_<?php echo $row->medidaId?> texto_unidad"><?php echo $row->medidaUnidades;?></span></td>
                  <td><span class="textMedida_<?php echo $row->medidaId?> texto_medida"><?php echo $row->medidaUnidadNombre;?></td>
                    <span hidden="hidden" class="id_des_<?php echo $row->medidaId?> texto_id" type="hidden"><?php echo $row->medidaId?></span>
                                      */  
                                id = $(this).val();
                                console.log(id);
                                var txt = $(".textNombre_"+id+"").html();
                                $(".textNombre_"+id+"").replaceWith("<input value='" + txt + "' name='"+aux+"[medidaNombre]'/>");

                                var txt_des1 = $(".textUnidad_"+id+"").html();
                                $(".textUnidad_"+id+"").replaceWith("<input value='" + txt_des1 + "' name='"+aux+"[medidaUnidades]'/>");

                                var txt_des2 = $(".textMedida_"+id+"").html();
                                $(".textMedida_"+id+"").replaceWith("<input value='" + txt_des2 + "' name='"+aux+"[medidaUnidadNombre]'/>");


                                var txt_id = $(".id_des_"+id+"").html();
                                $(".id_des_"+id+"").replaceWith("<input type='hidden' value='" + txt_id + "' name='"+aux+"[id]'/>");

                                aux = aux +1;
                            };
                      });

                      $("body").keypress(function(e) {
                        if(e.which == 13) {
                          //obtenerDatos();
                          
                          var form = $("#formulario_editar").serializeArray();
                         // console.log(form);
                            url = "<?php echo base_url()?>index.php/medidas/guarda_edicion"
                            ajaxJson(url,form,
                                        "POST","",function(result){
                                            correoValido = false;
                                            console.log(result);
                                            exito_redirect("EDITADO CON EXITO","success","<?php echo base_url()?>index.php/medidas/alta");
                                            
                                          });

                        }
                      });



                    
                 });

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>


   <div class="row">
      <div class="col-lg-12">
       
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Nuevo
        </button>

        <button type="button" class="btn btn-info btn_editar">Editar</button>

        <button type="button" class="btn btn-danger btn_eliminar">Eliminar</button>

      </div><!--/.col-->
    </div>

<!--div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Alta de medidas</strong>
        </div>
        <div class="card-body">
          
          <div id="pay-invoice">
              <div class="card-body">


                  
              </div>
          </div>

        </div>
    </div> 

  </div>
    </div-->


    </div><!-- .animated -->
</div><!-- .content -->


<script type="text/javascript">
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});
    } );
</script>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                   <form id="formulario_editar">
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>
                                   
                            <input type="checkbox" name=""  id="todos_boton" class="todos_boton" >
                                    
                          </th>
                          <th>Unidad</th>
                          <th>Descripción</th>
                          <th>Cantidad</th>
                         
                          <!--th>Opciones</th-->
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(is_array($rows)):?>
                        <?php foreach($rows as $row):?>
                          <tr id="borrar_<?php echo $row->medidaId?>">
                            <td><input type="checkbox" name="" class="myCheckBox" id="<?php echo $row->medidaId?>" value="<?php echo $row->medidaId?>"></td>
                            <td><span class="textNombre_<?php echo $row->medidaId?> texto_nombre"><?php echo $row->medidaNombre;?></span></td>
                            <td><span class="textMedida_<?php echo $row->medidaId?> texto_medida"><?php echo $row->medidaUnidadNombre;?></td>
                            <td><span class="textUnidad_<?php echo $row->medidaId?> texto_unidad"><?php echo $row->medidaUnidades;?></span></td>

                            
                              <span hidden="hidden" class="id_des_<?php echo $row->medidaId?> texto_id" type="hidden"><?php echo $row->medidaId?></span>
                            <!--td><?php echo $row->plagaDescripcion?></td-->

                            <!--td>
                              <a href="<?php echo base_url()?>index.php/medidas/editar/<?php echo $row->medidaId;?>">
                              <button type="button" class="btn btn-info">Editar</button>
                              </a>
                              <a href="<?php echo base_url()?>index.php/medidas/eliminar/<?php echo $row->medidaId;?>" class="eliminar_relacion" flag="<?php echo $row->medidaId?>" id="delete<?php echo $row->medidaId?>" >
                              <button type="button" class="btn btn-danger ">Eliminar</button>
                              </a>
                            </td-->
                          </tr>
                       <?php endforeach;?>
                       <?php endif;?>
                      </tbody>

                    </table>
                  </form>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->




<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva unidad </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Unidad</label>
                                  <input id="medidaNombre" name="medidaNombre" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="Nombre" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Cantidad</label>
                                  <input id="medidaUnidades" name="medidaUnidades" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="#" autocomplete="off" style="width:45px;">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Descripción</label>
                                  <input id="medidaUnidadNombre" name="medidaUnidadNombre" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="unidad nombre" autocomplete="off" >
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          
                      </div>

                      <div class="row">
                        <div class="col-12"> 

                          <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>

                        </div>
                        

                      </div>

                      


                  </form>
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>

