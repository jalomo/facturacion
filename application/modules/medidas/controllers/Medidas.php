<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Medidas extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $titulo['titulo'] = "Unidades" ;
      $titulo['titulo_dos'] = "Alta Medidas" ;
      $rows['rows'] = $this->Mgeneral->get_result('status',0,'medidas');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('medidas/alta', $rows, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
    }

    public function guarda_edicion(){

      if($this->input->post()){
        $textos = $this->input->post();
        $this->Mgeneral->save_editar_formulario($textos,'medidaId','medidas');
        echo "editado";
      }
    }

    public function guarda(){
      $this->form_validation->set_rules('medidaNombre', 'medidaNombre', 'required');
      $this->form_validation->set_rules('medidaUnidades', 'medidaUnidades', 'required');
      $this->form_validation->set_rules('medidaUnidadNombre', 'medidaUnidadNombre', 'required');
      $response = validate($this);

      if($response['status']){
        $data['medidaNombre'] = $this->input->post('medidaNombre');
        $data['medidaUnidades'] = $this->input->post('medidaUnidades');
        $data['medidaUnidadNombre'] = $this->input->post('medidaUnidadNombre');

        $this->Mgeneral->save_register('medidas', $data);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
    }

    public function editar($id_medida){
      $rows['medi'] = $this->Mgeneral->get_row('medidaId',$id_medida,'medidas');
      $titulo['titulo'] = "Medidas" ;
      $titulo['titulo_dos'] = "Editar Medidas" ;
      $rows['rows'] = $this->Mgeneral->get_table('medidas');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('medidas/editar', $rows, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
    }

    public function editar_medida($id){
      $this->form_validation->set_rules('medidaNombre', 'medidaNombre', 'required');
      $this->form_validation->set_rules('medidaUnidades', 'medidaUnidades', 'required');
      $this->form_validation->set_rules('medidaUnidadNombre', 'medidaUnidadNombre', 'required');
      $response = validate($this);

      if($response['status']){
        $data['medidaNombre'] = $this->input->post('medidaNombre');
        $data['medidaUnidades'] = $this->input->post('medidaUnidades');
        $data['medidaUnidadNombre'] = $this->input->post('medidaUnidadNombre');

        $this->Mgeneral->update_table_row('medidas',$data,'medidaId',$id);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));

    }

    public function eliminar($id_plaga){
      $data['status'] = 1;
      $this->Mgeneral->update_table_row('medidas',$data,'medidaId',$id_plaga);

    }


}
