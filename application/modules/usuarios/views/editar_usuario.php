<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;
}
button{
  font-family: 'Roboto', sans-serif !important;;
  font-size: 12px !important;;
}
</style>
<script>
  menu_activo = "usuarios";
$("#menu_alta_user").last().addClass("menu_estilo");

$(document).ready(function(){

  $("#usuarioFechaNacimiento").datepicker({
    altFormat: "YY-mm-dd",
    appendText: "(mes-día-año)"
  });


  $("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/usuarios/guarda_usuario";
  ajaxJson(url,{//"usuarioNombre":$("#usuarioNombre").val(),
                //"usuarioApellidoPaterno":$("#usuarioApellidoPaterno").val(),
                //"usuarioApellidoMaterno":$("#usuarioApellidoMaterno").val(),
                //"usuarioFechaNacimiento":$("#usuarioFechaNacimiento").val(),
                //"usuarioNivelestudios":$("#usuarioNivelestudios").val(),
                "idusuario":$("#iUsuario").val(),
                "usuario":$("#usuario").val(),
                "password":$("#password").val(),
               "idRol":$("#idRol").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("USUARIO GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/usuarios/ver_usuarios");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});

});
</script>



<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Editar de usuarios</strong>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <!--div class="row">
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                  <input id="usuarioNombre" name="usuarioNombre" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Nombre" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Apellido paterno</label>
                                <input id="usuarioApellidoPaterno" name="usuarioApellidoPaterno" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Apellido paterno" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Apellido materno</label>
                                  <input id="usuarioApellidoMaterno" name="usuarioApellidoMaterno" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Apellido materno" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div-->

                      <div class="row">

                          
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Empleado</label>
                                <?php echo nombre_usuario_completo($admin->idusuario);?>

                            </div>
                          </div>
                          
                         
                      </div>

                      <div class="row">

                        <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Rol</label>
                                <select class="form-control form-control-sm cc-exp" id="idRol">
                                  <?php foreach($roles as $rol):?>
                                  <option value="<?php echo $rol->rolId;?>"><?php echo $rol->rolNombre;?></option>
                                <?php endforeach;?>
                                </select>

                            </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Usuario</label>
                                  <input id="usuario" name="usuario" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $admin->adminUsername?>" placeholder="Usuario" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Contraseña</label>
                                <input id="password" name="password" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $admin->adminPassword?>" placeholder="Contraseña"  autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>



                      </div>

                      
                      <div class="row">
                          <div class="col-6">
                            <div align="right">
                            <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                                <i class="fa fa-edit fa-lg"></i>&nbsp;
                                <span id="payment-button-amount">Guardar</span>
                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                            </button>
                            <div align="center">
                             <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                            </div>
                            </div>
                          </div>
                      </div>


                    <br/>
                    <!--p class="font-weight-light">
                      Estos usuarios podran entrar al sistema con el "Rol" que les asignes.<br/>
                      Estos usuarios podran entrar a la APP.<br/>
                      Para agregar un usuario de una empresa, ir a "Empleados->Agregar" y asignar en el campo "Empresa".
                    </p-->
                    
                      


                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
