<script type="text/javascript">
  menu_activo = "usuarios";
   $("#menu_lista_user").last().addClass("menu_estilo");
  $(document).ready(function() {
     $('#bootstrap-data-table').DataTable({
      "language": {
        "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
      }
    });

    $("#cargando").hide();
    $('#alta_usuario').submit(function(event){
      event.preventDefault();
      $("#enviar").hide();
      $("#cargando").show();
      var url ="<?php echo base_url()?>index.php/usuarios/guarda_usuario";
      ajaxJson(url,{//"usuarioNombre":$("#usuarioNombre").val(),
                    //"usuarioApellidoPaterno":$("#usuarioApellidoPaterno").val(),
                    //"usuarioApellidoMaterno":$("#usuarioApellidoMaterno").val(),
                    //"usuarioFechaNacimiento":$("#usuarioFechaNacimiento").val(),
                    //"usuarioNivelestudios":$("#usuarioNivelestudios").val(),
                    "idusuario":$("#iUsuario").val(),
                    "usuario":$("#usuario").val(),
                    "password":$("#password").val(),
                  "idRol":$("#idRol").val()},
                "POST","",function(result){
        correoValido = false;
        console.log(result);
        json_response = JSON.parse(result);
        obj_output = json_response.output;
        obj_status = obj_output.status;
        if(obj_status == false){
          aux = "";
          $.each( obj_output.errors, function( key, value ) {
            aux +=value+"<br/>";
          });
          exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
          $("#enviar").show();
          $("#cargando").hide();
        }
        if(obj_status == true){
          exito_redirect("USUARIO GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/usuarios/ver_usuarios");
          $("#enviar").show();
          $("#cargando").hide();
        }


      });
    });


    $("#todos_boton").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
    });



    $(".btn_eliminar").click(function(event){
                     event.preventDefault();

                     bootbox.dialog({
                         message: "Desea eliminar los registros seleccionados?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {

                                   $('.myCheckBox').each(function(){ //this.checked = true; 
                                      if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());
                                           id = $(this).val();
                                           url = "<?php echo base_url()?>index.php/usuarios/eliminar/"+id;//$("#delete"+id).attr('href');
                                           $("#borrar_"+id).slideUp();
                                           $.get(url);
                                                };
                                           });

                                   
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
      });

      $(".btn_editar").click(function(event){
                     event.preventDefault();

                      //$( "#todos_boton" ).prop( "checked", true );

                     //$('.myCheckBox').each(function(){ this.checked = true; });
                      aux = 1;
                      $('.myCheckBox').each(function(){ //this.checked = true; 
                            if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());           
                                id = $(this).val();
                                console.log(id);
                                var txt = $(".textNombre_"+id+"").html();
                                $(".textNombre_"+id+"").replaceWith("<input value='" + txt + "' name='"+aux+"[adminUsername]'/>");

                                var txt = $(".textPassword_"+id+"").html();
                                $(".textPassword_"+id+"").replaceWith("<input value='" + txt + "' name='"+aux+"[adminPassword]'/>");




                                var uniOrigenId = $(".id_des_"+id+"").html();
                                url = "<?php echo base_url()?>index.php/usuarios/roles_usuario/"+uniOrigenId+"/"+aux+"/idRol";
                                ajaxJson(url,{},
                                        "POST","",function(result){
                                         $(".unidadOrigen"+id+"").replaceWith(result);
                                });

                                


                                var txt_id = $(".id_des_"+id+"").html();
                                $(".id_des_"+id+"").replaceWith("<input type='hidden' value='" + txt_id + "' name='"+aux+"[id]'/>");

                                aux = aux +1;
                            };
                      });

                      $("body").keypress(function(e) {
                        if(e.which == 13) {
                          //obtenerDatos();
                          
                          var form = $("#formulario_editar").serializeArray();
                         // console.log(form);

                            url = "<?php echo base_url()?>index.php/usuarios/guarda_edicion"
                            ajaxJson(url,form,
                                        "POST","",function(result){
                                            correoValido = false;
                                            console.log(result);
                                            exito_redirect("EDITADO CON EXITO","success","<?php echo base_url()?>index.php/usuarios/ver_usuarios");
                                            
                                          });

                        }
                      });



                    
                 });

  });
</script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">



        <div class="col-md-12">
            <div class="card">
                <!--div class="card-header">
                    <strong class="card-title">Empleados</strong>
                </div-->
                <div class="row">
                  <div class="col-lg-12">
                    <!--div class="card">
                        <div class="card-header">
                            <strong class="card-title">Alta de plagas</strong>
                        </div>
                        <div class="card-body">
                         
                          <div id="pay-invoice">
                              <div class="card-body">

                                    
                                  
                              </div>
                          </div>

                        </div>
                    </div--> 

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                          Nuevo
                    </button>

                    <button type="button" class="btn btn-info btn_editar">Editar</button>

                    <button type="button" class="btn btn-danger btn_eliminar">Eliminar</button>

                  </div><!--/.col-->
                    </div>
                <div class="card-body">
                <form id="formulario_editar">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                    <th>
                              
                              <input type="checkbox" name=""  id="todos_boton" class="todos_boton" >
                              
                            </th>
                      <th>Nombre</th>
                      <th>Usuario</th>
                      <th>Contraseña</th>
                      <th>Rol</th>
                      <th>Puesto</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(is_array($admin)):?>
                    <?php foreach($admin as $row):?>
                      <tr id="borrar_<?php echo $row->adminId?>">
                      <td><input type="checkbox" name="" class="myCheckBox" id="<?php echo $row->adminId?>" value="<?php echo $row->adminId?>"></td>
                        <td><?php echo nombre_usuario_completo($row->idusuario)?></td>
                        
                        <!--td><?php echo $row->adminUsername?></td-->

                        <td><span class="textNombre_<?php echo $row->adminId?> texto_nombre"><?php echo $row->adminUsername;?></span></td>

                        <!--td>****</td-->
                        <td><span class="textPassword_<?php echo $row->adminId?> texto_pass">*</span></td>

                        <!--td><?php echo nombre_rol($row->idRol);?></td-->
                        <td><span class="unidadOrigen<?php echo $row->adminId?> unidad_origen"><?php echo nombre_rol($row->idRol);?></span></td>
                        <span hidden="hidden" class="id_des_<?php echo $row->adminId?> texto_id" type="hidden"><?php echo $row->adminId?></span>

                        <?php $puesto = $this->Mgeneral->get_row('usuarioId',$row->idusuario,'usuarios')?>
                        <td><?php echo nombre_puestos($puesto->usuarioPuesto );?></td>
                        
                      </tr>
                  <?php endforeach;?>
                  <?php endif;?>
                  </tbody>
                </table>
              </form>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->




<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo usuario </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i class="fas fa-times"></i>
        </button>
      </div>
      <div class="modal-body" align="center">
      <form action="" method="post" novalidate="novalidate" id="alta_usuario">




        <div class="row">

            
            <div class="col-3">
              <div class="form-group">
                  <label for="cc-exp" class="control-label mb-1">Empleado</label>
                  <select class="form-control form-control-sm cc-exp" id="iUsuario">
                    <?php foreach($empleados as $rol):?>
                    <option value="<?php echo $rol->usuarioId;?>"><?php echo nombre_usuario_completo($rol->usuarioId);?></option>
                  <?php endforeach;?>
                  </select>

              </div>
            </div>


            <div class="col-3">
              <div class="form-group">
                  <label for="cc-exp" class="control-label mb-1">Rol</label>
                  <select class="form-control form-control-sm cc-exp" id="idRol">
                    <?php foreach($roles as $rol):?>
                    <option value="<?php echo $rol->rolId;?>"><?php echo $rol->rolNombre;?></option>
                  <?php endforeach;?>
                  </select>

              </div>
            </div>
            
          
        </div>

        <div class="row">

          

            <div class="col-3">
                <div class="form-group">
                    <label for="cc-exp" class="control-label mb-1">Usuario</label>
                    <input id="usuario" name="usuario" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Usuario" autocomplete="off">
                    <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                </div>
            </div>
            <div class="col-3">
              <div class="form-group">
                  <label for="cc-exp" class="control-label mb-1">Contraseña</label>
                  <input id="password" name="password" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Contraseña"  autocomplete="off">
                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
              </div>
            </div>



        </div>


        <div class="row">
            <div class="col-6">
              <div align="right">
              <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                  <i class="fa fa-edit fa-lg"></i>&nbsp;
                  <span id="payment-button-amount">Guardar</span>
                  <span id="payment-button-sending" style="display:none;">Sending…</span>
              </button>
              <div align="center">
              <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
              </div>
              </div>
            </div>
        </div>


        <br/>




      </form>
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>


