<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Usuarios extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html','companies','validation','astillero', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta_usuario(){

      $rol_usuario = get_rol_id($this->session->userdata('id'));
      $data['permiso'] = get_rol_permiso('USUARIOS',$rol_usuario);

      $titulo['titulo'] = "Agregar Usuario" ;
      $titulo['titulo_dos'] = "Agregar Usuario" ;
      $data['roles'] = $this->Mgeneral->get_table('roles');

      $data['empleados'] = $this->Mgeneral->get_result('status',0,'usuarios');

      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('usuarios/alta_usuario', $data, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }

    public function guarda_usuario(){
      //$this->config->set_item('language', 'spanish');
      //$this->form_validation->set_rules('usuarioNombre', 'usuarioNombre', 'required');
      //$this->form_validation->set_rules('usuarioApellidoPaterno', 'usuarioApellidoPaterno', 'required');
      //$this->form_validation->set_rules('usuarioApellidoMaterno', 'usuarioApellidoMaterno', 'required');
      //$this->form_validation->set_rules('usuarioFechaNacimiento', 'usuarioFechaNacimiento', 'required');
      $this->form_validation->set_rules('usuario', 'usuario', 'trim|required|min_length[5]|max_length[12]|is_unique[admin.adminUsername]');
      $this->form_validation->set_rules('password', 'password', 'required');
      //$this->form_validation->set_rules('usuarioNivelestudios', 'usuarioNivelestudios', 'required');
      //$this->form_validation->set_rules('usuarioSueldoMensual', 'usuarioSueldoMensual', 'required');
      $response = validate($this);


      if($response['status']){
        //$data['usuarioNombre'] = $this->input->post('usuarioNombre');
        //$data['usuarioApellidoPaterno'] = $this->input->post('usuarioApellidoPaterno');
        //$data['usuarioApellidoMaterno'] = $this->input->post('usuarioApellidoMaterno');
        //$data['usuarioFechaNacimiento'] = $this->input->post('usuarioFechaNacimiento');
        //$data['usuarioNivelestudios'] = $this->input->post('usuarioNivelestudios');
        //$data['usuarioSueldoMensual'] = $this->input->post('usuarioSueldoMensual');
        $data['idRol'] = $this->input->post('idRol');
        //$data['idusuario'] = $this->input->post('idusuario');
        $data['usuario'] = $this->input->post('usuario');
        $data['password'] = $this->input->post('password');
        $id_usuario = $this->input->post('idusuario');//$this->Mgeneral->save_register('usuarios', $data);

        $data_status['status'] = 1;
        $this->Mgeneral->update_table_row('usuarios',$data_status,'usuarioId',$id_usuario);

        $pass = encrypt_password($this->input->post('usuario'),
                                 $this->config->item('encryption_key'),
                                 $this->input->post('password'));
        $post['adminPassword'] = $pass;
        $post['adminStatus'] = 0;
        $post['idusuario'] = $id_usuario;
        $post['idRol'] = $this->input->post('idRol');
        $post['adminFecha'] = date('Y-m-d');;
        $post['adminUsername']=$this->input->post('usuario');
        $id = $this->Mgeneral->save_admin($post);
        $menu['id_usuario'] = $id;
        $menu['idusuario'] = $id_usuario;
        $this->Mgeneral->save_register('menu', $menu);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
    }

    public function ver_usuarios(){
      $rol_usuario = get_rol_id($this->session->userdata('id'));
      $usuarios['permiso'] = get_rol_permiso('USUARIOS',$rol_usuario);

      $titulo['titulo'] = "Lista de Usuarios" ;
      $titulo['titulo_dos'] = "Lista de Usuarios" ;

      $usuarios['admin'] = $this->Mgeneral->get_result('status',0,'admin');//$this->Mgeneral->get_table('admin');

      $usuarios['empleados'] = $this->Mgeneral->get_result('status',0,'usuarios');
      $usuarios['roles'] = $this->Mgeneral->get_table('roles');

      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('usuarios/ver_usuarios', $usuarios, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_css'=>array(''),
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js',
                                          'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
    }

    public function eliminar($id){
      $data['status'] = 1;
      $this->Mgeneral->update_table_row('admin',$data,'adminId',$id);

    }


    public function editar_usuario($id_usuario){
      $rol_usuario = get_rol_id($this->session->userdata('id'));
      $data['permiso'] = get_rol_permiso('USUARIOS',$rol_usuario);

      $data['admin'] = $this->Mgeneral->get_row('adminId',$id_usuario,'admin');
      $data['usuario'] = $this->Mgeneral->get_row('usuarioId',$data['admin']->idusuario,'usuarios');

      $titulo['titulo'] = "Editar Usuario" ;
      $titulo['titulo_dos'] = "Editar Usuario" ;
      $data['roles'] = $this->Mgeneral->get_table('roles');

      $data['empleados'] = $this->Mgeneral->get_result('status',0,'usuarios');

      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('usuarios/editar_usuario', $data, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }

    

    public function edita_usuario($id_usuario){
      $this->form_validation->set_rules('usuarioNombre', 'usuarioNombre', 'required');
      $this->form_validation->set_rules('usuarioApellidoPaterno', 'usuarioApellidoPaterno', 'required');
      $this->form_validation->set_rules('usuarioApellidoMaterno', 'usuarioApellidoMaterno', 'required');
      $this->form_validation->set_rules('usuarioFechaNacimiento', 'usuarioFechaNacimiento', 'required');
      $this->form_validation->set_rules('usuario', 'usuario', 'required');
      $this->form_validation->set_rules('password', 'password', 'required');
      //$this->form_validation->set_rules('usuarioNivelestudios', 'usuarioNivelestudios', 'required');
      //$this->form_validation->set_rules('usuarioSueldoMensual', 'usuarioSueldoMensual', 'required');
      $response = validate($this);
    if($response['status']){
        $data['usuarioNombre'] = $this->input->post('usuarioNombre');
        $data['usuarioApellidoPaterno'] = $this->input->post('usuarioApellidoPaterno');
        $data['usuarioApellidoMaterno'] = $this->input->post('usuarioApellidoMaterno');
        $data['usuarioFechaNacimiento'] = $this->input->post('usuarioFechaNacimiento');
        //$data['usuarioNivelestudios'] = $this->input->post('usuarioNivelestudios');
        //$data['usuarioSueldoMensual'] = $this->input->post('usuarioSueldoMensual');
        $data['idRol'] = $this->input->post('idRol');
        $data['usuario'] = $this->input->post('usuario');
        $data['password'] = $this->input->post('password');
        $this->Mgeneral->update_table_row('usuarios',$data,'usuarioId',$id_usuario);
      }
    echo json_encode(array('output' => $response));
    }

    public function guarda_edicion(){

      if($this->input->post()){
        $textos = $this->input->post();
        $this->Mgeneral->save_editar_formulario_usuarios($textos,'adminId','admin');
        echo "editado";
      }
      //echo var_dump($this->input->post());
    }

    public function roles_usuario($id_usuario,$aux1,$campo){
      $aux = $this->Mgeneral->get_row('adminId',$id_usuario,'admin');
      $puestos = $this->Mgeneral->get_table('roles');
      $html = '<select class="form-control form-control-sm cc-exp" name="'.$aux1.'['.$campo.']" >';
                foreach($puestos as $rol):
                  if($aux->idRol == $rol->rolId  ){
                    $html .= '<option value="'.$rol->rolId.'" selected>'.$rol->rolNombre.'</option>';
                  }else{
                    $html .= '<option value="'.$rol->rolId.'">'.$rol->rolNombre.'</option>';
                  }
                
               endforeach;
      $html .= '</select>';
      echo $html;
    }


    public function puestos_usuario($id_usuario,$aux1,$campo){
      $aux = $this->Mgeneral->get_row('adminId',$id_usuario,'admin');
      $puestos = $this->Mgeneral->get_table('roles');
      $html = '<select class="form-control form-control-sm cc-exp" name="'.$aux1.'['.$campo.']" >';
                foreach($puestos as $rol):
                  if($aux->idRol == $rol->rolId  ){
                    $html .= '<option value="'.$rol->rolId.'" selected>'.$rol->rolNombre.'</option>';
                  }else{
                    $html .= '<option value="'.$rol->rolId.'">'.$rol->rolNombre.'</option>';
                  }
                
               endforeach;
      $html .= '</select>';
      echo $html;
    }

    public function permisos(){

      $titulo['titulo'] = "Usuarios" ;
      $titulo['titulo_dos'] = "Ver Usuarios" ;
      $usuarios['usuarios'] = $this->Mgeneral->get_table('usuarios');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('usuarios/permisos', $usuarios, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_css'=>array(''),
                                           'included_js'=>array('statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));

    }
    /*
    1.-usuario
    2.-empresa
    3.-productos
    4.-servicios
    5.-proveedores
    6.-infraestructura
    7.-catalogos
    8.-clientes
    9.-inventario
    10.-vrntas
    11.-gastos
    12.-almacentes
    13.-serviciosFumigacion
    14.-reportes
    */
    public function cambiar_permiso(){
      $idusuario = $this->input->post('idusuario');
      $permiso = $this->input->post('permiso');
      $menu = $this->Mgeneral->get_row('idusuario',$idusuario,'menu');
      switch ($permiso) {
          case 1://USUARIOS
              if($menu->usuarios==1){
                  $data['usuarios'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['usuarios'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 2://EMPRESA
              if($menu->empresa==1){
                  $data['empresa'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['empresa'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 3://PRODUCTOS
              if($menu->productos==1){
                  $data['productos'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['productos'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 4://SERVICIOS
              if($menu->	servicios==1){
                  $data['	servicios'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['	servicios'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 5://PROVEEDORES
              if($menu->proveedores==1){
                  $data['proveedores'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['proveedores'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 6://INFRAESTRUCTURA
              if($menu->infraestructura==1){
                  $data['infraestructura'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['infraestructura'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 7://CATALOGOS
              if($menu->catalogos==1){
                  $data['catalogos'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['catalogos'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 8://CLIENTES
              if($menu->clientes==1){
                  $data['clientes'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['clientes'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 9://INVENTARIO
              if($menu->inventario==1){
                  $data['inventario'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['inventario'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 10://VENTAS
              if($menu->ventas==1){
                  $data['ventas'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['ventas'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 11://GASTOS
              if($menu->gastos==1){
                  $data['gastos'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['gastos'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 12://ALMACENES
              if($menu->almacentes==1){
                  $data['almacentes'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['almacentes'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 13://SERVICIOSFUMIGACION
              if($menu->serviciosFumigacion	==1){
                  $data['serviciosFumigacion	'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['serviciosFumigacion	'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;
          case 14://REPORTES
              if($menu->reportes==1){
                  $data['reportes'] = 0;
                  $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }else{
                $data['reportes'] = 1;
                $this->Mgeneral->update_table_row('menu',$data,'idusuario',$idusuario);
              }
              break;

      }
    }

}
