<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends MX_Controller {

  /**

   **/
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Mgeneral', '', TRUE);
      $this->load->library(array('session'));
      $this->load->helper(array('form', 'html', 'validation', 'url'));

      date_default_timezone_set('America/Mexico_City');

       if($this->session->userdata('id')){}else{redirect('login/');}

  }

  public function guarda_edicion(){

      if($this->input->post()){
        $textos = $this->input->post();
        $this->Mgeneral->save_editar_formulario($textos,'id','clientes');
        echo "editado";
      }
    }

  public function pagos($id_factura){

    
    $titulo['titulo'] = "Pagos";
    $titulo['titulo_dos'] = "Facturas Cliente" ;
    $datos['factura'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
    $datos['complementos'] = $this->Mgeneral->get_result('uuid',$datos['factura']->sat_uuid,'complemento_cfdi_relacionado_p');
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);

    $titulo = $this->load->view('main/titulo', $titulo, TRUE);

    $contenido = $this->load->view('clientes/pagos', $datos, TRUE);

    $footer = $this->load->view('main/footer', '', TRUE);
    
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_css'=>array(''),
                                         'included_js'=>array('statics/tema/assets/js/popper.min.js',
                                         'statics/tema/assets/js/plugins.js',
                                         'statics/tema/assets/js/main.js',
                                         'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                         'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                         'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                         'statics/tema/assets/js/lib/data-table/datatables-init.js')));

  }

  public function alta(){
    $titulo['titulo'] = "Alta Cliente" ;
    $titulo['titulo_dos'] = "Alta Cliente" ;
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/alta', '', TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js')));
  }

  public function guarda_datos(){
    $this->form_validation->set_rules('nombre', 'nombre', 'required');
    $this->form_validation->set_rules('rfc', 'rfc', 'required');
    $this->form_validation->set_rules('razon_social', 'razon_social', 'required');
    $this->form_validation->set_rules('calle', 'calle', 'required');
    $this->form_validation->set_rules('numero', 'numero', 'required');
    $this->form_validation->set_rules('colonia', 'colonia', 'required');
    $this->form_validation->set_rules('cp', 'cp', 'required');
    $this->form_validation->set_rules('municipio', 'municipio', 'required');
    $this->form_validation->set_rules('ciudad', 'ciudad', 'required');
    $this->form_validation->set_rules('estado', 'estado', 'required');
    $this->form_validation->set_rules('pais', 'pais', 'required');
    $this->form_validation->set_rules('telefono', 'telefono', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');
    //$this->form_validation->set_rules('datos_bancarios', 'datos_bancarios', 'required');
    $this->form_validation->set_rules('regimen', 'regimen', 'required');
    $this->form_validation->set_rules('password', 'contraseña', 'required');
    $response = validate($this);

    if($response['status']){

      $data['nombre'] = $this->input->post('nombre');
      $data['rfc'] = $this->input->post('rfc');
      $data['razon_social'] = $this->input->post('razon_social');
      $data['calle'] = $this->input->post('calle');
      $data['numero'] = $this->input->post('numero');
      $data['colonia'] = $this->input->post('colonia');
      $data['cp'] = $this->input->post('cp');
      $data['municipio'] = $this->input->post('municipio');
      $data['ciudad'] = $this->input->post('ciudad');
      $data['estado'] = $this->input->post('estado');
      $data['pais'] = $this->input->post('pais');
      $data['telefono'] = $this->input->post('telefono');
      $data['email'] = $this->input->post('email');
      //$data['tados_bancarios'] = $this->input->post('datos_bancarios');
      $data['regimen'] = $this->input->post('regimen');
      $data['password'] = $this->input->post('password');
      $data['fecha_actualizacion'] = date('Y-m-d H:i:s');

      $data['tados_bancarios'] = $this->input->post('datos_bancarios');
      $data['numero_cuenta'] = $this->input->post('numero_cuenta');
      $data['clabe'] = $this->input->post('clabe');
      $data['sucursal'] = $this->input->post('sucursal');
      $data['codigo_banco'] = $this->input->post('codigo_banco');
      $data['uso_CFDI'] = $this->input->post('uso_CFDI');
      $data['factura_formaPago'] = $this->input->post('factura_formaPago');
      $data['comentario_extra'] = $this->input->post('comentario_extra');

      $this->Mgeneral->save_register('clientes', $data);
    }
  //  echo $response;
  echo json_encode(array('output' => $response));
  }


  public function guarda_datos_direccion($id_cliente){
    $this->form_validation->set_rules('descripcion', 'descripcion', 'required');
   
    
    $this->form_validation->set_rules('calle', 'calle', 'required');
    $this->form_validation->set_rules('numero', 'numero', 'required');
    $this->form_validation->set_rules('colonia', 'colonia', 'required');
    $this->form_validation->set_rules('cp', 'cp', 'required');
    $this->form_validation->set_rules('municipio', 'municipio', 'required');
    $this->form_validation->set_rules('ciudad', 'ciudad', 'required');
    $this->form_validation->set_rules('estado', 'estado', 'required');
    $this->form_validation->set_rules('pais', 'pais', 'required');
   
    $response = validate($this);

    if($response['status']){

      $data['descripcion'] = $this->input->post('descripcion');
      $data['rol'] = $this->input->post('rol');
      $data['principal'] = $this->input->post('principal');
      $data['calle'] = $this->input->post('calle');
      $data['numero'] = $this->input->post('numero');
      $data['colonia'] = $this->input->post('colonia');
      $data['cp'] = $this->input->post('cp');
      $data['municipio'] = $this->input->post('municipio');
      $data['ciudad'] = $this->input->post('ciudad');
      $data['estado'] = $this->input->post('estado');
      $data['pais'] = $this->input->post('pais');
    
      $data['id_cliente'] = $id_cliente;
      $data['fecha_creacion'] = date('Y-m-d H:i:s');

      if(($this->input->post("principal") == "1") || ($this->input->post("principal") == 1)){
        $data_aux['principal'] = 0;
        $this->Mgeneral->update_table_row('direccion_cliente',$data_aux,'id_cliente',$id_cliente);
      }
      

      $this->Mgeneral->save_register('direccion_cliente', $data);


    }
  //  echo $response;
  echo json_encode(array('output' => $response));
  }


  public function editar_datos_direccion($id_direccion,$id_cliente){
    $this->form_validation->set_rules('descripcion', 'descripcion', 'required');
   
    
    $this->form_validation->set_rules('calle', 'calle', 'required');
    $this->form_validation->set_rules('numero', 'numero', 'required');
    $this->form_validation->set_rules('colonia', 'colonia', 'required');
    $this->form_validation->set_rules('cp', 'cp', 'required');
    $this->form_validation->set_rules('municipio', 'municipio', 'required');
    $this->form_validation->set_rules('ciudad', 'ciudad', 'required');
    $this->form_validation->set_rules('estado', 'estado', 'required');
    $this->form_validation->set_rules('pais', 'pais', 'required');
   
    $response = validate($this);

    if($response['status']){

      $data['descripcion'] = $this->input->post('descripcion');
      $data['rol'] = $this->input->post('rol');
      $data['principal'] = $this->input->post('principal');
      $data['calle'] = $this->input->post('calle');
      $data['numero'] = $this->input->post('numero');
      $data['colonia'] = $this->input->post('colonia');
      $data['cp'] = $this->input->post('cp');
      $data['municipio'] = $this->input->post('municipio');
      $data['ciudad'] = $this->input->post('ciudad');
      $data['estado'] = $this->input->post('estado');
      $data['pais'] = $this->input->post('pais');
    
      //$data['id_cliente'] = $id_cliente;
      $data['fecha_creacion'] = date('Y-m-d H:i:s');

      if(($this->input->post("principal") == "1") || ($this->input->post("principal") == 1)){
        $data_aux['principal'] = 0;
        $this->Mgeneral->update_table_row('direccion_cliente',$data_aux,'id_cliente',$id_cliente);
      }
      

      $this->Mgeneral->update_table_row('direccion_cliente',$data,'ID',$id_direccion);


    }
  //  echo $response;
  echo json_encode(array('output' => $response));
  }

  public function get_datos_direccion($id){
    $direccion =  $this->Mgeneral->get_row('id',$id,'direccion_cliente');
    echo json_encode($direccion);
  }


  public function ver_clientes(){
    $titulo['titulo'] = "Clientes" ;
    $titulo['titulo_dos'] = "Cliente" ;
    $datos['rows'] = $this->Mgeneral->get_result('status',0,'clientes');//$this->Mgeneral->get_table('clientes');
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/ver_clientes', $datos, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_css'=>array(''),
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                         'statics/tema/assets/js/popper.min.js',
                                        'statics/bootstrap4/js/bootstrap.min.js',
                                         'statics/tema/assets/js/plugins.js',
                                         'statics/tema/assets/js/main.js',
                                         'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                         'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                         'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                         'statics/tema/assets/js/lib/data-table/datatables-init.js')));
  }


  public function ver($id_cliente){
    $datos['roles'] = $this->Mgeneral->get_result('status',0,'roles_direcciones');
    $datos['direcciones'] = $this->Mgeneral->get_result('id_cliente',$id_cliente,'direccion_cliente');
    $titulo['titulo'] = "Datos" ;
    $titulo['titulo_dos'] = "Cliente" ;
    $datos['id_cliente'] = $id_cliente;
    $datos['contactos'] = $this->Mgeneral->get_contacto_usuario($id_cliente);
    $datos['facturas'] = $this->Mgeneral->get_result('receptor_id_cliente',$id_cliente,'factura');
    $datos['row'] = $this->Mgeneral->get_row('id',$id_cliente,'clientes');
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/ver', $datos, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js')));
  }

  public function editar_datos(){
    $this->form_validation->set_rules('nombre', 'nombre', 'required');
    $this->form_validation->set_rules('rfc', 'rfc', 'required');
    $this->form_validation->set_rules('razon_social', 'razon_social', 'required');
    $this->form_validation->set_rules('calle', 'calle', 'required');
    $this->form_validation->set_rules('numero', 'numero', 'required');
    $this->form_validation->set_rules('colonia', 'colonia', 'required');
    $this->form_validation->set_rules('cp', 'cp', 'required');
    $this->form_validation->set_rules('municipio', 'municipio', 'required');
    $this->form_validation->set_rules('ciudad', 'ciudad', 'required');
    $this->form_validation->set_rules('estado', 'estado', 'required');
    $this->form_validation->set_rules('pais', 'pais', 'required');
    $this->form_validation->set_rules('telefono', 'telefono', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');
    //$this->form_validation->set_rules('datos_bancarios', 'datos_bancarios', 'required');
    $this->form_validation->set_rules('regimen', 'regimen', 'required');
    $this->form_validation->set_rules('password', 'contraseña', 'required');
    $response = validate($this);

    if($response['status']){

      $data['nombre'] = $this->input->post('nombre');
      $data['rfc'] = $this->input->post('rfc');
      $data['razon_social'] = $this->input->post('razon_social');
      $data['calle'] = $this->input->post('calle');
      $data['numero'] = $this->input->post('numero');
      $data['colonia'] = $this->input->post('colonia');
      $data['cp'] = $this->input->post('cp');
      $data['municipio'] = $this->input->post('municipio');
      $data['ciudad'] = $this->input->post('ciudad');
      $data['estado'] = $this->input->post('estado');
      $data['pais'] = $this->input->post('pais');
      $data['telefono'] = $this->input->post('telefono');
      $data['email'] = $this->input->post('email');
      //$data['tados_bancarios'] = $this->input->post('datos_bancarios');
      $data['regimen'] = $this->input->post('regimen');
      $data['password'] = $this->input->post('password');
      $data['fecha_actualizacion'] = date('Y-m-d H:i:s');

      $data['tados_bancarios'] = $this->input->post('datos_bancarios');
      $data['numero_cuenta'] = $this->input->post('numero_cuenta');
      $data['clabe'] = $this->input->post('clabe');
      $data['sucursal'] = $this->input->post('sucursal');
      $data['codigo_banco'] = $this->input->post('codigo_banco');
      $data['uso_CFDI'] = $this->input->post('uso_CFDI');
      $data['factura_formaPago'] = $this->input->post('factura_formaPago');
      $data['comentario_extra'] = $this->input->post('comentario_extra');

      $this->Mgeneral->update_table_row('clientes',$data,'id',$this->input->post('id_cliente'));
    }
  //  echo $response;
  echo json_encode(array('output' => $response));
  }

  public function guardar_contacto($id_usuario){
      
      $response['status'] = true;
      //$response = validate($this);
    if($response['status']){
        $data['email'] = $this->input->post('contacto_email');
        $data['nombre'] = $this->input->post('contacto_nombre');
        $data['telefono'] = $this->input->post('contacto_telefono');
        $data['tipo'] = 1;
        $data['id_tabla'] = $id_usuario;
        
        $id_usuario = $this->Mgeneral->save_register('usuarios_contactos', $data);
      }
    echo json_encode(array('output' => $response));
    }
    public function eliminar_contacto($id,$id_usuario){
      $this->Mgeneral->delete_row('usuarios_contactos','id',$id);
      redirect('clientes/ver/'.$id_usuario);
    }


  public function ver_factura_cliente($id_cliente){
    $datos['cliente'] = $this->Mgeneral->get_row('id',$id_cliente,'clientes');
    $titulo['titulo'] = "Facturas Cliente: ".$datos['cliente']->nombre ;
    $titulo['titulo_dos'] = "Facturas Cliente" ;
    $datos['id_cliente'] = $id_cliente;
    
    $datos['facturas'] = $this->Mgeneral->get_result('receptor_id_cliente',$id_cliente,'factura');
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/clientes_facturas', $datos, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_css'=>array(''),
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js','statics/tema/assets/js/popper.min.js',
                                         'statics/tema/assets/js/plugins.js',
                                         'statics/tema/assets/js/main.js',
                                         'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                         'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                         'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                         'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                         'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                         'statics/tema/assets/js/lib/data-table/datatables-init.js')));
  }

  public function ver_datos_factura($id_factura){
    $res = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
    echo json_decode($res);
  }

  public function lugares($id_cliente){
    $titulo['titulo'] = "Lugares" ;
    $titulo['titulo_dos'] = "Lugares" ;
    $data['lugares'] = $this->Mgeneral->get_result('id_cliente',$id_cliente,'cliente_lugares');
    $data['boquillas'] = $this->Mgeneral->get_table('boquillas');
    $data['id_cliente'] = $id_cliente;
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/clientes_lugares', $data, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js')));
  }

  public function guarda_lugar(){
      $this->form_validation->set_rules('lugar', 'lugar', 'required');
      $this->form_validation->set_rules('metros', 'metros', 'required');
      $this->form_validation->set_rules('boquilla', 'boquilla', 'required');

      $response = validate($this);


      if($response['status']){
        $data['lugar'] = $this->input->post('lugar');
        $data['metros'] = $this->input->post('metros');
        $data['id_boquilla'] = $this->input->post('boquilla');
        $data['id_cliente'] = $this->input->post('id_cliente');

        $this->Mgeneral->save_register('cliente_lugares', $data);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
    }

    public function lugares_eliminar($id,$id_cliente){
      $this->Mgeneral->delete_row('cliente_lugares','id',$id);
      redirect('clientes/lugares/'.$id_cliente);
    }

    public function programas($id_cliente){
    $titulo['titulo'] = "programas" ;
    $titulo['titulo_dos'] = "programas" ;
    $data['productos'] = $this->Mgeneral->get_table('productos');
    $data['esquemas'] = $this->Mgeneral->get_result('id_cliente',$id_cliente,'esquema');
    $data['metodos'] = $this->Mgeneral->get_table('metodos');
    $data['plagas'] = $this->Mgeneral->get_table('plagas');
    $data['actividades'] = $this->Mgeneral->get_table('actividades');
    
    $data['id_cliente'] = $id_cliente;
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/programas', $data, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js')));
  }


  public function guarda_esquema($id_cliente){


      $this->form_validation->set_rules('producto_nombre', 'producto_nombre', 'required');
      $this->form_validation->set_rules('producto_clave', 'producto_clave', 'required');


      $response = validate($this);


      if($response['status']){
        $data['producto_clave'] = $this->input->post('producto_clave');
        $data['producto_id'] = $this->input->post('producto_id');
        $data['producto_nombre'] = $this->input->post('producto_nombre');
        $data['actividad'] = $this->input->post('actividad');
        $data['metodo'] = $this->input->post('metodo');
        $data['plaga'] = $this->input->post('plaga');
        $data['id_cliente'] = $id_cliente;
        $data['dosis'] = $this->input->post('dosis');

        $this->Mgeneral->save_register('esquema', $data);
      }
      //echo $response;
    echo json_encode(array('output' => $response));
    }

    public function guarda_programa($id_cliente){

       //repetir cada X tiempo
       $repetir_cada = $this->input->post("repetir_cada");
      
       /*
        <option value="1">Todos los días</option>
        <option value="2">Repetir cada 15 días</option>
        <option value="3">Repetir cada semana</option>
        <option value="4">Repetir cada mes</option>
      */
      $repeticion = $this->input->post("repeticion");

      //fecha en que iniciara la iteracion
      $fecha_inicio = $this->input->post("fecha_inicio");

      //si es true es todo el año
      $todo_anio = $this->input->post("todo_anio");

      //numero que se va a repetir la secuencia
      $numero_repeticiones = $this->input->post("numero_repeticiones");

      $lunes = $this->input->post("lunes");
      $martes = $this->input->post("martes");
      $miercoles = $this->input->post("miercoles");
      $jueves = $this->input->post("jueves");
      $viernes = $this->input->post("viernes");
      $sabado = $this->input->post("sabado");
      $domingo = $this->input->post("domingo");

      $dias_semana = "";
      if($lunes == 1){$dias_semana.="Mo,";}
      if($martes == 1){$dias_semana.="Tu,";}
      if($miercoles == 1){$dias_semana.="We,";}
      if($jueves == 1){$dias_semana.="Th,";}
      if($viernes == 1){$dias_semana.="Fr,";}
      if($sabado == 1){$dias_semana.="Sa,";}
      if($domingo == 1){$dias_semana.="Su,";}
      

      //sacar el numero de veces que se repetira el evento
      $iteraciones = 0;
      if($todo_anio == 0){
        $iteraciones = $numero_repeticiones;
      }else{
        //sacar la cuenta de todo el año
      }

      
      switch ($repeticion) {

          case 1:// TODOS LOS DÍAS
               $r = new When();
               $r->startDate( new DateTime("".$fecha_inicio." 23:59:00",new DateTimeZone("America/Mexico_City")))
               ->rrule("FREQ=daily;interval=1;COUNT=".$iteraciones."")
                        ->generateOccurrences();
               $occurrences = $r->occurrences;
               echo json_encode($r->occurrences);
              break;

          case 2://REPETIR CADA SEMANA
               $r = new When();
               $r->startDate( new DateTime("".$fecha_inicio." 23:59:00",new DateTimeZone("America/Mexico_City")))
               ->rrule("FREQ=weekly;COUNT=".$iteraciones.";BYDAY=".substr( $dias_semana , 0 , -1).";interval=".$repetir_cada."")
                        ->generateOccurrences();
               $occurrences = $r->occurrences;

               echo json_encode($r->occurrences);

               
              break;
          case 3://REPETIR CADA MES
              $r = new When();
               $r->startDate( new DateTime("".$fecha_inicio." 23:59:00",new DateTimeZone("America/Mexico_City")))
               //->rrule("FREQ=MONTHLY;COUNT=".$iteraciones.";BYDAY=1FR")
               ->rrule("FREQ=MONTHLY;COUNT=".$iteraciones.";")
                        ->generateOccurrences();
               $occurrences = $r->occurrences;

               echo json_encode($r->occurrences);
              break;
          case 4://REPETIR CADA AÑO
              echo "i es igual a 2";
              break;
      }





      die;
      
      //$response = validate($this);
      $response['status'] = true;
      $aux_insertar_calendartio = 0;
      $aux_id_programa = 0;

      if($response['status']){

        for($i = 1; $i<=12; $i++):
          for($sem = 1;$sem<=4;$sem++):
            for($dias = 1;$dias<=7;$dias++):
                if($dias == $this->input->post('dia')):
                  if(is_object($this->Mgeneral->get_fecha_esquema($i,$sem,$this->input->post('dia')))):  

                    $aux_id_programa = $this->Mgeneral->get_fecha_esquema($i,$sem,$this->input->post('dia'))->id_prgrama;
                    $data_pe['id_programa'] = $this->Mgeneral->get_fecha_esquema($i,$sem,$this->input->post('dia'))->id_prgrama;
                    $data_pe['id_esquema'] = $this->input->post('id_esquema');
                    $data_pe['id_cliente'] = $id_cliente;
                    $this->Mgeneral->save_register('programa_esquema', $data_pe);
                    $aux_insertar_calendartio = 0;
                  else:
                    $aux_insertar_calendartio = 1;
                    $data['id_cliente'] = $id_cliente;
                    $data['mes'] =$i; //$this->input->post('mes');
                    $data['dia'] = $this->input->post('dia');
                    $data['semana'] = $sem;//$this->input->post('semana');
                    $data['id_esquema'] = 0;//$this->input->post('id_esquema');
                    $data['anio'] = 2019; 
                    $data['fecha_creacion'] = date('Y-m-d H:i:s');
                    //$data['fecha'] = $this->input->post('fecha');
                    $id_p = $this->Mgeneral->save_register('programa', $data);
                    $data_pe['id_programa'] = $id_p;
                    $data_pe['id_esquema'] = $this->input->post('id_esquema');
                    $data_pe['id_cliente'] = $id_cliente;
                    $this->Mgeneral->save_register('programa_esquema', $data_pe);
                    $aux_id_programa = $id_p;

                  endif;
                endif;  
            endfor;
          endfor;
        endfor;

        if($aux_insertar_calendartio == 1):
        $fechaInicio = strtotime("2019-01-01");
        $fechaFin = strtotime("2019-12-31");
        //Recorro las fechas y con la función strotime obtengo los lunes
        for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
          //Sacar el dia de la semana con el modificador N de la funcion date
          $dia = date('N', $i);
          if($dia==$this->input->post('dia')){
            //echo "Lunes. ". date ("Y-m-d", $i)."<br>";
            $data_insert = array(
              'author' => $this->input->post('nombre_cliente'),
              'title' => $this->input->post('nombre_cliente'),
              'id_cliente' => $id_cliente,
              'id_programa' => $aux_id_programa,
              'date' => date ("Y-m-d", $i)//$this->input->post('fecha'),
            );
            // create new appointment from($data_insert)
            $res = $this->db->insert('appointment', $data_insert);
          }
        }
        endif;


        
      }
      //echo $response;
    echo json_encode(array('output' => $response));
    }


    public function guarda_calendario(){
      /*
      $dia = 1
      $semana = 2
      $mes = 3
      $anio = 4
      */
      //if()
      
    }

    public function get_esquemas($i,$semm,$diass){

        if(is_object($this->Mgeneral->get_fecha_esquema($i,$semm,$diass))){

          $id_pro = $this->Mgeneral->get_fecha_esquema( $i,$semm,$diass)->id_prgrama;
          $aux1_rows = $this->Mgeneral->get_result('id_programa',$id_pro ,'programa_esquema');
          foreach($aux1_rows as $key_aux1):
            
            echo '<li class="list-group-item">'.
                  $this->Mgeneral->get_row('id_esquema',$key_aux1->id_esquema,'esquema')->producto_clave.
                  '</li>';
          endforeach;

         }else{
            echo 0;
        }
                       
    }

    public function enviar_email($id_factura,$id_cliente){
    $titulo['titulo'] = "Enviar email" ;
    $titulo['titulo_dos'] = "" ;
    $data['factura_metodo'] = $this->Mgeneral->get_row('facturaID',$id_factura,'factura')->factura_medotoPago;
    $data['id_factura'] = $id_factura;
    $data['email_usuario'] = $this->Mgeneral->get_row('id',1,'datos')->email;
    $data['email_cliente'] = $this->Mgeneral->get_row('id',$id_cliente,'clientes')->email;

   

  
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/enviar_email', $data, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_css'=>array('statics/editor_texto/jquery-te-1.4.0.css'),
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js',
                                        'statics/editor_texto/jquery-te-1.4.0.min.js')));
  }


  public function sen_email($factura){
    $mensaje = $this->input->post("mensaje");
    $de = $this->input->post("txt_de");
    $para = $this->input->post("txt_para");
    $this->load->library('email');
    $config['mailtype'] = 'html';
    $this->email->initialize($config);
    $this->email->to($para);
    $this->email->from($de);
    $this->email->subject('El astillero fumigaciones');
    $this->email->message($mensaje);
    $this->email->attach("".base_url()."statics/facturas/facturas_xml/".$factura.".xml",'attachment', 'factura.xml');
    
    $this->email->send();
    //echo "".base_url()."statics/facturas/facturas_xml/".$factura.".xml";
    $res['status'] = true;
     
     echo json_encode(array('output' => $res));
  }


  public function calendario($id_cliente){
    $titulo['titulo'] = "" ;
    $titulo['titulo_dos'] = "" ;
    $data['id_cliente'] = $id_cliente;
  
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('clientes/calendario', $data, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array('header'=>$header,
                                         'menu'=>$menu,
                                         'header_dos'=>$header_dos,
                                         'titulo'=>$titulo,
                                         'contenido'=>$contenido,
                                         'footer'=>$footer,
                                         'included_css'=>array('statics/editor_texto/jquery-te-1.4.0.css'),
                                         'included_js'=>array('statics/js/bootbox.min.js',
                                         'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js',
                                        'statics/editor_texto/jquery-te-1.4.0.min.js')));
  }

  public function ajaxevent($id_cliente){
    header('Access-Control-Allow-Origin: *');
    // if user not login, redirect to login(homepage)
    $this->db->where('id_cliente',$id_cliente);
    $result = $this->db->get('appointment')->result_array();
    print_r(json_encode($result));
  }

  public function verifica_programa($id_programa){
    $pro = $this->Mgeneral->get_row('id',$id_programa,'appointment');
    if(is_object($pro)){
      echo $pro->servicio_realizado; 
    }else{
      echo 4;
    }
  }

  public function crear_servicio($id_programa){
    //se crea la orden
    $data1['servicioFechaCreacion'] = date('Y-m-d H:i:s');
    $data1['servicio_id'] = get_guid();
    $id_orden = $this->Mgeneral->save_register('servicios', $data1);

    $data_actualizar['servicioReferencia'] = date('Y-m-d')."-".$id_orden;
    $this->Mgeneral->update_table_row('servicios',$data_actualizar,'servicioId',$id_orden);
    //se da de alta en el programa y se cambia de estatus
    $data_programa['servicio_id'] = $data1['servicio_id'];
    $data_programa['servicio_realizado'] = 1;
    $this->Mgeneral->update_table_row('appointment',$data_programa,'id',$id_programa);
    //se llenan los demas campos del servicio
    $programa = $this->Mgeneral->get_row('id_prgrama',$id_programa,'programa');

    $cliente  = $this->Mgeneral->get_row('id',$programa->id_cliente,'clientes');

    $data['servicioIdCliente'] = $cliente->id;
    $data['servicioDomicilioCalle'] = $cliente->calle;
    $data['servicioDomicilioColonia'] = $cliente->colonia;
    $data['servicioDomicilioMmunicipio'] = $cliente->municipio;
    $data['servicioDomicilioEstado'] = $cliente->estado;
    $data['servicioDomicilioNumero'] = $cliente->numero;
    $data['servicioDomicilioCP'] = $cliente->cp;
    $data['servicioReferenciaLugar'] = $this->input->post('servicioReferenciaLugar');
    $data['servicioCertificado'] = $this->input->post('servicioCertificado');
    $data['servicioIDCreador'] = $this->input->post('servicioIDCreador');
    $data['servicioFolio'] = $this->input->post('servicioFolio');
    $data['servicioReferencia'] = "";
    //$data['servicioFechaServicio'] = $this->input->post('servicioFechaServicio');
    //$data['servicioFechaProximaVisita'] = $this->input->post('servicioFechaProximaVisita');
    //$data['servicioComentariosConfidenciales'] = $this->input->post('servicioComentariosConfidenciales');
    //$data['servicioComentariosCliente'] = $this->input->post('servicioComentariosCliente');
    //$data['servicioIdUsuarioEncargado'] = $this->input->post('servicioIdUsuarioEncargado');
    //$data['servicioIdVehiculo'] = $this->input->post('servicioIdVehiculo');
    //$data['servicioFechaCreacion'] = $this->input->post('servicioFechaCreacion');
    $this->Mgeneral->update_table_row('servicios',$data,'servicio_id',$data1['servicio_id']);
    //var_dump($data1['servicio_id']);

    //ALTA DE ACTIVIDADES
    $lugares_clientes = $this->Mgeneral->get_result('id_cliente',$cliente->id ,'cliente_lugares');
    foreach($lugares_clientes as $lugares):
      $aux1_rows = $this->Mgeneral->get_result('id_programa',$id_programa ,'programa_esquema');
      foreach($aux1_rows as $key_aux1):
        $esquema = $this->Mgeneral->get_row('id_esquema',$key_aux1->id_esquema,'esquema');
        $dosis = "";

        $productoE = $this->Mgeneral->get_row('productoId',$esquema->producto_id,'productos');
        $dosisC = 0;

        if($esquema->dosis==1){
          $dosis = $productoE->productoDosisAlta.$productoE->productoMedidaAlta;
          $dosisC = $productoE->productoDosisAlta;
          $saTipoDosis = "Alta";
        }else{
          $dosis = $productoE->productoDosisBaja.$productoE->productoMedidaBaja;
          $dosisC = $productoE->productoDosisBaja;
          $saTipoDosis = "Baja";
        } 

        

        $data_actividades['saIdServicio'] = $id_orden;
        $data_actividades['saNombreDosis'] = $dosis;
        $data_actividades['saTipoDosis'] = $saTipoDosis;
        $data_actividades['saIdActividad'] = 0;
        $data_actividades['saIdLugar'] = $lugares->id;
        $data_actividades['saIdPlaga'] = "";
        $data_actividades['saIdProducto'] = $esquema->producto_id;
        $data_actividades['saDosis'] = $dosisC;
        $data_actividades['saCantidad'] = number_format((($lugares->metros/2.2) * boquilla_aolicacion($lugares->id_boquilla)));
        $data_actividades['saMedida'] = $productoE->productoMedida;//$this->input->post('saMedida');
        $data_actividades['saIdMetodo'] = $esquema->metodo;//$this->input->post('saIdMetodo');
        $data_actividades['saIdActividad'] = $esquema->actividad;
        $data_actividades['saIdPlaga'] = $esquema->plaga;
        $this->Mgeneral->save_register('servicios_actividades', $data_actividades);
      endforeach;

    endforeach;
    

    echo $id_orden;
    
  }

  public function ir_servicio($id_calendario){
    $calendario = $this->Mgeneral->get_row('id',$id_calendario,'appointment');
    
    $servicio = $this->Mgeneral->get_row('servicio_id',$calendario->servicio_id,'servicios');
    echo $servicio->servicioId;

  }

  public function eliminar($id_plaga){
    $data['status'] = 1;
    $this->Mgeneral->update_table_row('clientes',$data,'id',$id_plaga);

  }


}
