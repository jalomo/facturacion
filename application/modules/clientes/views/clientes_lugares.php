<script>
       menu_activo = "clientes";
$("#menu_clientes_lista").last().addClass("menu_estilo");
$(document).ready(function(){
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/clientes/guarda_lugar";
  ajaxJson(url,{"lugar":$("#lugar").val(),
                "metros":$("#metros").val(),
                "boquilla":$("#boquilla").val(),
                 "id_cliente":<?php echo $id_cliente;?>},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect(" GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/lugares/<?php echo $id_cliente;?>");
    }


  });
});

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title"><h3><?php echo nombre_cliente($id_cliente);?></h3></strong>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Lugar</label>
                                  <input id="lugar" name="lugar" type="text" class="form-control  form-control-sm" value="" placeholder=""  autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Metros</label>
                                <input id="metros" name="metros" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="Metros" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Boquilla</label>
                                <select class="form-control cc-exp form-control-sm" id="boquilla" name="boquilla">
                                  <?php foreach($boquillas as $bo):?>
                                  <option value="<?php echo $bo->id;?>"><?php echo $bo->boquilla." ".$bo->milimetrosxsegundo;?></option>
                                  <?php endforeach;?>
                                </select>
                            </div>
                          </div>
                      </div>


                      <div class="col-7">
                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fas fa-save"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                      </div>


                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->


<script type="text/javascript">
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable();
    } );
</script>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
          <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Lugar</th>
                <th>Metros lineales</th>
                <th>Segundos Caminados(metro/2.2seg)</th>
                <th>boquilla</th>
                <th>Aplicación</th>
                <th>Cantidad redondeada</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php if(is_array($lugares)):?>
              <?php foreach($lugares as $row):?>
                <tr>
                  <th><?php echo $row->lugar;?></th>
                  <th><?php echo $row->metros;?></th>
                  <th><?php echo number_format($row->metros/2.2,2);?></th>
                  <th><?php echo nombre_boquilla($row->id_boquilla);?></th>
                  <th><?php echo number_format((($row->metros/2.2) * boquilla_aolicacion($row->id_boquilla)),2);?></th>
                  <th><?php echo number_format((($row->metros/2.2) * boquilla_aolicacion($row->id_boquilla)));?></th>
                  <th><a href="<?php echo base_url()?>index.php/clientes/lugares_eliminar/<?php echo $row->id;?>/<?php echo $row->id_cliente;?>">
                    <button type="button" class="btn btn-danger" ><i class="fas fa-trash-alt"></i>&nbsp;Eliminar</button>
                    </a></th>
                </tr>
             <?php endforeach;?>
             <?php endif;?>
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
