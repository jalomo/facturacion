
<script type="text/javascript">
        menu_activo = "clientes";
$("#menu_clientes_lista").last().addClass("menu_estilo");
     $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});
    } );
</script>

<script>
          menu_activo = "clientes";
$("#menu_clientes_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/clientes/guarda_datos";
  ajaxJson(url,{"nombre":$("#nombre").val(),
                "rfc":$("#rfc").val(),
                "razon_social":$("#razon_social").val(),
                "calle":$("#calle").val(),
                "numero":$("#numero").val(),
                "colonia":$("#colonia").val(),
                "cp":$("#cp").val(),
                "municipio":$("#municipio").val(),
                "ciudad":$("#ciudad").val(),
                "estado":$("#estado").val(),
                "pais":$("#pais").val(),
                "telefono":$("#telefono").val(),
                "email":$("#email").val(),
                "password":$("#password").val(),
                //"datos_bancarios":$("#datos_bancarios").val(),
                "datos_bancarios":$("#datos_bancarios").val(),
                "numero_cuenta":$("#numero_cuenta").val(),
                "clabe":$("#clabe").val(),
                "sucursal":$("#sucursal").val(),
                "codigo_banco":$("#codigo_banco").val(),
                "regimen":$("#regimen").val(),
                "uso_CFDI":$("#uso_CFDI").val(),
                "factura_formaPago":$("#factura_formaPago").val(),
                "comentario_extra":$("#comentario_extra").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/clientes/ver_clientes");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});


$(".btn_eliminar").click(function(event){
                     event.preventDefault();

                     bootbox.dialog({
                         message: "Desea eliminar los registros seleccionados?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {

                                   $('.myCheckBox').each(function(){ //this.checked = true; 
                                      if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());
                                           id = $(this).val();
                                           url = "<?php echo base_url()?>index.php/clientes/eliminar/"+id;//$("#delete"+id).attr('href');
                                           $("#borrar_"+id).slideUp();
                                           $.get(url);
                                                };
                                           });

                                   
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });


                    
                 });


  

  $("#todos_boton").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });

  $(".btn_editar").click(function(event){
                     event.preventDefault();

                      //$( "#todos_boton" ).prop( "checked", true );

                     //$('.myCheckBox').each(function(){ this.checked = true; });
                      aux = 1;
                      $('.myCheckBox').each(function(){ //this.checked = true; 
                            if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());           
                                id = $(this).val();
                                console.log(id);
                                var txt = $(".textNombre_"+id+"").html();
                                $(".textNombre_"+id+"").replaceWith("<input value='" + txt + "' name='"+aux+"[rfc]'/>");

                                var txt_des = $(".textDescripion_"+id+"").html();
                                $(".textDescripion_"+id+"").replaceWith("<input value='" + txt_des + "' name='"+aux+"[nombre]'/>");

                               


                                var txt_telefono = $(".textTelefono_"+id+"").html();
                                $(".textTelefono_"+id+"").replaceWith("<input value='" + txt_telefono + "' name='"+aux+"[telefono]'/>");


                                var txt_id = $(".id_des_"+id+"").html();
                                $(".id_des_"+id+"").replaceWith("<input type='hidden' value='" + txt_id + "' name='"+aux+"[id]'/>");

                                aux = aux +1;
                            };
                      });

                      $("body").keypress(function(e) {
                        if(e.which == 13) {
                          //obtenerDatos();
                          
                          var form = $("#formulario_editar").serializeArray();
                         // console.log(form);
                            url = "<?php echo base_url()?>index.php/clientes/guarda_edicion"
                            ajaxJson(url,form,
                                        "POST","",function(result){
                                            correoValido = false;
                                            console.log(result);
                                            exito_redirect("EDITADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/ver_clientes");
                                            
                                          });

                        }
                      });



                    
                 });

});
</script>

<div class="card-header">
  <!--a href="<?php echo base_url()?>index.php/clientes/alta"><button type="button" class="btn btn-info"><i class="fas fa-plus-circle"></i>&nbsp;Alta Cliente</button></a-->



</div>

<div class="card-header">
  <!--a href="<?php echo base_url()?>index.php/proveedores/alta"><button type="button" class="btn btn-info">Alta Proveedor</button></a-->

  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Nuevo
            </button>

            <button type="button" class="btn btn-info btn_editar">Editar</button>

            <button type="button" class="btn btn-danger btn_eliminar">Eliminar</button>



</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                  <form id="formulario_editar">
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>
                         
                           <input type="checkbox" name=""  id="todos_boton" class="todos_boton" >
                          
                        </th>
                          <th>RFC</th>
                          <th>Nombre</th>
                          <th>Teléfono</th>
                          <th>Total</th>
                          <th>Abono</th>
                          <th>Saldo</th>
                          <th>Opciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(is_array($rows)):?>
                        <?php foreach($rows as $row):?>
                          <tr id="borrar_<?php echo $row->id?>">
                           <td><input type="checkbox" name="" class="myCheckBox" id="<?php echo $row->id?>" value="<?php echo $row->id?>"></td>
                            <td><span class="textNombre_<?php echo $row->id?> texto_nombre"><?php echo $row->rfc;?></span></td>
                            <td><span class="textDescripion_<?php echo $row->id?> texto_descripcion"><?php echo $row->nombre;?></span></td>
                            <td><span class="textTelefono_<?php echo $row->id?> texto_telefono"><?php echo $row->telefono;?></span></td>

                            <span hidden="hidden" class="id_des_<?php echo $row->id?> texto_id" type="hidden"><?php echo $row->id?></span>

                            <th><?php echo "$".$this->Mgeneral->total_factura_cliente($row->id);?></th>
                            <th><?php echo "$".$this->Mgeneral->abono_factura_cliente($row->id);?></th>
                            <th><?php echo "$".$this->Mgeneral->saldo_factura_cliente($row->id);?></th>

                            <td>
                              <div class="dropdown">
                                <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  
                                  <a class="dropdown-item" href="<?php echo base_url()?>index.php/clientes/ver/<?php echo $row->id;?>">
                                  <button type="button" class="btn btn-info"><i class="fas fa-eye"></i>&nbsp;Ver</button>
                                  </a>
                                   <a class="dropdown-item" href="<?php echo base_url()?>index.php/clientes/ver_factura_cliente/<?php echo $row->id;?>">
                                  <button type="button" class="btn btn-info"><i class="fas fa-list-ol"></i>&nbsp;Estado de cuentas</button>
                                  </a>
                              
                                </div>
                              </div>
                             
                              
                             
                            </td>
                          </tr>
                       <?php endforeach;?>
                       <?php endif;?>
                      </tbody>
                    </table>
                  </form>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->




<!-- Modal -->
<div class="modal fullscreen-modal  fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo cliente </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="" method="post" novalidate="novalidate" id="alta_usuario">

                      <div class="row">
                        <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                  <input id="nombre" name="nombre" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Nombre" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">RFC</label>
                                  <input id="rfc" name="rfc" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="RFC" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Razón Social</label>
                                <input id="razon_social" name="razon_social" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Razón Social" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Régimen</label>
                                  <input id="regimen" name="regimen" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          

                      </div>


                      <div class="row">
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Calle</label>
                                  <input id="calle" name="calle" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Calle" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="numero" name="numero" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="#" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Colonia</label>
                                <input id="colonia" name="colonia" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Colonia" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">CP</label>
                                <input id="cp" name="cp" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="CP" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Municipio</label>
                                  <input id="municipio" name="municipio" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Municipio" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Ciudad</label>
                                  <input id="ciudad" name="ciudad" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Ciudad" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>


                      <div class="row">
                          
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Estado</label>
                                <input id="estado" name="estado" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Estado" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">País</label>
                                <input id="pais" name="pais" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="País" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>

                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                  <input id="telefono" name="telefono" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Teléfono" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Email</label>
                                  <input id="email" name="email" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Email" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Contraseña</label>
                                  <input id="password" name="password" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Contraseña" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                      </div>



                        <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Nombre Banco</label>
                                <input id="datos_bancarios" name="datos_bancarios" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Nombre Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Clabe</label>
                                  <input id="clabe" name="clabe" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Clabe" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                <input id="sucursal" name="sucursal" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Sucursal" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Código Banco</label>
                                <input id="codigo_banco" name="codigo_banco" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Código Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>


                      <div class="row">

                        <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Comentario extra</label>
                                  <input id="comentario_extra" name="comentario_extra" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Comentario" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Cuenta bancaria</label>
                                  <input id="numero_cuenta" name="numero_cuenta" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Número de cuenta" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          
                          
                            <div class="col-3">
                              <div class="form-group">
                                <label for="uso_CFDI">Uso del CFDI:</label>
                                <select class="form-control-sm alto form-control" id="uso_CFDI" name="uso_CFDI" >
                                  <?php if($factura->receptor_uso_CFDI =="G01"){?>
                                  <option value="G01" selected>G01  Adquisición de mercancias</option>
                                  <?php }else{?>
                                    <option value="G01" selected>G01  Adquisición de mercancias</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="G02"){?>
                                  <option value="G02" selected>G02  Devoluciones, descuentos o bonificaciones</option>
                                  <?php }else{?>
                                    <option value="G02">G02 Devoluciones, descuentos o bonificaciones</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="G03"){?>
                                  <option value="G03" selected>G03  Gastos en general</option>
                                  <?php }else{?>
                                    <option value="G03">G03 Gastos en general</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I01"){?>
                                  <option value="I01" selected>I01  Construcciones</option>
                                  <?php }else{?>
                                    <option value="I01">I01 Construcciones</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I02"){?>
                                  <option value="I02" selected>I02  Mobilario y equipo de oficina por inversiones</option>
                                  <?php }else{?>
                                    <option value="I02">I02 Mobilario y equipo de oficina por inversiones</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I03"){?>
                                  <option value="I03" selected>I03  Equipo de transporte</option>
                                  <?php }else{?>
                                    <option value="I03">I03 Equipo de transporte</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I04"){?>
                                  <option value="I04" selected>I04  Equipo de computo y accesorios</option>
                                  <?php }else{?>
                                    <option value="I04">I04 Equipo de computo y accesorios</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I05"){?>
                                  <option value="I05" selected>I05  Dados, troqueles, moldes, matrices y herramental</option>
                                  <?php }else{?>
                                    <option value="I05">I05 Dados, troqueles, moldes, matrices y herramental</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I06"){?>
                                  <option value="I06" selected>I06  Comunicaciones telefónicas</option>
                                  <?php }else{?>
                                    option value="I06">I06  Comunicaciones telefónicas</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I07"){?>
                                  <option value="I07" selected>I07  Comunicaciones satelitales</option>
                                  <?php }else{?>
                                    <option value="I07">I07 Comunicaciones satelitales</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="I08"){?>
                                  <option value="I08" selected>I08  Otra maquinaria y equipo</option>
                                  <?php }else{?>
                                    <option value="I08">I08 Otra maquinaria y equipo</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D01"){?>
                                  <option value="D01" selected>D01  Honorarios médicos, dentales y gastos hospitalarios.</option>
                                  <?php }else{?>
                                    <option value="D01">D01 Honorarios médicos, dentales y gastos hospitalarios.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D02"){?>
                                  <option value="D02" selected>D02  Gastos médicos por incapacidad o discapacidad</option>
                                  <?php }else{?>
                                    <option value="D02">D02 Gastos médicos por incapacidad o discapacidad</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D03"){?>
                                  <option value="D03" selected>D03  Gastos funerales.</option>
                                  <?php }else{?>
                                    <option value="D03">D03 Gastos funerales.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D04"){?>
                                  <option value="D04" selected>D04  Donativos.</option>
                                  <?php }else{?>
                                    <option value="D04">D04 Donativos.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D05"){?>
                                  <option value="D05" selected>D05  Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                  <?php }else{?>
                                    <option value="D05">D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D06"){?>
                                  <option value="D06" selected>D06  Aportaciones voluntarias al SAR.</option>
                                  <?php }else{?>
                                    <option value="D06">D06 Aportaciones voluntarias al SAR.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D07"){?>
                                  <option value="D07" selected>D07  Primas por seguros de gastos médicos.</option>
                                  <?php }else{?>
                                    <option value="D07" >D07  Primas por seguros de gastos médicos.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D08"){?>
                                  <option value="D08" selected>D08  Gastos de transportación escolar obligatoria.</option>
                                  <?php }else{?>
                                    <option value="D08">D08 Gastos de transportación escolar obligatoria.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D09"){?>
                                  <option value="D09" selected>D09  Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                  <?php }else{?>
                                    <option value="D09">D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="D10"){?>
                                  <option value="D10" selected>D10  Pagos por servicios educativos (colegiaturas)</option>
                                  <?php }else{?>
                                    <option value="D10">D10 Pagos por servicios educativos (colegiaturas)</option>
                                  <?php }?>
                                  <?php if($factura->receptor_uso_CFDI =="P01"){?>
                                  <option value="P01" selected>P01  Por definir</option>
                                  <?php }else{?>
                                    <option value="P01">P01 Por definir</option>
                                  <?php }?>



                                </select>



                              
                            </div>
                          </div>


                           <div class="col-2">
                            <div class="form-group">
                              <label for="factura_formaPago">Forma de pago:</label>
                              <select  class="form-control-sm alto form-control" name="factura_formaPago" id="factura_formaPago">
                                <?php if($factura->factura_formaPago == "01"){?>
                                <option value="01" selected>1-Efectivo</option>
                              <?php }else{?>
                                <option value="01">1-Efectivo</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "02"){?>
                                <option value="02" selected>2-Cheque nominativo</option>
                              <?php }else{?>
                                <option value="02">2-Cheque nominativo</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "03"){?>
                                <option value="03" selected>3-Transferencia electrónica de fondos</option>
                              <?php }else{?>
                                <option value="03">3-Transferencia electrónica de fondos</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "04"){?>
                                <option value="04" selected>4-Tarjeta de crédito</option>
                              <?php }else{?>
                                <option value="04">4-Tarjeta de crédito</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "05"){?>
                                <option value="05" selected>5-Monedero electrónico</option>
                              <?php }else{?>
                                <option value="05">5-Monedero electrónico</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "06"){?>
                                <option value="06" selected>6-Dinero electrónico</option>
                              <?php }else{?>
                                <option value="06">6-Dinero electrónico</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "08"){?>
                                <option value="08" selected>8-Vales de despensa</option>
                              <?php }else{?>
                                <option value="08">8-Vales de despensa</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "12"){?>
                                <option value="12" selected>12-Dación en pago</option>
                              <?php }else{?>
                                <option value="12">12-Dación en pago</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "13"){?>
                                <option value="13" selected>13-Pago por subrogación</option>
                              <?php }else{?>
                                <option value="13">13-Pago por subrogación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "14"){?>
                                <option value="14" selected>14-Pago por consignación</option>
                              <?php }else{?>
                                <option value="14">14-Pago por consignación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "15"){?>
                                <option value="15" selected>15-Condonación</option>
                              <?php }else{?>
                                <option value="15">15-Condonación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "17"){?>
                                <option value="17" selected>17-Compensación</option>
                              <?php }else{?>
                                <option value="17">17-Compensación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "23"){?>
                                <option value="23" selected>23-Novación</option>
                              <?php }else{?>
                                <option value="23">23-Novación</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "24"){?>
                                <option value="24" selected>24-Confusión</option>
                              <?php }else{?>
                                <option value="24">24-Confusión</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "25"){?>
                                <option value="25" selected>25-Remisión de deuda</option>
                              <?php }else{?>
                                <option value="25">25-Remisión de deuda</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "26"){?>
                                <option value="26" selected>26-Prescripción o caducidad</option>
                              <?php }else{?>
                                <option value="26">26-Prescripción o caducidad</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "27"){?>
                                <option value="27" selected>27-A satisfacción del acreedor</option>
                              <?php }else{?>
                                <option value="27">27-A satisfacción del acreedor</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "28"){?>
                                <option value="28" selected>28-Tarjeta de débito</option>
                              <?php }else{?>
                                <option value="28">28-Tarjeta de débito</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "29"){?>
                                <option value="29" selected>29-Tarjeta de servicios</option>
                              <?php }else{?>
                                <option value="29">29-Tarjeta de servicios</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "30"){?>
                                <option value="30" selected>30-Aplicación de anticipos</option>
                              <?php }else{?>
                                <option value="30">30-Aplicación de anticipos</option>
                                <?php }?>
                                <?php if($factura->factura_formaPago == "99"){?>
                                <option value="99" selected>99-Por definir</option>
                              <?php }else{?>
                                <option value="99">99-Por definir</option>
                                <?php }?>

                              </select>

                            </div>
                         </div>


                      </div>



                      <div align="right">
                        <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-save fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Guardar</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                        <div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                        </div>
                      </div>
                  </form>
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>


