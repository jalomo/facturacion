 <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

  <style type="text/css">
.datepicker{z-index:1151 !important;}
  </style>
<script>
$(document).ready(function(){

$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/clientes/guarda_esquema/<?php echo $id_cliente;?>";
  ajaxJson(url,{"producto_nombre":$("#producto_nombre").val(),
                "producto_clave":$("#producto_clave").val(),
                "producto_id":$("#id_producto").val(),
                 "dosis":$("#dosis").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect(" GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/programas/<?php echo $id_cliente;?>");
    }


  });
});

});

function seleccionar_producto(id_producto){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/get_datos_producto/"+id_producto;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              var obj = JSON.parse(data);
              $('#producto_nombre').val(obj.productoNombre);
              $('#id_producto').val(obj.productoId);




              console.log("SUCCESS : ", data);





            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}

function selecciona_programa(){
  aux = $("#auxp").val(); //mes /semana /día
  var res = aux.split("/");
  
  /*switch(res[1]) {
    case "1":
      semana1 = 1;
      semana2 = 0;
      semana3 = 0;
      semana4 = 0;
      break;
    case "2":
      semana1 = 0;
      semana2 = 1;
      semana3 = 0;
      semana4 = 0;
      break;
    case "3":
      semana1 = 0;
      semana2 = 0;
      semana3 = 1;
      semana4 = 0;
      break;
    case "4":
      semana1 = 0;
      semana2 = 0;
      semana3 = 0;
      semana4 = 1;
      break;
    
  }
  */
  var url ="<?php echo base_url()?>index.php/clientes/guarda_programa/<?php echo $id_cliente;?>";
  ajaxJson(url,{"mes":res[0],
                "dia":res[2],
                "semana":res[1],
                 "id_esquema":$("#id_esquema_p").val(),
                 "fecha":$("#fecha").val(),
                 "nombre_cliente":$("#nombre_cliente").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect(" GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/programas/<?php echo $id_cliente;?>");
    }


  });
}

function selecciona_pro(id_p){
  $("#auxp").val(id_p);

  aux = $("#auxp").val(); //mes /semana /día
  var res = aux.split("/");
}
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>
<input value="<?php echo nombre_cliente($id_cliente);?>" id="nombre_cliente">
<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title"><h3><?php echo nombre_cliente($id_cliente);?></h3></strong>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                        
                            <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Producto</label>
                                  <input id="producto_nombre" name="producto_nombre" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="producto_nombre" data-toggle="modal"  data-target="#exampleModalProducto">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                                  <input type="hidden" id="id_producto">
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Clave</label>
                                <input id="producto_clave" name="producto_clave" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="clave" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Dosis:</label>
                                <select class="form-control cc-exp form-control-sm" name="dosis" id="dosis">
                                  <option value="1">Alta</option>
                                  <option value="2">Baja</option>
                                </select>
                                <br/>
                                
                               
                            </div>
                          </div>
                         
                      </div>



                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->


<script type="text/javascript">
    $(document).ready(function() {
      //$('#bootstrap-data-table').DataTable();
    } );
</script>

<?php 
/*
$fechaInicio=strtotime("2019-09-01");
$fechaFin=strtotime("2019-09-30");
//Recorro las fechas y con la función strotime obtengo los lunes
for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
    //Sacar el dia de la semana con el modificador N de la funcion date
    $dia = date('N', $i);
    if($dia==1){
        echo "Lunes. ". date ("Y-m-d", $i)."<br>";
    }
}

*/
?>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
          <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Clave</th>
                <th>Producto</th>
                <th>Dosis</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php if(is_array($esquemas)):?>
              <?php foreach($esquemas as $row):?>
                <tr>
                  <th><?php echo $row->producto_clave;?></th>
                  <th><?php echo $row->producto_nombre;?></th>
                 <th><?php 
                          if($row->dosis == 1){
                              echo "Alta";
                          }
                          if($row->dosis == 2){
                              echo "Baja";
                          }
                
                 ?></th>
                  <th><!--a href="<?php echo base_url()?>index.php/clientes/">
                    <button type="button" class="btn btn-danger" >Eliminar</button>
                    </a--></th>
                </tr>
             <?php endforeach;?>
             <?php endif;?>
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->




<?php  /*
                  $fechaInicio = strtotime("2019-01-01");
                  $fechaFin = strtotime("2019-12-31");
                  ?>
                  <?php
                    for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
                      //Sacar el dia de la semana con el modificador N de la funcion date
                      $dia = date('N', $i);
                      if($dia==1){
                          echo "Lunes. ". date ("Y-m-d", $i)."<br>";
                      }
                      
                  }
                 
                  $fechaInicio = strtotime("2019-02-01");
                  $fechaFin = strtotime("2019-02-28");
                  //Recorro las fechas y con la función strotime obtengo los lunes
                  for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
                      //Sacar el dia de la semana con el modificador N de la funcion date
                      $dia = date('N', $i);
                      if($dia==1){
                          echo "Lunes. ". date ("Y-m-d", $i)."<br>";
                      }
                  }
                  */
                  ?>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
          <table id="bootstrap-data-table" class="table-bordered">
            <thead>
              <tr>
                <th>Mes</th>
                <th colspan="7" >Primera semana</th>
                <th colspan="7" >Segunda semana</th>
                <th colspan="7" >Tercera semana</th>
                <th colspan="7" >Cuarta o quinta semana</th>
                
              </tr>
              <tr>
                <th></th>
                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>

                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>

                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>

                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>


               

              </tr>
            </thead>
            <tbody>
              <?php for($i = 1; $i<13; $i++){?>
                <tr>
                  <th><?php echo $i;?></th>

                  <!-- PRIMER SEMANA-->
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/1" onclick="selecciona_pro('<?php echo $i;?>/1/1')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,1,1))){

                         // echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,1)->id_esquema,'esquema')->producto_clave;

                          $id_pro = $this->Mgeneral->get_fecha_esquema( $i,1,1)->id_prgrama;
                          $aux1_rows = $this->Mgeneral->get_result('id_programa',$id_pro ,'programa_esquema');
                          foreach($aux1_rows as $key_aux1):
                            echo $this->Mgeneral->get_row('id_esquema',$key_aux1->id_esquema,'esquema')->producto_clave;
                          endforeach;



                        }else{
                          echo 0;
                        }
                       
                      ?>
                    </button>
                    
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/2" onclick="selecciona_pro('<?php echo $i;?>/1/2')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,1,2))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,2)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/3" onclick="selecciona_pro('<?php echo $i;?>/1/3')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,1,3))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,3)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/4" onclick="selecciona_pro('<?php echo $i;?>/1/4')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,1,4))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,4)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/5" onclick="selecciona_pro('<?php echo $i;?>/1/5')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,1,5))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,5)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/6" onclick="selecciona_pro('<?php echo $i;?>/1/6')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,1,6))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,6)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/1/7')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,1,7))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,7)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>


                <!-- SEGUNDA SEMANA-->
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/2/1')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,2,1))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,2,1)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/2/2')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,2,2))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,2,2)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/2/3')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,2,3))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,2,3)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/2/4')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,2,4))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,2,4)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/2/5')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,2,5))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,2,5)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/2/6')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,2,6))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,2,6)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/2/7')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,2,7))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,2,7)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>

                  <!-- TERCERA SEMANA-->
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/3/1')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,3,1))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,3,1)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/3/2')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,3,2))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,3,2)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/3/3')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,3,3))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,3,3)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/3/4')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,3,4))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,3,4)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/3/5')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,3,5))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,3,5)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/3/6')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,3,6))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,3,6)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/3/7')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,3,7))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,3,7)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>

                    <!-- Cuarta SEMANA-->

                  <th >
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/4/1')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,4,1))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,4,1)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                    </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/4/2')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,4,2))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,4,2)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/4/3')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,4,3))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,4,3)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/4/4')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,4,4))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,4,4)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/4/5')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,4,5))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,4,5)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/4/6')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,4,6))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,4,6)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                  <th>
                     <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/1/7" onclick="selecciona_pro('<?php echo $i;?>/4/7')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,4,7))){
                          echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,4,7)->id_esquema,'esquema')->producto_clave;
                        }else{
                          echo 0;
                        }
                      ?>
                     </button>
                  </th>
                </tr>
              <?php }?>
             
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->






<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="esquema_p" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un esquema</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <input type="hidden" id="auxp">
          <div class="row">
            <div class='col-sm-4'>
            <select class="form-control cc-exp " id="id_esquema_p">
              
              <?php if(is_array($esquemas)):?>
                <?php foreach($esquemas as $row):?>
                  <option value="<?php echo $row->id_esquema?>"><?php echo $row->producto_clave;?></option>
               <?php endforeach;?>
               <?php endif;?>

            </select>
            </div>
            <!--div class='col-sm-4'>
                      <div class="form-group">
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control" id="fecha" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                  </div>
                  <script type="text/javascript">
                      $(function () {
                          $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD'});
                      });
                  </script-->
          </div>
         
             
          
            

        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="selecciona_programa()">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModalProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Producto</th>
        <th>Producto referencia</th>

        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      <?php if(is_array($productos)):?>
      <?php foreach($productos as $row_producto):?>
        <tr id="<?php echo $row_producto->productoId;?>">
          <td><?php echo $row_producto->productoNombre;?></td>
          <td><?php echo $row_producto->productoReferencia;?></td>
          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_producto(<?php echo $row_producto->productoId;?>)" data-dismiss="modal" id="<?php echo $row_producto->productoId;?>" data-id="<?php echo $row_producto->productoId;?>">seleccionar</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody>
  </table>
        </div>
      </div>

    </div>
  </div>
</div>













