<script>

		$(document).ready(function() {
        $("#cargando").hide();
            $('#enviar_email').submit(function(event){
              event.preventDefault();
              $("#enviar").hide();
              $("#cargando").show();
               var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Enviando Datos...</p>',
                            closeButton: false
                        });
              var url ="<?php echo base_url()?>index.php/clientes/sen_email/<?php echo $id_factura?>";
              ajaxJson(url,{
                            "mensaje":$("#mensaje").val(),
                            "txt_de":$("#txt_de").val(),
                            "txt_para":$("#txt_para").val(),
                            },
                        "POST","",function(result){
               
                console.log(result);
                json_response = JSON.parse(result);
                obj_output = json_response.output;
                obj_status = obj_output.status;
                if(obj_status == false){
                  dialog_load.modal('hide');
                  aux = "";
                  $.each( obj_output.errors, function( key, value ) {
                    aux +=value+"<br/>";
                  });
                  exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                  $("#enviar").show();
                  $("#cargando").hide();
                }
                if(obj_status == true){
                  dialog_load.modal('hide');
                  exito_redirect("EMAIL ENVIADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/ver_clientes");
                  $("#enviar").show();
                  $("#cargando").hide();
                }


              });
            });



				 $('.jqte-test').jqte();
          // settings of status
          var jqteStatus = true;
          $(".status").click(function()
          {
            jqteStatus = jqteStatus ? false : true;
            $('.jqte-test').jqte({"status" : jqteStatus})
          });
			});
</script>

 <form action="" method="post" novalidate="novalidate" id="enviar_email">
<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Enviar email</strong>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">
<div class="row">

				
	<div class="container">
          <div class="row">
            <div class="col-lg-3 ">
              <div class="form-group">
                  <label for="cc-exp" class="control-label mb-1">De</label>
                  <input id="txt_de" name="nombre" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $email_usuario;?>" placeholder="Email" autocomplete="cc-exp">
                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                  <label for="cc-exp" class="control-label mb-1">Para</label>
                  <input id="txt_para" name="nombre" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $email_cliente;?>" placeholder="Email" autocomplete="cc-exp">
                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-lg-12 ">
              <div class="form-group">
                  <label for="cc-exp" class="control-label mb-1">Adjuntos:</label>
                  <a href="<?php echo "".base_url()."statics/facturas/facturas_xml/".$id_factura.".xml";?>" target="_blank" download>xml</a>
              </div>
            </div>
          </div>

					<div class="row">
						<div class="col-lg-12 ">
              <?php //echo $factura_metodo;?>
              <?php if($factura_metodo == "PUE"):?>
							<textarea name="textarea" class="jqte-test" id="mensaje">
                  <p style="text-align: justify;"><span style="font-family: 'Arial',sans-serif;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; text-align: justify; line-height: normal;"><span style="font-size: 10.0pt; font-family: 'Verdana',sans-serif;">Buenos d&iacute;as estimados </span></p>
<p style="margin-bottom: .0001pt; text-align: justify; line-height: normal;"><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif;">&nbsp;</span></p>
<p style="margin-bottom: 3.0pt; text-align: justify; line-height: normal; border: none; padding: 0cm;"><span style="font-size: 22.0pt; font-family: 'Baskerville Old Face',serif;">NUTRIPROTEOMICS S.A. DE C.V.</span></p>
<p style="margin-bottom: .0001pt; text-align: justify; line-height: normal;"><span style="font-size: 10.0pt; font-family: 'Verdana',sans-serif;">Les anexo la factura:</span></p>
<p style="margin-bottom: .0001pt; text-align: justify; line-height: normal;"><span style="font-size: 10.0pt; font-family: 'Verdana',sans-serif;">Del servicio de Control de Plagas del mes de:.</span></p>
<p style="margin-bottom: .0001pt; text-align: justify; line-height: normal;"><span style="font-size: 10.0pt; font-family: 'Verdana',sans-serif;">Quedo a sus &oacute;rdenes para cualquier comentario, que tenga muy bonito d&iacute;a.</span></p>
<p style="margin-bottom: .0001pt; text-align: justify; line-height: normal; background: white;"><span style="font-size: 10.0pt; font-family: 'Verdana',sans-serif; color: #222222;">Muchas gracias.</span></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 12.0pt; font-family: 'Arial',sans-serif;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="font-family: 'Arial',sans-serif; color: #222222; background: white;">​</span></strong></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="font-family: 'Arial',sans-serif; color: #222222; background: white;">&nbsp;FAVOR DE ACUSAR DE RECIBIDO</span></strong></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif;"><img src="http://globodi.com/elastillero/statics/logo/230518115118_logo.PNG" alt="" width="170" height="124" />&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">ATENTAMENTE</span></p>
<p><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222; background: white;"><br />ADMINISTRADORA<br /><br />LIC. ADRIANA ANABELLE LOZA MAGA&Ntilde;A<br /><br /><br /><br />TELS. 312 15 81 400 &nbsp; &nbsp; CEL &nbsp; &nbsp; &nbsp;312 125 6644. &nbsp;</span><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222;"><br /><br /><br /><br /><span style="background: white;">Este mensaje es confidencial y su uso est&aacute; limitado a los destinatarios arriba mencionados. Si usted recibi&oacute; este mensaje por error, le pedimos borrarlo inmediatamente y notificarlo al remitente. </span></span><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222; background: white;">Gracias<br /><br />This email is intended only for the person or entity to which it is addressed and may contain confidential, proprietary and/or privileged material. Any review, distribution, reliance on, or other use of this information by persons or entities other than the intended recipient is prohibited. If you receive this message in error, please immediately notify the sender and delete it and all copies of it from your system.<br /><br /></span><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222; background: white;">Thank you.</span></p>
              </textarea>
            <?php else:?>

            <textarea name="textarea" class="jqte-test" id="mensaje">
<p style="text-align: justify;"><span style="font-family: 'Arial',sans-serif;">&nbsp;</span></p>
<p style="text-align: justify;"><span style="font-family: 'Arial',sans-serif;">COMPLEMENTO DE PAGO DE &nbsp;EL ASTILLERO HIGIENE AMBIENTAL, S.A. DE C.V.</span></p>
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal; background: white;"><span style="font-family: 'Arial',sans-serif;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal; background: white;"><strong><span style="font-size: 12.0pt; font-family: 'Arial',sans-serif; color: #222222;">**BUENAS TARDES: LE ANEXO&nbsp;COMPLEMENTO&nbsp;DE&nbsp;PAGO&nbsp;DE LA FACTURA :.</span></strong></p>
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal; background: white;"><span style="font-size: 12.0pt; font-family: 'Arial',sans-serif; color: #222222;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal; background: white;"><strong><span style="font-size: 12.0pt; font-family: 'Arial',sans-serif; color: #222222;">&nbsp;MUCHAS GRACIAS POR SU&nbsp;DEP&Oacute;SITO.</span></strong></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 12.0pt; font-family: 'Arial',sans-serif;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="font-family: 'Arial',sans-serif; color: #222222; background: white;">​</span></strong></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="font-family: 'Arial',sans-serif; color: #222222; background: white;">&nbsp;FAVOR DE ACUSAR DE RECIBIDO</span></strong></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 12.0pt; font-family: 'Times New Roman',serif;">&nbsp;</span></p>
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.0pt; font-family: 'Times New Roman',serif;">ATENTAMENTE</span></p>
<p><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222; background: white;"><br />ADMINISTRADORA<br /><br />LIC. ADRIANA ANABELLE LOZA MAGA&Ntilde;A<br /><br /><br /><br />TELS. 312 15 81 400 &nbsp; &nbsp; CEL &nbsp; &nbsp; &nbsp;312 125 6644. &nbsp;</span><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222;"><br /><br /><br /><br /><span style="background: white;">Este mensaje es confidencial y su uso est&aacute; limitado a los destinatarios arriba mencionados. Si usted recibi&oacute; este mensaje por error, le pedimos borrarlo inmediatamente y notificarlo al remitente. </span></span><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222; background: white;">Gracias<br /><br />This email is intended only for the person or entity to which it is addressed and may contain confidential, proprietary and/or privileged material. Any review, distribution, reliance on, or other use of this information by persons or entities other than the intended recipient is prohibited. If you receive this message in error, please immediately notify the sender and delete it and all copies of it from your system.<br /><br /></span><span style="font-size: 10.0pt; line-height: 107%; font-family: 'Arial',sans-serif; color: #222222; background: white;">Thank you.</span></p>
            </textarea>
            <?php endif;?>
						</div>
					</div>

				</div>




</div>


<div>
                      <div align="right">
                        <button id="enviar_email" type="submit" class="btn btn-lg btn-info ">
                           
                            <span id="payment-button-amount">Enviar Email</span>
                            <span id="payment-button-sending" style="display:none;">Enviar Email</span>
                        </button>
                        <div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                        </div>
                      </div>
</div>


</div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
<form>
