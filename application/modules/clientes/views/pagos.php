

<script src="<?php echo base_url()?>statics/js/isloading.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      //$('#bootstrap-data-table').DataTable();

      $('#bootstrap-data-table').DataTable();

    } );
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<div class="card-header">
  <div class="">
      <h4>factura</h4>
      <h6>
        <?php echo "RFC:".$factura->receptor_RFC;
        echo"  CLIENTE:".$factura->emisor_nombre;
        echo" TOTAL:".($this->Mgeneral->factura_iva_total($factura->facturaID)+$this->Mgeneral->factura_subtotal($factura->facturaID));?>
      </h6>
  </div>

</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                   
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>fecha_creacion</th>
                        <th>Serie</th>
                        <th>Folio</th>
                        <th>numParcialidad</th>
                        <th>impSaldoAnt</th>
                        <th>impPagado</th>
                        <th>impSaldoInsoluto</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php foreach($complementos as $row_c):?>
                         <tr>
                            <th><?php echo $row_c->fecha_creacion;?></th>
                            <th><?php echo $row_c->serie;?></th>
                            <th><?php echo $row_c->folio;?></th>
                            <th><?php echo $row_c->numParcialidad;?></th>
                            <th><?php echo $row_c->impSaldoAnt;?></th>
                            <th><?php echo $row_c->impPagado;?></th>
                            <th><?php echo $row_c->impSaldoInsoluto;?></th>
                         </tr>
                   <?php endforeach;?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->



