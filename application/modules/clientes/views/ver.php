<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
}
input{
  font-family: 'Roboto', sans-serif;
}
</style>
<script>
     menu_activo = "clientes";
$("#menu_clientes_lista").last().addClass("menu_estilo");
$(document).ready(function(){


$('#guardar_contacto').click(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/clientes/guardar_contacto/<?php echo $row->id;?>";
  ajaxJson(url,{"contacto_nombre":$("#contacto_nombre").val(),
                "contacto_email":$("#contacto_email").val(),
                "contacto_telefono":$("#contacto_telefono").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect("CONTACTO GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/ver/<?php echo $row->id;?>");
    }


  });
});






$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url ="<?php echo base_url()?>index.php/clientes/editar_datos";
  ajaxJson(url,{"nombre":$("#nombre").val(),
                "rfc":$("#rfc").val(),
                "razon_social":$("#razon_social").val(),

              //  "calle":$("#calle").val(),
               // "numero":$("#numero").val(),
                //"colonia":$("#colonia").val(),
                //"cp":$("#cp").val(),
                //"municipio":$("#municipio").val(),
                //"ciudad":$("#ciudad").val(),
                //"estado":$("#estado").val(),
                //"pais":$("#pais").val(),
                "telefono":$("#telefono").val(),
                "email":$("#email").val(),
                "password":$("#password").val(),
                //"datos_bancarios":$("#datos_bancarios").val(),
                "regimen":$("#regimen").val(),
                "id_cliente":<?php echo $row->id;?>,
              "datos_bancarios":$("#datos_bancarios").val(),
                "numero_cuenta":$("#numero_cuenta").val(),
                "clabe":$("#clabe").val(),
                "sucursal":$("#sucursal").val(),
                "codigo_banco":$("#codigo_banco").val(),
                "regimen":$("#regimen").val(),
                "uso_CFDI":$("#uso_CFDI").val(),
                "factura_formaPago":$("#factura_formaPago").val(),
                "comentario_extra":$("#comentario_extra").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/clientes/ver/<?php echo $row->id;?>");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});





$('#alta_usuario2').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  aux_chexk = 0;
  if( $('#principal').prop('checked') ) {
    aux_chexk = 1;
   }
  var url ="<?php echo base_url()?>index.php/clientes/guarda_datos_direccion/<?php echo $id_cliente;?>";
  ajaxJson(url,{"descripcion":$("#descripcion").val(),
                "rol":$("#rol").val(),
                "principal":aux_chexk,

                "calle":$("#calle").val(),
                "numero":$("#numero").val(),
                "colonia":$("#colonia").val(),
                "cp":$("#cp").val(),
                "municipio":$("#municipio").val(),
                "ciudad":$("#ciudad").val(),
                "estado":$("#estado").val(),
                "pais":$("#pais").val(),
                },
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/clientes/ver/<?php echo $row->id;?>");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});



$('#alta_usuario3').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();

  aux_chexk = 0;
  if( $('#e_principal').prop('checked') ) {
    aux_chexk = 1;
   }


  var url ="<?php echo base_url()?>index.php/clientes/editar_datos_direccion/"+$("#radio_id").val()+"/<?php echo $id_cliente;?>";
  ajaxJson(url,{"descripcion":$("#e_descripcion").val(),
                "rol":$("#e_rol").val(),


                "principal":aux_chexk,

                "calle":$("#e_calle").val(),
                "numero":$("#e_numero").val(),
                "colonia":$("#e_colonia").val(),
                "cp":$("#e_cp").val(),
                "municipio":$("#e_municipio").val(),
                "ciudad":$("#e_ciudad").val(),
                "estado":$("#e_estado").val(),
                "pais":$("#e_pais").val(),
                },
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
      $("#enviar").show();
      $("#cargando").hide();
    }
    if(obj_status == true){
      exito_redirect("DATOS EDITADOS CON EXITO","success","<?php echo base_url()?>index.php/clientes/ver/<?php echo $row->id;?>");
      $("#enviar").show();
      $("#cargando").hide();
    }


  });
});





});

function editar_direccion(){

}

function radio_button(id){
  $("#radio_id").val(id);

  var url ="<?php echo base_url()?>index.php/clientes/get_datos_direccion/"+id;
  ajaxJson(url,{},
            "POST","",function(result){
    
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response;
    

    $("#e_descripcion").val(obj_output.descripcion);
    $("#e_rol").val(obj_output.rol);

    

    if(obj_output.principal == 1){
      $("#e_principal").prop('checked', true);
    }else{
      $("#e_principal").prop('checked', false);
    }
    

    $("#e_calle").val(obj_output.calle);
    $("#e_numero").val(obj_output.numero);
    $("#e_colonia").val(obj_output.colonia);
    $("#e_cp").val(obj_output.cp);
    $("#e_municipio").val(obj_output.municipio);
    $("#e_ciudad").val(obj_output.ciudad);
    $("#e_estado").val(obj_output.estado);
    $("#e_pais").val(obj_output.pais);

    


  });
 
}
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <!--div class="card-header">
            <button type="button" class="btn btn-info">Generar orden de venta</button>
            <button type="button" class="btn btn-info">Generar cotización</button>
            <button type="button" class="btn btn-info">ver pagos</button>
            <button type="button" class="btn btn-info">ver facturas</button>
            Total: $200 Adeudos:$100

        </div-->
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                  <input id="nombre" name="nombre" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->nombre;?>" placeholder="Nombre" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">RFC</label>
                                  <input id="rfc" name="rfc" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->rfc;?>" placeholder="RFC" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Razón Social</label>
                                <input id="razon_social" name="razon_social" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->razon_social;?>" placeholder="Razón Social" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Régimen</label>
                                  <input id="regimen" name="regimen" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->regimen;?>" placeholder="Régimen" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                      </div>

                     


                      <!--div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Calle</label>
                                  <input id="calle" name="calle" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->calle;?>" placeholder="Calle" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-1">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="numero" name="numero" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->numero;?>" placeholder="Número" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Colonia</label>
                                <input id="colonia" name="colonia" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->colonia;?>" placeholder="Colonia" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">CP</label>
                                <input id="cp" name="cp" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->cp;?>" placeholder="CP" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Municipio</label>
                                  <input id="municipio" name="municipio" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->municipio;?>" placeholder="Municipio" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Ciudad</label>
                                  <input id="ciudad" name="ciudad" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->ciudad;?>" placeholder="Ciudad" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Estado</label>
                                <input id="estado" name="estado" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->estado;?>" placeholder="Estado" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">País</label>
                                <input id="pais" name="pais" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->pais;?>" placeholder="País" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div-->


                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                  <input id="telefono" name="telefono" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->telefono;?>" placeholder="Teléfono" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Email</label>
                                  <input id="email" name="email" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->email;?>" placeholder="Email" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                           <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Contraseña</label>
                                  <input id="password" name="password" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->password;?>" placeholder="Contraseña" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-6">
                            <!--div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Datos Bancarios</label>
                                <input id="datos_bancarios" name="datos_bancarios" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->tados_bancarios;?>" placeholder="Datos Bancarios" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div-->
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Nombre Banco</label>
                                <input id="datos_bancarios" name="datos_bancarios" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->tados_bancarios;?>" placeholder="Nombre Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Clabe</label>
                                  <input id="clabe" name="clabe" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->clabe;?>" placeholder="Clabe" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                <input id="sucursal" name="sucursal" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->sucursal;?>" placeholder="Sucursal" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Código Banco</label>
                                <input id="codigo_banco" name="codigo_banco" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->sucursal;?>" placeholder="Código Banco" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>


                      <div class="row">

                        <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Comentario extra</label>
                                  <input id="comentario_extra" name="comentario_extra" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->comentario_extra;?>" placeholder="Comentario" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Cuenta bancaria</label>
                                  <input id="numero_cuenta" name="numero_cuenta" type="text" class="form-control-sm form-control cc-exp" value="<?php echo $row->numero_cuenta;?>" placeholder="Número de cuenta" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          
                          
                            <div class="col-3">
                              <div class="form-group">
                                <label for="uso_CFDI">Uso del CFDI:</label>
                                <select class="form-control-sm alto form-control" id="uso_CFDI" name="uso_CFDI" >
                                  <?php if($row->uso_CFDI =="G01"){?>
                                  <option value="G01" selected>G01  Adquisición de mercancias</option>
                                  <?php }else{?>
                                    <option value="G01" selected>G01  Adquisición de mercancias</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="G02"){?>
                                  <option value="G02" selected>G02  Devoluciones, descuentos o bonificaciones</option>
                                  <?php }else{?>
                                    <option value="G02">G02 Devoluciones, descuentos o bonificaciones</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="G03"){?>
                                  <option value="G03" selected>G03  Gastos en general</option>
                                  <?php }else{?>
                                    <option value="G03">G03 Gastos en general</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I01"){?>
                                  <option value="I01" selected>I01  Construcciones</option>
                                  <?php }else{?>
                                    <option value="I01">I01 Construcciones</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I02"){?>
                                  <option value="I02" selected>I02  Mobilario y equipo de oficina por inversiones</option>
                                  <?php }else{?>
                                    <option value="I02">I02 Mobilario y equipo de oficina por inversiones</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I03"){?>
                                  <option value="I03" selected>I03  Equipo de transporte</option>
                                  <?php }else{?>
                                    <option value="I03">I03 Equipo de transporte</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I04"){?>
                                  <option value="I04" selected>I04  Equipo de computo y accesorios</option>
                                  <?php }else{?>
                                    <option value="I04">I04 Equipo de computo y accesorios</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I05"){?>
                                  <option value="I05" selected>I05  Dados, troqueles, moldes, matrices y herramental</option>
                                  <?php }else{?>
                                    <option value="I05">I05 Dados, troqueles, moldes, matrices y herramental</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I06"){?>
                                  <option value="I06" selected>I06  Comunicaciones telefónicas</option>
                                  <?php }else{?>
                                    option value="I06">I06  Comunicaciones telefónicas</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I07"){?>
                                  <option value="I07" selected>I07  Comunicaciones satelitales</option>
                                  <?php }else{?>
                                    <option value="I07">I07 Comunicaciones satelitales</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="I08"){?>
                                  <option value="I08" selected>I08  Otra maquinaria y equipo</option>
                                  <?php }else{?>
                                    <option value="I08">I08 Otra maquinaria y equipo</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D01"){?>
                                  <option value="D01" selected>D01  Honorarios médicos, dentales y gastos hospitalarios.</option>
                                  <?php }else{?>
                                    <option value="D01">D01 Honorarios médicos, dentales y gastos hospitalarios.</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D02"){?>
                                  <option value="D02" selected>D02  Gastos médicos por incapacidad o discapacidad</option>
                                  <?php }else{?>
                                    <option value="D02">D02 Gastos médicos por incapacidad o discapacidad</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D03"){?>
                                  <option value="D03" selected>D03  Gastos funerales.</option>
                                  <?php }else{?>
                                    <option value="D03">D03 Gastos funerales.</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D04"){?>
                                  <option value="D04" selected>D04  Donativos.</option>
                                  <?php }else{?>
                                    <option value="D04">D04 Donativos.</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D05"){?>
                                  <option value="D05" selected>D05  Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                  <?php }else{?>
                                    <option value="D05">D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D06"){?>
                                  <option value="D06" selected>D06  Aportaciones voluntarias al SAR.</option>
                                  <?php }else{?>
                                    <option value="D06">D06 Aportaciones voluntarias al SAR.</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D07"){?>
                                  <option value="D07" selected>D07  Primas por seguros de gastos médicos.</option>
                                  <?php }else{?>
                                    <option value="D07" >D07  Primas por seguros de gastos médicos.</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D08"){?>
                                  <option value="D08" selected>D08  Gastos de transportación escolar obligatoria.</option>
                                  <?php }else{?>
                                    <option value="D08">D08 Gastos de transportación escolar obligatoria.</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D09"){?>
                                  <option value="D09" selected>D09  Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                  <?php }else{?>
                                    <option value="D09">D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="D10"){?>
                                  <option value="D10" selected>D10  Pagos por servicios educativos (colegiaturas)</option>
                                  <?php }else{?>
                                    <option value="D10">D10 Pagos por servicios educativos (colegiaturas)</option>
                                  <?php }?>
                                  <?php if($row->uso_CFDI =="P01"){?>
                                  <option value="P01" selected>P01  Por definir</option>
                                  <?php }else{?>
                                    <option value="P01">P01 Por definir</option>
                                  <?php }?>



                                </select>



                              
                            </div>
                          </div>


                           <div class="col-2">
                            <div class="form-group">
                              <label for="factura_formaPago">Forma de pago:</label>
                              <select  class="form-control-sm alto form-control" name="factura_formaPago" id="factura_formaPago">
                                <?php if($row->factura_formaPago == "01"){?>
                                <option value="01" selected>1-Efectivo</option>
                              <?php }else{?>
                                <option value="01">1-Efectivo</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "02"){?>
                                <option value="02" selected>2-Cheque nominativo</option>
                              <?php }else{?>
                                <option value="02">2-Cheque nominativo</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "03"){?>
                                <option value="03" selected>3-Transferencia electrónica de fondos</option>
                              <?php }else{?>
                                <option value="03">3-Transferencia electrónica de fondos</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "04"){?>
                                <option value="04" selected>4-Tarjeta de crédito</option>
                              <?php }else{?>
                                <option value="04">4-Tarjeta de crédito</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "05"){?>
                                <option value="05" selected>5-Monedero electrónico</option>
                              <?php }else{?>
                                <option value="05">5-Monedero electrónico</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "06"){?>
                                <option value="06" selected>6-Dinero electrónico</option>
                              <?php }else{?>
                                <option value="06">6-Dinero electrónico</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "08"){?>
                                <option value="08" selected>8-Vales de despensa</option>
                              <?php }else{?>
                                <option value="08">8-Vales de despensa</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "12"){?>
                                <option value="12" selected>12-Dación en pago</option>
                              <?php }else{?>
                                <option value="12">12-Dación en pago</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "13"){?>
                                <option value="13" selected>13-Pago por subrogación</option>
                              <?php }else{?>
                                <option value="13">13-Pago por subrogación</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "14"){?>
                                <option value="14" selected>14-Pago por consignación</option>
                              <?php }else{?>
                                <option value="14">14-Pago por consignación</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "15"){?>
                                <option value="15" selected>15-Condonación</option>
                              <?php }else{?>
                                <option value="15">15-Condonación</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "17"){?>
                                <option value="17" selected>17-Compensación</option>
                              <?php }else{?>
                                <option value="17">17-Compensación</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "23"){?>
                                <option value="23" selected>23-Novación</option>
                              <?php }else{?>
                                <option value="23">23-Novación</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "24"){?>
                                <option value="24" selected>24-Confusión</option>
                              <?php }else{?>
                                <option value="24">24-Confusión</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "25"){?>
                                <option value="25" selected>25-Remisión de deuda</option>
                              <?php }else{?>
                                <option value="25">25-Remisión de deuda</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "26"){?>
                                <option value="26" selected>26-Prescripción o caducidad</option>
                              <?php }else{?>
                                <option value="26">26-Prescripción o caducidad</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "27"){?>
                                <option value="27" selected>27-A satisfacción del acreedor</option>
                              <?php }else{?>
                                <option value="27">27-A satisfacción del acreedor</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "28"){?>
                                <option value="28" selected>28-Tarjeta de débito</option>
                              <?php }else{?>
                                <option value="28">28-Tarjeta de débito</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "29"){?>
                                <option value="29" selected>29-Tarjeta de servicios</option>
                              <?php }else{?>
                                <option value="29">29-Tarjeta de servicios</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "30"){?>
                                <option value="30" selected>30-Aplicación de anticipos</option>
                              <?php }else{?>
                                <option value="30">30-Aplicación de anticipos</option>
                                <?php }?>
                                <?php if($row->factura_formaPago == "99"){?>
                                <option value="99" selected>99-Por definir</option>
                              <?php }else{?>
                                <option value="99">99-Por definir</option>
                                <?php }?>

                              </select>

                            </div>
                         </div>


                      </div>

                      <div class="row">
                         <div class="col-9" >

                      <div align="right">
                        <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Editar</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                        <div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                        </div>
                      </div>

                       </div>
                      </div>


                  </form>
              </div>
          </div>

        </div>



            <div class="row">
            <div class="col-7" style="background: #ecf0f1">
                      <label>Contactos Cliente</label>
                      <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                <input id="contacto_nombre" name="contacto_nombre" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Nombre" >
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Email</label>
                                <input id="contacto_email" name="contacto_email" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Email" >
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                <input id="contacto_telefono" name="contacto_telefono" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Teléfono" >
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>

                      <div align="right">
                       
                          <button id="guardar_contacto" type="button" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar Contacto</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                        
                          <div align="center">
                           
                          </div>
                      </div>


                      <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php if(is_array($contactos)):?>
              <?php foreach($contactos as $row_contacto):?>
                <tr>
                  <td><?php echo $row_contacto->nombre?></td>
                  <td><?php echo $row_contacto->email?></td>
                  <td><?php echo $row_contacto->telefono?></td>
                  <td>
                    <a href="<?php echo base_url()?>index.php/clientes/eliminar_contacto/<?php echo $row_contacto->id;?>/<?php echo $row->id;?>">
                    <button type="button" class="btn btn-light" style="color:RED">Eliminar</button>
                    </a>
                  </td>
                </tr>
             <?php endforeach;?>
             <?php endif;?>
            </tbody>
          </table>



        </div>



        <?php if(is_array($facturas)):?>
                      <?php 
                        $general_total = 0;
                        $general_abono = 0;
                        $general_saldo = 0;
                      ?>
                      <?php foreach($facturas as $row):?>
                        
                           
                            
                          
                            <?php if($row->factura_medotoPago == "PPD"):?>

                              <?php $general_total+= $this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID);?>
                              
                              <?php $saldo1 = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                
                              ?>
                              <?php $general_abono+= $this->Mgeneral->complementoPagoPagado($row->sat_uuid);?>
                            <?php endif;?>
                          
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <?php 
                                $saldo = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                ?>
                              <?php 

                              $general_saldo+= (($this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID))-$saldo);?>
                            <?php endif;?>
                          
                            

                         
                          
                     <?php endforeach;?>
                     <?php endif;?>
                     
                        
                        
                        
                        


        <div class="col-5" style="background: #ecf0f1">
          <div class="row">
              <div class="col-12">
                  <div class="form-group">
                      <label for="cc-exp" class="control-label mb-1">Estado de cuenta</label>

                      <h3>Abonos:<?php echo "$".$general_abono;?></h3>
                      <h3>Saldo:<?php echo "$".$general_saldo;?></h3>
                      <h3>Total:<?php echo "$".$general_total;?></h3>
                      <div align="right">
                        <a href="<?php echo base_url()?>index.php/clientes/ver_factura_cliente/<?php echo $id_cliente;?>">
                    <button type="button" class="btn btn-info">Estado de cuentas</button>
                    </a>
                        
                      </div>
                      
                  </div>
              </div>
          </div>
        </div>

      </div>


    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->



<script type="text/javascript">
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});
    } );
</script>

<hr/>
<div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
  <div class="col-lg-12">
    <h3>Direcciones</h3>
    <br/>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Nuevo
    </button>

    <button type="button" class="btn btn-info btn_editar" data-toggle="modal" data-target="#exampleModal2" onclick="editar_direccion()">Editar</button>

    <button type="button" class="btn btn-danger btn_eliminar">Eliminar</button>

  </div>
</div>
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                  <form id="formulario_editar">
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>
                                   
                            <!--input type="checkbox" name=""  id="todos_boton" class="todos_boton" -->
                                    
                          </th>
                          <th>Descripción</th>
                          <th>Domicilio</th>
                          <th>Rol</th>
                          <th>Principal</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                          <input type="hidden" name="" id="radio_id">
                        <?php if(is_array($direcciones)):?>
                        <?php foreach($direcciones as $row):?>
                          <tr id="borrar_<?php echo $row->id?>">
                            <td><input type="radio" name="seleccion" class="myCheckBox" id="<?php echo $row->id?>" value="<?php echo $row->id?>" onclick="radio_button(<?php echo $row->id?>)">
                            
                            </td>

                            <td><span class="textNombre_ texto_nombre"><?php echo $row->descripcion;?></span></td>
                            <td><span class="textNombre_ texto_nombre"><?php echo $row->calle." ".$row->numero." ".$row->colonia." ".$row->municipio." ".$row->ciudad." ".$row->estado." CP:".$row->cp;?></span></td>
                            <td><span class="textNombre_ texto_nombre"><?php echo rol_direccion($row->rol);?></span></td>
                            <td><span class="textNombre_ texto_nombre">
                              <?php 
                             
                                if($row->principal == 1){
                                  echo '<input type="checkbox"  onclick="return false;" name="" value="" checked>';
                                }
                              ?>
                                
                              </span></td>
                            
                            <span hidden="hidden" class="id_des_<?php echo $row->id?> texto_id" type="hidden"><?php echo $row->id?></span>

                            
                           
                          </tr>
                       <?php endforeach;?>
                       <?php endif;?>
                      </tbody>
                    </table>
                  </form>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->





<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva dirección </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post" novalidate="novalidate" id="alta_usuario2">


          <div class="row">
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Descripción</label>
                                  <input id="descripcion" name="descripcion" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Descripciòn" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Rol</label>
                                  <select class="form-control-sm form-control cc-exp" id="rol" name="rol">
                                    <?php foreach($roles as $raw_rol):?>
                                      <option value="<?php echo $raw_rol->id;?>"><?php echo $raw_rol->rol;?></option>
                                    <?php endforeach;?>
                                  </select>
                                  
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Principal</label>
                                <input type="checkbox" name="principal" value="" id="principal">
                            </div>
                          </div>
                         
                      </div>

           <div class="row">
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Calle</label>
                                  <input id="calle" name="calle" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Calle" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="numero" name="numero" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Número" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Colonia</label>
                                <input id="colonia" name="colonia" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Colonia" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">CP</label>
                                <input id="cp" name="cp" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="CP" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Municipio</label>
                                  <input id="municipio" name="municipio" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Municipio" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Ciudad</label>
                                  <input id="ciudad" name="ciudad" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Ciudad" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Estado</label>
                                <input id="estado" name="estado" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Estado" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">País</label>
                                <input id="pais" name="pais" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="País" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>



                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                  </form>
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>





<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar dirección  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post" novalidate="novalidate" id="alta_usuario3">


          <div class="row">
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Descripción</label>
                                  <input id="e_descripcion" name="descripcion" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Descripciòn" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Rol</label>
                                  <select class="form-control-sm form-control cc-exp" id="e_rol" name="rol">
                                    <?php foreach($roles as $raw_rol):?>
                                      <option value="<?php echo $raw_rol->id;?>"><?php echo $raw_rol->rol;?></option>
                                    <?php endforeach;?>
                                  </select>
                                  
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Principal</label>
                                <input type="checkbox" name="principal" value="" id="e_principal">
                            </div>
                          </div>
                         
                      </div>

           <div class="row">
                          <div class="col-4">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Calle</label>
                                  <input id="e_calle" name="calle" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Calle" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Número</label>
                                  <input id="e_numero" name="numero" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Número" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-4">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Colonia</label>
                                <input id="e_colonia" name="colonia" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Colonia" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">CP</label>
                                <input id="e_cp" name="cp" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="CP" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Municipio</label>
                                  <input id="e_municipio" name="municipio" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Municipio" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Ciudad</label>
                                  <input id="e_ciudad" name="ciudad" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Ciudad" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Estado</label>
                                <input id="e_estado" name="estado" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Estado" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">País</label>
                                <input id="e_pais" name="pais" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="País" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                      </div>



                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Editar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                  </form>
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>


