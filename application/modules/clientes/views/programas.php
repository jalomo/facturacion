 <!--link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/-->
<!--link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script-->

  <style type="text/css">
.datepicker{z-index:1151 !important;}
  </style>
<script>
$(document).ready(function(){
$("#dias_semana").hide();
$("#repeticion_meses").hide();
$('#repeticion').on('change', function() {
 
  tipo_repeticion = this.value;
  switch(tipo_repeticion) {
    case "1":
      $("#dias_semana").hide();
      $("#repeticion_meses").hide();
      break;
    case "2":
      $("#dias_semana").show();
      $("#repeticion_meses").hide();
      break;
    case "3":
      $("#dias_semana").hide();
      $("#repeticion_meses").hide();
      break;
    case "4":
      $("#dias_semana").hide();
      $("#repeticion_meses").hide();
      break;
  }
});


$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/clientes/guarda_esquema/<?php echo $id_cliente;?>";
  ajaxJson(url,{"producto_nombre":$("#producto_nombre").val(),
                "producto_clave":$("#producto_clave").val(),
                "producto_id":$("#id_producto").val(),
                 "dosis":$("#dosis").val(),
                 "actividad":$("#actividad").val(),
                 "metodo":$("#metodo").val(),
                 "plaga":$("#plaga").val()
               },
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect(" GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/programas/<?php echo $id_cliente;?>");
    }


  });
});

});

function seleccionar_producto(id_producto){
  //$('#exampleModal').modal('hide');
  var url_sis ="<?php echo base_url()?>index.php/servicios/get_datos_producto/"+id_producto;
  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            processData: false,
            contentType: false,
            datatype: "JSON",
            cache: false,
            timeout: 600000,
            success: function (data) {
              var obj = JSON.parse(data);
              $('#producto_nombre').val(obj.productoNombre);
              $('#id_producto').val(obj.productoId);

              html = '<select class="form-control cc-exp form-control-sm" id="dosis" name="dosis">'
                         +' <option value="1" >ALTA:'+obj.productoDosisAlta+obj.productoMedidaAlta+'</option>'
                         + '<option value="2" >BAJA:'+obj.productoDosisBaja+obj.productoMedidaBaja+'</option>'
                        +'</select>';
             
              $("#dosis_html").html("");
              $("#dosis_html").html(html);




              console.log("SUCCESS : ", data);





            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
            }
        });
}

function selecciona_programa(){
  aux = $("#auxp").val(); //mes /semana /día
  var res = aux.split("/");
  
  /*switch(res[1]) {
    case "1":
      semana1 = 1;
      semana2 = 0;
      semana3 = 0;
      semana4 = 0;
      break;
    case "2":
      semana1 = 0;
      semana2 = 1;
      semana3 = 0;
      semana4 = 0;
      break;
    case "3":
      semana1 = 0;
      semana2 = 0;
      semana3 = 1;
      semana4 = 0;
      break;
    case "4":
      semana1 = 0;
      semana2 = 0;
      semana3 = 0;
      semana4 = 1;
      break;
    
  }
  */
  if( $('#todo_anio').prop('checked') ) {
    //alert("seleccionado");
    todo_anio = 1;
  }else{
    //alert("no selecci0onado");
     todo_anio = 0;
  }
lunes = 0;
martes= 0;
miercoles = 0;
jueves = 0;
viernes = 0;
sabado = 0;
domingo = 0;
   if( $('#lunes').prop('checked') ) {lunes = 1;}else{lunes = 0;}
   if( $('#martes').prop('checked') ) {martes = 1;}else{martes = 0;}
   if( $('#miercoles').prop('checked') ) {miercoles = 1;}else{miercoles = 0;}
   if( $('#jueves').prop('checked') ) {jueves = 1;}else{jueves = 0;}
   if( $('#viernes').prop('checked') ) {viernes = 1;}else{viernes = 0;}
   if( $('#sabado').prop('checked') ) {sabado = 1;}else{sabado = 0;}
   if( $('#domingo').prop('checked') ) {domingo = 1;}else{domingo = 0;}

  var url ="<?php echo base_url()?>index.php/clientes/guarda_programa/<?php echo $id_cliente;?>";
  ajaxJson(url,{"mes":res[0],
                "dia":res[2],
                "semana":res[1],
                 "id_esquema":$("#id_esquema_p").val(),
                 "fecha":$("#fecha").val(),
                 "nombre_cliente":$("#nombre_cliente").val(),
                 "lunes":lunes,
                 "martes":martes,
                 "miercoles":miercoles,
                 "jueves":jueves,
                 "viernes":viernes,
                 "sabado":sabado,
                 "domingo":domingo,
                 "repeticion":$("#repeticion").val(),
                 "fecha_inicio":$("#fecha_inicio").val(),
                 "todo_anio":todo_anio,
                 "repetir_cada":$("#repetir_cada").val(),
                 "numero_repeticiones":$("#numero_repeticiones").val(),

               },
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect(" GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/clientes/programas/<?php echo $id_cliente;?>");
    }


  });
}

function selecciona_pro(id_p){
  $("#auxp").val(id_p);

  aux = $("#auxp").val(); //mes /semana /día
  var res = aux.split("/");
  
  var url ="<?php echo base_url()?>index.php/clientes/get_esquemas/"+res[0]+"/"+res[1]+"/"+res[2];
  ajaxJson(url,{},
            "POST","",function(result){
    $("#lista_esquema").html(result);

  });
}

function revisar_anio(){
  if( $('#todo_anio').prop('checked') ) {
    //alert("seleccionado");
    $("#numero_re").hide();
  }else{
    //alert("no selecci0onado");
    $("#numero_re").show();
  }
}
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>
<input value="<?php echo nombre_cliente($id_cliente);?>" id="nombre_cliente" type="hidden">
<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title"><h3><?php echo nombre_cliente($id_cliente);?></h3></strong>
            <a href="<?php echo base_url()?>index.php/clientes/calendario/<?php echo $id_cliente;?>">Calendario</a>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                        
                            <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Producto</label>
                                  <input id="producto_nombre" name="producto_nombre" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="producto_nombre" data-toggle="modal"  data-target="#exampleModalProducto" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                                  <input type="hidden" id="id_producto">
                              </div>
                          </div>
                          <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Clave</label>
                                <input id="producto_clave" name="producto_clave" type="text" class="form-control cc-exp form-control-sm" value="" placeholder="clave" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                          <div class="col-1">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Dosis:</label>
                                <div id="dosis_html"></div>
                                <!--select class="form-control cc-exp form-control-sm" name="dosis" id="dosis">
                                  <option value="1">Alta</option>
                                  <option value="2">Baja</option>
                                </select-->
                                <br/>
                                
                               
                            </div>
                          </div>
                         
                      </div>
                      <div class="row">
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Actividad</label>
                              <select class="form-control cc-exp form-control-sm" id="actividad" >
                                <?php foreach($actividades as $row_actividad):?>
                                  <option value="<?php echo $row_actividad->actividadId?>"><?php echo $row_actividad->actividadNombre?></option>
                                <?php endforeach;?>
                              </select>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Método</label>
                                <select class="form-control cc-exp form-control-sm" id="metodo" >
                                 
                                  <?php foreach($metodos as $row_metodos):?>
                                  
                                    <option value="<?php echo $row_metodos->metodoId?>" flag="<?php echo $row_metodos->metodoBoquilla?>"><?php echo $row_metodos->metodoNombre?></option>
                                  <?php endforeach;?>
                                </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-2">
                          <label for="cc-exp" class="control-label mb-1">Plaga</label>
                          <select class="form-control cc-exp form-control-sm" id="plaga">
                            <?php foreach($plagas as $row_plaga):?>
                              <option value="<?php echo $row_plaga->plagaId?>"><?php echo $row_plaga->plagaNombre?></option>
                            <?php endforeach;?>
                          </select>

                        </div>
                      </div>


                      <div>
                      <div align="right" class="col-7">
                          <button id="" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-save fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                    </div>


                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->


<script type="text/javascript">
    $(document).ready(function() {
      //$('#bootstrap-data-table').DataTable();
    } );
</script>

<?php 
/*
$fechaInicio=strtotime("2019-09-01");
$fechaFin=strtotime("2019-09-30");
//Recorro las fechas y con la función strotime obtengo los lunes
for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
    //Sacar el dia de la semana con el modificador N de la funcion date
    $dia = date('N', $i);
    if($dia==1){
        echo "Lunes. ". date ("Y-m-d", $i)."<br>";
    }
}

*/
?>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
          <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Clave</th>
                <th>Producto</th>
                <th>Dosis</th>
                <th>Actividad</th>
                <th>Metodo</th>
                <th>Plaga</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php if(is_array($esquemas)):?>
              <?php foreach($esquemas as $row):?>
                <tr>
                  <th><?php echo $row->producto_clave;?></th>
                  <th><?php echo $row->producto_nombre;?></th>
                 <th><?php 
                          if($row->dosis == 1){
                              echo "Alta";
                          }
                          if($row->dosis == 2){
                              echo "Baja";
                          }
                
                 ?></th>
                  <th><?php echo nombre_actividad($row->actividad);?></th>
                   <th><?php echo nombre_metodo($row->metodo);?></th>
                    <th><?php echo nombre_plaga($row->plaga);?></th>
                  <th><!--a href="<?php echo base_url()?>index.php/clientes/">
                    <button type="button" class="btn btn-danger" >Eliminar</button>
                    </a--></th>
                </tr>
             <?php endforeach;?>
             <?php endif;?>
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->



<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
          <table id="bootstrap-data-table" class="table-bordered">
            <thead>
              <tr>
                <th>Mes</th>
                <th colspan="7" >Primera semana</th>
                <th colspan="7" >Segunda semana</th>
                <th colspan="7" >Tercera semana</th>
                <th colspan="7" >Cuarta o quinta semana</th>
                
              </tr>
              <tr>
                <th></th>
                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>

                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>

                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>

                <th>L</th>
                <th>M</th>
                <th>M</th>
                <th>J</th>
                <th>V</th>
                <th>S</th>
                <th>D</th>


               

              </tr>
            </thead>
            <tbody>
              <?php for($i = 1; $i<13; $i++){?>
                <tr>
                  <th><?php echo $i;?></th>

                  <!-- PRIMER SEMANA-->

                  <?php for($semm=1;$semm<=4;$semm++):?>
                  <?php for($diass=1;$diass<=7;$diass++):?>
                  <th>
                    <button data-toggle="modal"  data-target="#esquema_p" id="<?php echo $i;?>/<?php echo $semm;?>/<?php echo $diass;?>" onclick="selecciona_pro('<?php echo $i;?>/<?php echo $semm;?>/<?php echo $diass;?>')">
                      <?php
                        if(is_object($this->Mgeneral->get_fecha_esquema($i,$semm,$diass))){

                         // echo $this->Mgeneral->get_row('id_esquema',$this->Mgeneral->get_fecha_esquema( $i,1,1)->id_esquema,'esquema')->producto_clave;

                          $id_pro = $this->Mgeneral->get_fecha_esquema( $i,$semm,$diass)->id_prgrama;
                          $aux1_rows = $this->Mgeneral->get_result('id_programa',$id_pro ,'programa_esquema');
                          foreach($aux1_rows as $key_aux1):
                            echo $this->Mgeneral->get_row('id_esquema',$key_aux1->id_esquema,'esquema')->producto_clave.",";
                          endforeach;



                        }else{
                          echo 0;
                        }
                       
                      ?>
                    </button>
                    
                  </th>
                  <?php endfor;?>
                <?php endfor;?>







                  
                </tr>
              <?php }?>
             
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->





<link rel="stylesheet" href="<?php echo base_url()?>statics/css/jquery-ui.css">
<script src="<?php echo base_url()?>statics/js/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

      $("#fecha_inicio").datepicker({
        //altFormat: "yy-mm-dd",
        dateFormat: 'yy-m-d' ,
        appendText: ""
      });
     });
</script>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="esquema_p" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar un esquema</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <input type="hidden" id="auxp">

          <div class="row" style="font-size: 12px;">
            <div class="col-sm-7">
              Fecha de inicio:<br/>
              <input id="fecha_inicio" name="" type="text" class="" value="" placeholder="Fecha inicio" autocomplete="off">
              <br/><br/>
              Repetir cada<input type="number" style="width:50px" id="repetir_cada" value="1">
               <select id="repeticion">
                <option value="1">Día</option>
                <option value="2">Semana</option>
                <option value="3">Mes</option>
                <!--option value="4">Año</option-->
              </select>
              <br/>
              <br/>
              <div id="dias_semana">
              Lunes<input type="checkbox" name="" value="" id="lunes">&nbsp
              Martes<input type="checkbox" name="" value="" id="martes">&nbsp
              Miercoles<input type="checkbox" namevalue="" id="miercoles">&nbsp
              Jueves<input type="checkbox" name="" value="" id="jueves">&nbsp
              Viernes<input type="checkbox" name="" value="" id="viernes">&nbsp
              Sábado<input type="checkbox" name="" value="" id="sabado">&nbsp
              Domingo<input type="checkbox" name="" value="" id="domingo">&nbsp
              </div>
              <div id="r_meses">
                <select id="repeticion_meses">
                  <option value="1">Cada Mes</option>
                  <option value="2">repetir promer  miercoles de cada mes</option>
               
                </select>
              </div>
              <br/>
              <hr/>
              Finaliza:
              <br/>
              <br/>
              Fin de año<input type="checkbox" name="" value="1" id="todo_anio" onclick="revisar_anio()"><br/><br/>
              <div id="numero_re">
              Despues de <input type="number" style="width:50px" id="numero_repeticiones">ocurrencias
              </div>
              
            </div>
            <div class="col-sm-5" >

             Esquema:
            <select class="form-control cc-exp " id="id_esquema_p">
              
              <?php if(is_array($esquemas)):?>
                <?php foreach($esquemas as $row):?>
                  <option value="<?php echo $row->id_esquema?>"><?php echo $row->producto_clave;?></option>
               <?php endforeach;?>
               <?php endif;?>

            </select>
              
            </div>
          </div>



         
         
             
          
            

        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="selecciona_programa()">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>



       <div class="row card-body">
            
            <div class='col-sm-8'>
              Esquemas agregados:
              <ul class="list-group" id="lista_esquema">
               
            
              </ul>
            </div>


            <div class='col-sm-4'>
              
            </div>
            
          </div>

    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModalProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Producto</th>
        <th>Producto referencia</th>

        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      <?php if(is_array($productos)):?>
      <?php foreach($productos as $row_producto):?>
        <tr id="<?php echo $row_producto->productoId;?>">
          <td><?php echo $row_producto->productoNombre;?></td>
          <td><?php echo $row_producto->productoReferencia;?></td>
          <td>


            <button type="button" class="btn btn-info boton" onclick="seleccionar_producto(<?php echo $row_producto->productoId;?>)" data-dismiss="modal" id="<?php echo $row_producto->productoId;?>" data-id="<?php echo $row_producto->productoId;?>">seleccionar</button>
          </td>
        </tr>
     <?php endforeach;?>
     <?php endif;?>
    </tbody>
  </table>
        </div>
      </div>

    </div>
  </div>
</div>
