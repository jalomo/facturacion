
	<link rel="stylesheet" href="<?php echo base_url()?>statics/node_modules/fullcalendar/dist/fullcalendar.min.css"  type="text/css">
	<link rel="stylesheet" href="<?php echo base_url()?>statics/node_modules/fullcalendar/dist/fullcalendar.print.min.css"  type="text/css" media="print">
	<!--link rel="stylesheet" href="<?php echo base_url()?>statics/css/bootstrap.min.css"  type="text/css"-->
	<script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/moment/min/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/fullcalendar/dist/fullcalendar.min.js"></script>
	<!--script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/bootstrap/dist/js/bootstrap.min.js"></script-->

	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<style>
		li{
			font-family: 'Roboto', sans-serif;
		}
		.fc-time{
   display : none;
}
	</style>
	<script type="text/javascript">
		     menu_activo = "clientes";
$("#menu_clientes_lista").last().addClass("menu_estilo");
	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
      //hiddenDays: [ 0,4,5,6 ],
      dayRender: function(date, cell){
        
    },
      monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
      monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
      dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
      dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
			dayClick: function(date, jsEvent, view) {

        aux = date.format('dddd');
        



			},
			header: {
				//left: 'prev,next today',
				left: 'prev,next',
				center: 'title',
				//right: 'month,agendaDay,listWeek'
			},
			eventClick: function(calEvent, jsEvent, view) {
				//alert(calEvent.id_programa);

				$.ajax({ // ajax for delete appointment
			    		type : 'POST',
			    		data : {},
			    		url : "<?php echo base_url();?>index.php/clientes/verifica_programa/"+calEvent.id,
			    		beforeSend: function( xhr ) {
			    			// before change from server, first change on browser
	        				
	  					},
			    		success: function(result){ // return from PHP server
			    			if(result==0){
			    			var dialog = bootbox.dialog({
										    title: '',
										    message: "<p>Seleccione una opción.</p>",
										    size: 'large',
										    buttons: {
										        cancel: {
										            label: "Cerrar",
										            className: 'btn-danger',
										            callback: function(){
										                console.log('Custom cancel clicked');
										            }
										        },
										        noclose: {
										            label: "Generar servicio",
										            className: 'btn-warning',
										            callback: function(){
										                //console.log('Custom button clicked');
										                //return false;
										                $.ajax({ // ajax for delete appointment
												    		type : 'POST',
												    		data : {},
												    		url : "<?php echo base_url();?>index.php/clientes/crear_servicio/"+calEvent.id,
												    		beforeSend: function( xhr ) {
												    			// before change from server, first change on browser
										        				
										  					},
												    		success: function(result){ // return from PHP server
												    			window.location.href = "<?php echo base_url()?>index.php/servicios/nueva/"+result;
												    		}
												    	});
										            }
										        },
										        ok: {
										            label: "Ir a programa",
										            className: 'btn-info',
										            callback: function(){
										                //console.log('Custom OK clicked');
										                window.location.href = "<?php echo base_url()?>index.php/clientes/programas/<?php echo $id_cliente;?>";
										            }
										        }
										    }
										});
							}

							if(result>0){
								var dialog = bootbox.dialog({
										    title: '',
										    message: "<p>Seleccione una opción.</p>",
										    size: 'large',
										    buttons: {
										        cancel: {
										            label: "Cerrar",
										            className: 'btn-danger',
										            callback: function(){
										                console.log('Custom cancel clicked');
										            }
										        },
										        noclose: {
										            label: "Ir a servicio",
										            className: 'btn-info',
										            callback: function(){
										                //console.log('Custom button clicked');
										                //return false;
										                $.ajax({ // ajax for delete appointment
												    		type : 'POST',
												    		data : {},
												    		url : "<?php echo base_url();?>index.php/clientes/ir_servicio/"+calEvent.id,
												    		beforeSend: function( xhr ) {
												    			// before change from server, first change on browser
										        				
										  					},
												    		success: function(result){ // return from PHP server
												    			window.location.href = "<?php echo base_url()?>index.php/servicios/nueva/"+result;
												    		}
												    	});
										            }
										        },
										        ok: {
										            label: "Ir a programa",
										            className: 'btn-info',
										            callback: function(){
										                //console.log('Custom OK clicked');
										                window.location.href = "<?php echo base_url()?>index.php/clientes/programas/<?php echo $id_cliente;?>";
										            }
										        }
										    }
										});

							}
			    			
			    		}
			    	})

				
				
			    //$('#modalUpdate').modal('show');
				//window.location.href = "<?php echo base_url()?>index.php/servicios/nueva/2";
				/*$('#modalUpdate').modal();
				$('#ModalAppointmentUpdate').val(calEvent.title);
				$('#nivel_v').val("Nivel: "+calEvent.nivel);
				$('#grado_v').val("Grado: "+calEvent.grado);
				$('#turno_v').val("Turno: "+calEvent.turno);
				$('#nombre_v').val("Contacto: "+calEvent.nombre);
				$('#email_v').val("Email: "+calEvent.email);
				$('#celular_v').val("Teléfono: "+calEvent.celular);
				$('#municipio_v').val("Municipio: "+calEvent.municipio);
				$('#alumnos_v').val("Alumnos: "+calEvent.alumnos);
				$('#comentarios_v').val(calEvent.comentarios);
				//$('#ModalDateUpdate').val($.fullCalendar.formatDate(calEvent._start, 'YYYY-MM-DD HH:mm:ss'))

			    // update appointment
			    jQuery('#ModalUpdate').click(function () {
			    	$.ajax({ // ajax for delete appointment
			    		type : 'POST',
			    		data : {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>', id: calEvent.id, title: $('#ModalAppointmentUpdate').val(), date: $('#ModalDateUpdate').val() },
			    		url : "<?php echo base_url();?>index.php/calendario/ajaxupdate/",
			    		beforeSend: function( xhr ) {
			    			// before change from server, first change on browser
	        				calEvent.title = $('#ModalAppointmentUpdate').val();
	        				calEvent.start = $('#ModalDateUpdate').val();
						    $('#calendar').fullCalendar('updateEvent', calEvent);
	  					},
			    		success: function(result){ // return from PHP server
			    			location.reload();
			    		}
			    	})
			    });*/

			    // delete appointment
			    $remove_appoinment = $(this).css('display', '');
				jQuery('#ModalDelete').click(function () {
					$remove_appoinment.css('display', 'none');
					$.ajax({ // ajax for delete appointment
			    		type : 'POST',
			    		data : {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>', id: calEvent.id},
			    		url : "calendario/ajaxdelete/",
			    		success: function(result){ // return from PHP server
			    		}
			    	})
				});
	    	},
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: '<?php echo base_url();?>index.php/clientes/ajaxevent/<?php echo $id_cliente;?>' // get event list from JSON format
		});
	});
	</script>
	<style>
		body{margin:40px 10px;padding:0;font-family:"Lucida Grande",Helvetica,Arial,Verdana,sans-serif;font-size:14px}#calendar{max-width:900px;margin:0 auto}
	</style>


	<div class="card-header">
            <strong class="card-title"><h3><?php echo nombre_cliente($id_cliente);?></h3></strong>
        </div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div id='calendar'></div>
			</div>
			
		</div>

	</div>





<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Seleccione un producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">

        </div>
      </div>

    </div>
  </div>
</div>


