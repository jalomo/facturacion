

<script src="<?php echo base_url()?>statics/js/isloading.js"></script>
<script type="text/javascript">
       menu_activo = "clientes";
$("#menu_clientes_lista").last().addClass("menu_estilo");
    $(document).ready(function() {
      //$('#bootstrap-data-table').DataTable();



      $(".eliminar_factura").click(function(event){
           event.preventDefault();

            bootbox.dialog({
               message: "Desea cancelar la factura?",
               closeButton: true,
               buttons:
               {
                   "danger":
                   {
                       "label": "Aceptar ",
                       "className": "btn-danger",
                       "callback": function () {

                        var dialog_load = bootbox.dialog({
                            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Cancelando...</p>',
                            closeButton: false
                        });

                         id = $(event.currentTarget).attr('flag');
                         url = $("#delete"+id).attr('href');
                         //$.get(url);
                         ajaxJson(url,{},
                                   "GET",true,function(result){

                            dialog_load.modal('hide');

                           console.log(result);
                           json_response = JSON.parse(result);

                           obj_status = json_response.error;
                           if(obj_status == true){

                             exito("<h3>ERROR intente de nuevo<h3/> ","danger");
                           }
                           if(obj_status == false){

                             exito_redirect("FACTURA CANCELADA CON EXITO","success","<?php echo base_url()?>index.php/factura/lista");
                           }
                         });
                       }
                   },
                   "cancel":
                   {
                       "label": "<i class='icon-remove'></i> Cancelar",
                       "className": "btn-sm btn-info",
                       "callback": function () {

                       }
                   }

               }
           });
       });

    } );

function ver_factura(id_factura){

  url = "<?php echo base_url()?>index.php/clientes/ver_datos_factura/"+id_factura;

  ajaxJson(url,{}, "GET",true,function(result){

            dialog_load.modal('hide');

            console.log(result);
            json_response = JSON.parse(result);

            obj_status = json_response.error;
            if(obj_status == true){

              exito("<h3>ERROR intente de nuevo<h3/> ","danger");
             }
              if(obj_status == false){

               exito_redirect("FACTURA CANCELADA CON EXITO","success","<?php echo base_url()?>index.php/factura/lista");
             }
   });

}
</script>
<style>


label{
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
}
input{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
select{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
button{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
table{
  font-family: 'Roboto', sans-serif !important;
  font-size: 12px !important;;
}
strong{
  font-family: 'Roboto', sans-serif !important;
}
</style>



<!--div class="card-header">
  <div class="">
      <a href="<?php echo base_url();?>index.php/factura/crear_pre"><button type="button" class="btn btn-primary">Crear factura</button></a>
  </div>

</div-->

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                   
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        
                        <th>Fecha</th>
                        <th>M.P.</th>
                        <th>UUID</th>
                        <th>Total</th>
                        <th>Abono</th>
                        <th>Saldo</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($facturas)):?>
                      <?php 
                        $general_total = 0;
                        $general_abono = 0;
                        $general_saldo = 0;
                      ?>
                      <?php foreach($facturas as $row):?>
                        <tr style="<?php if($row->cancelar_EstatusUUID == "202"){echo "background:#fab1a0;";} if( ($row->sat_error == "0")){ echo "background:#c7f8d9;"; }?>">
                          
                          <td><?php echo $row->factura_fecha;?></td>
                          <td><?php echo $row->factura_medotoPago;?></td>
                          <td><?php echo $row->sat_uuid;?></td>
                          <td>
                            $<?php echo $this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID);?>
                            <?php $general_total+= $this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID);?>
                          </td>
                          <td>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <?php $saldo1 = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                echo "$".$saldo1;
                              ?>
                              <?php $general_abono+= $this->Mgeneral->complementoPagoPagado($row->sat_uuid);?>
                            <?php endif;?>
                          </td>
                          <td>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <?php 

                                $saldo = $this->Mgeneral->complementoPagoPagado($row->sat_uuid);
                                echo "$".(($this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID))-$saldo);
                              ?>
                              <?php 

                              $general_saldo+= (($this->Mgeneral->factura_subtotal($row->facturaID) + $this->Mgeneral->factura_iva_total($row->facturaID))-$saldo);?>
                            <?php endif;?>
                          </td>

                          <td>
                            <?php if($row->factura_medotoPago == "PPD"):?>
                              <a href="<?php echo base_url()?>index.php/clientes/pagos/<?php echo $row->facturaID;?>" >
                            <button type="button" class="btn btn-info">Pagos</button>
                            </a>
                            <?php endif;?>
                            <?php if(($row->prefactura == 1) && ($row->sat_error!="0")):?>
                            <a target="_blank" href="<?php echo base_url()?>index.php/factura/genera_pdf_prefactura/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-success">PDF</button>
                            </a>
                            <?php endif;?>
                            <?php if( ($row->sat_error == "0")):?>
                            <a href="<?php echo base_url()?>statics/facturas/facturas_xml/<?php echo $row->facturaID;?>.xml" download>
                            <button type="button" class="btn btn-success">XML</button>
                            </a>
                            <a target="_blank" href="<?php echo base_url()?>index.php/factura/genera_pdf/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-success">PDF</button>
                            </a>
                            <?php if($row->cancelar_EstatusUUID != "202"):?>
                            <a class="eliminar_factura" flag="<?php echo $row->facturaID;?>" id="delete<?php echo $row->facturaID;?>" href="<?php echo base_url()?>index.php/factura/cancelar_factura/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-danger">Cancelar</button>
                            </a>
                           <?php endif;?>

                          <?php endif;?>
                            <?php if($row->sat_error != "0"):?>
                            <a href="<?php echo base_url()?>index.php/factura/nueva/<?php echo $row->facturaID;?>">
                            <button type="button" class="btn btn-info">Ver/Editar</button>
                            </a>
                          <?php endif;?>
                          <a href="<?php echo base_url()?>index.php/clientes/enviar_email/<?php echo $row->facturaID;?>/<?php echo $id_cliente;?>">
                            <button type="button" class="btn btn-info">Enviar</button>
                            </a>
                          </td>
                        </tr>
                     <?php endforeach;?>
                     <?php endif;?>
                     <tr>
                        <th colspan="3" ><div align="right">Total</div></th>
                        <th><?php echo "$".$general_total;?></th>
                        <th><?php echo "$".$general_abono;?></th>
                        <th><?php echo "$".$general_saldo;?></th>
                        <th></th>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->





