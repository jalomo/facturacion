<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Productos extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $titulo['titulo'] = "Alta de Producto" ;
      $titulo['titulo_dos'] = "Alta de Producto" ;
      $medidas['medidas'] = $this->Mgeneral->get_result('status',0,'medidas');//$this->Mgeneral->get_table('medidas');
      //$medidas['grupos'] = $this->Mgeneral->get_table('grupo');
      $medidas['marcas'] = $this->Mgeneral->get_result('status',0,'marca');//$this->Mgeneral->get_table('marca');
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('productos/alta', $medidas, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }

    public function guarda_edicion(){

      if($this->input->post()){
        $textos = $this->input->post();
        $this->Mgeneral->save_editar_formulario($textos,'productoId','productos');
        echo "editado";
      }
    }

    public function add_unidad(){

        $data['unidad'] = $this->input->post('unidad');
        $data['tipo_unidad'] = $this->input->post('tipo_unidad');
        $data['id_producto'] = $this->input->post('id_producto');
        $data['fecha_creacion'] = date("Y-m-d H:i:s");

        $aux = $this->Mgeneral->get_unidades_productos1($this->input->post('id_producto'), $this->input->post('unidad'),$this->input->post('tipo_unidad'));

        if(is_object($aux)){

        }else{

          $this->Mgeneral->save_register('productos_unidades', $data);

        

        }




        $unidades = $this->Mgeneral->get_unidades_productos2($this->input->post('id_producto'),$this->input->post('tipo_unidad'));

        $html = '<ul class="list-group">';
        foreach($unidades as $uni):
          if($uni->tipo_unidad == $this->input->post('tipo_unidad')):
          $html .='<li class="list-group-item" id="borrar_'.$uni->id.'">'.nombre_medida($uni->unidad).'<a href="'.base_url().'index.php/productos/eliminar_unidad_producto/'.$uni->id.'" flag="'.$uni->id.'" id="delete'.$uni->id.'" class="delete_uni" title="Eliminar"><i class="fa fa-trash" aria-hidden="true" ></i></a></li> ';
          endif;
        endforeach;
        $html .='</ul>';

        echo $html;

       
    }

    public function load_unidades($id_producto,$id_tipo){
      $unidades = $this->Mgeneral->get_unidades_productos2($id_producto, $id_tipo);

        $html = '<ul class="list-group">';
        foreach($unidades as $uni):
          if($uni->tipo_unidad == $id_tipo):
          $html .='<li class="list-group-item">'.nombre_medida($uni->unidad).'<a href="'.base_url().'index.php/productos/eliminar_unidad_producto/'.$uni->id.'" flag="'.$uni->id.'" id="delete'.$uni->id.'" class="delete_uni" title="Eliminar"><i class="fa fa-trash" aria-hidden="true" ></i></a></li> ';
          endif;
        endforeach;
        $html .='</ul>';

        echo $html;
    }

    public function eliminar_unidad_producto($id){
      $data['status'] = 1;
      $this->Mgeneral->update_table_row('productos_unidades',$data,'id',$id);
    }


    /**/

    public function guarda(){
      /*$this->form_validation->set_rules('productoNombre', 'productoNombre', 'required');
      $this->form_validation->set_rules('productoMedida', 'productoMedida', 'required');
      $this->form_validation->set_rules('productoDescripcion', 'productoDescripcion', 'required');
      */

      $this->form_validation->set_rules('productoNombre', 'Nombre', 'required');
      //$this->form_validation->set_rules('productoReferencia', 'productoReferencia', 'required');
      //$this->form_validation->set_rules('productoMedida', 'Medida', 'required');
      //$this->form_validation->set_rules('productoClasificado', 'productoClasificado', 'required');
      /*$this->form_validation->set_rules('productoModelo', 'productoModelo', 'required');
      $this->form_validation->set_rules('productoIdMarca', 'productoIdMarca', 'required');
      //$this->form_validation->set_rules('productoFlete', 'productoFlete', 'required');
      //$this->form_validation->set_rules('productoTipo', 'productoTipo', 'required');
      $this->form_validation->set_rules('procuctoContenido', 'procuctoContenido', 'required');
      $this->form_validation->set_rules('productoIngredienteActivo', 'productoIngredienteActivo', 'required');
      
      $this->form_validation->set_rules('productoRegEpa', 'productoRegEpa', 'required');
      $this->form_validation->set_rules('productoPresantacion', 'productoPresantacion', 'required');
      $this->form_validation->set_rules('productoDosisAlta', 'productoDosisAlta', 'required');
      $this->form_validation->set_rules('productoDosisBaja', 'productoDosisBaja', 'required');
      $this->form_validation->set_rules('productoMedidaAlta', 'productoMedidaAlta', 'required');
      $this->form_validation->set_rules('productoMedidaBaja', 'productoMedidaBaja', 'required');
      $this->form_validation->set_rules('productoFichaTecnica', 'productoFichaTecnica', 'required');
      $this->form_validation->set_rules('productoEspecificaciones', 'productoEspecificaciones', 'required');
      $this->form_validation->set_rules('productoPrecioPublico', 'productoPrecioPublico', 'required');
      $this->form_validation->set_rules('productoPrecioMayoreo', 'productoPrecioMayoreo', 'required');
      $this->form_validation->set_rules('productoPrecioCredito', 'productoPrecioCredito', 'required');
      $this->form_validation->set_rules('poductoInventariar', 'poductoInventariar', 'required');
      $this->form_validation->set_rules('productoGrupo', 'productoGrupo', 'required');
      $this->form_validation->set_rules('productoCantidadMinima', 'productoCantidadMinima', 'required');
      $this->form_validation->set_rules('productoCantidadMaxima', 'productoCantidadMaxima', 'required');
      */
      $auxPlagucida = $this->input->post('productoNormalPlaguecida');
      if($auxPlagucida == 2){
        $this->form_validation->set_rules('productoRegCofepris', 'REG. COFEPRIS', 'required');

      }
      
      $this->form_validation->set_rules('clave_sat', 'clave_sat', 'required');
      $this->form_validation->set_rules('unidad_sat', 'unidad_sat', 'required');

      $response = validate($this);


      if($response['status']){
        /*$data['productoNombre'] = $this->input->post('productoNombre');
        $data['productoMedida'] = $this->input->post('productoMedida');
        $data['productoDescripcion'] = $this->input->post('productoDescripcion');
        */

        $data['productoNombre'] = $this->input->post('productoNombre');
        $data['productoReferencia'] = $this->input->post('productoReferencia');
        $data['productoMedida'] = $this->input->post('productoMedida');
        $data['productoClasificado'] = $this->input->post('productoClasificado');
        $data['productoModelo'] = $this->input->post('productoModelo');
        $data['productoIdMarca'] = $this->input->post('productoIdMarca');
        //$data['productoFlete'] = $this->input->post('productoFlete');
        //$data['productoTipo'] = $this->input->post('productoTipo');
        $data['procuctoContenido'] = $this->input->post('procuctoContenido');
        $data['productoIngredienteActivo'] = "";//$this->input->post('productoIngredienteActivo');
        $data['productoRegCofepris'] = "";//$this->input->post('productoRegCofepris');
        $data['productoRegEpa'] = "";//$this->input->post('productoRegEpa');
        $data['productoPresantacion'] = "0";//$this->input->post('productoPresantacion');
        $data['productoDosisAlta'] =  "0";//$this->input->post('productoDosisAlta');
        $data['productoDosisBaja'] =  "0";//$this->input->post('productoDosisBaja');
        $data['productoMedidaAlta'] =  "0";//$this->input->post('productoMedidaAlta');
        $data['productoMedidaBaja'] =  "0";//$this->input->post('productoMedidaBaja');
        $data['productoFichaTecnica'] = $this->input->post('productoFichaTecnica');
        $data['productoEspecificaciones'] = $this->input->post('productoEspecificaciones');
        $data['productoPrecioPublico'] = $this->input->post('productoPrecioPublico');
        $data['productoPrecioMayoreo'] = 0;//$this->input->post('productoPrecioMayoreo');
        $data['productoPrecioCredito'] = 0;//$this->input->post('productoPrecioCredito');
        $data['poductoInventariar'] = "SI";//$this->input->post('poductoInventariar');
        $data['productoGrupo'] = $this->input->post('productoGrupo');
        $data['productoCantidadMinima'] = 0;//$this->input->post('productoCantidadMinima');
        $data['productoCantidadMaxima'] = 0;//$this->input->post('productoCantidadMaxima');
        $data['productoCantidadMayoreo'] = 0;//$this->input->post('productoCantidadMayoreo');
        $data['productoLote'] = $this->input->post('productoLote');
        $data['ieps'] = "0";//$this->input->post('ieps');

        $data['unidad_compra'] = "0";//$this->input->post('unidad_compra');
        $data['unidad_inventario'] = "0";//$this->input->post('unidad_inventario');
        $data['unidad_venta'] = "0";//$this->input->post('unidad_venta');
        $data['unidad_consumo'] = "0";//$this->input->post('unidad_consumo');

        $data['clave_sat'] = $this->input->post('clave_sat');
        $data['unidad_sat'] = $this->input->post('unidad_sat');

        $data['reorden'] = "0";//$this->input->post('reorden');
        $data['id_proveedor'] = 0;//$this->input->post('proveedor');

        $this->Mgeneral->save_register('productos', $data);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
    }


    public function ver_productos(){
      $data['medidas'] = $this->Mgeneral->get_result('status',0,'medidas');
      $data['rows'] = $this->Mgeneral->get_result('status',0,'productos');//$this->Mgeneral->get_table('productos');
      $data['proveedores'] = $this->Mgeneral->get_table('proveedores');


      
      $data['grupos'] = $this->Mgeneral->get_table('grupo');
      $data['marcas'] = $this->Mgeneral->get_table('marca');


      $titulo['titulo'] = "Lista de Productos" ;
      $titulo['titulo_dos'] = "Lista de Productos" ;
      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('productos/ver_productos', $data, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                          'statics/js/general.js',
                                        'statics/bootstrap4/js/bootstrap.min.js',
                                        'statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                           'statics/tema/assets/js/popper.min.js',
                                           'statics/tema/assets/js/plugins.js',
                                           'statics/tema/assets/js/main.js',
                                           'statics/tema/assets/js/lib/data-table/datatables.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
                                           'statics/tema/assets/js/lib/data-table/jszip.min.js',
                                           'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
                                           'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
                                           'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
                                           'statics/tema/assets/js/lib/data-table/datatables-init.js')));
    }

    public function ver($id){
      $titulo['titulo'] = "Productos" ;
      $titulo['titulo_dos'] = "Ver Productos" ;
      $medidas['medidas'] = $this->Mgeneral->get_result('status',0,'medidas');//$this->Mgeneral->get_table('medidas');
      $medidas['grupos'] = $this->Mgeneral->get_result('status',0,'grupo');//$this->Mgeneral->get_table('grupo');
      $medidas['marcas'] = $this->Mgeneral->get_result('status',0,'marca');//$this->Mgeneral->get_table('marca');
      $medidas['row_producto'] = $this->Mgeneral->get_row('productoId',$id,'productos');

      $medidas['proveedores'] = $this->Mgeneral->get_table('proveedores');



      $header = $this->load->view('main/header', '', TRUE);
      $menu = $this->load->view('main/menu', '', TRUE);
      $header_dos = "";//$this->load->view('main/header_dos', '', TRUE);
      $titulo = $this->load->view('main/titulo', $titulo, TRUE);
      $contenido = $this->load->view('productos/ver', $medidas, TRUE);
      $footer = $this->load->view('main/footer', '', TRUE);
      $this->load->view('main/main', array('header'=>$header,
                                           'menu'=>$menu,
                                           'header_dos'=>$header_dos,
                                           'titulo'=>$titulo,
                                           'contenido'=>$contenido,
                                           'footer'=>$footer,
                                           'included_js'=>array('statics/js/bootbox.min.js',
                                           'statics/js/general.js',
                                          'statics/bootstrap4/js/bootstrap.min.js')));
    }

    public function editar_producto($id_producto){
      /*$this->form_validation->set_rules('productoNombre', 'productoNombre', 'required');
      $this->form_validation->set_rules('productoMedida', 'productoMedida', 'required');
      $this->form_validation->set_rules('productoDescripcion', 'productoDescripcion', 'required');
      */

      $this->form_validation->set_rules('productoNombre', 'productoNombre', 'required');
      /*$this->form_validation->set_rules('productoReferencia', 'productoReferencia', 'required');
      $this->form_validation->set_rules('productoMedida', 'productoMedida', 'required');
      $this->form_validation->set_rules('productoClasificado', 'productoClasificado', 'required');
      $this->form_validation->set_rules('productoModelo', 'productoModelo', 'required');
      $this->form_validation->set_rules('productoIdMarca', 'productoIdMarca', 'required');
      //$this->form_validation->set_rules('productoFlete', 'productoFlete', 'required');
      //$this->form_validation->set_rules('productoTipo', 'productoTipo', 'required');
      $this->form_validation->set_rules('procuctoContenido', 'procuctoContenido', 'required');
      $this->form_validation->set_rules('productoIngredienteActivo', 'productoIngredienteActivo', 'required');
      $this->form_validation->set_rules('productoRegCofepris', 'productoRegCofepris', 'required');
      $this->form_validation->set_rules('productoRegEpa', 'productoRegEpa', 'required');
      $this->form_validation->set_rules('productoPresantacion', 'productoPresantacion', 'required');
      $this->form_validation->set_rules('productoDosisAlta', 'productoDosisAlta', 'required');
      $this->form_validation->set_rules('productoDosisBaja', 'productoDosisBaja', 'required');
      $this->form_validation->set_rules('productoMedidaAlta', 'productoMedidaAlta', 'required');
      $this->form_validation->set_rules('productoMedidaBaja', 'productoMedidaBaja', 'required');
      $this->form_validation->set_rules('productoFichaTecnica', 'productoFichaTecnica', 'required');
      $this->form_validation->set_rules('productoEspecificaciones', 'productoEspecificaciones', 'required');
      $this->form_validation->set_rules('productoPrecioPublico', 'productoPrecioPublico', 'required');
      $this->form_validation->set_rules('productoPrecioMayoreo', 'productoPrecioMayoreo', 'required');
      $this->form_validation->set_rules('productoPrecioCredito', 'productoPrecioCredito', 'required');
      $this->form_validation->set_rules('poductoInventariar', 'poductoInventariar', 'required');
      $this->form_validation->set_rules('productoGrupo', 'productoGrupo', 'required');
      $this->form_validation->set_rules('productoCantidadMinima', 'productoCantidadMinima', 'required');
      $this->form_validation->set_rules('productoCantidadMaxima', 'productoCantidadMaxima', 'required');
      */

      $response = validate($this);


      if($response['status']){
        /*$data['productoNombre'] = $this->input->post('productoNombre');
        $data['productoMedida'] = $this->input->post('productoMedida');
        $data['productoDescripcion'] = $this->input->post('productoDescripcion');
        */

        $data['productoNombre'] = $this->input->post('productoNombre');
        $data['productoReferencia'] = $this->input->post('productoReferencia');
        $data['productoMedida'] = $this->input->post('productoMedida');
        $data['productoClasificado'] = $this->input->post('productoClasificado');
        $data['productoModelo'] = $this->input->post('productoModelo');
        $data['productoIdMarca'] = $this->input->post('productoIdMarca');
        //$data['productoFlete'] = $this->input->post('productoFlete');
        //$data['productoTipo'] = $this->input->post('productoTipo');
        $data['procuctoContenido'] = $this->input->post('procuctoContenido');
        $data['productoIngredienteActivo'] = "";//$this->input->post('productoIngredienteActivo');
        $data['productoRegCofepris'] = "";//$this->input->post('productoRegCofepris');
        $data['productoRegEpa'] = "";//$this->input->post('productoRegEpa');
        $data['productoPresantacion'] = "0";//$this->input->post('productoPresantacion');
        $data['productoDosisAlta'] = "0";// $this->input->post('productoDosisAlta');
        $data['productoDosisBaja'] =  "0";//$this->input->post('productoDosisBaja');
        $data['productoMedidaAlta'] =  "0";//$this->input->post('productoMedidaAlta');
        $data['productoMedidaBaja'] =  "0";//$this->input->post('productoMedidaBaja');
        $data['productoFichaTecnica'] = $this->input->post('productoFichaTecnica');
        $data['productoEspecificaciones'] = $this->input->post('productoEspecificaciones');
        $data['productoPrecioPublico'] = $this->input->post('productoPrecioPublico');
        $data['productoPrecioMayoreo'] = "0";//$this->input->post('productoPrecioMayoreo');
        $data['productoPrecioCredito'] = "0";//$this->input->post('productoPrecioCredito');
        $data['poductoInventariar'] = "SI";//$this->input->post('poductoInventariar');
        $data['productoGrupo'] = $this->input->post('productoGrupo');
        $data['productoCantidadMinima'] = "0";//$this->input->post('productoCantidadMinima');
        $data['productoCantidadMaxima'] = "0";//$this->input->post('productoCantidadMaxima');
        $data['productoCantidadMayoreo'] = "0";//$this->input->post('productoCantidadMayoreo');
        $data['productoLote'] = $this->input->post('productoLote');
        $data['ieps'] = "0";//$this->input->post('ieps');

        $data['unidad_compra'] = "0";//$this->input->post('unidad_compra');
        $data['unidad_inventario'] = "0";//$this->input->post('unidad_inventario');
        $data['unidad_venta'] = "0";//$this->input->post('unidad_venta');
        $data['unidad_consumo'] = "0";//$this->input->post('unidad_consumo');

        $data['clave_sat'] = $this->input->post('clave_sat');
        $data['unidad_sat'] = $this->input->post('unidad_sat');

        $data['reorden'] = "0";//$this->input->post('reorden');
        $data['id_proveedor'] = 0;//$this->input->post('proveedor');

        $this->Mgeneral->update_table_row('productos',$data,'productoId',$id_producto);
      }
    //  echo $response;
    echo json_encode(array('output' => $response));
    }

    public function leer_xml(){

      $xml = simplexml_load_file(base_url().'statics/facturas/facturas_xml/45BB2E13B3BBC0113E74A9BDB6CBAB1D.xml'); 
      $ns = $xml->getNamespaces(true);
      $xml->registerXPathNamespace('c', $ns['cfdi']);
      $xml->registerXPathNamespace('t', $ns['tfd']);
       
       
      //EMPIEZO A LEER LA INFORMACION DEL CFDI E IMPRIMIRLA 
      foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante){ 
            echo $cfdiComprobante['Version']; 
            echo "<br />"; 
            echo $cfdiComprobante['Fecha']; 
            echo "<br />"; 
            echo $cfdiComprobante['Sello']; 
            echo "<br />"; 
            echo $cfdiComprobante['Total']; 
            echo "<br />"; 
            echo $cfdiComprobante['SubTotal']; 
            echo "<br />"; 
            echo $cfdiComprobante['Certificado']; 
            echo "<br />"; 
            echo $cfdiComprobante['FormaPago']; 
            echo "<br />"; 
            echo $cfdiComprobante['NoCertificado']; 
            echo "<br />"; 
            echo $cfdiComprobante['TipoDeComprobante']; 
            echo "<br />"; 
      } 
      foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){ 
         echo $Emisor['Rfc']; 
         echo "<br />"; 
         echo $Emisor['Nombre']; 
         echo "<br />"; 
      } 
      foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal') as $DomicilioFiscal){ 
         echo $DomicilioFiscal['Pais']; 
         echo "<br />"; 
         echo $DomicilioFiscal['Calle']; 
         echo "<br />"; 
         echo $DomicilioFiscal['Cstado']; 
         echo "<br />"; 
         echo $DomicilioFiscal['Colonia']; 
         echo "<br />"; 
         echo $DomicilioFiscal['Municipio']; 
         echo "<br />"; 
         echo $DomicilioFiscal['NoExterior']; 
         echo "<br />"; 
         echo $DomicilioFiscal['CodigoPostal']; 
         echo "<br />"; 
      } 
      foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:ExpedidoEn') as $ExpedidoEn){ 
         echo $ExpedidoEn['Pais']; 
         echo "<br />"; 
         echo $ExpedidoEn['Calle']; 
         echo "<br />"; 
         echo $ExpedidoEn['Estado']; 
         echo "<br />"; 
         echo $ExpedidoEn['Colonia']; 
         echo "<br />"; 
         echo $ExpedidoEn['NoExterior']; 
         echo "<br />"; 
         echo $ExpedidoEn['CodigoPostal']; 
         echo "<br />"; 
      } 
      foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor){ 
         echo $Receptor['rfc']; 
         echo "<br />"; 
         echo $Receptor['Nombre']; 
         echo "<br />"; 
      } 
      foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio') as $ReceptorDomicilio){ 
         echo $ReceptorDomicilio['Pais']; 
         echo "<br />"; 
         echo $ReceptorDomicilio['Calle']; 
         echo "<br />"; 
         echo $ReceptorDomicilio['Estado']; 
         echo "<br />"; 
         echo $ReceptorDomicilio['Colonia']; 
         echo "<br />"; 
         echo $ReceptorDomicilio['Municipio']; 
         echo "<br />"; 
         echo $ReceptorDomicilio['NoExterior']; 
         echo "<br />"; 
         echo $ReceptorDomicilio['NoInterior']; 
         echo "<br />"; 
         echo $ReceptorDomicilio['CodigoPostal']; 
         echo "<br />"; 
      } 
      foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto){ 
         echo "<br />"; 
         echo $Concepto['Unidad']; 
         echo "<br />"; 
         echo $Concepto['Importe']; 
         echo "<br />"; 
         echo $Concepto['Cantidad']; 
         echo "<br />"; 
         echo $Concepto['Descripcion']; 
         echo "<br />"; 
         echo $Concepto['ValorUnitario']; 
         echo "<br />";   
         echo "<br />"; 
      } 
      foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado') as $Traslado){ 
         echo $Traslado['Tasa']; 
         echo "<br />"; 
         echo $Traslado['Importe']; 
         echo "<br />"; 
         echo $Traslado['Impuesto']; 
         echo "<br />";   
         echo "<br />"; 
      } 
       
      //ESTA ULTIMA PARTE ES LA QUE GENERABA EL ERROR
      foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
         echo $tfd['SelloCFD']; 
         echo "<br />"; 
         echo $tfd['FechaTimbrado']; 
         echo "<br />"; 
         echo $tfd['UUID']; 
         echo "<br />"; 
         echo $tfd['NoCertificadoSAT']; 
         echo "<br />"; 
         echo $tfd['Version']; 
         echo "<br />"; 
         echo $tfd['SelloSAT']; 
      } 
    }


    
    public function eliminar($id_plaga){
      $data['status'] = 1;
      $this->Mgeneral->update_table_row('productos',$data,'productoId',$id_plaga);
  
    }

}
