<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
}
input{
  font-family: 'Roboto', sans-serif;
}
strong{
  font-family: 'Roboto', sans-serif;
}
</style>
<script>
    menu_activo = "productos";
$("#menu_productos_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/productos/guarda";
  ajaxJson(url,{"productoNombre":$("#productoNombre").val(),
                "productoReferencia":$("#productoReferencia").val(),
                "productoMedida":$("#productoMedida").val(),
                "productoClasificado":$("#productoClasificado").val(),
                "productoModelo":$("#productoModelo").val(),
                "productoIdMarca":$("#productoIdMarca").val(),
                "productoFlete":$("#productoFlete").val(),
                "productoTipo":$("#productoTipo").val(),
                "procuctoContenido":$("#procuctoContenido").val(),
                "productoIngredienteActivo":$("#productoIngredienteActivo").val(),
                "productoRegCofepris":$("#productoRegCofepris").val(),
                "productoRegEpa":$("#productoRegEpa").val(),
                "productoPresantacion":$("#productoPresantacion").val(),
                "productoDosisAlta":$("#productoDosisAlta").val(),
                "productoDosisBaja":$("#productoDosisBaja").val(),
                "productoMedidaAlta":$("#productoMedidaAlta").val(),
                "productoMedidaBaja":$("#productoMedidaBaja").val(),
                "productoFichaTecnica":$("#productoFichaTecnica").val(),
                "productoEspecificaciones":$("#productoEspecificaciones").val(),
                "productoPrecioPublico":$("#productoPrecioPublico").val(),
                "productoPrecioMayoreo":$("#productoPrecioMayoreo").val(),
                "productoPrecioCredito":$("#productoPrecioCredito").val(),
                "poductoInventariar":$("#poductoInventariar").val(),
                "productoGrupo":$("#productoGrupo").val(),
                "productoCantidadMinima":$("#productoCantidadMinima").val(),
                "productoCantidadMaxima":$("#productoCantidadMaxima").val(),
                "productoCantidadMayoreo":$("#productoCantidadMayoreo").val(),
                "clave_sat":$("#clave_sat").val(),
                "unidad_sat":$("#unidad_sat").val(),
              "productoNormalPlaguecida":$("#productoNormalPlaguecida").val(),
                "productoLote":$("#productoLote").val(),
                "ieps":$("#ieps").val(),
              "unidad_compra":$("#unidad_compra").val(),
                "unidad_inventario":$("#unidad_inventario").val(),
                "unidad_venta":$("#unidad_venta").val(),
                "unidad_consumo":$("#unidad_consumo").val()
              },
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect("GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/productos/ver_productos");
    }


  });
});



});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <!--div class="card-header">
            <strong class="card-title">Alta de productos</strong>
        </div-->
        <div class="card-body">


          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">
                <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                    <!--INICIO PRIMERA SECCION-->
                    <div class="row">
                        <div class="col-3">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                <input id="productoNombre" name="productoNombre" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Nombre" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Referencia</label>
                                <input id="productoReferencia" name="productoReferencia" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Referencia" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>

                        <div class="col-1">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Tipo</label>
                              <select name="productoNormalPlaguecida" id="productoNormalPlaguecida" class="form-control form-control-sm">
                                <option value="1">Normal</option>
                                <option value="2">Plaguicida</option>
                            </select>
                          </div>
                        </div>

                        <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Clasificado</label>
                            <select name="productoClasificado" id="productoClasificado" class="form-control form-control-sm">
                              <option value="1">Producto</option>
                              <option value="2">Servicio</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group" >
                              <label for="cc-exp" class="control-label mb-1">Modelo</label>
                              <input id="productoModelo" name="productoModelo" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Modelo" autocomplete="off">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-1">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Grupo</label>
                            <select name="productoGrupo" id="productoGrupo" class="form-control form-control-sm">
                              <?php foreach($grupos as $row):?>
                              <option value="<?php echo $row->grupoId;?>"><?php echo $row->grupoNombre?></option>
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        
                    </div>


                    <div class="row">
                      <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Marca</label>
                            <select name="productoIdMarca" id="productoIdMarca" class="form-control form-control-sm">
                              <?php foreach($marcas as $row):?>
                              <option value="<?php echo $row->marcaId;?>"><?php echo $row->marcaNombre?></option>
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Contenido neto</label>
                                <input id="procuctoContenido" name="procuctoContenido" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:55px;">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Medida</label>
                            <select name="productoMedida" id="productoMedida" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                              <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Cantidad minima</label>
                              <input id="productoCantidadMinima" name="productoCantidadMinima" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:55px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Cantidad maxima</label>
                              <input id="productoCantidadMaxima" name="productoCantidadMaxima" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:55px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-2">
                        <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Inventariar</label>
                            
                            <select class="form-control form-control-sm" name="poductoInventariar" id="poductoInventariar" style="width:55px;">
                              <option value="SI">SI</option>
                              <option value="NO">NO</option>
                            </select>
                        </div>
                      </div>
                    </div>

                    

                    

                    <div class="row">
                      
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio público</label>
                                <input id="productoPrecioPublico" name="productoPrecioPublico" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Precio público" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio mayoreo</label>
                                <input id="productoPrecioMayoreo" name="productoPrecioMayoreo" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Precio mayoreo" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Cantidad mayoreo</label>
                                <input id="productoCantidadMayoreo" name="productoCantidadMayoreo" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Cantidad mayoreo" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio credito</label>
                                <input id="productoPrecioCredito" name="productoPrecioCredito" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Precio credito" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Lote</label>
                                <input id="productoLote" name="productoLote" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Lote" autocomplete="off" style="width:85px;">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>

                    </div>
                    <hr/>

                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                          <label for="cc-exp" class="control-label mb-1 ">Clave de producto SAT</label>
                          <select class="form-control alto form-control-sm" id="clave_sat" name="clave_sat">
                              <option value="10191506">10191506-Mata roedores</option>
                              <option value="10191509">10191509-Plaguisidas PRIRENAT</option>
                              <option value="10191703">10191703-Insectos voladores trampas PRIRENAT</option>
                              <option value="10191701">10191701-Trampas para control de animal</option>
                              <option value="10191706">10191706-Cebos, T-REX</option>
                              <option value="10191700">10191700-dispositivos para control de plagas</option>
                              <option value="72102100">72102100-Control de plagas</option>
                              <option value="14111518">14111518-Letreros para estación</option>
                              <option value="10191703">10191703-Lampara de luz 15 wats</option>
                              <option value="72101507">72101507-Servicio de reparacipon de tablaroca contra goteras y reparación de piso</option>
                              <option value="72101507">72101507-Mantenimiento y arreglo de filtración por puerta y ventana de agua</option>
                          </select>
                        </div>
                      </div>
                        <div class="col-4">
                            <div class="form-group" >
                              <label for="cc-exp" class="control-label mb-1">Unidad SAT</label>
                              <select class="form-control alto form-control-sm" id="unidad_sat" name="unidad_sat">
                                <option value="H87">H87-Pieza(Múltiplos / Fracciones / Decimales)</option>
                                <option value="EA">EA-Elemento(Unidades de venta)</option>
                                <option value="E48">E48-Unidad de Servicio(Unidades específicas de la industria (varias))</option>
                                <option value="ACT">ACT-Actividad(Unidades de venta)</option>
                                <option value="KGM">KGM-Kilogramo(Mecánica)</option>
                                <option value="E51">E51-Trabajo(Unidades específicas de la industria (varias))</option>
                                <option value="A9">A9-Tarifa(Diversos)</option>
                                <option value="MTR">MTR-Metro(Tiempo y Espacio)</option>
                                <option value="AB">AB-Paquete a granel(Diversos)</option>
                                <option value="BB">BB-Caja base(Unidades específicas de la industria (varias))</option>
                                <option value="KT">KT-Kit(Unidades de venta)</option>
                                <option value="SET">SET-Conjunto(Unidades de venta)</option>
                                <option value="LTR">LTR-Litro(Tiempo y Espacio)</option>
                                <option value="XBX">XBX-Caja(Unidades de empaque)</option>
                                <option value="MON">MON-Mes(Tiempo y Espacio)</option>
                                <option value="DAY">DAY-Día(Tiempo y Espacio)</option>
                                <option value="HUR">HUR-Hora(Tiempo y Espacio)</option>
                                <option value="MTK">MTK-Metro cuadrado(Tiempo y Espacio)</option>
                                <option value="11">11-Equipos(Diversos)</option>
                                <option value="MGM">MGM-Miligramo(Mecánica)</option>
                                <option value="XPK">XPK-Paquete(Unidades de empaque)</option>
                                <option value="XKI">XKI-Kit(Conjunto de piezas)(Unidades de empaque)</option>
                                <option value="AS">AS-Variedad(Diversos)</option>
                                <option value="GRM">GRM-Gramo(Mecánica)</option>
                                <option value="PR">PR-Par(Números enteros / Números / Ratios)</option>
                                <option value="DPC">DPC-Docenas de piezas(Unidades de venta)</option>
                                <option value="xun">xun-Unidad(Unidades de empaque)</option>
                                <option value="XLT">XLT-Lote(Unidades de empaque)</option>
                                <option value="10">10-Grupos(Diversos)</option>
                                <option value="MLT">MLT-Mililitro(Tiempo y Espacio)</option>
                                <option value="E54">E54-Viaje(Unidades específicas de la industria (varias))</option>
                              </select>
                            </div>
                        </div>

                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">IEPS</label>
                              <input id="ieps" name="ieps" type="text" class="form-control form-control-sm cc-exp" value="0.0" placeholder="IEPS" autocomplete="off" style="width:85px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                    </div>

                  
                  <!--FIN PRIMERA SECCION-->
                  
                    <!--INICIO SEGUNDA SECCION-->

                    <hr/>
                    <div class="row">
                      <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">REG. COFEPRIS</label>
                              <input id="productoRegCofepris" name="productoRegCofepris" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="REG. COFEPRIS" autocomplete="off">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">INGREDIENTE ACTIVO</label>
                              <input id="productoIngredienteActivo" name="productoIngredienteActivo" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="INGREDIENTE ACTIVO" autocomplete="off">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">REG. EPA</label>
                              <input id="productoRegEpa" name="productoRegEpa" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="REG. EPA" autocomplete="off">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                    </div>

                    <div class="row">
                        
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">PRESENTACIÓN</label>
                              <input id="productoPresantacion" name="productoPresantacion" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:45px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">DOSIS ALTA</label>
                              <input id="productoDosisAlta" name="productoDosisAlta" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:45px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">MEDIDA ALTA</label>
                              <input id="productoMedidaAlta" name="productoMedidaAlta" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:45px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">DOSIS BAJA</label>
                              <input id="productoDosisBaja" name="productoDosisBaja" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:45px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">MEDIDA BAJA</label>
                              <input id="productoMedidaBaja" name="productoMedidaBaja" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:45px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                    </div>


                    

                   <hr/>

                    <div class="row">
                      <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de compra</label>
                              <select name="unidad_compra" id="unidad_compra" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_compra == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de inventario</label>
                              <select name="unidad_inventario" id="unidad_inventario" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_inventario == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de venta</label>
                              <select name="unidad_venta" id="unidad_venta" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_venta == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de consumo</label>
                             <select name="unidad_consumo" id="unidad_consumo" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_consumo == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        
                    </div>


                    <hr/>

                    <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Ficha tecnica</label>
                              <textarea class="form-control form-control-sm" name="productoFichaTecnica" id="productoFichaTecnica" rows="4" cols="50" placeholder="Ficha tecnica"></textarea>
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Especificaciones</label>
                              <textarea class="form-control form-control-sm" name="productoEspecificaciones" id="productoEspecificaciones" rows="4" cols="50" placeholder="Especificaciones"></textarea>
                          </div>
                        </div>
                    </div>

                    
                    <br/>
                    <br/>

                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
