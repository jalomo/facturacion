<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
}
input{
  font-family: 'Roboto', sans-serif;
}
</style>
<script>
     menu_activo = "productos";
$("#menu_productos_lista").last().addClass("menu_estilo");
$(document).ready(function(){
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/productos/editar_producto/<?php echo $row_producto->productoId;?>";
  ajaxJson(url,{"productoNombre":$("#productoNombre").val(),
                "productoReferencia":$("#productoReferencia").val(),
                "productoMedida":$("#productoMedida").val(),
                //"productoClasificado":$("#productoClasificado").val(),
                //"productoModelo":$("#productoModelo").val(),
                "productoIdMarca":$("#productoIdMarca").val(),
                "productoFlete":$("#productoFlete").val(),
                "productoTipo":$("#productoTipo").val(),
                "procuctoContenido":$("#procuctoContenido").val(),
                "productoIngredienteActivo":$("#productoIngredienteActivo").val(),
                "productoRegCofepris":$("#productoRegCofepris").val(),
                "productoRegEpa":$("#productoRegEpa").val(),
                "productoPresantacion":$("#productoPresantacion").val(),
                "productoDosisAlta":$("#productoDosisAlta").val(),
                "productoDosisBaja":$("#productoDosisBaja").val(),
                "productoMedidaAlta":$("#productoMedidaAlta").val(),
                "productoMedidaBaja":$("#productoMedidaBaja").val(),
                "productoFichaTecnica":$("#productoFichaTecnica").val(),
                "productoEspecificaciones":$("#productoEspecificaciones").val(),
                "productoPrecioPublico":$("#productoPrecioPublico").val(),
                "productoPrecioMayoreo":$("#productoPrecioMayoreo").val(),
                "productoPrecioCredito":$("#productoPrecioCredito").val(),
                "poductoInventariar":$("#poductoInventariar").val(),
                "productoGrupo":$("#productoGrupo").val(),
                "productoCantidadMinima":$("#productoCantidadMinima").val(),
                "productoCantidadMaxima":$("#productoCantidadMaxima").val(),
                "productoCantidadMayoreo":$("#productoCantidadMayoreo").val(),
                "productoLote":$("#productoLote").val(),
                "unidad_compra":$("#unidad_compra").val(),
                "unidad_inventario":$("#unidad_inventario").val(),
                "unidad_venta":$("#unidad_venta").val(),
                "clave_sat":$("#clave_sat").val(),
                "unidad_sat":$("#unidad_sat").val(),
                "unidad_consumo":$("#unidad_consumo").val(),
                "reorden":$("#reorden").val(),
                "proveedor":$("#proveedor").val(),
              "ieps":$("#ieps").val()},
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect("EDITADO CON EXITO","success","<?php echo base_url()?>index.php/productos/ver_productos");
    }


  });
});




$("#add_unidad_compra").click(function(){ 

  var url ="<?php echo base_url()?>index.php/productos/add_unidad";
  ajaxJson(url,{"unidad":$("#unidad_compra").val(),
                "tipo_unidad":1,
                "id_producto":"<?php echo $row_producto->productoId;?>"
                },
            "POST","",function(result){



              $("#load_u_compra").html("");
              $("#load_u_compra").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });

});


$("#add_unidad_inventario").click(function(){ 

  var url ="<?php echo base_url()?>index.php/productos/add_unidad";
  ajaxJson(url,{"unidad":$("#unidad_inventario").val(),
                "tipo_unidad":2,
                "id_producto":"<?php echo $row_producto->productoId;?>"
                },
            "POST","",function(result){



              $("#load_u_inventario").html("");
              $("#load_u_inventario").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });

});



$("#add_unidad_venta").click(function(){ 

  var url ="<?php echo base_url()?>index.php/productos/add_unidad";
  ajaxJson(url,{"unidad":$("#unidad_venta").val(),
                "tipo_unidad":3,
                "id_producto":"<?php echo $row_producto->productoId;?>"
                },
            "POST","",function(result){



              $("#load_u_venta").html("");
              $("#load_u_venta").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });

});



$("#add_unidad_consumo").click(function(){ 

  var url ="<?php echo base_url()?>index.php/productos/add_unidad";
  ajaxJson(url,{"unidad":$("#unidad_consumo").val(),
                "tipo_unidad":4,
                "id_producto":"<?php echo $row_producto->productoId;?>"
                },
            "POST","",function(result){



              $("#load_u_consumo").html("");
              $("#load_u_consumo").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });

});



var url ="<?php echo base_url()?>index.php/productos/load_unidades/<?php echo $row_producto->productoId;?>/1";
  ajaxJson(url,{},
            "POST","",function(result){

              $("#load_u_compra").html("");
              $("#load_u_compra").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });

var url ="<?php echo base_url()?>index.php/productos/load_unidades/<?php echo $row_producto->productoId;?>/2";
  ajaxJson(url,{},
            "POST","",function(result){

              $("#load_u_inventario").html("");
              $("#load_u_inventario").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });


  var url ="<?php echo base_url()?>index.php/productos/load_unidades/<?php echo $row_producto->productoId;?>/3";
  ajaxJson(url,{},
            "POST","",function(result){

              $("#load_u_venta").html("");
              $("#load_u_venta").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });


  var url ="<?php echo base_url()?>index.php/productos/load_unidades/<?php echo $row_producto->productoId;?>/4";
  ajaxJson(url,{},
            "POST","",function(result){

              $("#load_u_consumo").html("");
              $("#load_u_consumo").html(result);

              $(".delete_uni").click(function(event){
                     event.preventDefault();
                     bootbox.dialog({
                         message: "Desea eliminar la unidad?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {
                                   id = $(event.currentTarget).attr('flag');
                                   url = $("#delete"+id).attr('href');
                                   $("#borrar_"+id).slideUp();
                                   $.get(url);
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });
                 });

              

           });



});
</script>

<style type="text/css">
  
  .fa-trash {
  color: red;
}
</style>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Editar de productos</strong>
        </div>
        <div class="card-body">


          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">
                <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                    <!--INICIO PRIMERA SECCION-->
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                <input id="productoNombre" name="productoNombre" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoNombre;?>" placeholder="Nombre" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Referencia</label>
                              <input id="productoReferencia" name="productoReferencia" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoReferencia;?>" placeholder="Referencia" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                        <!--div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Clasificado</label>
                            <select name="productoClasificado" id="productoClasificado" class="form-control form-control-sm">
                              <?php if($row_producto->productoClasificado == 1){?>
                                <option value="1" selected>Producto</option>
                              <?php }else{?>
                                <option value="1" >Producto</option>
                              <?php }?>
                              <?php if($row_producto->productoClasificado == 2){?>
                                <option value="2" selected>Servicio</option>
                              <?php }else{?>
                                <option value="2">Servicio</option>
                              <?php }?>
                              
                              
                            </select>
                          </div>
                        </div-->
                        <!--div class="col-2">
                          <div class="form-group" >
                              <label for="cc-exp" class="control-label mb-1">Modelo</label>
                              <input id="productoModelo" name="productoModelo" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoModelo;?>" placeholder="Modelo" autocomplete="cc-exp">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div-->
                        <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Grupo</label>
                            <select name="productoGrupo" id="productoGrupo" class="form-control form-control-sm">
                              <?php foreach($grupos as $row):?>
                                <?php if($row_producto->productoModelo == $row->grupoId){?>
                                  <option value="<?php echo $row->grupoId;?>" selected><?php echo $row->grupoNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->grupoId;?>"><?php echo $row->grupoNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        
                    </div>


                    <div class="row">

                      <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Marca</label>
                            <select name="productoIdMarca" id="productoIdMarca" class="form-control form-control-sm">
                              <?php foreach($marcas as $row):?>
                                 <?php if($row_producto->productoIdMarca == $row->marcaId){?>
                                  <option value="<?php echo $row->marcaId;?>" selected><?php echo $row->marcaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->marcaId;?>"><?php echo $row->marcaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>


                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Contenido neto</label>
                                <input id="procuctoContenido" name="procuctoContenido" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->procuctoContenido;?>" placeholder="Contenido neto" autocomplete="cc-exp"  style="width:55px;">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Medida</label>
                            <select name="productoMedida" id="productoMedida" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->productoMedida == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>

                        <!--div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Cantidad minima</label>
                              <input id="productoCantidadMinima" name="productoCantidadMinima" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoCantidadMinima;?>" placeholder="Cantidad minima" autocomplete="cc-exp" style="width:55px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Cantidad maxima</label>
                              <input id="productoCantidadMaxima" name="productoCantidadMaxima" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoCantidadMaxima;?>" placeholder="Cantidad maxima" autocomplete="cc-exp" style="width:55px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>

                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Inventariar</label>
                              
                              <select class="form-control form-control-sm" name="poductoInventariar" id="poductoInventariar" style="width:55px;">
                                <?php if($row_producto->poductoInventariar == "SI"){?>
                                  <option value="SI" selected>SI</option>
                                <?php }else{?>
                                  <option value="SI">SI</option>
                                <?php }?>
                                <?php if($row_producto->poductoInventariar == "NO"){?>
                                  <option value="NO" selected>NO</option>
                                <?php }else{?>
                                  <option value="NO">NO</option>
                                <?php }?>
                                
                                
                              </select>
                          </div>
                      </div-->
                    </div>

                    <!--div class="row">
                      <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Reorden</label>
                              <input id="reorden" name="reorden" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->reorden;?>" placeholder="" autocomplete="off" style="width:55px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                      
                    </div-->

                    

                    <div class="row">
                      
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio público</label>
                                <input id="productoPrecioPublico" name="productoPrecioPublico" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoPrecioPublico;?>" placeholder="Precio público" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <!--div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio mayoreo</label>
                                <input id="productoPrecioMayoreo" name="productoPrecioMayoreo" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoPrecioMayoreo;?>" placeholder="Precio mayoreo" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Cantidad mayoreo</label>
                                <input id="productoCantidadMayoreo" name="productoCantidadMayoreo" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoCantidadMayoreo;?>" placeholder="Cantidad mayoreo" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio credito</label>
                                <input id="productoPrecioCredito" name="productoPrecioCredito" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoPrecioCredito;?>" placeholder="Precio credito" autocomplete="cc-exp">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>


                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Lote</label>
                                <input id="productoLote" name="productoLote" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->productoLote;?>" placeholder="Lote" autocomplete="off" style="width:85px;">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div-->

                    </div>

                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                          <label for="cc-exp" class="control-label mb-1 ">Clave de producto SAT</label>
                         
                         
                          <?php 
                          
                              $options = [
                                '80161500'  => '80161500-Servicios de apoyo gerencial',
                                '80'  => '80-Servicios de Gestión, Servicios Profesionales de Empresa y Servicios Administrativos',
                                '8016'  => '8016-Servicios de administración de empresas',
                                '80161600'  => '80161600-Supervisión de instalaciones de negocios',
                                '80161700'  => '80161700-Servicios de recuperación de activos',
                                '80161800'  => '80161800-Servicios de alquiler o arrendamiento de equipo de oficina',
                                '76111800'  => '76111800-Limpieza de vehículos de transporte',
                                '76'  => '76-Servicios de Limpieza, Descontaminación y Tratamiento de Residuos',
                                '7611'=>'7611-Servicios de aseo y limpieza'
                                ];
                              $js = [
                                'id'       => 'clave_sat',
                                'class' => 'form-control alto form-control-sm'
                                ];
                                echo form_dropdown('clave_sat', $options, $row_producto->clave_sat, $js);
                              ?>

                         
                        </div>
                      </div>
                        <div class="col-4">
                            <div class="form-group" >
                              <label for="cc-exp" class="control-label mb-1">Unidad SAT</label>

                              
                              <select class="form-control alto form-control-sm" id="unidad_sat" name="unidad_sat">
                                <?php if($row_producto->unidad_sat == "H87"):?>
                                   <option value="H87" selected>H87-Pieza(Múltiplos / Fracciones / Decimales)</option>
                                <?php else:?>
                                   <option value="H87">H87-Pieza(Múltiplos / Fracciones / Decimales)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "EA"):?>
                                   <option value="EA" selected>EA-Elemento(Unidades de venta)</option>
                                <?php else:?>
                                   <option value="EA">EA-Elemento(Unidades de venta)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "E48"):?>
                                   <option value="E48" selected>E48-Unidad de Servicio(Unidades específicas de la industria (varias))</option>
                                <?php else:?>
                                   <option value="E48">E48-Unidad de Servicio(Unidades específicas de la industria (varias))</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "ACT"):?>
                                   <option value="ACT" selected>ACT-Actividad(Unidades de venta)</option>
                                <?php else:?>
                                   <option value="ACT">ACT-Actividad(Unidades de venta)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "KGM"):?>
                                   <option value="KGM" selected>KGM-Kilogramo(Mecánica)</option>
                                <?php else:?>
                                   <option value="KGM">KGM-Kilogramo(Mecánica)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "E51"):?>
                                   <option value="E51" selected>E51-Trabajo(Unidades específicas de la industria (varias))</option>
                                <?php else:?>
                                   <option value="E51">E51-Trabajo(Unidades específicas de la industria (varias))</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "A9"):?>
                                   <option value="A9" selected>A9-Tarifa(Diversos)</option>
                                <?php else:?>
                                   <option value="A9">A9-Tarifa(Diversos)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "MTR"):?>
                                   <option value="MTR" selected>MTR-Metro(Tiempo y Espacio)</option>
                                <?php else:?>
                                   <option value="MTR">MTR-Metro(Tiempo y Espacio)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "AB"):?>
                                   <option value="AB" selected>AB-Paquete a granel(Diversos)</option>
                                <?php else:?>
                                   <option value="AB">AB-Paquete a granel(Diversos)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "BB"):?>
                                   <option value="BB" selected>BB-Caja base(Unidades específicas de la industria (varias))</option>
                                <?php else:?>
                                  <option value="BB">BB-Caja base(Unidades específicas de la industria (varias))</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "KT"):?>
                                   <option value="KT" selected>KT-Kit(Unidades de venta)</option>
                                <?php else:?>
                                   <option value="KT">KT-Kit(Unidades de venta)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "SET"):?>
                                   <option value="SET" selected>SET-Conjunto(Unidades de venta)</option>
                                <?php else:?>
                                   <option value="SET">SET-Conjunto(Unidades de venta)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "LTR"):?>
                                   <option value="LTR" selected>LTR-Litro(Tiempo y Espacio)</option>
                                <?php else:?>
                                   <option value="LTR">LTR-Litro(Tiempo y Espacio)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "XBX"):?>
                                    <option value="XBX" selected>XBX-Caja(Unidades de empaque)</option>
                                <?php else:?>
                                    <option value="XBX">XBX-Caja(Unidades de empaque)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "MON"):?>
                                    <option value="MON" selected>MON-Mes(Tiempo y Espacio)</option>
                                <?php else:?>
                                    <option value="MON">MON-Mes(Tiempo y Espacio)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "DAY"):?>
                                    <option value="DAY" selected>DAY-Día(Tiempo y Espacio)</option>
                                <?php else:?>
                                    <option value="DAY">DAY-Día(Tiempo y Espacio)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "HUR"):?>
                                    <option value="HUR" selected>HUR-Hora(Tiempo y Espacio)</option>
                                <?php else:?>
                                    <option value="HUR">HUR-Hora(Tiempo y Espacio)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "MTK"):?>
                                    <option value="MTK" selected>MTK-Metro cuadrado(Tiempo y Espacio)</option>
                                <?php else:?>
                                    <option value="MTK">MTK-Metro cuadrado(Tiempo y Espacio)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "11"):?>
                                   <option value="11" selected>11-Equipos(Diversos)</option>
                                <?php else:?>
                                    <option value="11">11-Equipos(Diversos)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "MGM"):?>
                                   <option value="MGM" selected>MGM-Miligramo(Mecánica)</option>
                                <?php else:?>
                                    <option value="MGM">MGM-Miligramo(Mecánica)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "XPK"):?>
                                   <option value="XPK" selected>XPK-Paquete(Unidades de empaque)</option>
                                <?php else:?>
                                    <option value="XPK">XPK-Paquete(Unidades de empaque)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "XKI"):?>
                                   <option value="XKI" selected>XKI-Kit(Conjunto de piezas)(Unidades de empaque)</option>
                                <?php else:?>
                                   <option value="XKI">XKI-Kit(Conjunto de piezas)(Unidades de empaque)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "AS"):?>
                                    <option value="AS" selected>AS-Variedad(Diversos)</option>
                                <?php else:?>
                                   <option value="AS">AS-Variedad(Diversos)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "GRM"):?>
                                    <option value="GRM" selected>GRM-Gramo(Mecánica)</option>
                                <?php else:?>
                                   <option value="GRM">GRM-Gramo(Mecánica)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "PR"):?>
                                    <option value="PR" selected>PR-Par(Números enteros / Números / Ratios)</option>
                                <?php else:?>
                                   <option value="PR">PR-Par(Números enteros / Números / Ratios)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "DPC"):?>
                                    <option value="DPC" selected>DPC-Docenas de piezas(Unidades de venta)</option>
                                <?php else:?>
                                   <option value="DPC">DPC-Docenas de piezas(Unidades de venta)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "xun"):?>
                                    <option value="xun" selected>xun-Unidad(Unidades de empaque)</option>
                                <?php else:?>
                                   <option value="xun">xun-Unidad(Unidades de empaque)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "XLT"):?>
                                    <option value="XLT" selected>XLT-Lote(Unidades de empaque)</option>
                                <?php else:?>
                                  <option value="XLT">XLT-Lote(Unidades de empaque)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "10"):?>
                                   <option value="10" selected>10-Grupos(Diversos)</option>
                                <?php else:?>
                                  <option value="10">10-Grupos(Diversos)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "MLT"):?>
                                   <option value="MLT" selected>MLT-Mililitro(Tiempo y Espacio)</option>
                                <?php else:?>
                                  <option value="MLT">MLT-Mililitro(Tiempo y Espacio)</option>
                                <?php endif;?>

                                <?php if($row_producto->unidad_sat == "E54"):?>
                                   <option value="E54" selected>E54-Viaje(Unidades específicas de la industria (varias))</option>
                                <?php else:?>
                                  <option value="E54">E54-Viaje(Unidades específicas de la industria (varias))</option>
                                <?php endif;?>

                                
                              </select>
                            </div>
                        </div>

                        <!--div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">IEPS</label>
                              <input id="ieps" name="ieps" type="text" class="form-control form-control-sm cc-exp" value="<?php echo $row_producto->ieps;?>" placeholder="IEPS" autocomplete="off" style="width:85px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div-->

                    </div>

                  
                  <!--FIN PRIMERA SECCION-->
                  
                    <!--INICIO SEGUNDA SECCION-->

                    

                    <hr/>

                    <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Ficha tecnica</label>
                              <textarea class="form-control form-control-sm" name="productoFichaTecnica" id="productoFichaTecnica" rows="4" cols="50" placeholder="Ficha tecnica"><?php echo $row_producto->productoFichaTecnica;?></textarea>
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Especificaciones</label>
                              <textarea class="form-control form-control-sm" name="productoEspecificaciones" id="productoEspecificaciones" rows="4" cols="50" placeholder="Especificaciones"><?php echo $row_producto->productoEspecificaciones;?></textarea>
                          </div>
                        </div>
                    </div>

                    

                    
                    <br/>
                    <br/>

                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Editar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->
