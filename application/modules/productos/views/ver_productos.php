<script type="text/javascript">
     menu_activo = "productos";
$("#menu_productos_lista").last().addClass("menu_estilo");
    $(document).ready(function() {
      $('#bootstrap-data-table').DataTable({
  "language": {
    "url": "<?php echo base_url();?>statics/lenguajes/Spanish.json"
  }
});

    } );
</script>
<script>
    menu_activo = "productos";
$("#menu_productos_alta").last().addClass("menu_estilo");
$(document).ready(function(){
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  var url ="<?php echo base_url()?>index.php/productos/guarda";
  ajaxJson(url,{"productoNombre":$("#productoNombre").val(),
                "productoReferencia":$("#productoReferencia").val(),
                "productoMedida":$("#productoMedida").val(),
                //"productoClasificado":$("#productoClasificado").val(),
                //"productoModelo":$("#productoModelo").val(),
                "productoIdMarca":$("#productoIdMarca").val(),
                "productoFlete":$("#productoFlete").val(),
                "productoTipo":$("#productoTipo").val(),
                "procuctoContenido":$("#procuctoContenido").val(),
                "productoIngredienteActivo":$("#productoIngredienteActivo").val(),
                "productoRegCofepris":$("#productoRegCofepris").val(),
                "productoRegEpa":$("#productoRegEpa").val(),
                "productoPresantacion":$("#productoPresantacion").val(),
                "productoDosisAlta":$("#productoDosisAlta").val(),
                "productoDosisBaja":$("#productoDosisBaja").val(),
                "productoMedidaAlta":$("#productoMedidaAlta").val(),
                "productoMedidaBaja":$("#productoMedidaBaja").val(),
                "productoFichaTecnica":$("#productoFichaTecnica").val(),
                "productoEspecificaciones":$("#productoEspecificaciones").val(),
                "productoPrecioPublico":$("#productoPrecioPublico").val(),
                "productoPrecioMayoreo":$("#productoPrecioMayoreo").val(),
                "productoPrecioCredito":$("#productoPrecioCredito").val(),
                "poductoInventariar":$("#poductoInventariar").val(),
                "productoGrupo":$("#productoGrupo").val(),
                "productoCantidadMinima":$("#productoCantidadMinima").val(),
                "productoCantidadMaxima":$("#productoCantidadMaxima").val(),
                "productoCantidadMayoreo":$("#productoCantidadMayoreo").val(),
                "clave_sat":$("#clave_sat").val(),
                "unidad_sat":$("#unidad_sat").val(),
              "productoNormalPlaguecida":$("#productoNormalPlaguecida").val(),
                "productoLote":$("#productoLote").val(),
                "ieps":$("#ieps").val(),
              "unidad_compra":$("#unidad_compra").val(),
                "unidad_inventario":$("#unidad_inventario").val(),
                "unidad_venta":$("#unidad_venta").val(),
                "unidad_consumo":$("#unidad_consumo").val(),
                "reorden":$("#reorden").val()
              },
            "POST","",function(result){
    correoValido = false;
    console.log(result);
    json_response = JSON.parse(result);
    obj_output = json_response.output;
    obj_status = obj_output.status;
    if(obj_status == false){
      aux = "";
      $.each( obj_output.errors, function( key, value ) {
        aux +=value+"<br/>";
      });
      exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
    }
    if(obj_status == true){
      exito_redirect("GUARDADO CON EXITO","success","<?php echo base_url()?>index.php/productos/ver_productos");
    }


  });
});



$(".btn_eliminar").click(function(event){
                     event.preventDefault();

                     bootbox.dialog({
                         message: "Desea eliminar los registros seleccionados?",
                         closeButton: true,
                         buttons:
                         {
                             "danger":
                             {
                                 "label": "<i class='icon-remove'></i>Eliminar ",
                                 "className": "btn-danger",
                                 "callback": function () {

                                   $('.myCheckBox').each(function(){ //this.checked = true; 
                                      if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());
                                           id = $(this).val();
                                           url = "<?php echo base_url()?>index.php/productos/eliminar/"+id;//$("#delete"+id).attr('href');
                                           $("#borrar_"+id).slideUp();
                                           $.get(url);
                                                };
                                           });

                                   
                                 }
                             },
                             "cancel":
                             {
                                 "label": "<i class='icon-remove'></i> Cancelar",
                                 "className": "btn-sm btn-info",
                                 "callback": function () {

                                 }
                             }

                         }
                     });


                    
                 });


  

  $("#todos_boton").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });

  $(".btn_editar").click(function(event){
                     event.preventDefault();

                      //$( "#todos_boton" ).prop( "checked", true );

                     //$('.myCheckBox').each(function(){ this.checked = true; });
                      aux = 1;
                      $('.myCheckBox').each(function(){ //this.checked = true; 
                            if ($(this).is(':checked')) {
                                           // Do something...
                                           //alert($(this).val());           
                                id = $(this).val();
                                console.log(id);
                                var txt = $(".textNombre_"+id+"").html();
                                $(".textNombre_"+id+"").replaceWith("<input value='" + txt + "' name='"+aux+"[productoNombre]'/>");

                                var txt_des = $(".textDescripion_"+id+"").html();
                                $(".textDescripion_"+id+"").replaceWith("<input value='" + txt_des + "' name='"+aux+"[productoReferencia]'/>");

                                var txt_email = $(".textEmail_"+id+"").html();
                                $(".textEmail_"+id+"").replaceWith("<input value='" + txt_email + "' name='"+aux+"[productoPrecioPublico]'/>");


                                /*var txt_telefono = $(".textTelefono_"+id+"").html();
                                $(".textTelefono_"+id+"").replaceWith("<input value='" + txt_telefono + "' name='"+aux+"[productoPrecioCredito]'/>");


                                var txt_mayo = $(".textMayoreo_"+id+"").html();
                                $(".textMayoreo_"+id+"").replaceWith("<input value='" + txt_mayo + "' name='"+aux+"[productoPrecioMayoreo]'/>");
                                */


                                var txt_id = $(".id_des_"+id+"").html();
                                $(".id_des_"+id+"").replaceWith("<input type='hidden' value='" + txt_id + "' name='"+aux+"[id]'/>");

                                aux = aux +1;
                            };
                      });

                      $("body").keypress(function(e) {
                        if(e.which == 13) {
                          //obtenerDatos();
                          
                          var form = $("#formulario_editar").serializeArray();
                         // console.log(form);
                            url = "<?php echo base_url()?>index.php/productos/guarda_edicion"
                            ajaxJson(url,form,
                                        "POST","",function(result){
                                            correoValido = false;
                                            console.log(result);
                                            exito_redirect("EDITADO CON EXITO","success","<?php echo base_url()?>index.php/productos/ver_productos");
                                            
                                          });

                        }
                      });



                    
                 });



});
</script>



<div class="card-header">
  <!--a href="<?php echo base_url()?>index.php/proveedores/alta"><button type="button" class="btn btn-info">Alta Proveedor</button></a-->

  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Nuevo
            </button>

            <button type="button" class="btn btn-info btn_editar">Editar</button>

            <button type="button" class="btn btn-danger btn_eliminar">Eliminar</button>



</div>

<div class="row">
  <div class="col-6">
      <!--div class="form-group">
        <label for="factura_moneda">Cliente:</label>
        <input id="receptor_nombre" name="receptor_nombre" type="text" class="form-control-sm alto form-control cc-exp" value="<?php echo $factura->receptor_nombre;?>" placeholder="Cliente" autocomplete="cc-exp" data-toggle="modal"  data-target="#exampleModal">
        <input id="receptor_id_cliente" name="receptor_id_cliente" type="hidden" class="alto form-control cc-exp" value="<?php echo $factura->receptor_id_cliente;?>" placeholder="receptor_id_cliente" autocomplete="cc-exp" >
      </div-->
  </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                  <form id="formulario_editar">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>
                         
                           <input type="checkbox" name=""  id="todos_boton" class="todos_boton" >
                          
                        </th>
                        <th>Nombre</th>
                        <th>Referencia</th>
                        <!--th>Marca</th>
                        <th>Contenido</th-->
                        <th>Precio público</th>
                        <!--th>Precio Credito</th>
                        <th>Precio Mayoreo</th-->


                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($rows)):?>
                      <?php foreach($rows as $row):?>
                        <tr id="borrar_<?php echo $row->productoId?>">
                           <td><input type="checkbox" name="" class="myCheckBox" id="<?php echo $row->productoId?>" value="<?php echo $row->productoId?>"></td>
                          <td><span class="textNombre_<?php echo $row->productoId?> texto_nombre"><?php echo $row->productoNombre;?></span></td>
                          <td><span class="textDescripion_<?php echo $row->productoId?> texto_descripcion"><?php echo $row->productoReferencia	;?></span></td>
                          <!--td><?php echo nombre_marca($row->productoIdMarca) ;?></td>
                          <td><?php echo $row->procuctoContenido." ".nombre_medida($row->productoMedida) ;?></td-->
                          <td><span class="textEmail_<?php echo $row->productoId?> texto_email"><?php echo $row->productoPrecioPublico;?></span></td>
                          <!--td><span class="textTelefono_<?php echo $row->productoId?> texto_telefono"><?php echo $row->productoPrecioCredito;?></span></td>
                          <td><span class="textMayoreo_<?php echo $row->productoId?> texto_telefono"><?php echo $row->productoPrecioMayoreo;?></span></td-->

                          <span hidden="hidden" class="id_des_<?php echo $row->productoId?> texto_id" type="hidden"><?php echo $row->productoId?></span>


                          <td>

                            <a href="<?php echo base_url()?>index.php/productos/ver/<?php echo $row->productoId;?>">
                            <button type="button" class="btn btn-info">Ver/Editar</button>
                            </a>
                          </td>
                        </tr>
                     <?php endforeach;?>
                     <?php endif;?>
                    </tbody>
                  </table>
                </form>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->


<!-- Modal -->
<div class="modal fullscreen-modal  fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo producto </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                    <!--INICIO PRIMERA SECCION-->
                    <div class="row">
                        <div class="col-3">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                <input id="productoNombre" name="productoNombre" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Nombre" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Referencia</label>
                                <input id="productoReferencia" name="productoReferencia" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Referencia" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>

                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Tipo</label>
                              <select name="productoNormalPlaguecida" id="productoNormalPlaguecida" class="form-control form-control-sm">
                                <option value="1">Normal</option>
                                <option value="2">Plaguicida</option>
                            </select>
                          </div>
                        </div>

                        <!--div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Clasificado</label>
                            <select name="productoClasificado" id="productoClasificado" class="form-control form-control-sm">
                              <option value="1">Producto</option>
                              <option value="2">Servicio</option>
                            </select>
                          </div>
                        </div-->
                        <!--div class="col-2">
                          <div class="form-group" >
                              <label for="cc-exp" class="control-label mb-1">Modelo</label>
                              <input id="productoModelo" name="productoModelo" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Modelo" autocomplete="off">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div-->
                        
                        
                    </div>

                    <div class="row">
                        
                    </div>


                    <div class="row">
                    <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Grupo</label>
                            <select name="productoGrupo" id="productoGrupo" class="form-control form-control-sm">
                              <?php foreach($grupos as $row):?>
                              <option value="<?php echo $row->grupoId;?>"><?php echo $row->grupoNombre?></option>
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                      <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Marca</label>
                            <select name="productoIdMarca" id="productoIdMarca" class="form-control form-control-sm">
                              <?php foreach($marcas as $row):?>
                              <option value="<?php echo $row->marcaId;?>"><?php echo $row->marcaNombre?></option>
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Contenido neto</label>
                                <input id="procuctoContenido" name="procuctoContenido" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:55px;">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Medida</label>
                            <select name="productoMedida" id="productoMedida" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                              <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <!--div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Cantidad minima</label>
                              <input id="productoCantidadMinima" name="productoCantidadMinima" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:55px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div>
                        <div class="col-2">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Cantidad maxima</label>
                              <input id="productoCantidadMaxima" name="productoCantidadMaxima" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:55px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div-->
                        
                    </div>

                    <!--div class="row">
                      <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Inventariar</label>
                                
                                <select class="form-control form-control-sm" name="poductoInventariar" id="poductoInventariar" style="width:55px;">
                                  <option value="SI">SI</option>
                                  <option value="NO">NO</option>
                                </select>
                            </div>
                          </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Reorden</label>
                                <input id="reorden" name="reorden" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="" autocomplete="off" style="width:55px;">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                          </div>
                        
                    </div-->

                    

                    <div class="row">
                      
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio público</label>
                                <input id="productoPrecioPublico" name="productoPrecioPublico" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Precio público" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <!--div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio mayoreo</label>
                                <input id="productoPrecioMayoreo" name="productoPrecioMayoreo" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Precio mayoreo" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Cantidad mayoreo</label>
                                <input id="productoCantidadMayoreo" name="productoCantidadMayoreo" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Cantidad mayoreo" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Precio credito</label>
                                <input id="productoPrecioCredito" name="productoPrecioCredito" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Precio credito" autocomplete="off">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" >
                                <label for="cc-exp" class="control-label mb-1">Lote</label>
                                <input id="productoLote" name="productoLote" type="text" class="form-control form-control-sm cc-exp" value="" placeholder="Lote" autocomplete="off" style="width:85px;">
                                <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                            </div>
                        </div-->

                    </div>
                    <hr/>

                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                          <label for="cc-exp" class="control-label mb-1 ">Clave de producto SAT</label>
                          <select class="form-control alto form-control-sm" id="clave_sat" name="clave_sat">
                                    <option value="80161500">80161500-Servicios de apoyo gerencial</option>
                                    <option value="80161600">80161600-Supervisión de instalaciones de negocios</option>
                                    <option value="80161700">80161700-Servicios de recuperación de activos</option>
                                    <option value="80161800">80161800-Servicios de alquiler o arrendamiento de equipo de oficina</option>
                                    <option value="76111800">76111800-Limpieza de vehículos de transporte</option>
                                    <option value="84131609">84131609- Programas de asistencia a empleados (Seguros laborales)</option>
                                     
                                </select>
                        </div>
                      </div>
                        <div class="col-4">
                            <div class="form-group" >
                              <label for="cc-exp" class="control-label mb-1">Unidad SAT</label>
                              <select class="form-control alto form-control-sm" id="unidad_sat" name="unidad_sat">
                                <option value="H87">H87-Pieza(Múltiplos / Fracciones / Decimales)</option>
                                <option value="EA">EA-Elemento(Unidades de venta)</option>
                                <option value="E48">E48-Unidad de Servicio(Unidades específicas de la industria (varias))</option>
                                <option value="ACT">ACT-Actividad(Unidades de venta)</option>
                                <option value="KGM">KGM-Kilogramo(Mecánica)</option>
                                <option value="E51">E51-Trabajo(Unidades específicas de la industria (varias))</option>
                                <option value="A9">A9-Tarifa(Diversos)</option>
                                <option value="MTR">MTR-Metro(Tiempo y Espacio)</option>
                                <option value="AB">AB-Paquete a granel(Diversos)</option>
                                <option value="BB">BB-Caja base(Unidades específicas de la industria (varias))</option>
                                <option value="KT">KT-Kit(Unidades de venta)</option>
                                <option value="SET">SET-Conjunto(Unidades de venta)</option>
                                <option value="LTR">LTR-Litro(Tiempo y Espacio)</option>
                                <option value="XBX">XBX-Caja(Unidades de empaque)</option>
                                <option value="MON">MON-Mes(Tiempo y Espacio)</option>
                                <option value="DAY">DAY-Día(Tiempo y Espacio)</option>
                                <option value="HUR">HUR-Hora(Tiempo y Espacio)</option>
                                <option value="MTK">MTK-Metro cuadrado(Tiempo y Espacio)</option>
                                <option value="11">11-Equipos(Diversos)</option>
                                <option value="MGM">MGM-Miligramo(Mecánica)</option>
                                <option value="XPK">XPK-Paquete(Unidades de empaque)</option>
                                <option value="XKI">XKI-Kit(Conjunto de piezas)(Unidades de empaque)</option>
                                <option value="AS">AS-Variedad(Diversos)</option>
                                <option value="GRM">GRM-Gramo(Mecánica)</option>
                                <option value="PR">PR-Par(Números enteros / Números / Ratios)</option>
                                <option value="DPC">DPC-Docenas de piezas(Unidades de venta)</option>
                                <option value="xun">xun-Unidad(Unidades de empaque)</option>
                                <option value="XLT">XLT-Lote(Unidades de empaque)</option>
                                <option value="10">10-Grupos(Diversos)</option>
                                <option value="MLT">MLT-Mililitro(Tiempo y Espacio)</option>
                                <option value="E54">E54-Viaje(Unidades específicas de la industria (varias))</option>
                              </select>
                            </div>
                        </div>

                        <!--div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">IEPS</label>
                              <input id="ieps" name="ieps" type="text" class="form-control form-control-sm cc-exp" value="0.0" placeholder="IEPS" autocomplete="off" style="width:85px;">
                              <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                          </div>
                        </div-->

                    </div>

                  
                  <!--FIN PRIMERA SECCION-->
                  
                    <!--INICIO SEGUNDA SECCION-->

                  
                    

                   

                    <!--div class="row">
                      <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de compra</label>
                              <select name="unidad_compra" id="unidad_compra" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_compra == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de inventario</label>
                              <select name="unidad_inventario" id="unidad_inventario" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_inventario == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de venta</label>
                              <select name="unidad_venta" id="unidad_venta" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_venta == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Unidad de consumo</label>
                             <select name="unidad_consumo" id="unidad_consumo" class="form-control form-control-sm" style="width:75px;">
                              <?php foreach($medidas as $row):?>
                                <?php if($row_producto->unidad_consumo == $row->medidaId){?>
                                  <option value="<?php echo $row->medidaId;?>" selected><?php echo $row->medidaNombre?></option>
                                <?php }else{?>
                                  <option value="<?php echo $row->medidaId;?>"><?php echo $row->medidaNombre?></option>
                                <?php }?>
                              
                              <?php endforeach;?>
                            </select>
                          </div>
                        </div>
                        
                    </div-->



                    <hr/>

                    <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Ficha tecnica</label>
                              <textarea class="form-control form-control-sm" name="productoFichaTecnica" id="productoFichaTecnica" rows="4" cols="50" placeholder="Ficha tecnica"></textarea>
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                              <label for="cc-exp" class="control-label mb-1">Especificaciones</label>
                              <textarea class="form-control form-control-sm" name="productoEspecificaciones" id="productoEspecificaciones" rows="4" cols="50" placeholder="Especificaciones"></textarea>
                          </div>
                        </div>
                    </div>

                    

                    
                    <br/>
                    <br/>

                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                  </form>



                      
      </div>
      
    </div>
  </div>
</div>
