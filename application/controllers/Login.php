<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Login extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html','companies', 'url'));
        $this->load->library('form_validation');
        date_default_timezone_set('America/Mexico_City');

    }

    public function index()
  	{
      $this->load->view('login/index', '', FALSE);

  	}

    public function crear_admin(){

          $this->load->view('login/registro_admin');

  	}


    public function checkDataLogin()
      {
          $username = $this->input->post('username');
          $password = $this->input->post('password');
          if(isset($username) && isset($password) && !empty($password) && !empty($username))
          {
              $pass = encrypt_password($username,
                                       $this->config->item('encryption_key'),
                                       $password);
              $total = $this->Mgeneral->count_results_users($username, $pass);
              if($total == 1)
              {
                  echo "1";
              }
              else{
                  echo "0";
              }
          }
          else{
              redirect('login');
          }
      }

  	/*
  	*metodo para inicio de session
  	*/
  	public function mainView()
      {
          $post = $this->input->post('Login');


          if(isset($post) && !empty($post))
          {
              $pass = encrypt_password($post['adminUsername'],
                                       $this->config->item('encryption_key'),
                                       $post['adminPassword']);


              $dataUser = $this->Mgeneral->get_all_data_users_specific($post['adminUsername'], $pass);

              $array_session = array('id'=>$dataUser->idusuario);
              $this->session->set_userdata($array_session);

              if($this->session->userdata('id'))
              {
  			   	redirect('inicio');
                  /*$aside = $this->load->view('companies/left_menu', '', TRUE);
                  $content = $this->load->view('companies/main_view', '', TRUE);
                  $this->load->view('main/template', array('aside'=>$aside,
                                                           'content'=>'',
  														 'included_js'=>array('statics/js/modules/menu.js')));*/
              }
              else{
              }
          }
          else{
          }
      }


    public function guarda_admin()
    {
        $post = $this->input->post('Registro');
        if($post)
        {
            $pass = encrypt_password($post['adminUsername'],
                                     $this->config->item('encryption_key'),
                                     $post['adminPassword']);
            $post['adminPassword'] = $pass;
            $post['adminStatus'] = 1;
			      $post['adminFecha']=date('Y-m-d');
            $id = $this->Mgeneral->save_admin($post);
            echo $id;
        }
        else{
        }
    }
    public function cerrar_sesion()
    {
        $this->session->unset_userdata('id');
        $this->session->sess_destroy();
        redirect('login');
    }
}
