<?php
/**

 **/
class Mgeneral extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function get_pro_uni($id_producto){
      $unidades = array();
      
      
      $aux = $this->get_unidades_conversion11($id_producto);//$this->get_row('id_articulo',$id_producto,'conversion_unidades');

      if(is_object($aux)){
        $uni = $this->get_unidades_conversion($id_producto);

        foreach($uni as $rs):
          $data = array();
         // $data['id_unidad'] = $rs->unidad_origen;
          array_push($unidades,$rs->unidad_origen);
        endforeach;

        foreach($uni as $rs):
          $data = array();
         // $data['id_unidad'] = $rs->unidad_destino;
          array_push($unidades,$rs->unidad_destino);
        endforeach;

  


       $lista_simple = array_unique($unidades);

        //print_r($lista_simple);
       return $lista_simple;

        

      }
    }


    public function get_unidades_conversion($id_producto){

       //$this->db->where('unidad',$unidad);
       $this->db->where('id_articulo',$id_producto);
       
       $this->db->where('status',0);
       $res = $this->db->get('conversion_unidades')->result();
       return $res ;
    }

    public function get_unidades_conversion11($id_producto){

       //$this->db->where('unidad',$unidad);
       $this->db->where('id_articulo',$id_producto);
       
       $this->db->where('status',0);
       $res = $this->db->get('conversion_unidades')->row();
       return $res ;
    }

    public function validar_conversion_unidades($id_producto, $unidad){

       //$this->db->where('unidad',$unidad);
       $this->db->where('id_articulo',$id_producto);
       $this->db->where('unidad_origen',$unidad);
       $this->db->where('status',0);
       $res = $this->db->get('conversion_unidades')->row();
       return $res ;
    }

     public function login_app($user, $pass)
        {
            $this->db->where('adminUsername', $user);
            $this->db->where('adminPassword', $pass);
            $total = $this->db->get('usuarios')->row();
            return $total;
        }

    // unidad:    1.-compra,2.-inventario,3.-venta,4.-consumo 
    public function get_unidades_productos($id_producto, $tipo){

       //$this->db->where('unidad',$unidad);
       $this->db->where('tipo_unidad',$tipo);
       $this->db->where('id_producto',$id_producto);
       $this->db->where('status',0);
       $res = $this->db->get('productos_unidades')->result();
       return $res ;
    }

    public function get_unidades_productos1($id_producto, $unidad, $tipo){

       $this->db->where('unidad',$unidad);
       $this->db->where('tipo_unidad',$tipo);
       $this->db->where('id_producto',$id_producto);
       $this->db->where('status',0);
       $res = $this->db->get('productos_unidades')->row();
       return $res ;
    }

    public function get_unidades_productos2($id_producto,  $tipo){

       
       $this->db->where('tipo_unidad',$tipo);
       $this->db->where('id_producto',$id_producto);
       $this->db->where('status',0);
       $res = $this->db->get('productos_unidades')->result();
       return $res ;
    }


    public function save_editar_formulario($post,$id_table_id,$tabla){
      foreach($post as $key):
          if(is_array($key)){
            while( list( $field, $value ) = each( $key )) {
               if($field == "id"){
                  $id_table = $id_table_id;
                  $id_value = $value;
               }else{
                  $data[$field] = $value;
               }
            }
            $this->update_table_row($tabla,$data,$id_table,$id_value);
          }
      endforeach;
      
    }


    public function get_result_estaciones($campo,$value,$tabla){
        return $this->db->where($campo,$value)->order_by('ceIEstacion')->get($tabla)->result();
    }

    public function cordones_estaciones($estacion, $id_plaga, $cordon){
      $this->db->select('cordon_estaciones.ceIEstacion,cordon_estaciones.ceId, estacion_plagas.cantidad,estacion_plagas.id_plaga, cordon_estaciones.ceCambioTrampa');
      $this->db->from('estacion_plagas');
      $this->db->join('cordon_estaciones', 'estacion_plagas.id_estacion = cordon_estaciones.ceId');
      $this->db->where('cordon_estaciones.ceIEstacion',$estacion);
      $this->db->where('estacion_plagas.id_plaga',$estacion);
      $this->db->where('cordon_estaciones.ceIdCordon',$cordon);
  
      $test_aux = $this->db->get()->result();
      
     
      //return $resultado;

      //var_dump($test);
              $cantidad = 0;
              $cantidad_anterios = 0;
              foreach($test_aux as $row_es):

                if($row_es->ceCambioTrampa == "N"){
                  $cantidad = $row_es->cantidad -$cantidad_anterios;

                }
                if($row_es->ceCambioTrampa == "S"){
                  $cantidad = $row_es->cantidad ;

                }
                $cantidad_anterios = $row_es->cantidad;

                //echo $row_es->cantidad."-".$cantidad."<br/>";
                //echo $cantidad."<br/>";
              endforeach;
        return $cantidad;

    }

    public function get_plaga_estacion($plaga,$estacion){

      $this->db->select('plagas.plagaNombre,plagas.plagaId, estacion_plagas.cantidad');
      $this->db->from('estacion_plagas');
      $this->db->join('plagas', 'estacion_plagas.id_plaga = plagas.plagaId');
      $this->db->where('estacion_plagas.id_plaga',$plaga);
      $this->db->where('estacion_plagas.id_estacion',$estacion);
      $resultado = $this->db->get()->row();
      
     
      return $resultado;

    }

    public function plagas_app($id_cordon){
      $this->db->select('cordones_plagas.cpIdPlaga,cordones_plagas.cpId,cordones_plagas.cpIdCordon,plagas.plagaNombre');
      $this->db->from('cordones_plagas');
      $this->db->join('plagas', 'cordones_plagas.cpIdPlaga = plagas.plagaId');
      $this->db->where('cordones_plagas.cpIdCordon',$id_cordon);
      $resultado = $this->db->get()->result();
      
     
      return $resultado;
    }


   public function ordenes_clientes_app($id_cliente){
    $this->db->where("status_orden",0);
    $this->db->where("servicioIdCliente",$id_cliente);
    $query = $this->db->get('servicios')->result();

      return $query;
   }

    public function get_facturas_ppd($rfc){

       $this->db->where('receptor_RFC',$rfc);
       $this->db->where('factura_medotoPago','PPD');
       $res = $this->db->get('factura')->result();
       return $res ;
    }

    public function get_fecha_esquema($mes,$semana,$dia){
      $this->db->where("mes",$mes);
      $this->db->where("dia",$dia);
      $this->db->where("semana",$semana);
      $query = $this->db->get('programa')->row();

      return $query;
    }

    public function total_factura_cliente($id_cliente){
      $general_total = 0;
      $facturas = $this->get_result('receptor_id_cliente',$id_cliente,'factura');
      foreach($facturas as $fac):
        $general_total+= $this->factura_subtotal($fac->facturaID) + $this->factura_iva_total($fac->facturaID);
      endforeach;
      return $general_total;
    }

    public function abono_factura_cliente($id_cliente){
      $general_abono = 0;
      $facturas = $this->get_result('receptor_id_cliente',$id_cliente,'factura');
      foreach($facturas as $fac):
       $general_abono+= $this->complementoPagoPagado($fac->sat_uuid);
      endforeach;
      return $general_abono;
    }

    public function saldo_factura_cliente($id_cliente){
      $total_saldo = 0;

      $facturas = $this->get_result('receptor_id_cliente',$id_cliente,'factura');
      foreach($facturas as $fac):
        if($fac->factura_medotoPago == "PPD"):
        $saldo = $this->Mgeneral->complementoPagoPagado($fac->sat_uuid);
        $total_saldo+= (($this->factura_subtotal($fac->facturaID) + $this->factura_iva_total($fac->facturaID))-$saldo);
       endif;
      endforeach;
      return $total_saldo;
    }

    public function complementos_total_pagado($uuid){
      $com = $this->get_row('uuid',$uuid,'complemento_cfdi_relacionado_p');
      $filas = $this-> get_result('id_complemento',$com->id_complemento,'complemento_datos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->total_pago;
      endforeach;
      return $subtotal;
    }

    public function complementoPagoPagado($uuid){
      $this->db->select('complemento_datos.total_pago');
      $this->db->from('complemento_cfdi_relacionado_p');
      $this->db->join('complemento_datos', 'complemento_cfdi_relacionado_p.id_complemento = complemento_datos.id_complemento');
      $this->db->where('complemento_cfdi_relacionado_p.uuid',$uuid);
      $resultado = $this->db->get()->result();
      
      $subtotal = 0;
      foreach($resultado as $fil):
        $subtotal+= $fil->total_pago;
      endforeach;
      return $subtotal;
    }

     public function complemento_numero_pagos($uuid){
      $this->db->select('complemento_datos.total_pago');
      $this->db->from('complemento_cfdi_relacionado_p');
      $this->db->join('complemento_datos', 'complemento_cfdi_relacionado_p.id_complemento = complemento_datos.id_complemento');
      $this->db->where('complemento_cfdi_relacionado_p.uuid',$uuid);
      $resultado = $this->db->get()->result();
      
      $subtotal = 0;
      foreach($resultado as $fil):
        $subtotal+= 1;
      endforeach;
      return $subtotal;
    }

    public function factura_subtotal($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal;
    }

    public function factura_iva_total($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal*.16;
    }

    public function factura_descuento($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $descuento = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

    public function factura_iva(){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $iva = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

    public function factura_traslados($id_factura){

    }

    public function factura_retenciones($id_factura){

    }

    public function factura_total($id_factura){

    }

    public function get_productos_orden($id_producto,$almacen = null){
      if($almacen == null):
        $query = $this->db->query("SELECT SUM(orden_compra_productos.ordenP_cantidad) AS total
        FROM orden_compra_productos
        INNER JOIN orden_compra
        ON orden_compra_productos.ordenP_idOrden = orden_compra.ordenId
        WHERE orden_compra.orden_status = 2 AND orden_compra_productos.ordenP_idProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->row();
      else:
        $query = $this->db->query("SELECT SUM(orden_compra_productos.ordenP_cantidad) AS total
        FROM orden_compra_productos
        INNER JOIN orden_compra
        ON orden_compra_productos.ordenP_idOrden = orden_compra.ordenId
        WHERE orden_compra.orden_status = 2 AND orden_compra.ordenIdAlmacen =".$almacen ." AND orden_compra_productos.ordenP_idProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->row();
      endif;
    }

    public function get_productos_orden2($id_producto, $almacen = null){
      if($almacen == null):
        $query = $this->db->query("SELECT *
        FROM orden_compra_productos
        INNER JOIN orden_compra
        ON orden_compra_productos.ordenP_idOrden = orden_compra.ordenId
        WHERE orden_compra.orden_status = 2  AND orden_compra_productos.ordenP_idProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->result();
      else:
        $query = $this->db->query("SELECT *
        FROM orden_compra_productos
        INNER JOIN orden_compra
        ON orden_compra_productos.ordenP_idOrden = orden_compra.ordenId
        WHERE orden_compra.orden_status = 2 AND orden_compra.ordenIdAlmacen =".$almacen ." AND orden_compra_productos.ordenP_idProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->result();
      endif;
    }

    public function get_productos_servicios($id_producto,$almacen = null){
      if($almacen == null):
        $query = $this->db->query("SELECT SUM(servicios_actividades.saCantidad) AS total
        FROM servicios_actividades
        INNER JOIN servicios
        ON servicios.servicioId = servicios_actividades.saIdServicio
        WHERE servicios.status_orden = 1 AND servicios_actividades.saIdProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->row();
      else:
        $query = $this->db->query("SELECT SUM(servicios_actividades.saCantidad) AS total, 
        FROM servicios_actividades
        INNER JOIN servicios
        ON servicios.servicioId = servicios_actividades.saIdServicio
        WHERE servicios.status_orden = 1 AND servicios.servicioIdVehiculo =".$almacen." AND servicios_actividades.saIdProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->row();
      endif;

    }

     public function get_productos_servicios_nuevo($id_producto,$almacen = null){
      if($almacen == null):
        $query = $this->db->query("SELECT servicios_actividades.saCantidad as cantidad , servicios_actividades.saDosis as dosis, servicios_actividades.saMedida as medida
        FROM servicios_actividades
        INNER JOIN servicios
        ON servicios.servicioId = servicios_actividades.saIdServicio
        WHERE servicios.status_orden = 1 AND servicios_actividades.saIdProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->result();
      else:
        $query = $this->db->query("SELECT servicios_actividades.saCantidad as cantidad , servicios_actividades.saDosis as dosis, servicios_actividades.saMedida as medida
        FROM servicios_actividades
        INNER JOIN servicios
        ON servicios.servicioId = servicios_actividades.saIdServicio
        WHERE servicios.status_orden = 1 AND servicios.servicioIdVehiculo =".$almacen." AND servicios_actividades.saIdProducto = ".$id_producto);

        //$data = $this->db->get('productos');
        return $query->result();
      endif;

    }

    public function buscar_producto($busqueda){
      $this->db->select("productoNombre AS label, `productoId`, `productoNombre`, `productoMedida`, `productoReferencia`, `productoClasificado`, `productoModelo`, `productoIdMarca`, `productoFlete`, `productoTipo`, `procuctoContenido`, `productoIngredienteActivo`, `productoRegCofepris`, `productoRegEpa`, `productoPresantacion`, `productoDosisAlta`, `productoDosisBaja`, `productoMedidaAlta`, `productoMedidaBaja`, `productoFichaTecnica`, `productoEspecificaciones`, `productoPrecioPublico`, `productoPrecioMayoreo`, `productoPrecioCredito`, `productoGrupo`, `productoCantidadMinima`, `productoCantidadMaxima`, `poductoInventariar`");
      $this->db->like('productoNombre', $busqueda);
      $data = $this->db->get('productos','both');
      return $data->result();

    }

    public function buscar_cliente($busqueda){
      $this->db->select("nombre AS label, `id`, `rfc`, `razon_social`, `calle`, `numero`, `colonia`, `cp`, `municipio`, `ciudad`, `estado`, `pais`, `telefono`, `email`, `tados_bancarios`, `nombre`, `fecha_actualizacion`, `regimen`");
      $this->db->like('nombre', $busqueda);
      $data = $this->db->get('clientes','both');
      return $data->result();

    }

    public function buscar_proveedor($busqueda){
      $this->db->select("CONCAT(rfc,'-',nombre) AS label, `id`, `rfc`, `razon_social`, `calle`, `numero`, `colonia`, `cp`, `municipio`, `ciudad`, `estado`, `pais`, `telefono`, `email`, `tados_bancarios`, `nombre`, `fecha_actualizacion`, `regimen`");
      $this->db->like('nombre', $busqueda,'both');
      $data = $this->db->get('clientes');
      return $data->result();

    }

    public function ver_permiso_menu($id_usuario){
      $respuesta = $this->get_row('id_usuario',$id_usuario,'menu');
      return $respuesta;
    }

    public function save_register($table, $data)
      {
          $this->db->insert($table, $data);
          return $this->db->insert_id();
      }

      public function delete_row($tabla,$id_tabla,$id){
		   $this->db->delete($tabla, array($id_tabla=>$id));
    	}

      public function get_table($table){
    		$data = $this->db->get($table)->result();
    		return $data;
    	}

      public function get_row($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->row();
    	}

      public function get_result($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->result();
    	}

      public function update_table_row($tabla,$data,$id_table,$id){
    		$this->db->update($tabla, $data, array($id_table=>$id));
    	}

      public function update_permiso_rol($data,$permiso,$id_rol){
        $this->db->update('permisos_roles', $data, array("permisoCategoria"=>$permiso,"permisoIdRol"=>$id_rol));

      }

      public function save_admin($data){
    		$this->db->insert('admin', $data);
            return $this->db->insert_id();
    	}

      public function count_results_users($user, $pass)
        {
            $this->db->where('adminUsername', $user);
            $this->db->where('adminPassword', $pass);
            $total = $this->db->count_all_results('admin');
            return $total;
        }

        public function get_user_login($user, $pass)
        {
            $this->db->where('adminUsername', $user);
            $this->db->where('adminPassword', $pass);
            $total = $this->db->get('admin')->row();
            return $total;
        }

        public function get_all_data_users_specific($username, $pass)
          {
              $this->db->where('adminUsername', $username);
              $this->db->where('adminPassword', $pass);
              $data = $this->db->get('admin');
              return $data->row();
          }

      public function get_contacto_usuario($id_usuario){
        return $this->db->where('tipo',1)->where('id_tabla',$id_usuario)->get('usuarios_contactos')->result();
      }

      public function get_contacto_proveedor($id_usuario){
        return $this->db->where('tipo',2)->where('id_tabla',$id_usuario)->get('usuarios_contactos')->result();
      }
}
