<?php
/**
 * Template para generar PDF de CFDI 3.3 con complemento de Nómina 1.2
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.0.1 (10/05/2018)
 */

$charsPerLineBase = 111;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$compNomina12 = $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina');
?>
<style type="text/css">
<!--

.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

.text-muted{color:#777;}

*{
	font-size: 7pt;
	line-height: 125%;
}
.font-large{
	font-size: 12pt;
}
.font-medium,
.font-medium *{
	font-size: 9pt;
}
.font-system{
	font-family:courier;
	line-height: 110%;
}

p{
	margin:0;
}
h1{
	margin:0;
}
h2{
	margin:0;
}
h5{
	margin:0;
}
table{
	border-spacing: 0;
	border-collapse: collapse;
}

.spacing{
	height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
	margin-top:0.5mm;
}
.spacing-top-1mm{
	margin-top:1mm;
}
.spacing-top-2mm{
	margin-top:2mm;
}
.spacing-top-3mm{
	margin-top:3mm;
}
.spacing-bottom{
	margin-top:1mm;
}
.spacing-bottom-2mm{
	margin-bottom:2mm;
}


.100p{
	width:100%;
}
.99p{
	width:99%;
}
.80p{
	width:80%;
}
.75p{
	width:75%;
}
.60p{
	width:60%;
}
.50p{
	width:50%;
}
.40p{
	width:40%;
}
.33p{
	width:33%;
}
.34p{
	width:34%;
}
.25p{
	width:25%;
}

th,
.bg-gray{
	background: <?php echo $colorFondo; ?>;
	color: <?php echo $colorTexto; ?>;
	font-weight: bold;
}
.cell-padding,
.cell-padding-narrow,
.cell-padding-big,
.cell-padding-h {
	padding-left: 1.6mm;
	padding-right: 1.6mm;
}
.cell-padding,
.cell-padding-v {
	padding-top: 1.3mm;
	padding-bottom: 1.3mm;
}
.cell-padding-narrow{
	padding-top: 1mm;
	padding-bottom: 1mm;
}
.cell-padding-big{
	padding-top: 2.6mm;
	padding-bottom: 2.6mm;
}

.border-gray{
	border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
	border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
	border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
	border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
	padding-top: 1.2mm;
	padding-bottom: 0mm;
}
table.productos tr.last-row td {
	padding-bottom: 1.1mm;
}
table.sat-info{
	
}
table.sat-info h5{
	line-height: 120%;
}

thead { display: table-header-group }
tfoot { display: table-row-group }
tr { page-break-inside: avoid }

-->
</style>

<page backtop="<?php echo $pageMargin ?>mm" backbottom="<?php echo $bottomPageMargin ?>mm" backleft="<?php echo $pageMargin ?>mm" backright="<?php echo $pageMargin ?>mm">
	<page_footer>
		<table style="padding-bottom:<?php echo $footerMargin ?>mm">
			<tr>
				<td style="padding-left:<?php echo $pageMargin-($footerDefaultMargin/2) ?>mm" class="75p">
					<?php if(!empty($piePagina)) echo $piePagina ?>
				</td>
				<td style="padding-right:<?php echo $pageMargin-$footerDefaultMargin ?>mm" class="25p text-right">Página [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
	</page_footer>

	<table class="page-head">
		<tr>
			<td style="width:28%;text-align:center"><?php if(!empty($logo)) {
				echo '<img src="'.$logo.'" style="height:86px">';
			} ?></td>
			<td style="width:43%;">
				<h1 style="margin-top:2mm" class="font-large text-center"><?php echo $encabezado ?></h1>
				<?php if(!empty($direccionExpedicion)) {
					echo '<p style="margin-top:1mm" class="text-center">'.nl2br($direccionExpedicion).'</p>';
				} ?>
			</td>
			<td style="width:1%"></td>
			<td style="width:28%">
				<table class="text-center">
					<tr>
						<th style="width:54%" class="border-gray cell-padding-v">Serie - Folio</th>
						<th style="width:45%" class="border-gray cell-padding-v">Tipo</th>
					</tr>
					<tr>
						<td style="width:54%" class="border-gray cell-padding-v"><?php echo $cfdi->getSerieFolio(); ?></td>
						<td style="width:45%" class="border-gray cell-padding-v"><?php echo $cfdi->getTipoComprobante(); ?><br/></td>
					</tr>
					<tr>
						<th style="width:54%" class="border-gray cell-padding-v">Fecha</th>
						<th style="width:45%" class="border-gray cell-padding-v">Lugar Expedición</th>
					</tr>
					<tr>
						<td style="width:54%" class="border-gray cell-padding-v"><?php echo $cfdi->xml->getAttribute('Fecha'); ?><br/></td>
						<td style="width:45%" class="border-gray cell-padding-v"><?php echo $cfdi->xml->getAttribute('LugarExpedicion'); ?><br/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<table class="spacing-top-2mm">
		<tr>
			<td style="width:49.5%;vertical-align:top">
				<table>
					<tr><th style="width:100%" class="text-center cell-padding-v">EMISOR</th></tr>
					<tr>
						<td style="width:100%" class="cell-padding border-gray">
							<table>
								<tr>
									<td style="width:100%"><p><b>Razón Social:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Emisor')->getAttribute('Nombre'); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>RFC:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Emisor')->getAttribute('Rfc'); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:100%"><p><b>Régimen Fiscal:</b> <span><?php echo $cfdi->getRegimenFiscal(); ?></span></p></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td style="width:1%">
			</td>
			<td style="width:49.5%;vertical-align:top">
				<table>
					<tr><th style="width:100%" class="text-center cell-padding-v">EMPLEADO</th></tr>
					<tr>
						<td style="width:100%" class="cell-padding border-gray">
							<table>
								<tr>
									<td colspan="2" style="width:100%"><p><b>Nombre:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Receptor')->getAttribute('Nombre'); ?></span></p></td>
								</tr>
								<tr>
									<td style="width:50%"><p><b>CURP:</b> <span><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('Curp'); ?></span></p></td>
									<td style="width:50%"><p><b>RFC:</b> <span><?php echo $cfdi->xml->getChildren('cfdi:Receptor')->getAttribute('Rfc'); ?></span></p></td>
								</tr>
								<tr>
									<td colspan="2" style="width:100%"><p><b>Núm. S.S.:</b> <span><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('NumSeguridadSocial',''); ?></span></p></td>
								</tr>
								<tr>
									<td colspan="2" style="width:100%"><p><b>Uso del CFDI:</b> <span><?php echo $cfdi->getUsoCfdi(); ?></span></p></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<table class="spacing-top-2mm">
		<tr><th style="width:100%" class="text-center cell-padding-v">NOMINA</th></tr>
		<tr>
			<td class="100p cell-padding border-gray">
				<table>
					<tr>
						<td style="width:40%">
							<table>
								<tr>
									<td style="width:44%" class="text-bold">No. Empleado:</td>
									<td style="width:56%"><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('NumSeguridadSocial','-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Tipo de Contrato:</td>
									<td><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('TipoContrato','-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Departamento:</td>
									<td><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('Departamento','-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Puesto:</td>
									<td><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('Puesto','-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Riesgo de Puesto:</td>
									<td><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('RiesgoPuesto', '-'); ?></td>
								</tr>
							</table>
						</td>
						<td style="width:32%">
							<table>
								<tr>
									<td style="width:58%" class="text-bold">Tipo de Jornada:</td>
									<td style="width:42%" ><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('TipoJornada','-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Periodicidad de Pago:</td>
									<td><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('PeriodicidadPago'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Salario Diario Integrado:</td>
									<td><?php
									if($salB = $compNomina12->getChildren('nomina12:Receptor')->getAttribute('SalarioBaseCotApor',null)){
										echo '$'.$cfdi->formatDecimal($salB);
									} ?></td>
								</tr>
								<tr>
									<td class="text-bold">Salario Base de Cotización:</td>
									<td><?php
									if($salD = $compNomina12->getChildren('nomina12:Receptor')->getAttribute('SalarioDiarioIntegrado',null)){
										echo '$'.$cfdi->formatDecimal($salD);
									} ?></td>
								</tr>
								<tr>
									<td class="text-bold">Antigüedad:</td>
									<td><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('Antigüedad', '-'); ?></td>
								</tr>
							</table>
						</td>
						<td style="width:28%">
							<table>
								<tr>
									<td style="width:62%" class="text-bold">Número de Días Pagados:</td>
									<td style="width:38%"><?php echo $compNomina12->getAttribute('NumDiasPagados'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Fecha de Pago:</td>
									<td><?php echo $compNomina12->getAttribute('FechaPago', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Fecha Inicial de Pago:</td>
									<td><?php echo $compNomina12->getAttribute('FechaInicialPago', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Fecha Final de Pago:</td>
									<td><?php echo $compNomina12->getAttribute('FechaFinalPago', '-'); ?></td>
								</tr>
								<tr>
									<td class="text-bold">Fecha Inicio Rel. Laboral:</td>
									<td><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('FechaInicioRelLaboral', '-'); ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="width:17.5%" class="text-bold">Régimen de Contratación:</td>
						<td style="width:82.5%"><?php echo $compNomina12->getChildren('nomina12:Receptor')->getAttribute('TipoRegimen', '-'); ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<table class="spacing-top-2mm">
		<tr>
			<td style="width:49.5%; vertical-align: top">
<?php
$ps = $compNomina12->getChildren('nomina12:Percepciones');
if($ps->children){
?>
				<table class="99p">
					<tr><th style="width:100%" class="text-center cell-padding-v">PERCEPCIONES</th></tr>
					<tr><td class="border-gray">
							<table class="productos">
								<tr>
									<th style="width:10%" class="cell-padding">Tipo</th>
									<th style="width:15%" class="cell-padding">Clave</th>
									<th style="width:38%" class="cell-padding">Concepto</th>
									<th style="width:19%" class="cell-padding text-right">Gravado</th>
									<th style="width:18%" class="cell-padding text-right">Exento</th>
								</tr>
<?php
	$sums = array(
		'importe_gravado'=>0,
		'importe_exento'=>0
	);
	foreach($ps->children as $row){
		$row = $row->attributes;

		$importeGravado = (float)$row['ImporteGravado'];
		$importeExento = (float)$row['ImporteExento'];

		$sums['importe_gravado'] += $importeGravado;
		$sums['importe_exento'] += $importeExento;
?>
								<tr>
									<td style="width:10%" class="cell-padding-narrow"><?php echo $row['TipoPercepcion']; ?></td>
									<td style="width:15%" class="cell-padding-narrow"><?php echo $row['Clave']; ?></td>
									<td style="width:38%" class="cell-padding-narrow"><?php echo $row['Concepto']; ?></td>
									<td style="width:19%" class="text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($importeGravado); ?></td>
									<td style="width:18%" class="text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($importeExento); ?></td>
								</tr>
<?php
	}
?>
								<tr class="last-row">
									<td colspan="4" class="text-bold text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($sums['importe_gravado']); ?></td>
									<td class="text-bold text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($sums['importe_exento']); ?></td>
								</tr>
							</table>
					</td></tr>
				</table>
<?php
} ?>

			</td>
			<td style="width:1%"></td>
			<td style="width:49.5%; vertical-align: top">
<?php
$ps = $compNomina12->getChildren('nomina12:Deducciones');
if($ps->children){
?>
				<table class="99p">
					<tr><th style="width:100%" class="text-center cell-padding-v">DEDUCCIONES</th></tr>
					<tr><td class="border-gray">
							<table class="productos">
								<tr>
									<th style="width:10%" class="cell-padding">Tipo</th>
									<th style="width:15%" class="cell-padding">Clave</th>
									<th style="width:57%" class="cell-padding">Concepto</th>
									<th style="width:18%" class="cell-padding text-right">Importe</th>
								</tr>
<?php
	$sums = array(
		'importe'=>0
	);

	foreach($ps->children as $row){
		$row = $row->attributes;
		$importe = (float)$row['Importe'];
		$sums['importe'] += $importe;
?>
								<tr>
									<td style="width:10%" class="cell-padding-narrow"><?php echo $row['TipoDeduccion']; ?></td>
									<td style="width:15%" class="cell-padding-narrow"><?php echo $row['Clave']; ?></td>
									<td style="width:57%" class="cell-padding-narrow"><?php echo $row['Concepto']; ?></td>
									<td style="width:18%" class="text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($importe); ?></td>
								</tr>
<?php
}
?>
								<tr class="last-row">
									<td colspan="4" class="text-bold text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($sums['importe']); ?></td>
								</tr>
							</table>
					</td></tr>
				</table>
<?php
} ?>
			</td>
		</tr>
	</table>

	<table class="spacing-top-2mm">
		<tr>
			<td style="width:49.5%; vertical-align: top">
<?php
$totalOtrosPagos = 0;
$ps = $compNomina12->getChildren('nomina12:OtrosPagos');
if($ps->children){
?>
		<table class="99p">
			<tr><th style="width:100%" class="text-center cell-padding-v">OTROS PAGOS</th></tr>
			<tr><td class="border-gray">
					<table class="productos">
						<tr>
							<th style="width:10%" class="cell-padding">Tipo</th>
							<th style="width:15%" class="cell-padding">Clave</th>
							<th style="width:57%" class="cell-padding">Concepto</th>
							<th style="width:18%" class="cell-padding text-right">Importe</th>
						</tr>
<?php
	$sums = array(
		'importe'=>0,
		// 'subsidio_importe_causado'=>0
	);
	foreach($ps->children as $row){
		$rowAttrs = $row->attributes;

		$importe = (float)$rowAttrs['Importe'];
		// $subsidioImporteCausado = (float)$row->getChildren('nomina12:SubsidioAlEmpleo')->getAttribute('SubsidioCausado');

		$totalOtrosPagos += $importe;

		$sums['importe'] += $importe;
		// $sums['subsidio_importe_causado'] += $subsidioImporteCausado;
?>
						<tr>
							<td style="width:10%" class="cell-padding-narrow"><?php echo $rowAttrs['TipoOtroPago']; ?></td>
							<td style="width:15%" class="cell-padding-narrow"><?php echo $rowAttrs['Clave']; ?></td>
							<td style="width:57%" class="cell-padding-narrow"><?php echo $rowAttrs['Concepto']; ?></td>
							<td style="width:18%" class="text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($importe); ?></td>
						</tr>
<?php
	}
?>
						<tr class="last-row">
							<td colspan="4" class="text-bold text-right cell-padding-narrow"><?php echo $cfdi->formatDecimal($sums['importe']); ?></td>
						</tr>
					</table>
			</td></tr>
		</table>
<?php
} ?>
			</td>
			<td style="width:1%"></td>
			<td style="width:49.5%"></td>
		</tr>
	</table>

	<table class="productos spacing-top-2mm">
		<tr><th style="width:100%" colspan="6" class="text-center cell-padding-v">CONCEPTOS</th></tr>
		<tr>
			<th class="cell-padding" style="width: 8%">Clave</th>
			<th class="cell-padding" style="width:10%">Cantidad</th>
			<th class="cell-padding" style="width: 9%">Unidad</th>
			<th class="cell-padding" style="width:51%">Descripción</th>
			<th class="cell-padding text-right" style="width:10%">Precio</th>
			<th class="cell-padding text-right" style="width:12%">Importe</th>
		</tr>
		<?php
		foreach ($cfdi->xml->getChildren('cfdi:Conceptos')->children as $i => $row) {
		?>
		<tr>
			<td style="width: 8%" class="cell-padding-narrow border-left"><?php echo $row->getAttribute('NoIdentificacion'); ?></td>
			<td style="width:10%" class="cell-padding-narrow"><?php echo $row->getAttribute('Cantidad'); ?></td>
			<td style="width: 9%" class="cell-padding-narrow"><?php echo $row->getAttribute('Unidad'); ?></td>
			<td style="width:51%" class="cell-padding-narrow"><?php echo $row->getAttribute('Descripcion'); ?></td>
			<td style="width:10%" class="cell-padding-narrow text-right">$<?php echo $row->getAttribute('ValorUnitario'); ?></td>
			<td style="width:12%" class="cell-padding-narrow text-right border-right">$<?php echo $cfdi->formatDecimal($row->getAttribute('Importe')); ?></td>
		</tr>
		<tr class="last-row">
			<td style="width:100%" colspan="6" class="border-left border-bottom cell-padding-narrow border-right text-muted">
				<b>ClaveProdServ:</b> <?php echo $row->getAttribute('ClaveProdServ'); ?>.
				<b>ClaveUnidad:</b> <?php echo $row->getAttribute('ClaveUnidad'); ?>.
				<b>Impuestos:</b> <?php echo $cfdi->getImpuestos($row); ?>.
				<?php
				$desc = $row->getAttribute('Descuento', '');
				if($desc != '') echo '<b>Descuento:</b> '.$desc;
				?>
			</td>
		</tr>
		<?php
		}
		?>
	</table>
<?php
if($cfdi->xml->getChildren('cfdi:CfdiRelacionados')->children) {
?>
	<table class="spacing-top-2mm">
		<tr><th colspan="2" class="100p text-center cell-padding-v">CFDI RELACIONADO</th></tr>
		<tr>
			<th style="width:18%" class="cell-padding-narrow">Tipo de Relación</th>
			<td style="width:82%" class="cell-padding-narrow border-gray">
				<?php echo getTipoRelacion($cfdi->xml->getChildren('cfdi:CfdiRelacionados')->getAttribute('TipoRelacion')); ?>
			</td>
		</tr>
		<tr>
			<th style="width:18%" class="cell-padding-narrow">UUID</th>
			<td style="width:82%" class="cell-padding-narrow border-gray">
				<?php
				foreach ($cfdi->xml->getChildren('cfdi:CfdiRelacionados')->children as $i => $row) {
					if($i > 0) echo '<br/>';
					echo $row->getAttribute('UUID');
				}
				?>
			</td>
		</tr>
	</table>
<?php
}
?>
	<table class="spacing-top-2mm">
		<tr>
			<td style="width:74%">
				<table>
					<tr><th colspan="2" class="100p text-center cell-padding-v">DATOS GENERALES</th></tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Forma de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->getFormaPago();
						$condicionesPago = $cfdi->xml->getAttribute('CondicionesDePago');
						if(!empty($condicionesPago)) echo ' | <b>Condiciones Pago</b>: '.$condicionesPago;
						?></td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Método de Pago</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->getMetodoPago(); ?></td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">T.C. / Moneda</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->xml->getAttribute('TipoCambio', '1.000000'); ?> / <?php echo $cfdi->xml->getAttribute('Moneda'); ?></td>
					</tr>
					<tr>
						<th style="width:18%" class="cell-padding-narrow">Total con Letra</th>
						<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $cfdi->totalConLetra(); ?></td>
					</tr>
				</table>
			</td>
			<td style="width:1%">
			</td>
			<td style="width:25%">
				<table>
<?php
// $ps = $compNomina12->getChildren('nomina12:Percepciones');
// $totalPercepciones = $ps->getAttribute('TotalGravado') + $ps->getAttribute('TotalExento');
// $ps = $compNomina12->getChildren('nomina12:Deducciones');
// $totalDeducciones = $ps->getAttribute('TotalGravado') + $ps->getAttribute('TotalExento');
?>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Subtotal</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $cfdi->formatDecimal($cfdi->xml->getAttribute('SubTotal')); ?></td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Descuento</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $cfdi->formatDecimal($cfdi->xml->getAttribute('Descuento')); ?></td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Traslados</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $cfdi->formatDecimal($cfdi->xml->getChildren('cfdi:Impuestos')->getAttribute('TotalImpuestosTrasladados') ?: '0.00'); ?></td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">Retenciones</th>
						<td style="width:55%" class="text-right cell-padding-narrow border-gray">$<?php echo $cfdi->formatDecimal($cfdi->xml->getChildren('cfdi:Impuestos')->getAttribute('TotalImpuestosRetenidos') ?: '0.00'); ?></td>
					</tr>
					<tr>
						<th style="width:45%" class="cell-padding-narrow">TOTAL</th>
						<td style="width:55%" class="text-right text-bold cell-padding-narrow border-gray">$<?php echo $cfdi->formatDecimal($cfdi->xml->getAttribute('Total')); ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<?php if($cfdi->timbrado()) { ?>
	<table class="spacing-top-2mm">
		<tr>
			<td class="100p cell-padding border-gray">
				<table class="sat-info">
					<tr>
						<td style="width:16%;vertical-align:top">
							<qrcode value='<?php echo $cfdi->getQr(); ?>' ec="M" style="width: 28.5mm; background-color: white; color: black; border:none"></qrcode>
							<h5 class="text-center spacing-top-2mm"">Versión de CFDI</h5>
							<p class="text-center font-system"><?php echo $cfdi->xml->getAttribute('Version') ?></p>
						</td>
						<td style="width:84%">
							<table class="100p">
								<tr>
									<td style="width:21%">
										<h5>Fecha de Timbrado</h5>
										<p class="font-system"><?php echo $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('FechaTimbrado'); ?></p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado SAT</h5>
										<p class="font-system"><?php echo $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('NoCertificadoSAT'); ?></p>
									</td>
									<td style="width:22%">
										<h5>No. Certificado Emisor</h5>
										<p class="font-system"><?php echo $cfdi->xml->getAttribute('NoCertificado'); ?></p>
									</td>
									<td style="width:35%">
										<h5>Folio Fiscal</h5>
										<p class="font-system"><?php echo $cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID'); ?></p>
									</td>
								</tr>
							</table>
							<h5 class="spacing-top-1mm">Cadena Original</h5>
							<p class="font-system"><?php echo chunk_split($cfdi->getCadenaOriginalTFD(), $charsPerLineBase, '<br>'); ?></p>
							<h5 class="spacing-top-1mm">Sello Digital del CFDI</h5>
							<p class="font-system"><?php echo chunk_split($cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('SelloCFD'), $charsPerLineBase, '<br>'); ?></p>
							<h5 class="spacing-top-1mm">Sello Digital del SAT</h5>
							<p class="font-system"><?php echo chunk_split($cfdi->xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('SelloSAT'), $charsPerLineBase, '<br>'); ?></p>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="100p">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?php } ?>

	<?php if(!empty($mensajeFactura)){ ?>
	<table class="spacing-top-2mm">
		<tr><th style="width:100%" class="text-center cell-padding-v">NOTA</th></tr>
		<tr>
			<td style="width:100%" class="text-center cell-padding border-gray">
				<?php echo $mensajeFactura ?>
			</td>
		</tr>
	</table>
	<?php } ?>

	<div class="spacing-top-2mm">
		<p class="text-center text-bold">Este documento es una representación impresa de un CFDI.</p>
	</div>

	<?php
	if($mensajeSello){
		$color = '#ff0101';
		$w = 4.5;
	?>
		<div style="position: absolute;
			left:50%;
			top:30%;
			margin-left:-<?php echo (strlen($mensajeSello)*$w)/2 ?>mm;
			margin-top:-5mm;
			width:<?php echo strlen($mensajeSello)*$w ?>mm;
			height:8mm;
			border:solid 1mm <?php echo $color ?>;
			padding:1.5mm 0 0 0;
			text-align:center;
			font-weight:bold;
			font-size:16pt;
			color:<?php echo $color ?>;
		">
			<?php echo $mensajeSello ?>
		</div>
	<?php
	}
	?>
</page>