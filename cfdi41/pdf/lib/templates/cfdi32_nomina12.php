<?php
/**
 * Template para generar PDF de Nomina 1.2 (CFDI 3.2)
 *
 * @author  Noel Miranda <noelmrnd@gmail.com>
 * @version 1.0.0
 */

$charsPerLineBase = 113;
$pageMargin = 8;
$footerMargin = 5;
$bottomPageMargin = $footerMargin + 8;
$footerDefaultMargin = 4;

$xml = $xmlParser->getObject();

$timbrado = $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID') != null;

?>
<style type="text/css">
<!--

.text-right{text-align: right;}
.text-center{text-align: center;}
.text-bold{font-weight: bold;}
.text-normal{font-weight: normal;}

*{
	font-size: 7pt;
	line-height: 125%;
}
.font-large{
	font-size: 12pt;
}
.font-medium,
.font-medium *{
	font-size: 9pt;
}
.font-system{
	font-family:courier;
	line-height: 110%;
}

p{
	margin:0;
}
h1{
	margin:0;
}
h2{
	margin:0;
}
h5{
	margin:0;
}
table{
	border-spacing: 0;
	border-collapse: collapse;
}

.spacing{
	height: 3.4mm; /* minimo visible: 3.4mm */
}
.spacing-top-0mm{
	margin-top:0.5mm;
}
.spacing-top-1mm{
	margin-top:1mm;
}
.spacing-top-2mm{
	margin-top:2mm;
}
.spacing-top-3mm{
	margin-top:3mm;
}
.spacing-bottom{
	margin-top:1mm;
}
.spacing-bottom-2mm{
	margin-bottom:2mm;
}


.100p{
	width:100%;
}
.99p{
	width:99%;
}
.80p{
	width:80%;
}
.75p{
	width:75%;
}
.60p{
	width:60%;
}
.50p{
	width:50%;
}
.40p{
	width:40%;
}
.33p{
	width:33%;
}
.34p{
	width:34%;
}
.25p{
	width:25%;
}

th,
.bg-gray{
	background: <?php echo $colorFondo; ?>;
	color: <?php echo $colorTexto; ?>;
	font-weight: bold;
}
.bg-lgray{
	background: #f4f4f4;
	color:#000;
	font-weight: bold;
}
th,
.cell-padding{
	padding:1.3mm 1.6mm;
}
.cell-padding-narrow{
	padding:1mm 1.6mm;
}
.cell-padding-big{
	padding:2.6mm 1.6mm;
}

.border-gray{
	border: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-left{
	border-left: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-right{
	border-right: solid 0.25mm <?php echo $colorFondo; ?>;
}
.border-bottom{
	border-bottom: solid 0.25mm <?php echo $colorFondo; ?>;
}


table.productos td{
	padding-top: 1.2mm;
	padding-bottom: 0mm;
}
table.productos tr.last-row td {
	padding-bottom: 1.1mm;
}
table.sat-info{
	
}
table.sat-info h5{
	line-height: 120%;
}
-->
</style>

<page backtop="<?php echo $pageMargin ?>mm" backbottom="<?php echo $bottomPageMargin ?>mm" backleft="<?php echo $pageMargin ?>mm" backright="<?php echo $pageMargin ?>mm">
	<page_footer>
		<table style="padding-bottom:<?php echo $footerMargin ?>mm">
			<tr>
				<td style="padding-left:<?php echo $pageMargin-($footerDefaultMargin/2) ?>mm" class="75p">
					<?php if(!empty($piePagina)) echo $piePagina ?>
				</td>
				<td style="padding-right:<?php echo $pageMargin-$footerDefaultMargin ?>mm" class="25p text-right">Página [[page_cu]]/[[page_nb]]</td>
			</tr>
		</table>
	</page_footer>

	<div>
		<table class="page-head">
			<tr>
<?php
if(!empty($logo)){ ?>
				<td style="width:28%">
					<div class="logo"><?php echo '<img src="'.$logo.'" style="height:120px;width:200px">'; ?></div>
				</td>
				<td style="width:0.5%"></td>
				<td style="width:43.5%"><?php
}else{ ?>
				<td style="width:72%"><?php
} ?>
					<h1 class="font-medium"><?php echo $xml->getChildren('cfdi:Emisor')->getAttribute('nombre'); ?></h1>
					<p class="font-medium spacing-top-0mm"><?php echo $xmlParser->getDomicilio($xml->getChildren('cfdi:Emisor')->getChildren('cfdi:DomicilioFiscal')->attributes); ?></p>
					<p class="font-medium spacing-top-0mm"><b>RFC:</b> <span><?php echo $xml->getChildren('cfdi:Emisor')->getAttribute('rfc'); ?></span></p>
					<p class="font-medium spacing-top-0mm"><b>Régimen Fiscal:</b> <span><?php echo $xml->getChildren('cfdi:Emisor')->getChildren('cfdi:RegimenFiscal')->getAttribute('Regimen'); ?></span></p>
				</td>
				<td style="width:1%"></td>
				<td style="width:27%">
					<table class="text-center">
						<tr>
							<th class="60p cell-padding" style="font-size:8pt;vertical-align:middle"><?php echo $tipoComprobante ?></th>
							<td class="40p text-right text-bold cell-padding border-gray" style="font-size:8pt;vertical-align:middle"><?php echo $xmlParser->getSerieFolio($xml); ?></td>
						</tr>
					</table>
					<table class="text-center spacing-top-3mm">
						<tr><th class="100p">Lugar de Expedición</th></tr>
						<tr><td class="100p cell-padding border-gray">
							<?php echo $xml->getAttribute('LugarExpedicion'); ?><br/>
						</td></tr>
						<tr><th class="100p">Fecha y Hora de Emisión</th></tr>
						<tr><td class="100p cell-padding border-gray">
							<?php echo $xml->getAttribute('fecha'); ?>
						</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
		
	<div class="spacing-top-2mm">
		<table>
			<tr><th class="100p">EMPLEADO</th></tr>
			<tr>
				<td class="100p cell-padding border-gray">
					<table>
						<tr>
							<td style="width:50%">
								<p><b>Nombre:</b> <span><?php echo $xml->getChildren('cfdi:Receptor')->getAttribute('nombre'); ?></span></p>
							</td>
							<td style="width:50%">
								<p class="spacing-top-1mm"><b>CURP:</b> <span><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('Curp'); ?></span></p>
							</td>
						</tr>
						<tr>
							<td style="width:50%">
								<p><b>RFC:</b> <span><?php echo $xml->getChildren('cfdi:Receptor')->getAttribute('rfc'); ?></span></p>
							</td>
							<td style="width:50%">
								<p class="spacing-top-1mm"><b>Núm. S.S.</b> <span><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('NumSeguridadSocial',''); ?></span></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="spacing-top-2mm">
		<table>
			<tr><th class="100p">DETALLE</th></tr>
			<tr>
				<td class="100p cell-padding border-gray">
					<table>
						<tr>
							<td style="width:40%">
								<table>
									<tr>
										<td style="width:44%" class="text-bold">No. Empleado:</td>
										<td style="width:56%"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('NumSeguridadSocial','-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Tipo de Contrato:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('TipoContrato','-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Departamento:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('Departamento','-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Puesto:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('Puesto','-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Riesgo de Puesto:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('RiesgoPuesto', '-'); ?></td>
									</tr>
								</table>
							</td>
							<td style="width:32%">
								<table>
									<tr>
										<td style="width:58%" class="text-bold">Tipo de Jornada:</td>
										<td style="width:42%" ><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('TipoJornada','-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Periodicidad de Pago:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('PeriodicidadPago'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Salario Diario Integrado:</td>
										<td><?php
										if($salB = $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('SalarioBaseCotApor',null)){
											echo '$'.$salB;
										} ?></td>
									</tr>
									<tr>
										<td class="text-bold">Salario Base de Cotización:</td>
										<td><?php
										if($salD = $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('SalarioDiarioIntegrado',null)){
											echo '$'.$salD;
										} ?></td>
									</tr>
									<tr>
										<td class="text-bold">Antigüedad:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('Antigüedad', '-'); ?></td>
									</tr>
								</table>
							</td>
							<td style="width:28%">
								<table>
									<tr>
										<td style="width:62%" class="text-bold">Número de Días Pagados:</td>
										<td style="width:38%"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getAttribute('NumDiasPagados'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Fecha de Pago:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getAttribute('FechaPago', '-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Fecha Inicial de Pago:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getAttribute('FechaInicialPago', '-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Fecha Final de Pago:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getAttribute('FechaFinalPago', '-'); ?></td>
									</tr>
									<tr>
										<td class="text-bold">Fecha Inicio Rel. Laboral:</td>
										<td><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('FechaInicioRelLaboral', '-'); ?></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table>
						<tr>
							<td style="width:17.5%" class="text-bold">Régimen de Contratación:</td>
							<td style="width:82.5%"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Receptor')->getAttribute('TipoRegimen', '-'); ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<div class="spacing-top-2mm">
		<table>
			<tr>
				<td style="width:49.5%">


<?php
$ps = $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Percepciones');
if($ps->children){
?>
					<table class="99p">
						<tr><th>PERCEPCIONES</th></tr>
						<tr><td class="border-gray">
								<table class="productos">
									<tr>
										<th class="bg-lgray">Tipo</th>
										<th class="bg-lgray">Clave</th>
										<th class="bg-lgray">Concepto</th>
										<th class="text-right bg-lgray">Gravado</th>
										<th class="text-right bg-lgray">Exento</th>
									</tr>
<?php
	$sums = array(
		'importe_gravado'=>0,
		'importe_exento'=>0
	);
	foreach($ps->children as $row){
		$row = $row->attributes;

		$importeGravado = (float)$row['ImporteGravado'];
		$importeExento = (float)$row['ImporteExento'];

		$sums['importe_gravado'] += $importeGravado;
		$sums['importe_exento'] += $importeExento;
?>
									<tr>
										<td style="width:10%" class="cell-padding-narrow"><?php echo $row['TipoPercepcion']; ?></td>
										<td style="width:15%" class="cell-padding-narrow"><?php echo $row['Clave']; ?></td>
										<td style="width:38%" class="cell-padding-narrow"><?php echo $row['Concepto']; ?></td>
										<td style="width:19%" class="text-right cell-padding-narrow"><?php echo number_format($importeGravado, 2); ?></td>
										<td style="width:18%" class="text-right cell-padding-narrow"><?php echo number_format($importeExento, 2); ?></td>
									</tr>
<?php
	}
?>
									<tr class="last-row">
										<td colspan="3" class="text-bold text-right cell-padding-narrow">Total</td>
										<td class="text-bold text-right cell-padding-narrow"><?php echo number_format($sums['importe_gravado'], 2); ?></td>
										<td class="text-bold text-right cell-padding-narrow"><?php echo number_format($sums['importe_exento'], 2); ?></td>
									</tr>
								</table>
						</td></tr>
					</table>
<?php
} ?>

				</td>
				<td style="width:1%"></td>
				<td style="width:49.5%">
<?php
$ps = $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Deducciones');
if($ps->children){
?>
					<table class="99p">
						<tr><th>DEDUCCIONES</th></tr>
						<tr><td class="border-gray">
								<table class="productos">
									<tr>
										<th class="bg-lgray">Tipo</th>
										<th class="bg-lgray">Clave</th>
										<th class="bg-lgray">Concepto</th>
										<th class="text-right bg-lgray">Importe</th>
									</tr>
<?php
	$sums = array(
		'importe'=>0
	);

	foreach($ps->children as $row){
		$row = $row->attributes;

		$importe = (float)$row['Importe'];

		$sums['importe'] += $importe;
?>
									<tr>
										<td style="width:10%" class="cell-padding-narrow"><?php echo $row['TipoDeduccion']; ?></td>
										<td style="width:15%" class="cell-padding-narrow"><?php echo $row['Clave']; ?></td>
										<td style="width:57%" class="cell-padding-narrow"><?php echo $row['Concepto']; ?></td>
										<td style="width:18%" class="text-right cell-padding-narrow"><?php echo number_format($importe, 2); ?></td>
									</tr>
<?php
}
?>
									<tr class="last-row">
										<td colspan="3" class="text-bold text-right cell-padding-narrow">Total</td>
										<td class="text-bold text-right cell-padding-narrow"><?php echo number_format($sums['importe'], 2); ?></td>
									</tr>
								</table>
						</td></tr>
					</table>
<?php
} ?>
				</td>
			</tr>
		</table>
	</div>

	<div class="spacing-top-2mm">
		<table>
			<tr>
				<td style="width:49.5%">
<?php
$totalOtrosPagos = 0;
$ps = $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:OtrosPagos');
if($ps->children){
?>
			<table class="99p">
				<tr><th>OTROS PAGOS</th></tr>
				<tr><td class="border-gray">
						<table class="productos">
							<tr>
								<th class="bg-lgray">Tipo</th>
								<th class="bg-lgray">Clave</th>
								<th class="bg-lgray">Concepto</th>
								<th class="text-right bg-lgray">Importe</th>
							</tr>
<?php
	$sums = array(
		'importe'=>0,
		// 'subsidio_importe_causado'=>0
	);
	foreach($ps->children as $row){
		$rowAttrs = $row->attributes;

		$importe = (float)$rowAttrs['Importe'];
		// $subsidioImporteCausado = (float)$row->getChildren('nomina12:SubsidioAlEmpleo')->getAttribute('SubsidioCausado');

		$totalOtrosPagos += $importe;

		$sums['importe'] += $importe;
		// $sums['subsidio_importe_causado'] += $subsidioImporteCausado;
?>
							<tr>
								<td style="width:10%" class="cell-padding-narrow"><?php echo $rowAttrs['TipoOtroPago']; ?></td>
								<td style="width:15%" class="cell-padding-narrow"><?php echo $rowAttrs['Clave']; ?></td>
								<td style="width:56%" class="cell-padding-narrow"><?php echo $rowAttrs['Concepto']; ?></td>
								<td style="width:19%" class="text-right cell-padding-narrow"><?php echo number_format($importe, 2); ?></td>
							</tr>
<?php
	}
?>
							<tr class="last-row">
								<td colspan="3" class="text-bold text-right cell-padding-narrow">Total</td>
								<td class="text-bold text-right cell-padding-narrow"><?php echo number_format($sums['importe'], 2); ?></td>
							</tr>
						</table>
				</td></tr>
			</table>
<?php
} ?>
				</td>
				<td style="width:1%"></td>
				<td style="width:49.5%"></td>
			</tr>
		</table>
	</div>

	<div class="spacing-top-2mm">
		<table>
			<tr>
				<td style="width:74%">
					<table class="productos">
						<tr>
							<th style="width:8%" class="text-center first-row">Cantidad</th>
							<th style="width:12%" class="text-center">Unidad</th>
							<th style="width:56%">Descripción</th>
							<th style="width:12%" class="text-right">P. Unitario</th>
							<th style="width:12%" class="text-right">Importe</th>
						</tr>
			<?php
			$concepto = $xml->getChildren('cfdi:Conceptos')->children[0];
			?>
						<tr class="last-row">
							<td style="width:8%" class="border-left border-bottom text-center cell-padding-narrow">1</td>
							<td style="width:12%" class="border-bottom cell-padding-narrow text-center"><?php echo $concepto->getAttribute('unidad'); ?></td>
							<td style="width:56%" class="border-bottom cell-padding-narrow"><?php echo $concepto->getAttribute('descripcion'); ?></td>
							<td style="width:12%" class="border-bottom text-right cell-padding-narrow">$<?php echo number_format($concepto->getAttribute('valorUnitario'), 2); ?></td>
							<td style="width:12%" class="border-right border-bottom text-right cell-padding-narrow">$<?php echo number_format($concepto->getAttribute('importe'), 2); ?></td>
						</tr>
					</table>

					<table class="spacing-top-2mm">
						<tr>
							<th style="width:18%" class="cell-padding-narrow">Forma de Pago</th>
							<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $xml->getAttribute('formaDePago');
							if($condP = $xml->getAttribute('condicionesDePago',null)){
								echo ' (Condiciones: '.$condP.')';
							}
							?></td>
						</tr>
						<tr>
							<th class="cell-padding-narrow">Método de Pago</th>
							<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo $xml->getAttribute('metodoDePago');
							/*$cuentaP = $xml->getAttribute('NumCtaPago',null);
							if($cuentaP){
								echo ' (Cuenta: '.$cuentaP.')';
							}*/
							?>
							</td>
						</tr>
						<tr>
							<th class="cell-padding-narrow">Total con Letra</th>
							<td style="width:82%" class="cell-padding-narrow border-gray"><?php echo CantidadConLetra::convertir($xml->getAttribute('total'), 'PESO', 'MXN'); ?></td>
						</tr>
					</table>
				</td>
				<td style="width:1%"></td>
				<td style="width:25%">
					<table>
<?php
$ps = $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Percepciones');
$totalPercepciones = $ps->getAttribute('TotalGravado') + $ps->getAttribute('TotalExento');

$ps = $xml->getChildren('cfdi:Complemento')->getChildren('nomina12:Nomina')->getChildren('nomina12:Deducciones');
$totalDeducciones = $ps->getAttribute('TotalGravado') + $ps->getAttribute('TotalExento');

$totalRetenciones = $xml->getChildren('cfdi:Impuestos')->getAttribute('totalImpuestosRetenidos');
$totalDescuentos = $xml->getAttribute('descuento');
?>
						<tr>
							<th style="width:46%" class="cell-padding-narrow">Percepciones</th>
							<td style="width:54%" class="text-right text-bold cell-padding-narrow border-gray">$<?php echo number_format($totalPercepciones, 2); ?></td>
						</tr>
						<tr>
							<th style="width:46%" class="cell-padding-narrow">Otros Pagos</th>
							<td style="width:54%" class="text-right text-bold cell-padding-narrow border-gray">$<?php echo number_format($totalOtrosPagos, 2); ?></td>
						</tr>
						<tr>
							<th class="cell-padding-narrow">Descuentos</th>
							<td class="text-right text-bold cell-padding-narrow border-gray">$<?php echo number_format($totalDescuentos, 2); ?></td>
						</tr>
						<tr>
							<th class="cell-padding-narrow">Retenciones</th>
							<td class="text-right text-bold cell-padding-narrow border-gray">$<?php echo number_format($totalRetenciones, 2); ?></td>
						</tr>
						<tr>
							<th class="cell-padding-narrow">Total</th>
							<td class="text-right text-bold cell-padding-narrow border-gray">$<?php echo number_format($xml->getAttribute('total'), 2); ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<?php if($timbrado) { ?>
	<div class="spacing-top-2mm">
		<table>
			<tr>
				<td class="100p cell-padding border-gray">
					<table class="sat-info">
						<tr>
							<td style="width:14%">
								<qrcode value='<?php echo $xmlParser->getQr($xml); ?>' ec="M" style="width: 25mm; background-color: white; color: black; border:none"></qrcode>
							</td>
							<td style="width:86%">
								<table class="100p">
									<tr>
										<td style="width:24%">
											<h5>Fecha y Hora de Certificación </h5>
											<p class="font-system"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('FechaTimbrado'); ?></p>
										</td>
										<td style="width:21%">
											<h5>No. Serie Certificado SAT</h5>
											<p class="font-system"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('noCertificadoSAT'); ?></p>
										</td>
										<td style="width:23%">
											<h5>No. Serie Certificado Emisor</h5>
											<p class="font-system"><?php echo $xml->getAttribute('noCertificado'); ?></p>
										</td>
										<td style="width:32%">
											<h5>Folio Fiscal</h5>
											<p class="font-system"><?php echo $xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('UUID'); ?></p>
										</td>
									</tr>
								</table>
								<h5 class="spacing-top-1mm">Sello Digital del CFDI</h5>
								<p class="font-system"><?php echo chunk_split($xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('selloCFD'), $charsPerLineBase, '<br>'); ?></p>
								<h5 class="spacing-top-1mm">Sello Digital del SAT</h5>
								<p class="font-system"><?php echo chunk_split($xml->getChildren('cfdi:Complemento')->getChildren('tfd:TimbreFiscalDigital')->getAttribute('selloSAT'), $charsPerLineBase, '<br>'); ?></p>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="100p">
								<h5 class="spacing-top-1mm">Cadena Original del Complemento de Certificación Digital del SAT</h5>
								<p class="font-system"><?php echo chunk_split($xmlParser->getCadenaOriginalTFD($xml), $charsPerLineBase+19, '<br>'); ?></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<?php } ?>

	<?php if(!empty($mensajeFactura)){ ?>
	<div class="spacing-top-2mm bg-gray cell-padding-big text-center">
		<?php echo $mensajeFactura ?>
	</div>
	<?php } ?>

	<div class="spacing-top-2mm">
		<p class="text-center text-bold">Este documento es una representación impresa de un CFDI.</p>
	</div>

	<?php
	if($mensajeSello){
		$color = '#ff0101';
		$w = 4.5;
	?>
		<div style="position: absolute;
			left:50%;
			top:30%;
			margin-left:-<?php echo (strlen($mensajeSello)*$w)/2 ?>mm;
			margin-top:-5mm;
			width:<?php echo strlen($mensajeSello)*$w ?>mm;
			height:8mm;
			border:solid 1mm <?php echo $color ?>;
			padding:1.5mm 0 0 0;
			text-align:center;
			font-weight:bold;
			font-size:16pt;
			color:<?php echo $color ?>;
		">
			<?php echo $mensajeSello ?>
		</div>
	<?php
	}
	?>
</page>